﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "System_ServiceModel_System_ServiceModel_ClientBase_561984977.h"
#include "mscorlib_System_Void1185182177.h"
#include "System_ServiceModel_System_ServiceModel_InstanceCo3593205954.h"
#include "mscorlib_System_String1847450689.h"
#include "System_ServiceModel_System_ServiceModel_Channels_Bi859993683.h"
#include "System_ServiceModel_System_ServiceModel_EndpointAd3119842923.h"
#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "mscorlib_System_Boolean97287965.h"
#include "System_ServiceModel_System_ServiceModel_ServiceHos3741910535.h"
#include "mscorlib_System_EventHandler1348719766.h"
#include "mscorlib_System_TimeSpan881159249.h"
#include "System_ServiceModel_System_ServiceModel_ChannelFact585335377.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio4038493094.h"
#include "System_ServiceModel_System_ServiceModel_ChannelFact628250694.h"
#include "System_ServiceModel_System_ServiceModel_Communicat1609160389.h"
#include "System_ServiceModel_System_ServiceModel_Configurat3597295982.h"
#include "System_Configuration_System_Configuration_Configur2852175726.h"
#include "System_ServiceModel_System_ServiceModel_Configurat1398412972.h"
#include "System_ServiceModel_System_ServiceModel_Configurat4182893664.h"
#include "mscorlib_System_Type2483944760.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "mscorlib_System_NotImplementedException3489357830.h"
#include "System_ServiceModel_System_ServiceModel_Configurati274909112.h"
#include "System_ServiceModel_System_ServiceModel_Configurat2544877501.h"
#include "System_Configuration_System_Configuration_Configur3318566633.h"
#include "System_System_ComponentModel_TypeConverter2249118273.h"
#include "System_Configuration_System_Configuration_Configura888490966.h"
#include "System_Configuration_System_Configuration_Configur1059028815.h"
#include "System_Configuration_System_Configuration_Configur3590861854.h"
#include "System_System_ComponentModel_StringConverter3216726494.h"
#include "System_Configuration_System_Configuration_StringVa4226114403.h"
#include "mscorlib_System_Int322950945753.h"
#include "System_ServiceModel_System_ServiceModel_Configurat3718990380.h"
#include "System_Configuration_System_Configuration_Configura446763386.h"
#include "System_Configuration_System_Configuration_Configur2560831360.h"
#include "System_ServiceModel_System_ServiceModel_Configurat1951097806.h"
#include "System_ServiceModel_System_ServiceModel_Configurat1583623670.h"
#include "mscorlib_System_UInt322560061978.h"
#include "mscorlib_System_NotSupportedException1314879016.h"
#include "System_ServiceModel_System_Collections_Generic_Key1429017627.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2024462082.h"
#include "mscorlib_System_Collections_ObjectModel_KeyedCollec368913236.h"
#include "System_Xml_System_Xml_XmlReader3121518892.h"
#include "System_ServiceModel_System_ServiceModel_Configurat3123063704.h"
#include "System_ServiceModel_System_ServiceModel_Configurati741974772.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol580582265.h"
#include "mscorlib_System_Collections_Generic_List_1_gen840080720.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher4103390264.h"
#include "mscorlib_System_Exception1436737249.h"
#include "System_ServiceModel_System_ServiceModel_Dispatcher4138186672.h"
#include "mscorlib_System_Threading_AutoResetEvent1333520283.h"
#include "mscorlib_System_Threading_EventWaitHandle777845177.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "System_ServiceModel_System_ServiceModel_DuplexChan1618788632.h"
#include "System_System_Uri100236324.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio1411178514.h"
#include "System_ServiceModel_System_ServiceModel_DuplexClien737641411.h"
#include "System_ServiceModel_System_ServiceModel_ExtensionC1857785261.h"
#include "System_ServiceModel_System_Collections_Generic_Syn3223601106.h"
#include "System_Web_System_Web_Compilation_AppResourcesLeng3587265839.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3319525431.h"
#include "System_Web_System_Web_Compilation_AppResourceFileI3599744125.h"
#include "mscorlib_System_IO_FileInfo1169991790.h"
#include "System_Web_System_Web_Compilation_GenericBuildProv3300278628.h"
#include "System_Web_System_Web_Compilation_BuildProvider3736381005.h"
#include "System_Web_System_Web_VirtualPath4270372584.h"
#include "System_Web_System_Web_HttpContext1969259010.h"
#include "System_Web_System_Web_HttpRequest809700260.h"
#include "mscorlib_System_IO_TextReader283511965.h"
#include "System_Web_System_Web_Compilation_AspGenerator2810561191.h"
#include "System_Web_System_Web_Compilation_AssemblyBuilder187066735.h"
#include "System_Web_System_Web_Compilation_BaseCompiler69158820.h"
#include "System_System_CodeDom_CodeCompileUnit2527618915.h"
#include "System_Web_System_Web_HttpException2907797370.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat913802012.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2146457487.h"
#include "System_System_CodeDom_Compiler_CompilerResults1736487784.h"
#include "mscorlib_System_Reflection_Assembly4102432799.h"
#include "System_Web_System_Web_Compilation_CompilerType2533482247.h"
#include "System_Web_System_Web_UI_MainDirectiveAttribute_1_4076720260.h"
#include "System_Web_System_Web_UI_MainDirectiveAttribute_1_4205880671.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen2613165452.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall2423483305.h"
#include "UnityEngine_UnityEngine_Object631007953.h"
#include "mscorlib_System_Reflection_MethodInfo1877626248.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen214452203.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_982173797.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen3068109991.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall1111334208.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen3197270402.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3723462114.h"
#include "mscorlib_System_Single1397266774.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen1514431012.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2703961024.h"
#include "mscorlib_System_Delegate1188392813.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen682124106.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3535781894.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3664942305.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1982102915.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2672850562.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3140522465.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2273393761.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2741065664.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen362407658.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3283971887.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen4059188962.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen1557236713.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2756980746.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen682480391.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2933211702.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene2348375561.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2165061829.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3251202195.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1262235195.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen978947469.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase3960448221.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3832605257.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3961765668.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2278926278.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3437345828.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3037889027.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen614268397.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen2404744798.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen4085588227.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1764640198.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3903027533.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSe234526808.h"
#include "mscorlib_System_Collections_Generic_List_1_gen257213610.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen3384741.h"
#include "mscorlib_System_Predicate_1_gen3905400288.h"
#include "mscorlib_System_Comparison_1_gen2855037343.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween3860393442.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT809614380.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_TweenRu30141770.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float1274330004.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween3055525458.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_GameObject1113636619.h"
#include "UnityEngine_UnityEngine_Component1923634451.h"
#include "UnityEngine_UnityEngine_Coroutine3829159415.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween3520241082.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3980534944.h"
#include "mscorlib_System_Collections_Generic_List_1_gen128053199.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen4122643707.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen712889340.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen4109695355.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen4251804118.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen842049751.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3630090483.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4072576034.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3772199246.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen362444879.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen792119500.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1234605051.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen934228263.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1819441192.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen3185818714.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3628304265.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3327927477.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4213140406.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen456935359.h"
#include "mscorlib_System_Collections_Generic_List_1_gen899420910.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen599044122.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1484257051.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen53650832.h"
#include "mscorlib_System_Collections_Generic_List_1_gen496136383.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen195759595.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1080972524.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen2779729376.h"
#include "System_System_Collections_Generic_Stack_1_gen3923495619.h"

// System.ServiceModel.ClientBase`1<System.Object>
struct ClientBase_1_t561984977;
// System.String
struct String_t;
// System.ServiceModel.Channels.Binding
struct Binding_t859993683;
// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;
// System.ServiceModel.InstanceContext
struct InstanceContext_t3593205954;
// System.Object
struct Il2CppObject;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.ServiceModel.ServiceHostBase
struct ServiceHostBase_t3741910535;
// System.EventHandler
struct EventHandler_t1348719766;
// System.ServiceModel.ChannelFactory`1<System.Object>
struct ChannelFactory_1_t585335377;
// System.ServiceModel.Description.ServiceEndpoint
struct ServiceEndpoint_t4038493094;
// System.ServiceModel.ChannelFactory
struct ChannelFactory_t628250694;
// System.ServiceModel.IClientChannel
struct IClientChannel_t714275133;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.ServiceModel.Configuration.MexBindingElement`1<System.Object>
struct MexBindingElement_1_t1398412972;
// System.ServiceModel.Configuration.StandardBindingElement
struct StandardBindingElement_t4182893664;
// System.Type
struct Type_t;
// System.NotImplementedException
struct NotImplementedException_t3489357830;
// System.ServiceModel.Configuration.NamedServiceModelExtensionCollectionElement`1<System.Object>
struct NamedServiceModelExtensionCollectionElement_1_t274909112;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t3318566633;
// System.ComponentModel.StringConverter
struct StringConverter_t3216726494;
// System.Configuration.StringValidator
struct StringValidator_t4226114403;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.ComponentModel.TypeConverter
struct TypeConverter_t2249118273;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t888490966;
// System.ServiceModel.Configuration.ServiceModelConfigurationElementCollection`1<System.Object>
struct ServiceModelConfigurationElementCollection_1_t3718990380;
// System.Configuration.ConfigurationElementCollection
struct ConfigurationElementCollection_t446763386;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.ServiceModel.Configuration.ServiceModelEnhancedConfigurationElementCollection`1<System.Object>
struct ServiceModelEnhancedConfigurationElementCollection_1_t1951097806;
// System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator5_t1583623670;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>
struct ServiceModelExtensionCollectionElement_1_t2544877501;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t3512676632;
// System.Xml.XmlReader
struct XmlReader_t3121518892;
// System.ServiceModel.Configuration.StandardBindingCollectionElement`2<System.Object,System.Object>
struct StandardBindingCollectionElement_2_t3123063704;
// System.ServiceModel.Configuration.StandardBindingElementCollection`1<System.Object>
struct StandardBindingElementCollection_1_t741974772;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.ServiceModel.Configuration.IBindingConfigurationElement>
struct ReadOnlyCollection_1_t580582265;
// System.Collections.Generic.List`1<System.ServiceModel.Configuration.IBindingConfigurationElement>
struct List_1_t840080720;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.ServiceModel.Configuration.IBindingConfigurationElement
struct IBindingConfigurationElement_t3662973274;
// System.Collections.Generic.IList`1<System.ServiceModel.Configuration.IBindingConfigurationElement>
struct IList_1_t1183325761;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t4292682451;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// System.ServiceModel.Dispatcher.ListenerLoopManager/<CreateAcceptor>c__AnonStorey1C`1<System.Object>
struct U3CCreateAcceptorU3Ec__AnonStorey1C_1_t4103390264;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.ServiceModel.Dispatcher.ListenerLoopManager
struct ListenerLoopManager_t4138186672;
// System.ServiceModel.Channels.IChannel
struct IChannel_t937207545;
// System.Threading.EventWaitHandle
struct EventWaitHandle_t777845177;
// System.ServiceModel.DuplexChannelFactory`1<System.Object>
struct DuplexChannelFactory_1_t1618788632;
// System.Uri
struct Uri_t100236324;
// System.ServiceModel.Description.ContractDescription
struct ContractDescription_t1411178514;
// System.ServiceModel.DuplexClientRuntimeChannel
struct DuplexClientRuntimeChannel_t737641411;
// System.ServiceModel.ExtensionCollection`1<System.Object>
struct ExtensionCollection_1_t1857785261;
// System.ServiceModel.IExtension`1<System.Object>
struct IExtension_1_t2909257122;
// System.Web.Compilation.AppResourcesLengthComparer`1<System.Object>
struct AppResourcesLengthComparer_1_t3587265839;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.IO.FileInfo
struct FileInfo_t1169991790;
// System.Web.Compilation.GenericBuildProvider`1<System.Object>
struct GenericBuildProvider_1_t3300278628;
// System.Web.Compilation.BuildProvider
struct BuildProvider_t3736381005;
// System.Web.VirtualPath
struct VirtualPath_t4270372584;
// System.Web.HttpContext
struct HttpContext_t1969259010;
// System.Web.HttpRequest
struct HttpRequest_t809700260;
// System.Web.Compilation.AspGenerator
struct AspGenerator_t2810561191;
// System.IO.TextReader
struct TextReader_t283511965;
// System.Web.Compilation.AssemblyBuilder
struct AssemblyBuilder_t187066735;
// System.Web.Compilation.BaseCompiler
struct BaseCompiler_t69158820;
// System.CodeDom.CodeCompileUnit
struct CodeCompileUnit_t2527618915;
// System.Web.HttpException
struct HttpException_t2907797370;
// System.CodeDom.Compiler.CompilerResults
struct CompilerResults_t1736487784;
// System.Reflection.Assembly
struct Assembly_t4102432799;
// System.Web.Compilation.CompilerType
struct CompilerType_t2533482247;
// System.Web.UI.MainDirectiveAttribute`1<System.Int32>
struct MainDirectiveAttribute_1_t4076720260;
// System.Web.UI.MainDirectiveAttribute`1<System.Object>
struct MainDirectiveAttribute_1_t4205880671;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t2423483305;
// UnityEngine.Object
struct Object_t631007953;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t982173797;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t1111334208;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t3723462114;
// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t214452203;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2703961024;
// System.Delegate
struct Delegate_t1188392813;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t682124106;
// System.ArgumentException
struct ArgumentException_t132251570;
// UnityEngine.Events.InvokableCall`1<System.Int32>
struct InvokableCall_1_t3068109991;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t3535781894;
// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t3197270402;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t3664942305;
// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t1514431012;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t1982102915;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct InvokableCall_1_t2672850562;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t3140522465;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t2273393761;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t2741065664;
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t362407658;
// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
struct InvokableCall_3_t4059188962;
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct InvokableCall_4_t2756980746;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t2933211702;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t3283971887;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2165061829;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t1262235195;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t1557236713;
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t682480391;
// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct UnityEvent_1_t978947469;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t3960448221;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t3832605257;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t3961765668;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t2278926278;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t3437345828;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct UnityEvent_1_t3037889027;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t614268397;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t2404744798;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t4085588227;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t1764640198;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t234526808;
// System.Predicate`1<System.Object>
struct Predicate_1_t3905400288;
// System.Comparison`1<System.Object>
struct Comparison_1_t2855037343;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t3860393442;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ec__Iterator0_t30141770;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t3520241082;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1234605051;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t2779729376;
extern Il2CppCodeGenString* _stringLiteral3452614534;
extern const uint32_t ClientBase_1__ctor_m496987207_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3615648536;
extern Il2CppCodeGenString* _stringLiteral4253584768;
extern const uint32_t ClientBase_1__ctor_m861614820_MetadataUsageId;
extern Il2CppClass* EndpointAddress_t3119842923_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3342225779;
extern const uint32_t ClientBase_1__ctor_m1330080558_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1949055834;
extern const uint32_t ClientBase_1__ctor_m3438457069_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2385970378;
extern const uint32_t ClientBase_1__ctor_m1536589273_MetadataUsageId;
extern Il2CppClass* InstanceContext_t3593205954_il2cpp_TypeInfo_var;
extern const uint32_t ClientBase_1__cctor_m1315903106_MetadataUsageId;
extern Il2CppClass* ICommunicationObject_t3884464922_il2cpp_TypeInfo_var;
extern const uint32_t ClientBase_1_System_ServiceModel_ICommunicationObject_add_Opened_m1186943634_MetadataUsageId;
extern const uint32_t ClientBase_1_System_ServiceModel_ICommunicationObject_remove_Opened_m3027086614_MetadataUsageId;
extern const uint32_t ClientBase_1_System_ServiceModel_ICommunicationObject_add_Closed_m1099591474_MetadataUsageId;
extern const uint32_t ClientBase_1_System_ServiceModel_ICommunicationObject_remove_Closed_m313695681_MetadataUsageId;
extern const uint32_t ClientBase_1_System_ServiceModel_ICommunicationObject_add_Faulted_m2193939548_MetadataUsageId;
extern const uint32_t ClientBase_1_System_ServiceModel_ICommunicationObject_remove_Faulted_m1327224711_MetadataUsageId;
extern const uint32_t ClientBase_1_System_ServiceModel_ICommunicationObject_Close_m1448464074_MetadataUsageId;
extern const uint32_t ClientBase_1_System_ServiceModel_ICommunicationObject_Open_m1781439629_MetadataUsageId;
extern Il2CppClass* IClientChannel_t714275133_il2cpp_TypeInfo_var;
extern const uint32_t ClientBase_1_get_InnerChannel_m3917256438_MetadataUsageId;
extern const uint32_t ClientBase_1_get_State_m2846665432_MetadataUsageId;
extern const uint32_t ClientBase_1_Abort_m500071940_MetadataUsageId;
extern const uint32_t ClientBase_1_Close_m2683208739_MetadataUsageId;
extern const uint32_t ClientBase_1_Open_m2440460607_MetadataUsageId;
extern Il2CppClass* ConfigurationPropertyCollection_t2852175726_il2cpp_TypeInfo_var;
extern const uint32_t MexBindingBindingCollectionElement_2__cctor_m2004660320_MetadataUsageId;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t MexBindingElement_1_get_BindingElementType_m1396362435_MetadataUsageId;
extern Il2CppClass* NotImplementedException_t3489357830_il2cpp_TypeInfo_var;
extern const uint32_t MexBindingElement_1_OnApplyConfiguration_m3605806565_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral62725243;
extern const uint32_t NamedServiceModelExtensionCollectionElement_1_get_Name_m3401074842_MetadataUsageId;
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* StringConverter_t3216726494_il2cpp_TypeInfo_var;
extern Il2CppClass* StringValidator_t4226114403_il2cpp_TypeInfo_var;
extern Il2CppClass* ConfigurationProperty_t3590861854_il2cpp_TypeInfo_var;
extern const uint32_t NamedServiceModelExtensionCollectionElement_1_get_Properties_m1159763332_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern Il2CppClass* ConfigurationElement_t3318566633_il2cpp_TypeInfo_var;
extern const uint32_t ServiceModelConfigurationElementCollection_1_CreateNewElement_m274896085_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator5_Reset_m100070940_MetadataUsageId;
extern const uint32_t ServiceModelExtensionCollectionElement_1_System_Collections_Generic_ICollectionU3CTServiceModelExtensionElementU3E_get_IsReadOnly_m2146300109_MetadataUsageId;
extern const uint32_t ServiceModelExtensionCollectionElement_1_CopyTo_m2440639783_MetadataUsageId;
extern Il2CppClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const uint32_t ServiceModelExtensionCollectionElement_1_Reset_m916777949_MetadataUsageId;
extern const uint32_t StandardBindingCollectionElement_2_get_Bindings_m4261975795_MetadataUsageId;
extern Il2CppClass* List_1_t840080720_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadOnlyCollection_1_t580582265_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1663932146_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1539394036_MethodInfo_var;
extern const MethodInfo* ReadOnlyCollection_1__ctor_m1920932843_MethodInfo_var;
extern const uint32_t StandardBindingCollectionElement_2_get_ConfiguredBindings_m183988966_MetadataUsageId;
extern const uint32_t StandardBindingCollectionElement_2_get_Properties_m4110740973_MetadataUsageId;
extern const uint32_t StandardBindingCollectionElement_2_get_BindingType_m22019752_MetadataUsageId;
extern const uint32_t StandardBindingElementCollection_1__cctor_m3794528160_MetadataUsageId;
extern Il2CppClass* StandardBindingElement_t4182893664_il2cpp_TypeInfo_var;
extern const uint32_t StandardBindingElementCollection_1_GetElementKey_m3923440670_MetadataUsageId;
extern Il2CppClass* Exception_t1436737249_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t3208230065_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral981445416;
extern const uint32_t U3CCreateAcceptorU3Ec__AnonStorey1C_1_U3CU3Em__1F_m805831763_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3945668362;
extern const uint32_t U3CCreateAcceptorU3Ec__AnonStorey1C_1_U3CU3Em__20_m3819685881_MetadataUsageId;
extern const uint32_t DuplexChannelFactory_1__ctor_m4245981057_MetadataUsageId;
extern Il2CppClass* DuplexClientRuntimeChannel_t737641411_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4031800109;
extern const uint32_t DuplexChannelFactory_1_CreateChannel_m1751811269_MetadataUsageId;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ExtensionCollection_1__ctor_m4156153040_MetadataUsageId;
extern Il2CppClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern Il2CppClass* AppResourceFileInfo_t3599744125_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m1191301710_MethodInfo_var;
extern const uint32_t AppResourcesLengthComparer_1_System_Collections_Generic_IComparerU3CTU3E_Compare_m2250971867_MetadataUsageId;
extern Il2CppClass* HttpException_t2907797370_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1914822876;
extern const uint32_t GenericBuildProvider_1_GenerateCode_m3153712934_MetadataUsageId;
extern Il2CppClass* Enumerator_t913802012_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m2276455407_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2043795_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1270368552_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3828655022_MethodInfo_var;
extern const uint32_t GenericBuildProvider_1_GenerateCode_m3776095089_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2894761041;
extern Il2CppCodeGenString* _stringLiteral1220486295;
extern const uint32_t GenericBuildProvider_1_get_Parser_m3189097407_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m3594541075_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m3353156630_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m2686176129_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m2985117283_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m3721720690_MetadataUsageId;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1864861238;
extern const uint32_t InvokableCall_1_Invoke_m2288947156_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m3421292816_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m2227986097_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m513361773_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m1812996400_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m956488764_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m437830257_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m1720187399_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m3583307112_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m3027854863_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m2430730927_MetadataUsageId;
extern const uint32_t InvokableCall_2__ctor_m162480713_MetadataUsageId;
extern const uint32_t InvokableCall_2_Invoke_m4103345430_MetadataUsageId;
extern const uint32_t InvokableCall_3__ctor_m1759759471_MetadataUsageId;
extern const uint32_t InvokableCall_3_Invoke_m4268591549_MetadataUsageId;
extern const uint32_t InvokableCall_4__ctor_m2023811662_MetadataUsageId;
extern const uint32_t InvokableCall_4_Invoke_m3539026103_MetadataUsageId;
extern Il2CppClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m917793246_MetadataUsageId;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2958527564_MetadataUsageId;
extern Il2CppClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m118448792_MetadataUsageId;
extern Il2CppClass* Color_t2555686324_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m1854409024_MetadataUsageId;
extern Il2CppClass* Scene_t2348375561_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m1034040374_MetadataUsageId;
extern Il2CppClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2863020861_MetadataUsageId;
extern Il2CppClass* LoadSceneMode_t3251202195_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m3595933178_MetadataUsageId;
extern const uint32_t UnityAction_2_BeginInvoke_m453999018_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m3777630589_MetadataUsageId;
extern Il2CppClass* TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2660664345_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m2474648315_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m649722414_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m4234511999_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m1537875845_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m2218582587_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m1665304028_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m1293792034_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m4095274380_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m3675246889_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m3637444959_MetadataUsageId;
extern const uint32_t UnityEvent_2__ctor_m3404211184_MetadataUsageId;
extern const uint32_t UnityEvent_2_FindMethod_Impl_m933094994_MetadataUsageId;
extern const uint32_t UnityEvent_3__ctor_m3742571125_MetadataUsageId;
extern const uint32_t UnityEvent_3_FindMethod_Impl_m156112408_MetadataUsageId;
extern const uint32_t UnityEvent_4__ctor_m1543618728_MetadataUsageId;
extern const uint32_t UnityEvent_4_FindMethod_Impl_m96988021_MetadataUsageId;
extern const uint32_t IndexedSet_1_GetEnumerator_m3750514392_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3926843441;
extern const uint32_t IndexedSet_1_Insert_m1432638049_MetadataUsageId;
extern Il2CppClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m524356752_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m3175110837_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m4270440387_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m656428886_MetadataUsageId;
extern Il2CppClass* Object_t631007953_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1132744560;
extern const uint32_t TweenRunner_1_StartTween_m2247690200_MetadataUsageId;
extern const uint32_t TweenRunner_1_StartTween_m1055628540_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral46997234;
extern const uint32_t ObjectPool_1_Release_m3263354170_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t3940880105  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::.ctor(System.Collections.Generic.IList`1<!0>)
extern "C"  void ReadOnlyCollection_1__ctor_m3900376080_gshared (ReadOnlyCollection_1_t4292682451 * __this, Il2CppObject* p0, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3205374586_gshared (UnityAction_1_t682124106 * __this, bool ___arg00, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m286788042_gshared (UnityAction_1_t3535781894 * __this, int32_t ___arg00, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m85073476_gshared (UnityAction_1_t3664942305 * __this, Il2CppObject * ___arg00, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1880497053_gshared (UnityAction_1_t1982102915 * __this, float ___arg00, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2491965359_gshared (UnityAction_1_t3140522465 * __this, Color_t2555686324  ___arg00, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3689685399_gshared (UnityAction_1_t2933211702 * __this, Scene_t2348375561  ___arg00, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2871728_gshared (UnityAction_1_t2741065664 * __this, Vector2_t2156229523  ___arg00, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2615882976_gshared (UnityAction_2_t3283971887 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1297655681_gshared (UnityAction_2_t2165061829 * __this, Scene_t2348375561  ___arg00, int32_t ___arg11, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m770027984_gshared (UnityAction_2_t1262235195 * __this, Scene_t2348375561  ___arg00, Scene_t2348375561  ___arg11, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m2312220652_gshared (UnityAction_3_t1557236713 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
extern "C"  void UnityAction_4_Invoke_m1593023405_gshared (UnityAction_4_t682480391 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
extern "C"  void EventFunction_1_Invoke_m2429482587_gshared (EventFunction_1_t1764640198 * __this, Il2CppObject * ___handler0, BaseEventData_t3903027533 * ___eventData1, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ServiceModel.EndpointAddress::op_Equality(System.ServiceModel.EndpointAddress,System.ServiceModel.EndpointAddress)
extern "C"  bool EndpointAddress_op_Equality_m3747605200 (Il2CppObject * __this /* static, unused */, EndpointAddress_t3119842923 * ___address10, EndpointAddress_t3119842923 * ___address21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ServiceModel.EndpointAddress::.ctor(System.String)
extern "C"  void EndpointAddress__ctor_m2869907329 (EndpointAddress_t3119842923 * __this, String_t* ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ServiceModel.InstanceContext::.ctor(System.ServiceModel.ServiceHostBase)
extern "C"  void InstanceContext__ctor_m1125294515 (InstanceContext_t3593205954 * __this, ServiceHostBase_t3741910535 * ___host0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ServiceModel.Description.ServiceEndpoint System.ServiceModel.ChannelFactory::get_Endpoint()
extern "C"  ServiceEndpoint_t4038493094 * ChannelFactory_get_Endpoint_m777043750 (ChannelFactory_t628250694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationPropertyCollection::.ctor()
extern "C"  void ConfigurationPropertyCollection__ctor_m550516750 (ConfigurationPropertyCollection_t2852175726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ServiceModel.Configuration.StandardBindingElement::.ctor(System.String)
extern "C"  void StandardBindingElement__ctor_m2558281579 (StandardBindingElement_t4182893664 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m3058704252 (NotImplementedException_t3489357830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.ConfigurationElement::get_Item(System.String)
extern "C"  Il2CppObject * ConfigurationElement_get_Item_m3494575926 (ConfigurationElement_t3318566633 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.StringConverter::.ctor()
extern "C"  void StringConverter__ctor_m2070746565 (StringConverter_t3216726494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.StringValidator::.ctor(System.Int32,System.Int32,System.String)
extern "C"  void StringValidator__ctor_m315001218 (StringValidator_t4226114403 * __this, int32_t p0, int32_t p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationProperty::.ctor(System.String,System.Type,System.Object,System.ComponentModel.TypeConverter,System.Configuration.ConfigurationValidatorBase,System.Configuration.ConfigurationPropertyOptions)
extern "C"  void ConfigurationProperty__ctor_m2796875110 (ConfigurationProperty_t3590861854 * __this, String_t* p0, Type_t * p1, Il2CppObject * p2, TypeConverter_t2249118273 * p3, ConfigurationValidatorBase_t888490966 * p4, int32_t p5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationPropertyCollection::Add(System.Configuration.ConfigurationProperty)
extern "C"  void ConfigurationPropertyCollection_Add_m2688614548 (ConfigurationPropertyCollection_t2852175726 * __this, ConfigurationProperty_t3590861854 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationElement System.Configuration.ConfigurationElementCollection::BaseGet(System.Int32)
extern "C"  ConfigurationElement_t3318566633 * ConfigurationElementCollection_BaseGet_m735310723 (ConfigurationElementCollection_t446763386 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationElement System.Configuration.ConfigurationElementCollection::BaseGet(System.Object)
extern "C"  ConfigurationElement_t3318566633 * ConfigurationElementCollection_BaseGet_m1527703308 (ConfigurationElementCollection_t446763386 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConfigurationElementCollection::get_AddElementName()
extern "C"  String_t* ConfigurationElementCollection_get_AddElementName_m3713402850 (ConfigurationElementCollection_t446763386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationElementCollection::BaseAdd(System.Configuration.ConfigurationElement,System.Boolean)
extern "C"  void ConfigurationElementCollection_BaseAdd_m2671588688 (ConfigurationElementCollection_t446763386 * __this, ConfigurationElement_t3318566633 * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type,System.Object[])
extern "C"  Il2CppObject * Activator_CreateInstance_m94526014 (Il2CppObject * __this /* static, unused */, Type_t * p0, ObjectU5BU5D_t2843939325* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationElementCollection::get_ThrowOnDuplicate()
extern "C"  bool ConfigurationElementCollection_get_ThrowOnDuplicate_m3210315871 (ConfigurationElementCollection_t446763386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationElement::.ctor()
extern "C"  void ConfigurationElement__ctor_m4116197893 (ConfigurationElement_t3318566633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationElement::DeserializeElement(System.Xml.XmlReader,System.Boolean)
extern "C"  void ConfigurationElement_DeserializeElement_m2357743528 (ConfigurationElement_t3318566633 * __this, XmlReader_t3121518892 * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationElement::Reset(System.Configuration.ConfigurationElement)
extern "C"  void ConfigurationElement_Reset_m3089641269 (ConfigurationElement_t3318566633 * __this, ConfigurationElement_t3318566633 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConfigurationElement::get_Properties()
extern "C"  ConfigurationPropertyCollection_t2852175726 * ConfigurationElement_get_Properties_m976126252 (ConfigurationElement_t3318566633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.ServiceModel.Configuration.IBindingConfigurationElement>::.ctor()
#define List_1__ctor_m1663932146(__this, method) ((  void (*) (List_1_t840080720 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.ServiceModel.Configuration.IBindingConfigurationElement>::Add(!0)
#define List_1_Add_m1539394036(__this, p0, method) ((  void (*) (List_1_t840080720 *, Il2CppObject *, const MethodInfo*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Int32 System.Configuration.ConfigurationElementCollection::get_Count()
extern "C"  int32_t ConfigurationElementCollection_get_Count_m873586478 (ConfigurationElementCollection_t446763386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.ServiceModel.Configuration.IBindingConfigurationElement>::.ctor(System.Collections.Generic.IList`1<!0>)
#define ReadOnlyCollection_1__ctor_m1920932843(__this, p0, method) ((  void (*) (ReadOnlyCollection_1_t580582265 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3900376080_gshared)(__this, p0, method)
// System.String System.ServiceModel.Configuration.StandardBindingElement::get_Name()
extern "C"  String_t* StandardBindingElement_get_Name_m2797479792 (StandardBindingElement_t4182893664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ServiceModel.Dispatcher.ListenerLoopManager::ChannelAccepted(System.ServiceModel.Channels.IChannel)
extern "C"  void ListenerLoopManager_ChannelAccepted_m848092585 (ListenerLoopManager_t4138186672 * __this, Il2CppObject * ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.String)
extern "C"  void Console_WriteLine_m4182570127 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Object)
extern "C"  void Console_WriteLine_m68308674 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.EventWaitHandle::Set()
extern "C"  bool EventWaitHandle_Set_m2445193251 (EventWaitHandle_t777845177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ServiceModel.InstanceContext::.ctor(System.Object)
extern "C"  void InstanceContext__ctor_m1179855330 (InstanceContext_t3593205954 * __this, Il2CppObject * ___implementation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ServiceModel.ChannelFactory::EnsureOpened()
extern "C"  void ChannelFactory_EnsureOpened_m4135578152 (ChannelFactory_t628250694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ServiceModel.Description.ContractDescription System.ServiceModel.Description.ServiceEndpoint::get_Contract()
extern "C"  ContractDescription_t1411178514 * ServiceEndpoint_get_Contract_m872865802 (ServiceEndpoint_t4038493094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.ServiceModel.ClientProxyGenerator::CreateProxyType(System.Type,System.ServiceModel.Description.ContractDescription,System.Boolean)
extern "C"  Type_t * ClientProxyGenerator_CreateProxyType_m1012746588 (Il2CppObject * __this /* static, unused */, Type_t * ___contractInterface0, ContractDescription_t1411178514 * ___cd1, bool ___duplex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ServiceModel.DuplexClientRuntimeChannel::set_CallbackInstance(System.ServiceModel.InstanceContext)
extern "C"  void DuplexClientRuntimeChannel_set_CallbackInstance_m2388021322 (DuplexClientRuntimeChannel_t737641411 * __this, InstanceContext_t3593205954 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m1191301710(__this, p0, method) ((  String_t* (*) (List_1_t3319525431 *, int32_t, const MethodInfo*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.String System.IO.FileInfo::get_Name()
extern "C"  String_t* FileInfo_get_Name_m33729499 (FileInfo_t1169991790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Compilation.BuildProvider::.ctor()
extern "C"  void BuildProvider__ctor_m4164598614 (BuildProvider_t3736381005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.HttpContext System.Web.HttpContext::get_Current()
extern "C"  HttpContext_t1969259010 * HttpContext_get_Current_m1955064578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.HttpRequest System.Web.HttpContext::get_Request()
extern "C"  HttpRequest_t809700260 * HttpContext_get_Request_m1929562575 (HttpContext_t1969259010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.Compilation.BuildProvider::get_VirtualPath()
extern "C"  String_t* BuildProvider_get_VirtualPath_m3357007857 (BuildProvider_t3736381005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.HttpRequest::MapPath(System.String)
extern "C"  String_t* HttpRequest_MapPath_m3338942164 (HttpRequest_t809700260 * __this, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.VirtualPath System.Web.Compilation.BuildProvider::get_VirtualPathInternal()
extern "C"  VirtualPath_t4270372584 * BuildProvider_get_VirtualPathInternal_m2593712449 (BuildProvider_t3736381005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Compilation.AspGenerator::Parse(System.IO.TextReader,System.String,System.Boolean)
extern "C"  void AspGenerator_Parse_m2884416108 (AspGenerator_t2810561191 * __this, TextReader_t283511965 * ___reader0, String_t* ___filename1, bool ___doInitParser2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Compilation.AspGenerator::Parse()
extern "C"  void AspGenerator_Parse_m1528274556 (AspGenerator_t2810561191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Compilation.BaseCompiler::ConstructType()
extern "C"  void BaseCompiler_ConstructType_m3989217184 (BaseCompiler_t69158820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.CodeDom.CodeCompileUnit System.Web.Compilation.BaseCompiler::get_CompileUnit()
extern "C"  CodeCompileUnit_t2527618915 * BaseCompiler_get_CompileUnit_m770786863 (BaseCompiler_t69158820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.HttpException::.ctor(System.String)
extern "C"  void HttpException__ctor_m2549443145 (HttpException_t2907797370 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Compilation.AssemblyBuilder::AddCodeCompileUnit(System.Web.Compilation.BuildProvider,System.CodeDom.CodeCompileUnit)
extern "C"  void AssemblyBuilder_AddCodeCompileUnit_m3424641995 (AssemblyBuilder_t187066735 * __this, BuildProvider_t3736381005 * ___buildProvider0, CodeCompileUnit_t2527618915 * ___compileUnit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Web.Compilation.AssemblyBuilder::AddCodeFile(System.String,System.Web.Compilation.BuildProvider,System.Boolean)
extern "C"  void AssemblyBuilder_AddCodeFile_m97128385 (AssemblyBuilder_t187066735 * __this, String_t* ___path0, BuildProvider_t3736381005 * ___bp1, bool ___isVirtual2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m2276455407(__this, method) ((  int32_t (*) (List_1_t3319525431 *, const MethodInfo*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
#define List_1_GetEnumerator_m2043795(__this, method) ((  Enumerator_t913802012  (*) (List_1_t3319525431 *, const MethodInfo*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
#define Enumerator_get_Current_m1270368552(__this, method) ((  String_t* (*) (Enumerator_t913802012 *, const MethodInfo*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Void System.Web.Compilation.AssemblyBuilder::AddAssemblyReference(System.String)
extern "C"  void AssemblyBuilder_AddAssemblyReference_m3403962749 (AssemblyBuilder_t187066735 * __this, String_t* ___assemblyLocation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
#define Enumerator_MoveNext_m3828655022(__this, method) ((  bool (*) (Enumerator_t913802012 *, const MethodInfo*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Reflection.Assembly System.CodeDom.Compiler.CompilerResults::get_CompiledAssembly()
extern "C"  Assembly_t4102432799 * CompilerResults_get_CompiledAssembly_m3588532760 (CompilerResults_t1736487784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.VirtualPath::get_Original()
extern "C"  String_t* VirtualPath_get_Original_m1834498342 (VirtualPath_t4270372584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.TextReader System.Web.Compilation.BuildProvider::OpenReader(System.String)
extern "C"  TextReader_t283511965 * BuildProvider_OpenReader_m2470198021 (BuildProvider_t3736381005 * __this, String_t* ___virtualPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Web.Compilation.BuildProvider::get_LanguageName()
extern "C"  String_t* BuildProvider_get_LanguageName_m890363898 (BuildProvider_t3736381005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Web.Compilation.CompilerType System.Web.Compilation.BuildProvider::GetDefaultCompilerTypeForLanguage(System.String)
extern "C"  CompilerType_t2533482247 * BuildProvider_GetDefaultCompilerTypeForLanguage_m448594919 (BuildProvider_t3736381005 * __this, String_t* ___language0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Web.UI.BaseParser::IsExpression(System.String)
extern "C"  bool BaseParser_IsExpression_m2969228073 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m1036035291 (BaseInvokableCall_t2703961024 * __this, Il2CppObject * ___target0, MethodInfo_t * ___function1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t1188392813 * NetFxCoreExtensions_CreateDelegate_m4283065679 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, Il2CppObject * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m3648827834 (BaseInvokableCall_t2703961024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m1250801794 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * ___delegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Delegate::get_Target()
extern "C"  Il2CppObject * Delegate_get_Target_m2361978888 (Delegate_t1188392813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m3154548066 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
#define UnityAction_1_Invoke_m3205374586(__this, ___arg00, method) ((  void (*) (UnityAction_1_t682124106 *, bool, const MethodInfo*))UnityAction_1_Invoke_m3205374586_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
#define UnityAction_1_Invoke_m286788042(__this, ___arg00, method) ((  void (*) (UnityAction_1_t3535781894 *, int32_t, const MethodInfo*))UnityAction_1_Invoke_m286788042_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
#define UnityAction_1_Invoke_m85073476(__this, ___arg00, method) ((  void (*) (UnityAction_1_t3664942305 *, Il2CppObject *, const MethodInfo*))UnityAction_1_Invoke_m85073476_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
#define UnityAction_1_Invoke_m1880497053(__this, ___arg00, method) ((  void (*) (UnityAction_1_t1982102915 *, float, const MethodInfo*))UnityAction_1_Invoke_m1880497053_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
#define UnityAction_1_Invoke_m2491965359(__this, ___arg00, method) ((  void (*) (UnityAction_1_t3140522465 *, Color_t2555686324 , const MethodInfo*))UnityAction_1_Invoke_m2491965359_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
#define UnityAction_1_Invoke_m3689685399(__this, ___arg00, method) ((  void (*) (UnityAction_1_t2933211702 *, Scene_t2348375561 , const MethodInfo*))UnityAction_1_Invoke_m3689685399_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
#define UnityAction_1_Invoke_m2871728(__this, ___arg00, method) ((  void (*) (UnityAction_1_t2741065664 *, Vector2_t2156229523 , const MethodInfo*))UnityAction_1_Invoke_m2871728_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m2615882976(__this, ___arg00, ___arg11, method) ((  void (*) (UnityAction_2_t3283971887 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))UnityAction_2_Invoke_m2615882976_gshared)(__this, ___arg00, ___arg11, method)
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m1297655681(__this, ___arg00, ___arg11, method) ((  void (*) (UnityAction_2_t2165061829 *, Scene_t2348375561 , int32_t, const MethodInfo*))UnityAction_2_Invoke_m1297655681_gshared)(__this, ___arg00, ___arg11, method)
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m770027984(__this, ___arg00, ___arg11, method) ((  void (*) (UnityAction_2_t1262235195 *, Scene_t2348375561 , Scene_t2348375561 , const MethodInfo*))UnityAction_2_Invoke_m770027984_gshared)(__this, ___arg00, ___arg11, method)
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
#define UnityAction_3_Invoke_m2312220652(__this, ___arg00, ___arg11, ___arg22, method) ((  void (*) (UnityAction_3_t1557236713 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))UnityAction_3_Invoke_m2312220652_gshared)(__this, ___arg00, ___arg11, ___arg22, method)
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
#define UnityAction_4_Invoke_m1593023405(__this, ___arg00, ___arg11, ___arg22, ___arg33, method) ((  void (*) (UnityAction_4_t682480391 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))UnityAction_4_Invoke_m1593023405_gshared)(__this, ___arg00, ___arg11, ___arg22, ___arg33, method)
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m1087366165 (UnityEventBase_t3960448221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m3632990696 (UnityEventBase_t3960448221 * __this, BaseInvokableCall_t2703961024 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void UnityEventBase_RemoveListener_m748247232 (UnityEventBase_t3960448221 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m1761163145 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t3940880105* ___argumentTypes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern "C"  void UnityEventBase_Invoke_m3022009851 (UnityEventBase_t3960448221 * __this, ObjectU5BU5D_t2843939325* ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
#define EventFunction_1_Invoke_m2429482587(__this, ___handler0, ___eventData1, method) ((  void (*) (EventFunction_1_t1764640198 *, Il2CppObject *, BaseEventData_t3903027533 *, const MethodInfo*))EventFunction_1_Invoke_m2429482587_gshared)(__this, ___handler0, ___eventData1, method)
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C"  void NotSupportedException__ctor_m2494070935 (NotSupportedException_t1314879016 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
extern "C"  bool ColorTween_ValidTarget_m376919233 (ColorTween_t809614380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
extern "C"  bool ColorTween_get_ignoreTimeScale_m1133957174 (ColorTween_t809614380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m3673896426 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m3562456068 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
extern "C"  float ColorTween_get_duration_m3264097060 (ColorTween_t809614380 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m4133291925 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
extern "C"  void ColorTween_TweenValue_m3895102629 (ColorTween_t809614380 * __this, float ___floatPercentage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::ValidTarget()
extern "C"  bool FloatTween_ValidTarget_m885246038 (FloatTween_t1274330004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::get_ignoreTimeScale()
extern "C"  bool FloatTween_get_ignoreTimeScale_m322812475 (FloatTween_t1274330004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_duration()
extern "C"  float FloatTween_get_duration_m1227071020 (FloatTween_t1274330004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::TweenValue(System.Single)
extern "C"  void FloatTween_TweenValue_m52237061 (FloatTween_t1274330004 * __this, float ___floatPercentage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1454075600 (Il2CppObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3661709751 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m2648350745 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m921367020 (GameObject_t1113636619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m4001331470 (MonoBehaviour_t3962482529 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m1038915083 (MonoBehaviour_t3962482529 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m610702577 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2059623341 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor()
extern "C"  void ClientBase_1__ctor_m2965983078_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		InstanceContext_t3593205954 * L_0 = ((ClientBase_1_t561984977_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_initialContxt_0();
		NullCheck((ClientBase_1_t561984977 *)__this);
		((  void (*) (ClientBase_1_t561984977 *, InstanceContext_t3593205954 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor(System.String)
extern "C"  void ClientBase_1__ctor_m2463998495_gshared (ClientBase_1_t561984977 * __this, String_t* ___endpointConfigurationName0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		InstanceContext_t3593205954 * L_0 = ((ClientBase_1_t561984977_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_initialContxt_0();
		String_t* L_1 = ___endpointConfigurationName0;
		NullCheck((ClientBase_1_t561984977 *)__this);
		((  void (*) (ClientBase_1_t561984977 *, InstanceContext_t3593205954 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_0, (String_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor(System.ServiceModel.Channels.Binding,System.ServiceModel.EndpointAddress)
extern "C"  void ClientBase_1__ctor_m4254830935_gshared (ClientBase_1_t561984977 * __this, Binding_t859993683 * ___binding0, EndpointAddress_t3119842923 * ___remoteAddress1, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		InstanceContext_t3593205954 * L_0 = ((ClientBase_1_t561984977_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_initialContxt_0();
		Binding_t859993683 * L_1 = ___binding0;
		EndpointAddress_t3119842923 * L_2 = ___remoteAddress1;
		NullCheck((ClientBase_1_t561984977 *)__this);
		((  void (*) (ClientBase_1_t561984977 *, InstanceContext_t3593205954 *, Binding_t859993683 *, EndpointAddress_t3119842923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_0, (Binding_t859993683 *)L_1, (EndpointAddress_t3119842923 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor(System.String,System.ServiceModel.EndpointAddress)
extern "C"  void ClientBase_1__ctor_m3105968599_gshared (ClientBase_1_t561984977 * __this, String_t* ___endpointConfigurationName0, EndpointAddress_t3119842923 * ___remoteAddress1, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		InstanceContext_t3593205954 * L_0 = ((ClientBase_1_t561984977_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_initialContxt_0();
		String_t* L_1 = ___endpointConfigurationName0;
		EndpointAddress_t3119842923 * L_2 = ___remoteAddress1;
		NullCheck((ClientBase_1_t561984977 *)__this);
		((  void (*) (ClientBase_1_t561984977 *, InstanceContext_t3593205954 *, String_t*, EndpointAddress_t3119842923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_0, (String_t*)L_1, (EndpointAddress_t3119842923 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor(System.String,System.String)
extern "C"  void ClientBase_1__ctor_m2549289957_gshared (ClientBase_1_t561984977 * __this, String_t* ___endpointConfigurationName0, String_t* ___remoteAddress1, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		InstanceContext_t3593205954 * L_0 = ((ClientBase_1_t561984977_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_initialContxt_0();
		String_t* L_1 = ___endpointConfigurationName0;
		String_t* L_2 = ___remoteAddress1;
		NullCheck((ClientBase_1_t561984977 *)__this);
		((  void (*) (ClientBase_1_t561984977 *, InstanceContext_t3593205954 *, String_t*, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_0, (String_t*)L_1, (String_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor(System.ServiceModel.InstanceContext)
extern "C"  void ClientBase_1__ctor_m496987207_gshared (ClientBase_1_t561984977 * __this, InstanceContext_t3593205954 * ___instance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1__ctor_m496987207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InstanceContext_t3593205954 * L_0 = ___instance0;
		NullCheck((ClientBase_1_t561984977 *)__this);
		((  void (*) (ClientBase_1_t561984977 *, InstanceContext_t3593205954 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_0, (String_t*)_stringLiteral3452614534, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor(System.ServiceModel.InstanceContext,System.String)
extern "C"  void ClientBase_1__ctor_m861614820_gshared (ClientBase_1_t561984977 * __this, InstanceContext_t3593205954 * ___instance0, String_t* ___endpointConfigurationName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1__ctor_m861614820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		InstanceContext_t3593205954 * L_0 = ___instance0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral3615648536, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		String_t* L_2 = ___endpointConfigurationName1;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, (String_t*)_stringLiteral4253584768, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		InstanceContext_t3593205954 * L_4 = ___instance0;
		String_t* L_5 = ___endpointConfigurationName1;
		NullCheck((ClientBase_1_t561984977 *)__this);
		VirtActionInvoker3< InstanceContext_t3593205954 *, String_t*, EndpointAddress_t3119842923 * >::Invoke(17 /* System.Void System.ServiceModel.ClientBase`1<System.Object>::Initialize(System.ServiceModel.InstanceContext,System.String,System.ServiceModel.EndpointAddress) */, (ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_4, (String_t*)L_5, (EndpointAddress_t3119842923 *)NULL);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor(System.ServiceModel.InstanceContext,System.String,System.ServiceModel.EndpointAddress)
extern "C"  void ClientBase_1__ctor_m1330080558_gshared (ClientBase_1_t561984977 * __this, InstanceContext_t3593205954 * ___instance0, String_t* ___endpointConfigurationName1, EndpointAddress_t3119842923 * ___remoteAddress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1__ctor_m1330080558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		InstanceContext_t3593205954 * L_0 = ___instance0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral3615648536, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		String_t* L_2 = ___endpointConfigurationName1;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, (String_t*)_stringLiteral4253584768, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		EndpointAddress_t3119842923 * L_4 = ___remoteAddress2;
		IL2CPP_RUNTIME_CLASS_INIT(EndpointAddress_t3119842923_il2cpp_TypeInfo_var);
		bool L_5 = EndpointAddress_op_Equality_m3747605200(NULL /*static, unused*/, (EndpointAddress_t3119842923 *)L_4, (EndpointAddress_t3119842923 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_6 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_6, (String_t*)_stringLiteral3342225779, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_003f:
	{
		InstanceContext_t3593205954 * L_7 = ___instance0;
		String_t* L_8 = ___endpointConfigurationName1;
		EndpointAddress_t3119842923 * L_9 = ___remoteAddress2;
		NullCheck((ClientBase_1_t561984977 *)__this);
		VirtActionInvoker3< InstanceContext_t3593205954 *, String_t*, EndpointAddress_t3119842923 * >::Invoke(17 /* System.Void System.ServiceModel.ClientBase`1<System.Object>::Initialize(System.ServiceModel.InstanceContext,System.String,System.ServiceModel.EndpointAddress) */, (ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_7, (String_t*)L_8, (EndpointAddress_t3119842923 *)L_9);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor(System.ServiceModel.InstanceContext,System.String,System.String)
extern "C"  void ClientBase_1__ctor_m3438457069_gshared (ClientBase_1_t561984977 * __this, InstanceContext_t3593205954 * ___instance0, String_t* ___endpointConfigurationName1, String_t* ___remoteAddress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1__ctor_m3438457069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		InstanceContext_t3593205954 * L_0 = ___instance0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral3615648536, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		String_t* L_2 = ___remoteAddress2;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, (String_t*)_stringLiteral1949055834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		String_t* L_4 = ___endpointConfigurationName1;
		if (L_4)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_5 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_5, (String_t*)_stringLiteral4253584768, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0039:
	{
		InstanceContext_t3593205954 * L_6 = ___instance0;
		String_t* L_7 = ___endpointConfigurationName1;
		String_t* L_8 = ___remoteAddress2;
		EndpointAddress_t3119842923 * L_9 = (EndpointAddress_t3119842923 *)il2cpp_codegen_object_new(EndpointAddress_t3119842923_il2cpp_TypeInfo_var);
		EndpointAddress__ctor_m2869907329(L_9, (String_t*)L_8, /*hidden argument*/NULL);
		NullCheck((ClientBase_1_t561984977 *)__this);
		VirtActionInvoker3< InstanceContext_t3593205954 *, String_t*, EndpointAddress_t3119842923 * >::Invoke(17 /* System.Void System.ServiceModel.ClientBase`1<System.Object>::Initialize(System.ServiceModel.InstanceContext,System.String,System.ServiceModel.EndpointAddress) */, (ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_6, (String_t*)L_7, (EndpointAddress_t3119842923 *)L_9);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.ctor(System.ServiceModel.InstanceContext,System.ServiceModel.Channels.Binding,System.ServiceModel.EndpointAddress)
extern "C"  void ClientBase_1__ctor_m1536589273_gshared (ClientBase_1_t561984977 * __this, InstanceContext_t3593205954 * ___instance0, Binding_t859993683 * ___binding1, EndpointAddress_t3119842923 * ___remoteAddress2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1__ctor_m1536589273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		InstanceContext_t3593205954 * L_0 = ___instance0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral3615648536, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Binding_t859993683 * L_2 = ___binding1;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, (String_t*)_stringLiteral2385970378, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		EndpointAddress_t3119842923 * L_4 = ___remoteAddress2;
		IL2CPP_RUNTIME_CLASS_INIT(EndpointAddress_t3119842923_il2cpp_TypeInfo_var);
		bool L_5 = EndpointAddress_op_Equality_m3747605200(NULL /*static, unused*/, (EndpointAddress_t3119842923 *)L_4, (EndpointAddress_t3119842923 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_6 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_6, (String_t*)_stringLiteral3342225779, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_003f:
	{
		InstanceContext_t3593205954 * L_7 = ___instance0;
		Binding_t859993683 * L_8 = ___binding1;
		EndpointAddress_t3119842923 * L_9 = ___remoteAddress2;
		NullCheck((ClientBase_1_t561984977 *)__this);
		VirtActionInvoker3< InstanceContext_t3593205954 *, Binding_t859993683 *, EndpointAddress_t3119842923 * >::Invoke(18 /* System.Void System.ServiceModel.ClientBase`1<System.Object>::Initialize(System.ServiceModel.InstanceContext,System.ServiceModel.Channels.Binding,System.ServiceModel.EndpointAddress) */, (ClientBase_1_t561984977 *)__this, (InstanceContext_t3593205954 *)L_7, (Binding_t859993683 *)L_8, (EndpointAddress_t3119842923 *)L_9);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::.cctor()
extern "C"  void ClientBase_1__cctor_m1315903106_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1__cctor_m1315903106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InstanceContext_t3593205954 * L_0 = (InstanceContext_t3593205954 *)il2cpp_codegen_object_new(InstanceContext_t3593205954_il2cpp_TypeInfo_var);
		InstanceContext__ctor_m1125294515(L_0, (ServiceHostBase_t3741910535 *)NULL, /*hidden argument*/NULL);
		((ClientBase_1_t561984977_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_initialContxt_0(L_0);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::System.ServiceModel.ICommunicationObject.add_Opened(System.EventHandler)
extern "C"  void ClientBase_1_System_ServiceModel_ICommunicationObject_add_Opened_m1186943634_gshared (ClientBase_1_t561984977 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_System_ServiceModel_ICommunicationObject_add_Opened_m1186943634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		EventHandler_t1348719766 * L_1 = ___value0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker1< EventHandler_t1348719766 * >::Invoke(4 /* System.Void System.ServiceModel.ICommunicationObject::add_Opened(System.EventHandler) */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (EventHandler_t1348719766 *)L_1);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::System.ServiceModel.ICommunicationObject.remove_Opened(System.EventHandler)
extern "C"  void ClientBase_1_System_ServiceModel_ICommunicationObject_remove_Opened_m3027086614_gshared (ClientBase_1_t561984977 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_System_ServiceModel_ICommunicationObject_remove_Opened_m3027086614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		EventHandler_t1348719766 * L_1 = ___value0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker1< EventHandler_t1348719766 * >::Invoke(5 /* System.Void System.ServiceModel.ICommunicationObject::remove_Opened(System.EventHandler) */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (EventHandler_t1348719766 *)L_1);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::System.ServiceModel.ICommunicationObject.add_Closed(System.EventHandler)
extern "C"  void ClientBase_1_System_ServiceModel_ICommunicationObject_add_Closed_m1099591474_gshared (ClientBase_1_t561984977 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_System_ServiceModel_ICommunicationObject_add_Closed_m1099591474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		EventHandler_t1348719766 * L_1 = ___value0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker1< EventHandler_t1348719766 * >::Invoke(0 /* System.Void System.ServiceModel.ICommunicationObject::add_Closed(System.EventHandler) */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (EventHandler_t1348719766 *)L_1);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::System.ServiceModel.ICommunicationObject.remove_Closed(System.EventHandler)
extern "C"  void ClientBase_1_System_ServiceModel_ICommunicationObject_remove_Closed_m313695681_gshared (ClientBase_1_t561984977 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_System_ServiceModel_ICommunicationObject_remove_Closed_m313695681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		EventHandler_t1348719766 * L_1 = ___value0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker1< EventHandler_t1348719766 * >::Invoke(1 /* System.Void System.ServiceModel.ICommunicationObject::remove_Closed(System.EventHandler) */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (EventHandler_t1348719766 *)L_1);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::System.ServiceModel.ICommunicationObject.add_Faulted(System.EventHandler)
extern "C"  void ClientBase_1_System_ServiceModel_ICommunicationObject_add_Faulted_m2193939548_gshared (ClientBase_1_t561984977 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_System_ServiceModel_ICommunicationObject_add_Faulted_m2193939548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		EventHandler_t1348719766 * L_1 = ___value0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker1< EventHandler_t1348719766 * >::Invoke(2 /* System.Void System.ServiceModel.ICommunicationObject::add_Faulted(System.EventHandler) */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (EventHandler_t1348719766 *)L_1);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::System.ServiceModel.ICommunicationObject.remove_Faulted(System.EventHandler)
extern "C"  void ClientBase_1_System_ServiceModel_ICommunicationObject_remove_Faulted_m1327224711_gshared (ClientBase_1_t561984977 * __this, EventHandler_t1348719766 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_System_ServiceModel_ICommunicationObject_remove_Faulted_m1327224711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		EventHandler_t1348719766 * L_1 = ___value0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker1< EventHandler_t1348719766 * >::Invoke(3 /* System.Void System.ServiceModel.ICommunicationObject::remove_Faulted(System.EventHandler) */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (EventHandler_t1348719766 *)L_1);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::System.IDisposable.Dispose()
extern "C"  void ClientBase_1_System_IDisposable_Dispose_m1235688553_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		((  void (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::System.ServiceModel.ICommunicationObject.Close(System.TimeSpan)
extern "C"  void ClientBase_1_System_ServiceModel_ICommunicationObject_Close_m1448464074_gshared (ClientBase_1_t561984977 * __this, TimeSpan_t881159249  ___timeout0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_System_ServiceModel_ICommunicationObject_Close_m1448464074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		TimeSpan_t881159249  L_1 = ___timeout0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker1< TimeSpan_t881159249  >::Invoke(9 /* System.Void System.ServiceModel.ICommunicationObject::Close(System.TimeSpan) */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (TimeSpan_t881159249 )L_1);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::System.ServiceModel.ICommunicationObject.Open(System.TimeSpan)
extern "C"  void ClientBase_1_System_ServiceModel_ICommunicationObject_Open_m1781439629_gshared (ClientBase_1_t561984977 * __this, TimeSpan_t881159249  ___timeout0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_System_ServiceModel_ICommunicationObject_Open_m1781439629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		TimeSpan_t881159249  L_1 = ___timeout0;
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker1< TimeSpan_t881159249  >::Invoke(11 /* System.Void System.ServiceModel.ICommunicationObject::Open(System.TimeSpan) */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0, (TimeSpan_t881159249 )L_1);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::Initialize(System.ServiceModel.InstanceContext,System.String,System.ServiceModel.EndpointAddress)
extern "C"  void ClientBase_1_Initialize_m3740889083_gshared (ClientBase_1_t561984977 * __this, InstanceContext_t3593205954 * ___instance0, String_t* ___endpointConfigurationName1, EndpointAddress_t3119842923 * ___remoteAddress2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___endpointConfigurationName1;
		EndpointAddress_t3119842923 * L_1 = ___remoteAddress2;
		ChannelFactory_1_t585335377 * L_2 = (ChannelFactory_1_t585335377 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10));
		((  void (*) (ChannelFactory_1_t585335377 *, String_t*, EndpointAddress_t3119842923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(L_2, (String_t*)L_0, (EndpointAddress_t3119842923 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((ClientBase_1_t561984977 *)__this);
		((  void (*) (ClientBase_1_t561984977 *, ChannelFactory_1_t585335377 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((ClientBase_1_t561984977 *)__this, (ChannelFactory_1_t585335377 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::Initialize(System.ServiceModel.InstanceContext,System.ServiceModel.Channels.Binding,System.ServiceModel.EndpointAddress)
extern "C"  void ClientBase_1_Initialize_m2466113726_gshared (ClientBase_1_t561984977 * __this, InstanceContext_t3593205954 * ___instance0, Binding_t859993683 * ___binding1, EndpointAddress_t3119842923 * ___remoteAddress2, const MethodInfo* method)
{
	{
		Binding_t859993683 * L_0 = ___binding1;
		EndpointAddress_t3119842923 * L_1 = ___remoteAddress2;
		ChannelFactory_1_t585335377 * L_2 = (ChannelFactory_1_t585335377 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 10));
		((  void (*) (ChannelFactory_1_t585335377 *, Binding_t859993683 *, EndpointAddress_t3119842923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)(L_2, (Binding_t859993683 *)L_0, (EndpointAddress_t3119842923 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		NullCheck((ClientBase_1_t561984977 *)__this);
		((  void (*) (ClientBase_1_t561984977 *, ChannelFactory_1_t585335377 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((ClientBase_1_t561984977 *)__this, (ChannelFactory_1_t585335377 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return;
	}
}
// System.ServiceModel.ChannelFactory`1<TChannel> System.ServiceModel.ClientBase`1<System.Object>::get_ChannelFactory()
extern "C"  ChannelFactory_1_t585335377 * ClientBase_1_get_ChannelFactory_m3877010469_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	{
		ChannelFactory_1_t585335377 * L_0 = (ChannelFactory_1_t585335377 *)__this->get_factory_1();
		return L_0;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::set_ChannelFactory(System.ServiceModel.ChannelFactory`1<TChannel>)
extern "C"  void ClientBase_1_set_ChannelFactory_m1172556125_gshared (ClientBase_1_t561984977 * __this, ChannelFactory_1_t585335377 * ___value0, const MethodInfo* method)
{
	{
		ChannelFactory_1_t585335377 * L_0 = ___value0;
		__this->set_factory_1(L_0);
		ChannelFactory_1_t585335377 * L_1 = (ChannelFactory_1_t585335377 *)__this->get_factory_1();
		NullCheck((ChannelFactory_1_t585335377 *)L_1);
		((  void (*) (ChannelFactory_1_t585335377 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((ChannelFactory_1_t585335377 *)L_1, (Il2CppObject *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return;
	}
}
// System.ServiceModel.Description.ServiceEndpoint System.ServiceModel.ClientBase`1<System.Object>::get_Endpoint()
extern "C"  ServiceEndpoint_t4038493094 * ClientBase_1_get_Endpoint_m1892323356_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	{
		ChannelFactory_1_t585335377 * L_0 = (ChannelFactory_1_t585335377 *)__this->get_factory_1();
		NullCheck((ChannelFactory_t628250694 *)L_0);
		ServiceEndpoint_t4038493094 * L_1 = ChannelFactory_get_Endpoint_m777043750((ChannelFactory_t628250694 *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.ServiceModel.IClientChannel System.ServiceModel.ClientBase`1<System.Object>::get_InnerChannel()
extern "C"  Il2CppObject * ClientBase_1_get_InnerChannel_m3917256438_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_get_InnerChannel_m3917256438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inner_channel_2();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(19 /* TChannel System.ServiceModel.ClientBase`1<System.Object>::CreateChannel() */, (ClientBase_1_t561984977 *)__this);
		__this->set_inner_channel_2(((Il2CppObject *)Castclass(L_1, IClientChannel_t714275133_il2cpp_TypeInfo_var)));
	}

IL_0021:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_inner_channel_2();
		return L_2;
	}
}
// TChannel System.ServiceModel.ClientBase`1<System.Object>::get_Channel()
extern "C"  Il2CppObject * ClientBase_1_get_Channel_m3474875452_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return ((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
	}
}
// System.ServiceModel.CommunicationState System.ServiceModel.ClientBase`1<System.Object>::get_State()
extern "C"  int32_t ClientBase_1_get_State_m2846665432_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_get_State_m2846665432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject *)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(6 /* System.ServiceModel.CommunicationState System.ServiceModel.ICommunicationObject::get_State() */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::Abort()
extern "C"  void ClientBase_1_Abort_m500071940_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_Abort_m500071940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(7 /* System.Void System.ServiceModel.ICommunicationObject::Abort() */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::Close()
extern "C"  void ClientBase_1_Close_m2683208739_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_Close_m2683208739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(8 /* System.Void System.ServiceModel.ICommunicationObject::Close() */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// TChannel System.ServiceModel.ClientBase`1<System.Object>::CreateChannel()
extern "C"  Il2CppObject * ClientBase_1_CreateChannel_m3009675197_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		ChannelFactory_1_t585335377 * L_0 = ((  ChannelFactory_1_t585335377 * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((ChannelFactory_1_t585335377 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (ChannelFactory_1_t585335377 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((ChannelFactory_1_t585335377 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return L_1;
	}
}
// System.Void System.ServiceModel.ClientBase`1<System.Object>::Open()
extern "C"  void ClientBase_1_Open_m2440460607_gshared (ClientBase_1_t561984977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ClientBase_1_Open_m2440460607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ClientBase_1_t561984977 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (ClientBase_1_t561984977 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ClientBase_1_t561984977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Il2CppObject *)L_0);
		InterfaceActionInvoker0::Invoke(10 /* System.Void System.ServiceModel.ICommunicationObject::Open() */, ICommunicationObject_t3884464922_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return;
	}
}
// System.Void System.ServiceModel.Configuration.MexBindingBindingCollectionElement`2<System.Object,System.Object>::.cctor()
extern "C"  void MexBindingBindingCollectionElement_2__cctor_m2004660320_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MexBindingBindingCollectionElement_2__cctor_m2004660320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ConfigurationPropertyCollection_t2852175726 * L_0 = (ConfigurationPropertyCollection_t2852175726 *)il2cpp_codegen_object_new(ConfigurationPropertyCollection_t2852175726_il2cpp_TypeInfo_var);
		ConfigurationPropertyCollection__ctor_m550516750(L_0, /*hidden argument*/NULL);
		((MexBindingBindingCollectionElement_2_t3597295982_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_properties_14(L_0);
		return;
	}
}
// System.Void System.ServiceModel.Configuration.MexBindingElement`1<System.Object>::.ctor(System.String)
extern "C"  void MexBindingElement_1__ctor_m3662592105_gshared (MexBindingElement_1_t1398412972 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		NullCheck((StandardBindingElement_t4182893664 *)__this);
		StandardBindingElement__ctor_m2558281579((StandardBindingElement_t4182893664 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Type System.ServiceModel.Configuration.MexBindingElement`1<System.Object>::get_BindingElementType()
extern "C"  Type_t * MexBindingElement_1_get_BindingElementType_m1396362435_gshared (MexBindingElement_1_t1398412972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MexBindingElement_1_get_BindingElementType_m1396362435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.ServiceModel.Configuration.MexBindingElement`1<System.Object>::OnApplyConfiguration(System.ServiceModel.Channels.Binding)
extern "C"  void MexBindingElement_1_OnApplyConfiguration_m3605806565_gshared (MexBindingElement_1_t1398412972 * __this, Binding_t859993683 * ___binding0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MexBindingElement_1_OnApplyConfiguration_m3605806565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.ServiceModel.Configuration.NamedServiceModelExtensionCollectionElement`1<System.Object>::.ctor()
extern "C"  void NamedServiceModelExtensionCollectionElement_1__ctor_m3719171797_gshared (NamedServiceModelExtensionCollectionElement_1_t274909112 * __this, const MethodInfo* method)
{
	{
		NullCheck((ServiceModelExtensionCollectionElement_1_t2544877501 *)__this);
		((  void (*) (ServiceModelExtensionCollectionElement_1_t2544877501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ServiceModelExtensionCollectionElement_1_t2544877501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.String System.ServiceModel.Configuration.NamedServiceModelExtensionCollectionElement`1<System.Object>::get_Name()
extern "C"  String_t* NamedServiceModelExtensionCollectionElement_1_get_Name_m3401074842_gshared (NamedServiceModelExtensionCollectionElement_1_t274909112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NamedServiceModelExtensionCollectionElement_1_get_Name_m3401074842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ConfigurationElement_t3318566633 *)__this);
		Il2CppObject * L_0 = ConfigurationElement_get_Item_m3494575926((ConfigurationElement_t3318566633 *)__this, (String_t*)_stringLiteral62725243, /*hidden argument*/NULL);
		return ((String_t*)Castclass(L_0, String_t_il2cpp_TypeInfo_var));
	}
}
// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.NamedServiceModelExtensionCollectionElement`1<System.Object>::get_Properties()
extern "C"  ConfigurationPropertyCollection_t2852175726 * NamedServiceModelExtensionCollectionElement_1_get_Properties_m1159763332_gshared (NamedServiceModelExtensionCollectionElement_1_t274909112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NamedServiceModelExtensionCollectionElement_1_get_Properties_m1159763332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ConfigurationPropertyCollection_t2852175726 * L_0 = (ConfigurationPropertyCollection_t2852175726 *)__this->get__properties_16();
		if (L_0)
		{
			goto IL_0048;
		}
	}
	{
		ConfigurationPropertyCollection_t2852175726 * L_1 = (ConfigurationPropertyCollection_t2852175726 *)il2cpp_codegen_object_new(ConfigurationPropertyCollection_t2852175726_il2cpp_TypeInfo_var);
		ConfigurationPropertyCollection__ctor_m550516750(L_1, /*hidden argument*/NULL);
		__this->set__properties_16(L_1);
		ConfigurationPropertyCollection_t2852175726 * L_2 = (ConfigurationPropertyCollection_t2852175726 *)__this->get__properties_16();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		StringConverter_t3216726494 * L_4 = (StringConverter_t3216726494 *)il2cpp_codegen_object_new(StringConverter_t3216726494_il2cpp_TypeInfo_var);
		StringConverter__ctor_m2070746565(L_4, /*hidden argument*/NULL);
		StringValidator_t4226114403 * L_5 = (StringValidator_t4226114403 *)il2cpp_codegen_object_new(StringValidator_t4226114403_il2cpp_TypeInfo_var);
		StringValidator__ctor_m315001218(L_5, (int32_t)1, (int32_t)((int32_t)2147483647LL), (String_t*)NULL, /*hidden argument*/NULL);
		ConfigurationProperty_t3590861854 * L_6 = (ConfigurationProperty_t3590861854 *)il2cpp_codegen_object_new(ConfigurationProperty_t3590861854_il2cpp_TypeInfo_var);
		ConfigurationProperty__ctor_m2796875110(L_6, (String_t*)_stringLiteral62725243, (Type_t *)L_3, (Il2CppObject *)NULL, (TypeConverter_t2249118273 *)L_4, (ConfigurationValidatorBase_t888490966 *)L_5, (int32_t)6, /*hidden argument*/NULL);
		NullCheck((ConfigurationPropertyCollection_t2852175726 *)L_2);
		ConfigurationPropertyCollection_Add_m2688614548((ConfigurationPropertyCollection_t2852175726 *)L_2, (ConfigurationProperty_t3590861854 *)L_6, /*hidden argument*/NULL);
	}

IL_0048:
	{
		ConfigurationPropertyCollection_t2852175726 * L_7 = (ConfigurationPropertyCollection_t2852175726 *)__this->get__properties_16();
		return L_7;
	}
}
// ConfigurationElementType System.ServiceModel.Configuration.ServiceModelConfigurationElementCollection`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ServiceModelConfigurationElementCollection_1_get_Item_m331088308_gshared (ServiceModelConfigurationElementCollection_1_t3718990380 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ConfigurationElementCollection_t446763386 *)__this);
		ConfigurationElement_t3318566633 * L_1 = ConfigurationElementCollection_BaseGet_m735310723((ConfigurationElementCollection_t446763386 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}
}
// ConfigurationElementType System.ServiceModel.Configuration.ServiceModelConfigurationElementCollection`1<System.Object>::get_Item(System.Object)
extern "C"  Il2CppObject * ServiceModelConfigurationElementCollection_1_get_Item_m350915595_gshared (ServiceModelConfigurationElementCollection_1_t3718990380 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ConfigurationElementCollection_t446763386 *)__this);
		ConfigurationElement_t3318566633 * L_1 = ConfigurationElementCollection_BaseGet_m1527703308((ConfigurationElementCollection_t446763386 *)__this, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}
}
// System.Configuration.ConfigurationElementCollectionType System.ServiceModel.Configuration.ServiceModelConfigurationElementCollection`1<System.Object>::get_CollectionType()
extern "C"  int32_t ServiceModelConfigurationElementCollection_1_get_CollectionType_m3553406301_gshared (ServiceModelConfigurationElementCollection_1_t3718990380 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.String System.ServiceModel.Configuration.ServiceModelConfigurationElementCollection`1<System.Object>::get_ElementName()
extern "C"  String_t* ServiceModelConfigurationElementCollection_1_get_ElementName_m829446488_gshared (ServiceModelConfigurationElementCollection_1_t3718990380 * __this, const MethodInfo* method)
{
	{
		NullCheck((ConfigurationElementCollection_t446763386 *)__this);
		String_t* L_0 = ConfigurationElementCollection_get_AddElementName_m3713402850((ConfigurationElementCollection_t446763386 *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelConfigurationElementCollection`1<System.Object>::Add(ConfigurationElementType)
extern "C"  void ServiceModelConfigurationElementCollection_1_Add_m565944273_gshared (ServiceModelConfigurationElementCollection_1_t3718990380 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___element0;
		NullCheck((ServiceModelConfigurationElementCollection_1_t3718990380 *)__this);
		VirtActionInvoker1< ConfigurationElement_t3318566633 * >::Invoke(32 /* System.Void System.ServiceModel.Configuration.ServiceModelConfigurationElementCollection`1<System.Object>::BaseAdd(System.Configuration.ConfigurationElement) */, (ServiceModelConfigurationElementCollection_1_t3718990380 *)__this, (ConfigurationElement_t3318566633 *)L_0);
		return;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelConfigurationElementCollection`1<System.Object>::BaseAdd(System.Configuration.ConfigurationElement)
extern "C"  void ServiceModelConfigurationElementCollection_1_BaseAdd_m1292508842_gshared (ServiceModelConfigurationElementCollection_1_t3718990380 * __this, ConfigurationElement_t3318566633 * ___element0, const MethodInfo* method)
{
	{
		ConfigurationElement_t3318566633 * L_0 = ___element0;
		NullCheck((ConfigurationElementCollection_t446763386 *)__this);
		ConfigurationElementCollection_BaseAdd_m2671588688((ConfigurationElementCollection_t446763386 *)__this, (ConfigurationElement_t3318566633 *)L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Configuration.ConfigurationElement System.ServiceModel.Configuration.ServiceModelConfigurationElementCollection`1<System.Object>::CreateNewElement()
extern "C"  ConfigurationElement_t3318566633 * ServiceModelConfigurationElementCollection_1_CreateNewElement_m274896085_gshared (ServiceModelConfigurationElementCollection_1_t3718990380 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceModelConfigurationElementCollection_1_CreateNewElement_m274896085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		Il2CppObject * L_1 = Activator_CreateInstance_m94526014(NULL /*static, unused*/, (Type_t *)L_0, (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return ((ConfigurationElement_t3318566633 *)Castclass(L_1, ConfigurationElement_t3318566633_il2cpp_TypeInfo_var));
	}
}
// System.Boolean System.ServiceModel.Configuration.ServiceModelEnhancedConfigurationElementCollection`1<System.Object>::get_ThrowOnDuplicate()
extern "C"  bool ServiceModelEnhancedConfigurationElementCollection_1_get_ThrowOnDuplicate_m2112179668_gshared (ServiceModelEnhancedConfigurationElementCollection_1_t1951097806 * __this, const MethodInfo* method)
{
	{
		NullCheck((ConfigurationElementCollection_t446763386 *)__this);
		bool L_0 = ConfigurationElementCollection_get_ThrowOnDuplicate_m3210315871((ConfigurationElementCollection_t446763386 *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelEnhancedConfigurationElementCollection`1<System.Object>::BaseAdd(System.Configuration.ConfigurationElement)
extern "C"  void ServiceModelEnhancedConfigurationElementCollection_1_BaseAdd_m3375331053_gshared (ServiceModelEnhancedConfigurationElementCollection_1_t1951097806 * __this, ConfigurationElement_t3318566633 * ___element0, const MethodInfo* method)
{
	{
		ConfigurationElement_t3318566633 * L_0 = ___element0;
		NullCheck((ServiceModelConfigurationElementCollection_1_t3718990380 *)__this);
		((  void (*) (ServiceModelConfigurationElementCollection_1_t3718990380 *, ConfigurationElement_t3318566633 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ServiceModelConfigurationElementCollection_1_t3718990380 *)__this, (ConfigurationElement_t3318566633 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator5__ctor_m3574236268_gshared (U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TServiceModelExtensionElement System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5<System.Object>::System.Collections.Generic.IEnumerator<TServiceModelExtensionElement>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CTServiceModelExtensionElementU3E_get_Current_m2945656077_gshared (U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m684011668_gshared (U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator5_MoveNext_m3010306826_gshared (U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0050;
			}
		}
	}
	{
		goto IL_007b;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_005e;
	}

IL_002d:
	{
		ServiceModelExtensionCollectionElement_1_t2544877501 * L_2 = (ServiceModelExtensionCollectionElement_1_t2544877501 *)__this->get_U3CU3Ef__this_3();
		int32_t L_3 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck((ServiceModelExtensionCollectionElement_1_t2544877501 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (ServiceModelExtensionCollectionElement_1_t2544877501 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ServiceModelExtensionCollectionElement_1_t2544877501 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_U24current_2(L_4);
		__this->set_U24PC_1(1);
		goto IL_007d;
	}

IL_0050:
	{
		int32_t L_5 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_5+(int32_t)1)));
	}

IL_005e:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		ServiceModelExtensionCollectionElement_1_t2544877501 * L_7 = (ServiceModelExtensionCollectionElement_1_t2544877501 *)__this->get_U3CU3Ef__this_3();
		NullCheck((ServiceModelExtensionCollectionElement_1_t2544877501 *)L_7);
		int32_t L_8 = ((  int32_t (*) (ServiceModelExtensionCollectionElement_1_t2544877501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ServiceModelExtensionCollectionElement_1_t2544877501 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_007b:
	{
		return (bool)0;
	}

IL_007d:
	{
		return (bool)1;
	}
	// Dead block : IL_007f: ldloc.1
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator5_Dispose_m2539969228_gshared (U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1/<GetEnumerator>c__Iterator5<System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator5_Reset_m100070940_gshared (U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator5_Reset_m100070940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::.ctor()
extern "C"  void ServiceModelExtensionCollectionElement_1__ctor_m3284810400_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, const MethodInfo* method)
{
	{
		KeyedByTypeCollection_1_t1429017627 * L_0 = (KeyedByTypeCollection_1_t1429017627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (KeyedByTypeCollection_1_t1429017627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__list_14(L_0);
		NullCheck((ConfigurationElement_t3318566633 *)__this);
		ConfigurationElement__ctor_m4116197893((ConfigurationElement_t3318566633 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ServiceModelExtensionCollectionElement_1_System_Collections_IEnumerable_GetEnumerator_m2231242183_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, const MethodInfo* method)
{
	{
		NullCheck((ServiceModelExtensionCollectionElement_1_t2544877501 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ServiceModelExtensionCollectionElement_1_t2544877501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ServiceModelExtensionCollectionElement_1_t2544877501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_0;
	}
}
// System.Boolean System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::System.Collections.Generic.ICollection<TServiceModelExtensionElement>.get_IsReadOnly()
extern "C"  bool ServiceModelExtensionCollectionElement_1_System_Collections_Generic_ICollectionU3CTServiceModelExtensionElementU3E_get_IsReadOnly_m2146300109_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceModelExtensionCollectionElement_1_System_Collections_Generic_ICollectionU3CTServiceModelExtensionElementU3E_get_IsReadOnly_m2146300109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::Add(TServiceModelExtensionElement)
extern "C"  void ServiceModelExtensionCollectionElement_1_Add_m75714236_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	{
		__this->set_is_modified_15((bool)1);
		KeyedByTypeCollection_1_t1429017627 * L_0 = (KeyedByTypeCollection_1_t1429017627 *)__this->get__list_14();
		Il2CppObject * L_1 = ___element0;
		NullCheck((Collection_1_t2024462082 *)L_0);
		((  void (*) (Collection_1_t2024462082 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Collection_1_t2024462082 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::Clear()
extern "C"  void ServiceModelExtensionCollectionElement_1_Clear_m1971825461_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, const MethodInfo* method)
{
	{
		__this->set_is_modified_15((bool)1);
		KeyedByTypeCollection_1_t1429017627 * L_0 = (KeyedByTypeCollection_1_t1429017627 *)__this->get__list_14();
		NullCheck((Collection_1_t2024462082 *)L_0);
		((  void (*) (Collection_1_t2024462082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Collection_1_t2024462082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::Contains(TServiceModelExtensionElement)
extern "C"  bool ServiceModelExtensionCollectionElement_1_Contains_m3343637864_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	{
		KeyedByTypeCollection_1_t1429017627 * L_0 = (KeyedByTypeCollection_1_t1429017627 *)__this->get__list_14();
		Il2CppObject * L_1 = ___element0;
		NullCheck((Collection_1_t2024462082 *)L_0);
		bool L_2 = ((  bool (*) (Collection_1_t2024462082 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Collection_1_t2024462082 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_2;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::CopyTo(TServiceModelExtensionElement[],System.Int32)
extern "C"  void ServiceModelExtensionCollectionElement_1_CopyTo_m2440639783_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, ObjectU5BU5D_t2843939325* ___elements0, int32_t ___start1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceModelExtensionCollectionElement_1_CopyTo_m2440639783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TServiceModelExtensionElement> System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ServiceModelExtensionCollectionElement_1_GetEnumerator_m3417855500_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * L_0 = (U3CGetEnumeratorU3Ec__Iterator5_t1583623670 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator5_t1583623670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator5_t1583623670 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator5_t1583623670 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::Remove(TServiceModelExtensionElement)
extern "C"  bool ServiceModelExtensionCollectionElement_1_Remove_m2806127867_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	{
		__this->set_is_modified_15((bool)1);
		KeyedByTypeCollection_1_t1429017627 * L_0 = (KeyedByTypeCollection_1_t1429017627 *)__this->get__list_14();
		NullCheck((Il2CppObject *)(*(&___element0)));
		Type_t * L_1 = Object_GetType_m88164663((Il2CppObject *)(*(&___element0)), /*hidden argument*/NULL);
		NullCheck((KeyedCollection_2_t368913236 *)L_0);
		bool L_2 = ((  bool (*) (KeyedCollection_2_t368913236 *, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((KeyedCollection_2_t368913236 *)L_0, (Type_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// TServiceModelExtensionElement System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ServiceModelExtensionCollectionElement_1_get_Item_m681100612_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		KeyedByTypeCollection_1_t1429017627 * L_0 = (KeyedByTypeCollection_1_t1429017627 *)__this->get__list_14();
		int32_t L_1 = ___index0;
		NullCheck((Collection_1_t2024462082 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Collection_1_t2024462082 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Collection_1_t2024462082 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_2;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::DeserializeElement(System.Xml.XmlReader,System.Boolean)
extern "C"  void ServiceModelExtensionCollectionElement_1_DeserializeElement_m2256413031_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, XmlReader_t3121518892 * ___reader0, bool ___serializeCollectionKey1, const MethodInfo* method)
{
	{
		XmlReader_t3121518892 * L_0 = ___reader0;
		bool L_1 = ___serializeCollectionKey1;
		NullCheck((ConfigurationElement_t3318566633 *)__this);
		ConfigurationElement_DeserializeElement_m2357743528((ConfigurationElement_t3318566633 *)__this, (XmlReader_t3121518892 *)L_0, (bool)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::OnDeserializeUnrecognizedElement(System.String,System.Xml.XmlReader)
extern "C"  bool ServiceModelExtensionCollectionElement_1_OnDeserializeUnrecognizedElement_m109773677_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, String_t* ___elementName0, XmlReader_t3121518892 * ___reader1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___elementName0;
		XmlReader_t3121518892 * L_1 = ___reader1;
		NullCheck((ServiceModelExtensionCollectionElement_1_t2544877501 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, String_t*, XmlReader_t3121518892 * >::Invoke(34 /* TServiceModelExtensionElement System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::DeserializeExtensionElement(System.String,System.Xml.XmlReader) */, (ServiceModelExtensionCollectionElement_1_t2544877501 *)__this, (String_t*)L_0, (XmlReader_t3121518892 *)L_1);
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_0;
		if (L_3)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)0;
	}

IL_0016:
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((ServiceModelExtensionCollectionElement_1_t2544877501 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(33 /* System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::Add(TServiceModelExtensionElement) */, (ServiceModelExtensionCollectionElement_1_t2544877501 *)__this, (Il2CppObject *)L_4);
		return (bool)1;
	}
}
// TServiceModelExtensionElement System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::DeserializeExtensionElement(System.String,System.Xml.XmlReader)
extern "C"  Il2CppObject * ServiceModelExtensionCollectionElement_1_DeserializeExtensionElement_m3514873055_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, String_t* ___elementName0, XmlReader_t3121518892 * ___reader1, const MethodInfo* method)
{
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8)));
	}
}
// System.Int32 System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::get_Count()
extern "C"  int32_t ServiceModelExtensionCollectionElement_1_get_Count_m544038530_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, const MethodInfo* method)
{
	{
		KeyedByTypeCollection_1_t1429017627 * L_0 = (KeyedByTypeCollection_1_t1429017627 *)__this->get__list_14();
		NullCheck((Collection_1_t2024462082 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Collection_1_t2024462082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Collection_1_t2024462082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_1;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::Reset(System.Configuration.ConfigurationElement)
extern "C"  void ServiceModelExtensionCollectionElement_1_Reset_m916777949_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, ConfigurationElement_t3318566633 * ___parentElement0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceModelExtensionCollectionElement_1_Reset_m916777949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ServiceModelExtensionCollectionElement_1_t2544877501 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ConfigurationElement_t3318566633 * L_0 = ___parentElement0;
		NullCheck((ConfigurationElement_t3318566633 *)__this);
		ConfigurationElement_Reset_m3089641269((ConfigurationElement_t3318566633 *)__this, (ConfigurationElement_t3318566633 *)L_0, /*hidden argument*/NULL);
		ConfigurationElement_t3318566633 * L_1 = ___parentElement0;
		V_0 = (ServiceModelExtensionCollectionElement_1_t2544877501 *)((ServiceModelExtensionCollectionElement_1_t2544877501 *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14)));
		ServiceModelExtensionCollectionElement_1_t2544877501 * L_2 = V_0;
		NullCheck((ServiceModelExtensionCollectionElement_1_t2544877501 *)L_2);
		Il2CppObject* L_3 = ((  Il2CppObject* (*) (ServiceModelExtensionCollectionElement_1_t2544877501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ServiceModelExtensionCollectionElement_1_t2544877501 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_2 = (Il2CppObject*)L_3;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0028;
		}

IL_001a:
		{
			Il2CppObject* L_4 = V_2;
			NullCheck((Il2CppObject*)L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 15), (Il2CppObject*)L_4);
			V_1 = (Il2CppObject *)L_5;
			Il2CppObject * L_6 = V_1;
			NullCheck((ServiceModelExtensionCollectionElement_1_t2544877501 *)__this);
			VirtActionInvoker1< Il2CppObject * >::Invoke(33 /* System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::Add(TServiceModelExtensionElement) */, (ServiceModelExtensionCollectionElement_1_t2544877501 *)__this, (Il2CppObject *)L_6);
		}

IL_0028:
		{
			Il2CppObject* L_7 = V_2;
			NullCheck((Il2CppObject *)L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, (Il2CppObject *)L_7);
			if (L_8)
			{
				goto IL_001a;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_9 = V_2;
			if (L_9)
			{
				goto IL_003c;
			}
		}

IL_003b:
		{
			IL2CPP_END_FINALLY(56)
		}

IL_003c:
		{
			Il2CppObject* L_10 = V_2;
			NullCheck((Il2CppObject *)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			IL2CPP_END_FINALLY(56)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Boolean System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::IsModified()
extern "C"  bool ServiceModelExtensionCollectionElement_1_IsModified_m3695778831_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_is_modified_15();
		return L_0;
	}
}
// System.Void System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::ResetModified()
extern "C"  void ServiceModelExtensionCollectionElement_1_ResetModified_m3521946628_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, const MethodInfo* method)
{
	{
		__this->set_is_modified_15((bool)0);
		return;
	}
}
// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.ServiceModelExtensionCollectionElement`1<System.Object>::get_Properties()
extern "C"  ConfigurationPropertyCollection_t2852175726 * ServiceModelExtensionCollectionElement_1_get_Properties_m1556742636_gshared (ServiceModelExtensionCollectionElement_1_t2544877501 * __this, const MethodInfo* method)
{
	{
		ConfigurationPropertyCollection_t2852175726 * L_0 = (ConfigurationPropertyCollection_t2852175726 *)__this->get_properties_13();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		NullCheck((ConfigurationElement_t3318566633 *)__this);
		ConfigurationPropertyCollection_t2852175726 * L_1 = ConfigurationElement_get_Properties_m976126252((ConfigurationElement_t3318566633 *)__this, /*hidden argument*/NULL);
		__this->set_properties_13(L_1);
	}

IL_0017:
	{
		ConfigurationPropertyCollection_t2852175726 * L_2 = (ConfigurationPropertyCollection_t2852175726 *)__this->get_properties_13();
		return L_2;
	}
}
// System.ServiceModel.Configuration.StandardBindingElementCollection`1<TBindingConfiguration> System.ServiceModel.Configuration.StandardBindingCollectionElement`2<System.Object,System.Object>::get_Bindings()
extern "C"  StandardBindingElementCollection_1_t741974772 * StandardBindingCollectionElement_2_get_Bindings_m4261975795_gshared (StandardBindingCollectionElement_2_t3123063704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StandardBindingCollectionElement_2_get_Bindings_m4261975795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck((ConfigurationElement_t3318566633 *)__this);
		Il2CppObject * L_1 = ConfigurationElement_get_Item_m3494575926((ConfigurationElement_t3318566633 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return ((StandardBindingElementCollection_1_t741974772 *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.ServiceModel.Configuration.IBindingConfigurationElement> System.ServiceModel.Configuration.StandardBindingCollectionElement`2<System.Object,System.Object>::get_ConfiguredBindings()
extern "C"  ReadOnlyCollection_1_t580582265 * StandardBindingCollectionElement_2_get_ConfiguredBindings_m183988966_gshared (StandardBindingCollectionElement_2_t3123063704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StandardBindingCollectionElement_2_get_ConfiguredBindings_m183988966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t840080720 * V_0 = NULL;
	StandardBindingElementCollection_1_t741974772 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		List_1_t840080720 * L_0 = (List_1_t840080720 *)il2cpp_codegen_object_new(List_1_t840080720_il2cpp_TypeInfo_var);
		List_1__ctor_m1663932146(L_0, /*hidden argument*/List_1__ctor_m1663932146_MethodInfo_var);
		V_0 = (List_1_t840080720 *)L_0;
		NullCheck((StandardBindingCollectionElement_2_t3123063704 *)__this);
		StandardBindingElementCollection_1_t741974772 * L_1 = ((  StandardBindingElementCollection_1_t741974772 * (*) (StandardBindingCollectionElement_2_t3123063704 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((StandardBindingCollectionElement_2_t3123063704 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_1 = (StandardBindingElementCollection_1_t741974772 *)L_1;
		V_2 = (int32_t)0;
		goto IL_002a;
	}

IL_0014:
	{
		List_1_t840080720 * L_2 = V_0;
		StandardBindingElementCollection_1_t741974772 * L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck((ServiceModelConfigurationElementCollection_1_t3718990380 *)L_3);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (ServiceModelConfigurationElementCollection_1_t3718990380 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ServiceModelConfigurationElementCollection_1_t3718990380 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((List_1_t840080720 *)L_2);
		List_1_Add_m1539394036((List_1_t840080720 *)L_2, (Il2CppObject *)L_5, /*hidden argument*/List_1_Add_m1539394036_MethodInfo_var);
		int32_t L_6 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_7 = V_2;
		StandardBindingElementCollection_1_t741974772 * L_8 = V_1;
		NullCheck((ConfigurationElementCollection_t446763386 *)L_8);
		int32_t L_9 = ConfigurationElementCollection_get_Count_m873586478((ConfigurationElementCollection_t446763386 *)L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		List_1_t840080720 * L_10 = V_0;
		ReadOnlyCollection_1_t580582265 * L_11 = (ReadOnlyCollection_1_t580582265 *)il2cpp_codegen_object_new(ReadOnlyCollection_1_t580582265_il2cpp_TypeInfo_var);
		ReadOnlyCollection_1__ctor_m1920932843(L_11, (Il2CppObject*)L_10, /*hidden argument*/ReadOnlyCollection_1__ctor_m1920932843_MethodInfo_var);
		return L_11;
	}
}
// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.StandardBindingCollectionElement`2<System.Object,System.Object>::get_Properties()
extern "C"  ConfigurationPropertyCollection_t2852175726 * StandardBindingCollectionElement_2_get_Properties_m4110740973_gshared (StandardBindingCollectionElement_2_t3123063704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StandardBindingCollectionElement_2_get_Properties_m4110740973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ConfigurationPropertyCollection_t2852175726 * L_0 = (ConfigurationPropertyCollection_t2852175726 *)__this->get__properties_13();
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		ConfigurationPropertyCollection_t2852175726 * L_1 = (ConfigurationPropertyCollection_t2852175726 *)il2cpp_codegen_object_new(ConfigurationPropertyCollection_t2852175726_il2cpp_TypeInfo_var);
		ConfigurationPropertyCollection__ctor_m550516750(L_1, /*hidden argument*/NULL);
		__this->set__properties_13(L_1);
		ConfigurationPropertyCollection_t2852175726 * L_2 = (ConfigurationPropertyCollection_t2852175726 *)__this->get__properties_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 4)), /*hidden argument*/NULL);
		ConfigurationProperty_t3590861854 * L_5 = (ConfigurationProperty_t3590861854 *)il2cpp_codegen_object_new(ConfigurationProperty_t3590861854_il2cpp_TypeInfo_var);
		ConfigurationProperty__ctor_m2796875110(L_5, (String_t*)L_3, (Type_t *)L_4, (Il2CppObject *)NULL, (TypeConverter_t2249118273 *)NULL, (ConfigurationValidatorBase_t888490966 *)NULL, (int32_t)1, /*hidden argument*/NULL);
		NullCheck((ConfigurationPropertyCollection_t2852175726 *)L_2);
		ConfigurationPropertyCollection_Add_m2688614548((ConfigurationPropertyCollection_t2852175726 *)L_2, (ConfigurationProperty_t3590861854 *)L_5, /*hidden argument*/NULL);
	}

IL_0039:
	{
		ConfigurationPropertyCollection_t2852175726 * L_6 = (ConfigurationPropertyCollection_t2852175726 *)__this->get__properties_13();
		return L_6;
	}
}
// System.Type System.ServiceModel.Configuration.StandardBindingCollectionElement`2<System.Object,System.Object>::get_BindingType()
extern "C"  Type_t * StandardBindingCollectionElement_2_get_BindingType_m22019752_gshared (StandardBindingCollectionElement_2_t3123063704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StandardBindingCollectionElement_2_get_BindingType_m22019752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 5)), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.ServiceModel.Configuration.StandardBindingElementCollection`1<System.Object>::.cctor()
extern "C"  void StandardBindingElementCollection_1__cctor_m3794528160_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StandardBindingElementCollection_1__cctor_m3794528160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ConfigurationPropertyCollection_t2852175726 * L_0 = (ConfigurationPropertyCollection_t2852175726 *)il2cpp_codegen_object_new(ConfigurationPropertyCollection_t2852175726_il2cpp_TypeInfo_var);
		ConfigurationPropertyCollection__ctor_m550516750(L_0, /*hidden argument*/NULL);
		((StandardBindingElementCollection_1_t741974772_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_properties_23(L_0);
		return;
	}
}
// System.Object System.ServiceModel.Configuration.StandardBindingElementCollection`1<System.Object>::GetElementKey(System.Configuration.ConfigurationElement)
extern "C"  Il2CppObject * StandardBindingElementCollection_1_GetElementKey_m3923440670_gshared (StandardBindingElementCollection_1_t741974772 * __this, ConfigurationElement_t3318566633 * ___element0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StandardBindingElementCollection_1_GetElementKey_m3923440670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ConfigurationElement_t3318566633 * L_0 = ___element0;
		NullCheck((StandardBindingElement_t4182893664 *)((StandardBindingElement_t4182893664 *)Castclass(L_0, StandardBindingElement_t4182893664_il2cpp_TypeInfo_var)));
		String_t* L_1 = StandardBindingElement_get_Name_m2797479792((StandardBindingElement_t4182893664 *)((StandardBindingElement_t4182893664 *)Castclass(L_0, StandardBindingElement_t4182893664_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.ServiceModel.Dispatcher.ListenerLoopManager/<CreateAcceptor>c__AnonStorey1C`1<System.Object>::.ctor()
extern "C"  void U3CCreateAcceptorU3Ec__AnonStorey1C_1__ctor_m3228046448_gshared (U3CCreateAcceptorU3Ec__AnonStorey1C_1_t4103390264 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.ServiceModel.Dispatcher.ListenerLoopManager/<CreateAcceptor>c__AnonStorey1C`1<System.Object>::<>m__1F(System.IAsyncResult)
extern "C"  void U3CCreateAcceptorU3Ec__AnonStorey1C_1_U3CU3Em__1F_m805831763_gshared (U3CCreateAcceptorU3Ec__AnonStorey1C_1_t4103390264 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateAcceptorU3Ec__AnonStorey1C_1_U3CU3Em__1F_m805831763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1436737249 * V_0 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		ListenerLoopManager_t4138186672 * L_0 = (ListenerLoopManager_t4138186672 *)__this->get_U3CU3Ef__this_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_r_0();
		Il2CppObject * L_2 = ___result0;
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(3 /* TChannel System.ServiceModel.Channels.IChannelListener`1<System.Object>::EndAcceptChannel(System.IAsyncResult) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		NullCheck((ListenerLoopManager_t4138186672 *)L_0);
		ListenerLoopManager_ChannelAccepted_m848092585((ListenerLoopManager_t4138186672 *)L_0, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		goto IL_0048;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0021;
		throw e;
	}

CATCH_0021:
	{ // begin catch(System.Exception)
		V_0 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t3208230065_il2cpp_TypeInfo_var);
		Console_WriteLine_m4182570127(NULL /*static, unused*/, (String_t*)_stringLiteral981445416, /*hidden argument*/NULL);
		Exception_t1436737249 * L_4 = V_0;
		Console_WriteLine_m68308674(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		ListenerLoopManager_t4138186672 * L_5 = (ListenerLoopManager_t4138186672 *)__this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		AutoResetEvent_t1333520283 * L_6 = (AutoResetEvent_t1333520283 *)L_5->get_creator_handle_2();
		NullCheck((EventWaitHandle_t777845177 *)L_6);
		EventWaitHandle_Set_m2445193251((EventWaitHandle_t777845177 *)L_6, /*hidden argument*/NULL);
		goto IL_0048;
	} // end catch (depth: 1)

IL_0048:
	{
		return;
	}
}
// System.IAsyncResult System.ServiceModel.Dispatcher.ListenerLoopManager/<CreateAcceptor>c__AnonStorey1C`1<System.Object>::<>m__20()
extern "C"  Il2CppObject * U3CCreateAcceptorU3Ec__AnonStorey1C_1_U3CU3Em__20_m3819685881_gshared (U3CCreateAcceptorU3Ec__AnonStorey1C_1_t4103390264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateAcceptorU3Ec__AnonStorey1C_1_U3CU3Em__20_m3819685881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1436737249 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_0 = (Il2CppObject*)__this->get_r_0();
			AsyncCallback_t3962456242 * L_1 = (AsyncCallback_t3962456242 *)__this->get_callback_1();
			NullCheck((Il2CppObject*)L_0);
			Il2CppObject * L_2 = InterfaceFuncInvoker2< Il2CppObject *, AsyncCallback_t3962456242 *, Il2CppObject * >::Invoke(1 /* System.IAsyncResult System.ServiceModel.Channels.IChannelListener`1<System.Object>::BeginAcceptChannel(System.AsyncCallback,System.Object) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_0, (AsyncCallback_t3962456242 *)L_1, (Il2CppObject *)NULL);
			V_1 = (Il2CppObject *)L_2;
			goto IL_0035;
		}

IL_0018:
		{
			; // IL_0018: leave IL_0035
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_001d:
	{ // begin catch(System.Exception)
		{
			V_0 = (Exception_t1436737249 *)((Exception_t1436737249 *)__exception_local);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t3208230065_il2cpp_TypeInfo_var);
			Console_WriteLine_m4182570127(NULL /*static, unused*/, (String_t*)_stringLiteral3945668362, /*hidden argument*/NULL);
			Exception_t1436737249 * L_3 = V_0;
			Console_WriteLine_m68308674(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_0030:
		{
			goto IL_0035;
		}
	} // end catch (depth: 1)

IL_0035:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// System.Void System.ServiceModel.DuplexChannelFactory`1<System.Object>::.ctor(System.Object,System.ServiceModel.Channels.Binding)
extern "C"  void DuplexChannelFactory_1__ctor_m4245981057_gshared (DuplexChannelFactory_1_t1618788632 * __this, Il2CppObject * ___callbackInstance0, Binding_t859993683 * ___binding1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DuplexChannelFactory_1__ctor_m4245981057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___callbackInstance0;
		InstanceContext_t3593205954 * L_1 = (InstanceContext_t3593205954 *)il2cpp_codegen_object_new(InstanceContext_t3593205954_il2cpp_TypeInfo_var);
		InstanceContext__ctor_m1179855330(L_1, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Binding_t859993683 * L_2 = ___binding1;
		NullCheck((DuplexChannelFactory_1_t1618788632 *)__this);
		((  void (*) (DuplexChannelFactory_1_t1618788632 *, InstanceContext_t3593205954 *, Binding_t859993683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((DuplexChannelFactory_1_t1618788632 *)__this, (InstanceContext_t3593205954 *)L_1, (Binding_t859993683 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.ServiceModel.DuplexChannelFactory`1<System.Object>::.ctor(System.ServiceModel.InstanceContext,System.ServiceModel.Channels.Binding)
extern "C"  void DuplexChannelFactory_1__ctor_m3140772316_gshared (DuplexChannelFactory_1_t1618788632 * __this, InstanceContext_t3593205954 * ___callbackInstance0, Binding_t859993683 * ___binding1, const MethodInfo* method)
{
	{
		Binding_t859993683 * L_0 = ___binding1;
		NullCheck((ChannelFactory_1_t585335377 *)__this);
		((  void (*) (ChannelFactory_1_t585335377 *, Binding_t859993683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ChannelFactory_1_t585335377 *)__this, (Binding_t859993683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		InstanceContext_t3593205954 * L_1 = ___callbackInstance0;
		__this->set_callback_instance_12(L_1);
		return;
	}
}
// TChannel System.ServiceModel.DuplexChannelFactory`1<System.Object>::CreateChannel(System.ServiceModel.EndpointAddress,System.Uri)
extern "C"  Il2CppObject * DuplexChannelFactory_1_CreateChannel_m270227884_gshared (DuplexChannelFactory_1_t1618788632 * __this, EndpointAddress_t3119842923 * ___address0, Uri_t100236324 * ___via1, const MethodInfo* method)
{
	{
		InstanceContext_t3593205954 * L_0 = (InstanceContext_t3593205954 *)__this->get_callback_instance_12();
		EndpointAddress_t3119842923 * L_1 = ___address0;
		Uri_t100236324 * L_2 = ___via1;
		NullCheck((DuplexChannelFactory_1_t1618788632 *)__this);
		Il2CppObject * L_3 = VirtFuncInvoker3< Il2CppObject *, InstanceContext_t3593205954 *, EndpointAddress_t3119842923 *, Uri_t100236324 * >::Invoke(32 /* TChannel System.ServiceModel.DuplexChannelFactory`1<System.Object>::CreateChannel(System.ServiceModel.InstanceContext,System.ServiceModel.EndpointAddress,System.Uri) */, (DuplexChannelFactory_1_t1618788632 *)__this, (InstanceContext_t3593205954 *)L_0, (EndpointAddress_t3119842923 *)L_1, (Uri_t100236324 *)L_2);
		return L_3;
	}
}
// TChannel System.ServiceModel.DuplexChannelFactory`1<System.Object>::CreateChannel(System.ServiceModel.InstanceContext,System.ServiceModel.EndpointAddress,System.Uri)
extern "C"  Il2CppObject * DuplexChannelFactory_1_CreateChannel_m1751811269_gshared (DuplexChannelFactory_1_t1618788632 * __this, InstanceContext_t3593205954 * ___callbackInstance0, EndpointAddress_t3119842923 * ___address1, Uri_t100236324 * ___via2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DuplexChannelFactory_1_CreateChannel_m1751811269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		InstanceContext_t3593205954 * L_0 = ___callbackInstance0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral4031800109, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		NullCheck((ChannelFactory_t628250694 *)__this);
		ChannelFactory_EnsureOpened_m4135578152((ChannelFactory_t628250694 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck((ChannelFactory_t628250694 *)__this);
		ServiceEndpoint_t4038493094 * L_3 = ChannelFactory_get_Endpoint_m777043750((ChannelFactory_t628250694 *)__this, /*hidden argument*/NULL);
		NullCheck((ServiceEndpoint_t4038493094 *)L_3);
		ContractDescription_t1411178514 * L_4 = ServiceEndpoint_get_Contract_m872865802((ServiceEndpoint_t4038493094 *)L_3, /*hidden argument*/NULL);
		Type_t * L_5 = ClientProxyGenerator_CreateProxyType_m1012746588(NULL /*static, unused*/, (Type_t *)L_2, (ContractDescription_t1411178514 *)L_4, (bool)1, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		ObjectU5BU5D_t2843939325* L_7 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck((ChannelFactory_t628250694 *)__this);
		ServiceEndpoint_t4038493094 * L_8 = ChannelFactory_get_Endpoint_m777043750((ChannelFactory_t628250694 *)__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_8);
		ObjectU5BU5D_t2843939325* L_9 = (ObjectU5BU5D_t2843939325*)L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, __this);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)__this);
		ObjectU5BU5D_t2843939325* L_10 = (ObjectU5BU5D_t2843939325*)L_9;
		EndpointAddress_t3119842923 * L_11 = ___address1;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t2843939325* L_12 = (ObjectU5BU5D_t2843939325*)L_10;
		Uri_t100236324 * L_13 = ___via2;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_13);
		Il2CppObject * L_14 = Activator_CreateInstance_m94526014(NULL /*static, unused*/, (Type_t *)L_6, (ObjectU5BU5D_t2843939325*)L_12, /*hidden argument*/NULL);
		V_1 = (Il2CppObject *)L_14;
		Il2CppObject * L_15 = V_1;
		InstanceContext_t3593205954 * L_16 = ___callbackInstance0;
		NullCheck((DuplexClientRuntimeChannel_t737641411 *)((DuplexClientRuntimeChannel_t737641411 *)Castclass(L_15, DuplexClientRuntimeChannel_t737641411_il2cpp_TypeInfo_var)));
		DuplexClientRuntimeChannel_set_CallbackInstance_m2388021322((DuplexClientRuntimeChannel_t737641411 *)((DuplexClientRuntimeChannel_t737641411 *)Castclass(L_15, DuplexClientRuntimeChannel_t737641411_il2cpp_TypeInfo_var)), (InstanceContext_t3593205954 *)L_16, /*hidden argument*/NULL);
		Il2CppObject * L_17 = V_1;
		return ((Il2CppObject *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5)));
	}
}
// System.Void System.ServiceModel.ExtensionCollection`1<System.Object>::.ctor(T)
extern "C"  void ExtensionCollection_1__ctor_m4156153040_gshared (ExtensionCollection_1_t1857785261 * __this, Il2CppObject * ___owner0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtensionCollection_1__ctor_m4156153040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___owner0;
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_1, /*hidden argument*/NULL);
		NullCheck((ExtensionCollection_1_t1857785261 *)__this);
		((  void (*) (ExtensionCollection_1_t1857785261 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ExtensionCollection_1_t1857785261 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void System.ServiceModel.ExtensionCollection`1<System.Object>::.ctor(T,System.Object)
extern "C"  void ExtensionCollection_1__ctor_m3566873_gshared (ExtensionCollection_1_t1857785261 * __this, Il2CppObject * ___owner0, Il2CppObject * ___syncRoot1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___syncRoot1;
		NullCheck((SynchronizedCollection_1_t3223601106 *)__this);
		((  void (*) (SynchronizedCollection_1_t3223601106 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((SynchronizedCollection_1_t3223601106 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject * L_1 = ___owner0;
		__this->set_owner_2(L_1);
		return;
	}
}
// System.Boolean System.ServiceModel.ExtensionCollection`1<System.Object>::System.Collections.Generic.ICollection<System.ServiceModel.IExtension<T>>.get_IsReadOnly()
extern "C"  bool ExtensionCollection_1_System_Collections_Generic_ICollectionU3CSystem_ServiceModel_IExtensionU3CTU3EU3E_get_IsReadOnly_m3511636996_gshared (ExtensionCollection_1_t1857785261 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.ServiceModel.ExtensionCollection`1<System.Object>::ClearItems()
extern "C"  void ExtensionCollection_1_ClearItems_m4120465731_gshared (ExtensionCollection_1_t1857785261 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_001d;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		NullCheck((SynchronizedCollection_1_t3223601106 *)__this);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (SynchronizedCollection_1_t3223601106 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SynchronizedCollection_1_t3223601106 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_owner_2();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void System.ServiceModel.IExtension`1<System.Object>::Detach(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		int32_t L_3 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_4 = V_0;
		NullCheck((SynchronizedCollection_1_t3223601106 *)__this);
		int32_t L_5 = ((  int32_t (*) (SynchronizedCollection_1_t3223601106 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((SynchronizedCollection_1_t3223601106 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0007;
		}
	}
	{
		NullCheck((SynchronizedCollection_1_t3223601106 *)__this);
		((  void (*) (SynchronizedCollection_1_t3223601106 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((SynchronizedCollection_1_t3223601106 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return;
	}
}
// System.Void System.ServiceModel.ExtensionCollection`1<System.Object>::InsertItem(System.Int32,System.ServiceModel.IExtension`1<T>)
extern "C"  void ExtensionCollection_1_InsertItem_m1088008468_gshared (ExtensionCollection_1_t1857785261 * __this, int32_t ___index0, Il2CppObject* ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___item1;
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_owner_2();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.ServiceModel.IExtension`1<System.Object>::Attach(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		int32_t L_2 = ___index0;
		Il2CppObject* L_3 = ___item1;
		NullCheck((SynchronizedCollection_1_t3223601106 *)__this);
		((  void (*) (SynchronizedCollection_1_t3223601106 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((SynchronizedCollection_1_t3223601106 *)__this, (int32_t)L_2, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.ServiceModel.ExtensionCollection`1<System.Object>::RemoveItem(System.Int32)
extern "C"  void ExtensionCollection_1_RemoveItem_m4181562057_gshared (ExtensionCollection_1_t1857785261 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((SynchronizedCollection_1_t3223601106 *)__this);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (SynchronizedCollection_1_t3223601106 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SynchronizedCollection_1_t3223601106 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_owner_2();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void System.ServiceModel.IExtension`1<System.Object>::Detach(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		int32_t L_3 = ___index0;
		NullCheck((SynchronizedCollection_1_t3223601106 *)__this);
		((  void (*) (SynchronizedCollection_1_t3223601106 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((SynchronizedCollection_1_t3223601106 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Void System.ServiceModel.ExtensionCollection`1<System.Object>::SetItem(System.Int32,System.ServiceModel.IExtension`1<T>)
extern "C"  void ExtensionCollection_1_SetItem_m1955718617_gshared (ExtensionCollection_1_t1857785261 * __this, int32_t ___index0, Il2CppObject* ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((SynchronizedCollection_1_t3223601106 *)__this);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (SynchronizedCollection_1_t3223601106 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SynchronizedCollection_1_t3223601106 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_owner_2();
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void System.ServiceModel.IExtension`1<System.Object>::Detach(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		Il2CppObject* L_3 = ___item1;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_owner_2();
		NullCheck((Il2CppObject*)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.ServiceModel.IExtension`1<System.Object>::Attach(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject *)L_4);
		int32_t L_5 = ___index0;
		Il2CppObject* L_6 = ___item1;
		NullCheck((SynchronizedCollection_1_t3223601106 *)__this);
		((  void (*) (SynchronizedCollection_1_t3223601106 *, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((SynchronizedCollection_1_t3223601106 *)__this, (int32_t)L_5, (Il2CppObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return;
	}
}
// System.Void System.Web.Compilation.AppResourcesLengthComparer`1<System.Object>::.ctor()
extern "C"  void AppResourcesLengthComparer_1__ctor_m3561751963_gshared (AppResourcesLengthComparer_1_t3587265839 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Web.Compilation.AppResourcesLengthComparer`1<System.Object>::System.Collections.Generic.IComparer<T>.Compare(T,T)
extern "C"  int32_t AppResourcesLengthComparer_1_System_Collections_Generic_IComparerU3CTU3E_Compare_m2250971867_gshared (AppResourcesLengthComparer_1_t3587265839 * __this, Il2CppObject * ____a0, Il2CppObject * ____b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppResourcesLengthComparer_1_System_Collections_Generic_IComparerU3CTU3E_Compare_m2250971867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	List_1_t3319525431 * V_2 = NULL;
	AppResourceFileInfo_t3599744125 * V_3 = NULL;
	{
		V_0 = (String_t*)NULL;
		V_1 = (String_t*)NULL;
		Il2CppObject * L_0 = ____a0;
		if (!((String_t*)IsInst(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_1 = ____b1;
		if (!((String_t*)IsInst(L_1, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0041;
		}
	}
	{
		Il2CppObject * L_2 = ____a0;
		V_0 = (String_t*)((String_t*)IsInst(L_2, String_t_il2cpp_TypeInfo_var));
		Il2CppObject * L_3 = ____b1;
		V_1 = (String_t*)((String_t*)IsInst(L_3, String_t_il2cpp_TypeInfo_var));
		goto IL_00e5;
	}

IL_0041:
	{
		Il2CppObject * L_4 = ____a0;
		if (!((List_1_t3319525431 *)IsInst(L_4, List_1_t3319525431_il2cpp_TypeInfo_var)))
		{
			goto IL_008e;
		}
	}
	{
		Il2CppObject * L_5 = ____b1;
		if (!((List_1_t3319525431 *)IsInst(L_5, List_1_t3319525431_il2cpp_TypeInfo_var)))
		{
			goto IL_008e;
		}
	}
	{
		Il2CppObject * L_6 = ____a0;
		V_2 = (List_1_t3319525431 *)((List_1_t3319525431 *)IsInst(L_6, List_1_t3319525431_il2cpp_TypeInfo_var));
		List_1_t3319525431 * L_7 = V_2;
		NullCheck((List_1_t3319525431 *)L_7);
		String_t* L_8 = List_1_get_Item_m1191301710((List_1_t3319525431 *)L_7, (int32_t)0, /*hidden argument*/List_1_get_Item_m1191301710_MethodInfo_var);
		V_0 = (String_t*)L_8;
		Il2CppObject * L_9 = ____b1;
		V_2 = (List_1_t3319525431 *)((List_1_t3319525431 *)IsInst(L_9, List_1_t3319525431_il2cpp_TypeInfo_var));
		List_1_t3319525431 * L_10 = V_2;
		NullCheck((List_1_t3319525431 *)L_10);
		String_t* L_11 = List_1_get_Item_m1191301710((List_1_t3319525431 *)L_10, (int32_t)0, /*hidden argument*/List_1_get_Item_m1191301710_MethodInfo_var);
		V_1 = (String_t*)L_11;
		goto IL_00e5;
	}

IL_008e:
	{
		Il2CppObject * L_12 = ____a0;
		if (!((AppResourceFileInfo_t3599744125 *)IsInst(L_12, AppResourceFileInfo_t3599744125_il2cpp_TypeInfo_var)))
		{
			goto IL_00e3;
		}
	}
	{
		Il2CppObject * L_13 = ____b1;
		if (!((AppResourceFileInfo_t3599744125 *)IsInst(L_13, AppResourceFileInfo_t3599744125_il2cpp_TypeInfo_var)))
		{
			goto IL_00e3;
		}
	}
	{
		Il2CppObject * L_14 = ____a0;
		V_3 = (AppResourceFileInfo_t3599744125 *)((AppResourceFileInfo_t3599744125 *)IsInst(L_14, AppResourceFileInfo_t3599744125_il2cpp_TypeInfo_var));
		AppResourceFileInfo_t3599744125 * L_15 = V_3;
		NullCheck(L_15);
		FileInfo_t1169991790 * L_16 = (FileInfo_t1169991790 *)L_15->get_Info_2();
		NullCheck((FileInfo_t1169991790 *)L_16);
		String_t* L_17 = FileInfo_get_Name_m33729499((FileInfo_t1169991790 *)L_16, /*hidden argument*/NULL);
		V_0 = (String_t*)L_17;
		Il2CppObject * L_18 = ____b1;
		V_3 = (AppResourceFileInfo_t3599744125 *)((AppResourceFileInfo_t3599744125 *)IsInst(L_18, AppResourceFileInfo_t3599744125_il2cpp_TypeInfo_var));
		AppResourceFileInfo_t3599744125 * L_19 = V_3;
		NullCheck(L_19);
		FileInfo_t1169991790 * L_20 = (FileInfo_t1169991790 *)L_19->get_Info_2();
		NullCheck((FileInfo_t1169991790 *)L_20);
		String_t* L_21 = FileInfo_get_Name_m33729499((FileInfo_t1169991790 *)L_20, /*hidden argument*/NULL);
		V_1 = (String_t*)L_21;
		goto IL_00e5;
	}

IL_00e3:
	{
		return 0;
	}

IL_00e5:
	{
		String_t* L_22 = V_0;
		String_t* L_23 = V_1;
		NullCheck((AppResourcesLengthComparer_1_t3587265839 *)__this);
		int32_t L_24 = ((  int32_t (*) (AppResourcesLengthComparer_1_t3587265839 *, String_t*, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((AppResourcesLengthComparer_1_t3587265839 *)__this, (String_t*)L_22, (String_t*)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_24;
	}
}
// System.Int32 System.Web.Compilation.AppResourcesLengthComparer`1<System.Object>::CompareStrings(System.String,System.String)
extern "C"  int32_t AppResourcesLengthComparer_1_CompareStrings_m3551105951_gshared (AppResourcesLengthComparer_1_t3587265839 * __this, String_t* ___a0, String_t* ___b1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___a0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		String_t* L_1 = ___b1;
		if (L_1)
		{
			goto IL_000e;
		}
	}

IL_000c:
	{
		return 0;
	}

IL_000e:
	{
		String_t* L_2 = ___b1;
		NullCheck((String_t*)L_2);
		int32_t L_3 = String_get_Length_m3847582255((String_t*)L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___a0;
		NullCheck((String_t*)L_4);
		int32_t L_5 = String_get_Length_m3847582255((String_t*)L_4, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_3-(int32_t)L_5));
	}
}
// System.Void System.Web.Compilation.GenericBuildProvider`1<System.Object>::.ctor()
extern "C"  void GenericBuildProvider_1__ctor_m585533379_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	{
		NullCheck((BuildProvider_t3736381005 *)__this);
		BuildProvider__ctor_m4164598614((BuildProvider_t3736381005 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_Parsed()
extern "C"  bool GenericBuildProvider_1_get_Parsed_m3552230558_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get__parsed_7();
		return L_0;
	}
}
// System.String System.Web.Compilation.GenericBuildProvider`1<System.Object>::MapPath(System.Web.VirtualPath)
extern "C"  String_t* GenericBuildProvider_1_MapPath_m2859167432_gshared (GenericBuildProvider_1_t3300278628 * __this, VirtualPath_t4270372584 * ___virtualPath0, const MethodInfo* method)
{
	HttpContext_t1969259010 * V_0 = NULL;
	HttpRequest_t809700260 * V_1 = NULL;
	HttpRequest_t809700260 * G_B3_0 = NULL;
	{
		HttpContext_t1969259010 * L_0 = HttpContext_get_Current_m1955064578(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = (HttpContext_t1969259010 *)L_0;
		HttpContext_t1969259010 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		HttpContext_t1969259010 * L_2 = V_0;
		NullCheck((HttpContext_t1969259010 *)L_2);
		HttpRequest_t809700260 * L_3 = HttpContext_get_Request_m1929562575((HttpContext_t1969259010 *)L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = ((HttpRequest_t809700260 *)(NULL));
	}

IL_0018:
	{
		V_1 = (HttpRequest_t809700260 *)G_B3_0;
		HttpRequest_t809700260 * L_4 = V_1;
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		HttpRequest_t809700260 * L_5 = V_1;
		NullCheck((BuildProvider_t3736381005 *)__this);
		String_t* L_6 = BuildProvider_get_VirtualPath_m3357007857((BuildProvider_t3736381005 *)__this, /*hidden argument*/NULL);
		NullCheck((HttpRequest_t809700260 *)L_5);
		String_t* L_7 = HttpRequest_MapPath_m3338942164((HttpRequest_t809700260 *)L_5, (String_t*)L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_002c:
	{
		return (String_t*)NULL;
	}
}
// TParser System.Web.Compilation.GenericBuildProvider`1<System.Object>::Parse()
extern "C"  Il2CppObject * GenericBuildProvider_1_Parse_m3008328753_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	AspGenerator_t2810561191 * V_1 = NULL;
	{
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (GenericBuildProvider_1_t3300278628 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((GenericBuildProvider_1_t3300278628 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_0;
		bool L_1 = (bool)__this->get__parsed_7();
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}

IL_0014:
	{
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_IsDirectoryBuilder() */, (GenericBuildProvider_1_t3300278628 *)__this);
		if (L_3)
		{
			goto IL_0056;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		AspGenerator_t2810561191 * L_5 = VirtFuncInvoker1< AspGenerator_t2810561191 *, Il2CppObject * >::Invoke(17 /* System.Web.Compilation.AspGenerator System.Web.Compilation.GenericBuildProvider`1<System.Object>::CreateAspGenerator(TParser) */, (GenericBuildProvider_1_t3300278628 *)__this, (Il2CppObject *)L_4);
		V_1 = (AspGenerator_t2810561191 *)L_5;
		TextReader_t283511965 * L_6 = (TextReader_t283511965 *)__this->get__reader_6();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		AspGenerator_t2810561191 * L_7 = V_1;
		TextReader_t283511965 * L_8 = (TextReader_t283511965 *)__this->get__reader_6();
		NullCheck((BuildProvider_t3736381005 *)__this);
		VirtualPath_t4270372584 * L_9 = BuildProvider_get_VirtualPathInternal_m2593712449((BuildProvider_t3736381005 *)__this, /*hidden argument*/NULL);
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		String_t* L_10 = VirtFuncInvoker1< String_t*, VirtualPath_t4270372584 * >::Invoke(19 /* System.String System.Web.Compilation.GenericBuildProvider`1<System.Object>::MapPath(System.Web.VirtualPath) */, (GenericBuildProvider_1_t3300278628 *)__this, (VirtualPath_t4270372584 *)L_9);
		NullCheck((AspGenerator_t2810561191 *)L_7);
		AspGenerator_Parse_m2884416108((AspGenerator_t2810561191 *)L_7, (TextReader_t283511965 *)L_8, (String_t*)L_10, (bool)1, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0050:
	{
		AspGenerator_t2810561191 * L_11 = V_1;
		NullCheck((AspGenerator_t2810561191 *)L_11);
		AspGenerator_Parse_m1528274556((AspGenerator_t2810561191 *)L_11, /*hidden argument*/NULL);
	}

IL_0056:
	{
		__this->set__parsed_7((bool)1);
		Il2CppObject * L_12 = V_0;
		return L_12;
	}
}
// System.Void System.Web.Compilation.GenericBuildProvider`1<System.Object>::OverrideAssemblyPrefix(TParser,System.Web.Compilation.AssemblyBuilder)
extern "C"  void GenericBuildProvider_1_OverrideAssemblyPrefix_m297222786_gshared (GenericBuildProvider_1_t3300278628 * __this, Il2CppObject * ___parser0, AssemblyBuilder_t187066735 * ___assemblyBuilder1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Web.Compilation.GenericBuildProvider`1<System.Object>::GenerateCode()
extern "C"  void GenericBuildProvider_1_GenerateCode_m1216948280_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(20 /* TParser System.Web.Compilation.GenericBuildProvider`1<System.Object>::Parse() */, (GenericBuildProvider_1_t3300278628 *)__this);
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		BaseCompiler_t69158820 * L_2 = VirtFuncInvoker1< BaseCompiler_t69158820 *, Il2CppObject * >::Invoke(13 /* System.Web.Compilation.BaseCompiler System.Web.Compilation.GenericBuildProvider`1<System.Object>::CreateCompiler(TParser) */, (GenericBuildProvider_1_t3300278628 *)__this, (Il2CppObject *)L_1);
		__this->set__compiler_5(L_2);
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_NeedsConstructType() */, (GenericBuildProvider_1_t3300278628 *)__this);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		BaseCompiler_t69158820 * L_4 = (BaseCompiler_t69158820 *)__this->get__compiler_5();
		NullCheck((BaseCompiler_t69158820 *)L_4);
		BaseCompiler_ConstructType_m3989217184((BaseCompiler_t69158820 *)L_4, /*hidden argument*/NULL);
	}

IL_002a:
	{
		__this->set__codeGenerated_8((bool)1);
		return;
	}
}
// System.Void System.Web.Compilation.GenericBuildProvider`1<System.Object>::GenerateCode(System.Web.Compilation.AssemblyBuilder,TParser,System.Web.Compilation.BaseCompiler)
extern "C"  void GenericBuildProvider_1_GenerateCode_m3153712934_gshared (GenericBuildProvider_1_t3300278628 * __this, AssemblyBuilder_t187066735 * ___assemblyBuilder0, Il2CppObject * ___parser1, BaseCompiler_t69158820 * ___compiler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericBuildProvider_1_GenerateCode_m3153712934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CodeCompileUnit_t2527618915 * V_0 = NULL;
	{
		BaseCompiler_t69158820 * L_0 = (BaseCompiler_t69158820 *)__this->get__compiler_5();
		NullCheck((BaseCompiler_t69158820 *)L_0);
		CodeCompileUnit_t2527618915 * L_1 = BaseCompiler_get_CompileUnit_m770786863((BaseCompiler_t69158820 *)L_0, /*hidden argument*/NULL);
		V_0 = (CodeCompileUnit_t2527618915 *)L_1;
		CodeCompileUnit_t2527618915 * L_2 = V_0;
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		HttpException_t2907797370 * L_3 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_3, (String_t*)_stringLiteral1914822876, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001d:
	{
		AssemblyBuilder_t187066735 * L_4 = ___assemblyBuilder0;
		CodeCompileUnit_t2527618915 * L_5 = V_0;
		NullCheck((AssemblyBuilder_t187066735 *)L_4);
		AssemblyBuilder_AddCodeCompileUnit_m3424641995((AssemblyBuilder_t187066735 *)L_4, (BuildProvider_t3736381005 *)__this, (CodeCompileUnit_t2527618915 *)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Web.Compilation.GenericBuildProvider`1<System.Object>::GenerateCode(System.Web.Compilation.AssemblyBuilder)
extern "C"  void GenericBuildProvider_1_GenerateCode_m3776095089_gshared (GenericBuildProvider_1_t3300278628 * __this, AssemblyBuilder_t187066735 * ___assemblyBuilder0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericBuildProvider_1_GenerateCode_m3776095089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	List_1_t3319525431 * V_2 = NULL;
	String_t* V_3 = NULL;
	Enumerator_t913802012  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get__codeGenerated_8();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		VirtActionInvoker0::Invoke(5 /* System.Void System.Web.Compilation.GenericBuildProvider`1<System.Object>::GenerateCode() */, (GenericBuildProvider_1_t3300278628 *)__this);
	}

IL_0011:
	{
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(20 /* TParser System.Web.Compilation.GenericBuildProvider`1<System.Object>::Parse() */, (GenericBuildProvider_1_t3300278628 *)__this);
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		AssemblyBuilder_t187066735 * L_3 = ___assemblyBuilder0;
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		VirtActionInvoker2< Il2CppObject *, AssemblyBuilder_t187066735 * >::Invoke(21 /* System.Void System.Web.Compilation.GenericBuildProvider`1<System.Object>::OverrideAssemblyPrefix(TParser,System.Web.Compilation.AssemblyBuilder) */, (GenericBuildProvider_1_t3300278628 *)__this, (Il2CppObject *)L_2, (AssemblyBuilder_t187066735 *)L_3);
		Il2CppObject * L_4 = V_0;
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		String_t* L_5 = VirtFuncInvoker1< String_t*, Il2CppObject * >::Invoke(15 /* System.String System.Web.Compilation.GenericBuildProvider`1<System.Object>::GetCodeBehindSource(TParser) */, (GenericBuildProvider_1_t3300278628 *)__this, (Il2CppObject *)L_4);
		V_1 = (String_t*)L_5;
		String_t* L_6 = V_1;
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		AssemblyBuilder_t187066735 * L_7 = ___assemblyBuilder0;
		String_t* L_8 = V_1;
		NullCheck((AssemblyBuilder_t187066735 *)L_7);
		AssemblyBuilder_AddCodeFile_m97128385((AssemblyBuilder_t187066735 *)L_7, (String_t*)L_8, (BuildProvider_t3736381005 *)__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0037:
	{
		Il2CppObject * L_9 = V_0;
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		List_1_t3319525431 * L_10 = VirtFuncInvoker1< List_1_t3319525431 *, Il2CppObject * >::Invoke(18 /* System.Collections.Generic.List`1<System.String> System.Web.Compilation.GenericBuildProvider`1<System.Object>::GetReferencedAssemblies(TParser) */, (GenericBuildProvider_1_t3300278628 *)__this, (Il2CppObject *)L_9);
		V_2 = (List_1_t3319525431 *)L_10;
		List_1_t3319525431 * L_11 = V_2;
		if (!L_11)
		{
			goto IL_008b;
		}
	}
	{
		List_1_t3319525431 * L_12 = V_2;
		NullCheck((List_1_t3319525431 *)L_12);
		int32_t L_13 = List_1_get_Count_m2276455407((List_1_t3319525431 *)L_12, /*hidden argument*/List_1_get_Count_m2276455407_MethodInfo_var);
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_008b;
		}
	}
	{
		List_1_t3319525431 * L_14 = V_2;
		NullCheck((List_1_t3319525431 *)L_14);
		Enumerator_t913802012  L_15 = List_1_GetEnumerator_m2043795((List_1_t3319525431 *)L_14, /*hidden argument*/List_1_GetEnumerator_m2043795_MethodInfo_var);
		V_4 = (Enumerator_t913802012 )L_15;
	}

IL_0059:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006d;
		}

IL_005e:
		{
			String_t* L_16 = Enumerator_get_Current_m1270368552((Enumerator_t913802012 *)(&V_4), /*hidden argument*/Enumerator_get_Current_m1270368552_MethodInfo_var);
			V_3 = (String_t*)L_16;
			AssemblyBuilder_t187066735 * L_17 = ___assemblyBuilder0;
			String_t* L_18 = V_3;
			NullCheck((AssemblyBuilder_t187066735 *)L_17);
			AssemblyBuilder_AddAssemblyReference_m3403962749((AssemblyBuilder_t187066735 *)L_17, (String_t*)L_18, /*hidden argument*/NULL);
		}

IL_006d:
		{
			bool L_19 = Enumerator_MoveNext_m3828655022((Enumerator_t913802012 *)(&V_4), /*hidden argument*/Enumerator_MoveNext_m3828655022_MethodInfo_var);
			if (L_19)
			{
				goto IL_005e;
			}
		}

IL_0079:
		{
			IL2CPP_LEAVE(0x8B, FINALLY_007e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_007e;
	}

FINALLY_007e:
	{ // begin finally (depth: 1)
		Enumerator_t913802012  L_20 = V_4;
		Enumerator_t913802012  L_21 = L_20;
		Il2CppObject * L_22 = Box(Enumerator_t913802012_il2cpp_TypeInfo_var, &L_21);
		NullCheck((Il2CppObject *)L_22);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, (Il2CppObject *)L_22);
		IL2CPP_END_FINALLY(126)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(126)
	{
		IL2CPP_JUMP_TBL(0x8B, IL_008b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_008b:
	{
		AssemblyBuilder_t187066735 * L_23 = ___assemblyBuilder0;
		Il2CppObject * L_24 = V_0;
		BaseCompiler_t69158820 * L_25 = (BaseCompiler_t69158820 *)__this->get__compiler_5();
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		VirtActionInvoker3< AssemblyBuilder_t187066735 *, Il2CppObject *, BaseCompiler_t69158820 * >::Invoke(22 /* System.Void System.Web.Compilation.GenericBuildProvider`1<System.Object>::GenerateCode(System.Web.Compilation.AssemblyBuilder,TParser,System.Web.Compilation.BaseCompiler) */, (GenericBuildProvider_1_t3300278628 *)__this, (AssemblyBuilder_t187066735 *)L_23, (Il2CppObject *)L_24, (BaseCompiler_t69158820 *)L_25);
		return;
	}
}
// System.Type System.Web.Compilation.GenericBuildProvider`1<System.Object>::LoadTypeFromBin(System.Web.Compilation.BaseCompiler,TParser)
extern "C"  Type_t * GenericBuildProvider_1_LoadTypeFromBin_m3798126926_gshared (GenericBuildProvider_1_t3300278628 * __this, BaseCompiler_t69158820 * ___compiler0, Il2CppObject * ___parser1, const MethodInfo* method)
{
	{
		return (Type_t *)NULL;
	}
}
// System.Type System.Web.Compilation.GenericBuildProvider`1<System.Object>::GetGeneratedType(System.CodeDom.Compiler.CompilerResults)
extern "C"  Type_t * GenericBuildProvider_1_GetGeneratedType_m1057061081_gshared (GenericBuildProvider_1_t3300278628 * __this, CompilerResults_t1736487784 * ___results0, const MethodInfo* method)
{
	Assembly_t4102432799 * V_0 = NULL;
	Assembly_t4102432799 * G_B6_0 = NULL;
	{
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(27 /* System.Boolean System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_NeedsLoadFromBin() */, (GenericBuildProvider_1_t3300278628 *)__this);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		BaseCompiler_t69158820 * L_1 = (BaseCompiler_t69158820 *)__this->get__compiler_5();
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		BaseCompiler_t69158820 * L_2 = (BaseCompiler_t69158820 *)__this->get__compiler_5();
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (GenericBuildProvider_1_t3300278628 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((GenericBuildProvider_1_t3300278628 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		Type_t * L_4 = VirtFuncInvoker2< Type_t *, BaseCompiler_t69158820 *, Il2CppObject * >::Invoke(23 /* System.Type System.Web.Compilation.GenericBuildProvider`1<System.Object>::LoadTypeFromBin(System.Web.Compilation.BaseCompiler,TParser) */, (GenericBuildProvider_1_t3300278628 *)__this, (BaseCompiler_t69158820 *)L_2, (Il2CppObject *)L_3);
		return L_4;
	}

IL_0029:
	{
		CompilerResults_t1736487784 * L_5 = ___results0;
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		CompilerResults_t1736487784 * L_6 = ___results0;
		NullCheck((CompilerResults_t1736487784 *)L_6);
		Assembly_t4102432799 * L_7 = CompilerResults_get_CompiledAssembly_m3588532760((CompilerResults_t1736487784 *)L_6, /*hidden argument*/NULL);
		G_B6_0 = L_7;
		goto IL_003b;
	}

IL_003a:
	{
		G_B6_0 = ((Assembly_t4102432799 *)(NULL));
	}

IL_003b:
	{
		V_0 = (Assembly_t4102432799 *)G_B6_0;
		Assembly_t4102432799 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0044;
		}
	}
	{
		return (Type_t *)NULL;
	}

IL_0044:
	{
		Assembly_t4102432799 * L_9 = V_0;
		BaseCompiler_t69158820 * L_10 = (BaseCompiler_t69158820 *)__this->get__compiler_5();
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (GenericBuildProvider_1_t3300278628 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((GenericBuildProvider_1_t3300278628 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		String_t* L_12 = VirtFuncInvoker2< String_t*, BaseCompiler_t69158820 *, Il2CppObject * >::Invoke(16 /* System.String System.Web.Compilation.GenericBuildProvider`1<System.Object>::GetClassType(System.Web.Compilation.BaseCompiler,TParser) */, (GenericBuildProvider_1_t3300278628 *)__this, (BaseCompiler_t69158820 *)L_10, (Il2CppObject *)L_11);
		NullCheck((Assembly_t4102432799 *)L_9);
		Type_t * L_13 = VirtFuncInvoker1< Type_t *, String_t* >::Invoke(24 /* System.Type System.Reflection.Assembly::GetType(System.String) */, (Assembly_t4102432799 *)L_9, (String_t*)L_12);
		return L_13;
	}
}
// System.IO.TextReader System.Web.Compilation.GenericBuildProvider`1<System.Object>::SpecialOpenReader(System.Web.VirtualPath,System.String&)
extern "C"  TextReader_t283511965 * GenericBuildProvider_1_SpecialOpenReader_m2330665625_gshared (GenericBuildProvider_1_t3300278628 * __this, VirtualPath_t4270372584 * ___virtualPath0, String_t** ___physicalPath1, const MethodInfo* method)
{
	{
		String_t** L_0 = ___physicalPath1;
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)NULL);
		VirtualPath_t4270372584 * L_1 = ___virtualPath0;
		NullCheck((VirtualPath_t4270372584 *)L_1);
		String_t* L_2 = VirtualPath_get_Original_m1834498342((VirtualPath_t4270372584 *)L_1, /*hidden argument*/NULL);
		NullCheck((BuildProvider_t3736381005 *)__this);
		TextReader_t283511965 * L_3 = BuildProvider_OpenReader_m2470198021((BuildProvider_t3736381005 *)__this, (String_t*)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_LanguageName()
extern "C"  String_t* GenericBuildProvider_1_get_LanguageName_m648509076_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(20 /* TParser System.Web.Compilation.GenericBuildProvider`1<System.Object>::Parse() */, (GenericBuildProvider_1_t3300278628 *)__this);
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject * L_2 = V_0;
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		String_t* L_3 = VirtFuncInvoker1< String_t*, Il2CppObject * >::Invoke(14 /* System.String System.Web.Compilation.GenericBuildProvider`1<System.Object>::GetParserLanguage(TParser) */, (GenericBuildProvider_1_t3300278628 *)__this, (Il2CppObject *)L_2);
		return L_3;
	}

IL_001a:
	{
		NullCheck((BuildProvider_t3736381005 *)__this);
		String_t* L_4 = BuildProvider_get_LanguageName_m890363898((BuildProvider_t3736381005 *)__this, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Web.Compilation.CompilerType System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_CodeCompilerType()
extern "C"  CompilerType_t2533482247 * GenericBuildProvider_1_get_CodeCompilerType_m1615908398_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	{
		CompilerType_t2533482247 * L_0 = (CompilerType_t2533482247 *)__this->get__compilerType_4();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_LanguageName() */, (GenericBuildProvider_1_t3300278628 *)__this);
		NullCheck((BuildProvider_t3736381005 *)__this);
		CompilerType_t2533482247 * L_2 = BuildProvider_GetDefaultCompilerTypeForLanguage_m448594919((BuildProvider_t3736381005 *)__this, (String_t*)L_1, /*hidden argument*/NULL);
		__this->set__compilerType_4(L_2);
	}

IL_001d:
	{
		CompilerType_t2533482247 * L_3 = (CompilerType_t2533482247 *)__this->get__compilerType_4();
		return L_3;
	}
}
// TParser System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_Parser()
extern "C"  Il2CppObject * GenericBuildProvider_1_get_Parser_m3189097407_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericBuildProvider_1_get_Parser_m3189097407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VirtualPath_t4270372584 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__parser_3();
		if (L_0)
		{
			goto IL_008e;
		}
	}
	{
		NullCheck((BuildProvider_t3736381005 *)__this);
		VirtualPath_t4270372584 * L_1 = BuildProvider_get_VirtualPathInternal_m2593712449((BuildProvider_t3736381005 *)__this, /*hidden argument*/NULL);
		V_0 = (VirtualPath_t4270372584 *)L_1;
		VirtualPath_t4270372584 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		HttpException_t2907797370 * L_3 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_3, (String_t*)_stringLiteral2894761041, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_IsDirectoryBuilder() */, (GenericBuildProvider_1_t3300278628 *)__this);
		if (L_4)
		{
			goto IL_0060;
		}
	}
	{
		VirtualPath_t4270372584 * L_5 = V_0;
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		TextReader_t283511965 * L_6 = VirtFuncInvoker2< TextReader_t283511965 *, VirtualPath_t4270372584 *, String_t** >::Invoke(24 /* System.IO.TextReader System.Web.Compilation.GenericBuildProvider`1<System.Object>::SpecialOpenReader(System.Web.VirtualPath,System.String&) */, (GenericBuildProvider_1_t3300278628 *)__this, (VirtualPath_t4270372584 *)L_5, (String_t**)(&V_1));
		__this->set__reader_6(L_6);
		VirtualPath_t4270372584 * L_7 = V_0;
		String_t* L_8 = V_1;
		TextReader_t283511965 * L_9 = (TextReader_t283511965 *)__this->get__reader_6();
		HttpContext_t1969259010 * L_10 = HttpContext_get_Current_m1955064578(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		Il2CppObject * L_11 = VirtFuncInvoker4< Il2CppObject *, VirtualPath_t4270372584 *, String_t*, TextReader_t283511965 *, HttpContext_t1969259010 * >::Invoke(11 /* TParser System.Web.Compilation.GenericBuildProvider`1<System.Object>::CreateParser(System.Web.VirtualPath,System.String,System.IO.TextReader,System.Web.HttpContext) */, (GenericBuildProvider_1_t3300278628 *)__this, (VirtualPath_t4270372584 *)L_7, (String_t*)L_8, (TextReader_t283511965 *)L_9, (HttpContext_t1969259010 *)L_10);
		__this->set__parser_3(L_11);
		goto IL_0073;
	}

IL_0060:
	{
		VirtualPath_t4270372584 * L_12 = V_0;
		HttpContext_t1969259010 * L_13 = HttpContext_get_Current_m1955064578(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((GenericBuildProvider_1_t3300278628 *)__this);
		Il2CppObject * L_14 = VirtFuncInvoker3< Il2CppObject *, VirtualPath_t4270372584 *, String_t*, HttpContext_t1969259010 * >::Invoke(12 /* TParser System.Web.Compilation.GenericBuildProvider`1<System.Object>::CreateParser(System.Web.VirtualPath,System.String,System.Web.HttpContext) */, (GenericBuildProvider_1_t3300278628 *)__this, (VirtualPath_t4270372584 *)L_12, (String_t*)NULL, (HttpContext_t1969259010 *)L_13);
		__this->set__parser_3(L_14);
	}

IL_0073:
	{
		Il2CppObject * L_15 = (Il2CppObject *)__this->get__parser_3();
		if (L_15)
		{
			goto IL_008e;
		}
	}
	{
		HttpException_t2907797370 * L_16 = (HttpException_t2907797370 *)il2cpp_codegen_object_new(HttpException_t2907797370_il2cpp_TypeInfo_var);
		HttpException__ctor_m2549443145(L_16, (String_t*)_stringLiteral1220486295, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_008e:
	{
		Il2CppObject * L_17 = (Il2CppObject *)__this->get__parser_3();
		return L_17;
	}
}
// System.Boolean System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_IsDirectoryBuilder()
extern "C"  bool GenericBuildProvider_1_get_IsDirectoryBuilder_m4032337594_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_NeedsConstructType()
extern "C"  bool GenericBuildProvider_1_get_NeedsConstructType_m381177110_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Web.Compilation.GenericBuildProvider`1<System.Object>::get_NeedsLoadFromBin()
extern "C"  bool GenericBuildProvider_1_get_NeedsLoadFromBin_m1154017935_gshared (GenericBuildProvider_1_t3300278628 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void System.Web.UI.MainDirectiveAttribute`1<System.Int32>::.ctor(System.String)
extern "C"  void MainDirectiveAttribute_1__ctor_m516041465_gshared (MainDirectiveAttribute_1_t4076720260 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set_unparsedValue_0(L_0);
		String_t* L_1 = ___value0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___value0;
		bool L_3 = BaseParser_IsExpression_m2969228073(NULL /*static, unused*/, (String_t*)L_2, /*hidden argument*/NULL);
		__this->set_isExpression_2(L_3);
	}

IL_001f:
	{
		return;
	}
}
// System.Void System.Web.UI.MainDirectiveAttribute`1<System.Int32>::.ctor(T,System.Boolean)
extern "C"  void MainDirectiveAttribute_1__ctor_m1502357169_gshared (MainDirectiveAttribute_1_t4076720260 * __this, int32_t ___value0, bool ___unused1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// T System.Web.UI.MainDirectiveAttribute`1<System.Int32>::get_Value()
extern "C"  int32_t MainDirectiveAttribute_1_get_Value_m143613605_gshared (MainDirectiveAttribute_1_t4076720260 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
// System.Void System.Web.UI.MainDirectiveAttribute`1<System.Object>::.ctor(System.String)
extern "C"  void MainDirectiveAttribute_1__ctor_m2199428051_gshared (MainDirectiveAttribute_1_t4205880671 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value0;
		__this->set_unparsedValue_0(L_0);
		String_t* L_1 = ___value0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___value0;
		bool L_3 = BaseParser_IsExpression_m2969228073(NULL /*static, unused*/, (String_t*)L_2, /*hidden argument*/NULL);
		__this->set_isExpression_2(L_3);
	}

IL_001f:
	{
		return;
	}
}
// System.Void System.Web.UI.MainDirectiveAttribute`1<System.Object>::.ctor(T,System.Boolean)
extern "C"  void MainDirectiveAttribute_1__ctor_m3214841676_gshared (MainDirectiveAttribute_1_t4205880671 * __this, Il2CppObject * ___value0, bool ___unused1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// T System.Web.UI.MainDirectiveAttribute`1<System.Object>::get_Value()
extern "C"  Il2CppObject * MainDirectiveAttribute_1_get_Value_m45191265_gshared (MainDirectiveAttribute_1_t4205880671 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m3594541075_gshared (CachedInvokableCall_1_t2423483305 * __this, Object_t631007953 * ___target0, MethodInfo_t * ___theFunction1, bool ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m3594541075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t631007953 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t214452203 *)__this);
		((  void (*) (InvokableCall_1_t214452203 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t214452203 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)__this->get_m_Arg1_1();
		bool L_3 = ___argument2;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m2455036738_gshared (CachedInvokableCall_1_t2423483305 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t214452203 *)__this);
		((  void (*) (InvokableCall_1_t214452203 *, ObjectU5BU5D_t2843939325*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t214452203 *)__this, (ObjectU5BU5D_t2843939325*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m3353156630_gshared (CachedInvokableCall_1_t982173797 * __this, Object_t631007953 * ___target0, MethodInfo_t * ___theFunction1, int32_t ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m3353156630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t631007953 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t3068109991 *)__this);
		((  void (*) (InvokableCall_1_t3068109991 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t3068109991 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)__this->get_m_Arg1_1();
		int32_t L_3 = ___argument2;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m4259327819_gshared (CachedInvokableCall_1_t982173797 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t3068109991 *)__this);
		((  void (*) (InvokableCall_1_t3068109991 *, ObjectU5BU5D_t2843939325*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t3068109991 *)__this, (ObjectU5BU5D_t2843939325*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m2686176129_gshared (CachedInvokableCall_1_t1111334208 * __this, Object_t631007953 * ___target0, MethodInfo_t * ___theFunction1, Il2CppObject * ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m2686176129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t631007953 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t3197270402 *)__this);
		((  void (*) (InvokableCall_1_t3197270402 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t3197270402 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)__this->get_m_Arg1_1();
		Il2CppObject * L_3 = ___argument2;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m3994967940_gshared (CachedInvokableCall_1_t1111334208 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t3197270402 *)__this);
		((  void (*) (InvokableCall_1_t3197270402 *, ObjectU5BU5D_t2843939325*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t3197270402 *)__this, (ObjectU5BU5D_t2843939325*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m2985117283_gshared (CachedInvokableCall_1_t3723462114 * __this, Object_t631007953 * ___target0, MethodInfo_t * ___theFunction1, float ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m2985117283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t631007953 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t1514431012 *)__this);
		((  void (*) (InvokableCall_1_t1514431012 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t1514431012 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)__this->get_m_Arg1_1();
		float L_3 = ___argument2;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m1752113133_gshared (CachedInvokableCall_1_t3723462114 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t1514431012 *)__this);
		((  void (*) (InvokableCall_1_t1514431012 *, ObjectU5BU5D_t2843939325*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t1514431012 *)__this, (ObjectU5BU5D_t2843939325*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m3721720690_gshared (InvokableCall_1_t214452203 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m3721720690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m1036035291((BaseInvokableCall_t2703961024 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t214452203 *)__this);
		((  void (*) (InvokableCall_1_t214452203 *, UnityAction_1_t682124106 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t214452203 *)__this, (UnityAction_1_t682124106 *)((UnityAction_1_t682124106 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m3584895018_gshared (InvokableCall_1_t214452203 * __this, UnityAction_1_t682124106 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m3648827834((BaseInvokableCall_t2703961024 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t682124106 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t214452203 *)__this);
		((  void (*) (InvokableCall_1_t214452203 *, UnityAction_1_t682124106 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t214452203 *)__this, (UnityAction_1_t682124106 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m2923564695_gshared (InvokableCall_1_t214452203 * __this, UnityAction_1_t682124106 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t682124106 * V_0 = NULL;
	UnityAction_1_t682124106 * V_1 = NULL;
	{
		UnityAction_1_t682124106 * L_0 = (UnityAction_1_t682124106 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t682124106 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t682124106 * L_1 = V_0;
		V_1 = (UnityAction_1_t682124106 *)L_1;
		UnityAction_1_t682124106 ** L_2 = (UnityAction_1_t682124106 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t682124106 * L_3 = V_1;
		UnityAction_1_t682124106 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t682124106 * L_6 = V_0;
		UnityAction_1_t682124106 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t682124106 *>((UnityAction_1_t682124106 **)L_2, (UnityAction_1_t682124106 *)((UnityAction_1_t682124106 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t682124106 *)L_6);
		V_0 = (UnityAction_1_t682124106 *)L_7;
		UnityAction_1_t682124106 * L_8 = V_0;
		UnityAction_1_t682124106 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t682124106 *)L_8) == ((Il2CppObject*)(UnityAction_1_t682124106 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3070391773_gshared (InvokableCall_1_t214452203 * __this, UnityAction_1_t682124106 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t682124106 * V_0 = NULL;
	UnityAction_1_t682124106 * V_1 = NULL;
	{
		UnityAction_1_t682124106 * L_0 = (UnityAction_1_t682124106 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t682124106 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t682124106 * L_1 = V_0;
		V_1 = (UnityAction_1_t682124106 *)L_1;
		UnityAction_1_t682124106 ** L_2 = (UnityAction_1_t682124106 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t682124106 * L_3 = V_1;
		UnityAction_1_t682124106 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t682124106 * L_6 = V_0;
		UnityAction_1_t682124106 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t682124106 *>((UnityAction_1_t682124106 **)L_2, (UnityAction_1_t682124106 *)((UnityAction_1_t682124106 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t682124106 *)L_6);
		V_0 = (UnityAction_1_t682124106 *)L_7;
		UnityAction_1_t682124106 * L_8 = V_0;
		UnityAction_1_t682124106 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t682124106 *)L_8) == ((Il2CppObject*)(UnityAction_1_t682124106 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m2288947156_gshared (InvokableCall_1_t214452203 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m2288947156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, (String_t*)_stringLiteral1864861238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t682124106 * L_5 = (UnityAction_1_t682124106 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, (Delegate_t1188392813 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t682124106 * L_7 = (UnityAction_1_t682124106 *)__this->get_Delegate_0();
		ObjectU5BU5D_t2843939325* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t682124106 *)L_7);
		((  void (*) (UnityAction_1_t682124106 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t682124106 *)L_7, (bool)((*(bool*)((bool*)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2495666364_gshared (InvokableCall_1_t214452203 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t682124106 * L_0 = (UnityAction_1_t682124106 *)__this->get_Delegate_0();
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t682124106 * L_3 = (UnityAction_1_t682124106 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m3421292816_gshared (InvokableCall_1_t3068109991 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m3421292816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m1036035291((BaseInvokableCall_t2703961024 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t3068109991 *)__this);
		((  void (*) (InvokableCall_1_t3068109991 *, UnityAction_1_t3535781894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t3068109991 *)__this, (UnityAction_1_t3535781894 *)((UnityAction_1_t3535781894 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2173746276_gshared (InvokableCall_1_t3068109991 * __this, UnityAction_1_t3535781894 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m3648827834((BaseInvokableCall_t2703961024 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3535781894 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t3068109991 *)__this);
		((  void (*) (InvokableCall_1_t3068109991 *, UnityAction_1_t3535781894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t3068109991 *)__this, (UnityAction_1_t3535781894 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m137616200_gshared (InvokableCall_1_t3068109991 * __this, UnityAction_1_t3535781894 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3535781894 * V_0 = NULL;
	UnityAction_1_t3535781894 * V_1 = NULL;
	{
		UnityAction_1_t3535781894 * L_0 = (UnityAction_1_t3535781894 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3535781894 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3535781894 * L_1 = V_0;
		V_1 = (UnityAction_1_t3535781894 *)L_1;
		UnityAction_1_t3535781894 ** L_2 = (UnityAction_1_t3535781894 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3535781894 * L_3 = V_1;
		UnityAction_1_t3535781894 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3535781894 * L_6 = V_0;
		UnityAction_1_t3535781894 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3535781894 *>((UnityAction_1_t3535781894 **)L_2, (UnityAction_1_t3535781894 *)((UnityAction_1_t3535781894 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3535781894 *)L_6);
		V_0 = (UnityAction_1_t3535781894 *)L_7;
		UnityAction_1_t3535781894 * L_8 = V_0;
		UnityAction_1_t3535781894 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3535781894 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3535781894 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3576448582_gshared (InvokableCall_1_t3068109991 * __this, UnityAction_1_t3535781894 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3535781894 * V_0 = NULL;
	UnityAction_1_t3535781894 * V_1 = NULL;
	{
		UnityAction_1_t3535781894 * L_0 = (UnityAction_1_t3535781894 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3535781894 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3535781894 * L_1 = V_0;
		V_1 = (UnityAction_1_t3535781894 *)L_1;
		UnityAction_1_t3535781894 ** L_2 = (UnityAction_1_t3535781894 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3535781894 * L_3 = V_1;
		UnityAction_1_t3535781894 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3535781894 * L_6 = V_0;
		UnityAction_1_t3535781894 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3535781894 *>((UnityAction_1_t3535781894 **)L_2, (UnityAction_1_t3535781894 *)((UnityAction_1_t3535781894 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3535781894 *)L_6);
		V_0 = (UnityAction_1_t3535781894 *)L_7;
		UnityAction_1_t3535781894 * L_8 = V_0;
		UnityAction_1_t3535781894 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3535781894 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3535781894 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m2227986097_gshared (InvokableCall_1_t3068109991 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m2227986097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, (String_t*)_stringLiteral1864861238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3535781894 * L_5 = (UnityAction_1_t3535781894 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, (Delegate_t1188392813 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3535781894 * L_7 = (UnityAction_1_t3535781894 *)__this->get_Delegate_0();
		ObjectU5BU5D_t2843939325* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3535781894 *)L_7);
		((  void (*) (UnityAction_1_t3535781894 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3535781894 *)L_7, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Int32>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2259254216_gshared (InvokableCall_1_t3068109991 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3535781894 * L_0 = (UnityAction_1_t3535781894 *)__this->get_Delegate_0();
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3535781894 * L_3 = (UnityAction_1_t3535781894 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m513361773_gshared (InvokableCall_1_t3197270402 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m513361773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m1036035291((BaseInvokableCall_t2703961024 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t3197270402 *)__this);
		((  void (*) (InvokableCall_1_t3197270402 *, UnityAction_1_t3664942305 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t3197270402 *)__this, (UnityAction_1_t3664942305 *)((UnityAction_1_t3664942305 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m1248627709_gshared (InvokableCall_1_t3197270402 * __this, UnityAction_1_t3664942305 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m3648827834((BaseInvokableCall_t2703961024 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3664942305 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t3197270402 *)__this);
		((  void (*) (InvokableCall_1_t3197270402 *, UnityAction_1_t3664942305 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t3197270402 *)__this, (UnityAction_1_t3664942305 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m1571425726_gshared (InvokableCall_1_t3197270402 * __this, UnityAction_1_t3664942305 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3664942305 * V_0 = NULL;
	UnityAction_1_t3664942305 * V_1 = NULL;
	{
		UnityAction_1_t3664942305 * L_0 = (UnityAction_1_t3664942305 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3664942305 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3664942305 * L_1 = V_0;
		V_1 = (UnityAction_1_t3664942305 *)L_1;
		UnityAction_1_t3664942305 ** L_2 = (UnityAction_1_t3664942305 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3664942305 * L_3 = V_1;
		UnityAction_1_t3664942305 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3664942305 * L_6 = V_0;
		UnityAction_1_t3664942305 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3664942305 *>((UnityAction_1_t3664942305 **)L_2, (UnityAction_1_t3664942305 *)((UnityAction_1_t3664942305 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3664942305 *)L_6);
		V_0 = (UnityAction_1_t3664942305 *)L_7;
		UnityAction_1_t3664942305 * L_8 = V_0;
		UnityAction_1_t3664942305 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3664942305 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3664942305 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m518907935_gshared (InvokableCall_1_t3197270402 * __this, UnityAction_1_t3664942305 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3664942305 * V_0 = NULL;
	UnityAction_1_t3664942305 * V_1 = NULL;
	{
		UnityAction_1_t3664942305 * L_0 = (UnityAction_1_t3664942305 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3664942305 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3664942305 * L_1 = V_0;
		V_1 = (UnityAction_1_t3664942305 *)L_1;
		UnityAction_1_t3664942305 ** L_2 = (UnityAction_1_t3664942305 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3664942305 * L_3 = V_1;
		UnityAction_1_t3664942305 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3664942305 * L_6 = V_0;
		UnityAction_1_t3664942305 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3664942305 *>((UnityAction_1_t3664942305 **)L_2, (UnityAction_1_t3664942305 *)((UnityAction_1_t3664942305 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3664942305 *)L_6);
		V_0 = (UnityAction_1_t3664942305 *)L_7;
		UnityAction_1_t3664942305 * L_8 = V_0;
		UnityAction_1_t3664942305 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3664942305 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3664942305 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m1812996400_gshared (InvokableCall_1_t3197270402 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1812996400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, (String_t*)_stringLiteral1864861238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3664942305 * L_5 = (UnityAction_1_t3664942305 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, (Delegate_t1188392813 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3664942305 * L_7 = (UnityAction_1_t3664942305 *)__this->get_Delegate_0();
		ObjectU5BU5D_t2843939325* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3664942305 *)L_7);
		((  void (*) (UnityAction_1_t3664942305 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3664942305 *)L_7, (Il2CppObject *)((Il2CppObject *)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2845459943_gshared (InvokableCall_1_t3197270402 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3664942305 * L_0 = (UnityAction_1_t3664942305 *)__this->get_Delegate_0();
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3664942305 * L_3 = (UnityAction_1_t3664942305 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m956488764_gshared (InvokableCall_1_t1514431012 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m956488764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m1036035291((BaseInvokableCall_t2703961024 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t1514431012 *)__this);
		((  void (*) (InvokableCall_1_t1514431012 *, UnityAction_1_t1982102915 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1514431012 *)__this, (UnityAction_1_t1982102915 *)((UnityAction_1_t1982102915 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m3587258541_gshared (InvokableCall_1_t1514431012 * __this, UnityAction_1_t1982102915 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m3648827834((BaseInvokableCall_t2703961024 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t1982102915 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t1514431012 *)__this);
		((  void (*) (InvokableCall_1_t1514431012 *, UnityAction_1_t1982102915 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1514431012 *)__this, (UnityAction_1_t1982102915 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m4067662777_gshared (InvokableCall_1_t1514431012 * __this, UnityAction_1_t1982102915 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t1982102915 * V_0 = NULL;
	UnityAction_1_t1982102915 * V_1 = NULL;
	{
		UnityAction_1_t1982102915 * L_0 = (UnityAction_1_t1982102915 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t1982102915 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t1982102915 * L_1 = V_0;
		V_1 = (UnityAction_1_t1982102915 *)L_1;
		UnityAction_1_t1982102915 ** L_2 = (UnityAction_1_t1982102915 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t1982102915 * L_3 = V_1;
		UnityAction_1_t1982102915 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t1982102915 * L_6 = V_0;
		UnityAction_1_t1982102915 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t1982102915 *>((UnityAction_1_t1982102915 **)L_2, (UnityAction_1_t1982102915 *)((UnityAction_1_t1982102915 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t1982102915 *)L_6);
		V_0 = (UnityAction_1_t1982102915 *)L_7;
		UnityAction_1_t1982102915 * L_8 = V_0;
		UnityAction_1_t1982102915 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t1982102915 *)L_8) == ((Il2CppObject*)(UnityAction_1_t1982102915 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1306887219_gshared (InvokableCall_1_t1514431012 * __this, UnityAction_1_t1982102915 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t1982102915 * V_0 = NULL;
	UnityAction_1_t1982102915 * V_1 = NULL;
	{
		UnityAction_1_t1982102915 * L_0 = (UnityAction_1_t1982102915 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t1982102915 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t1982102915 * L_1 = V_0;
		V_1 = (UnityAction_1_t1982102915 *)L_1;
		UnityAction_1_t1982102915 ** L_2 = (UnityAction_1_t1982102915 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t1982102915 * L_3 = V_1;
		UnityAction_1_t1982102915 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t1982102915 * L_6 = V_0;
		UnityAction_1_t1982102915 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t1982102915 *>((UnityAction_1_t1982102915 **)L_2, (UnityAction_1_t1982102915 *)((UnityAction_1_t1982102915 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t1982102915 *)L_6);
		V_0 = (UnityAction_1_t1982102915 *)L_7;
		UnityAction_1_t1982102915 * L_8 = V_0;
		UnityAction_1_t1982102915 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t1982102915 *)L_8) == ((Il2CppObject*)(UnityAction_1_t1982102915 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m437830257_gshared (InvokableCall_1_t1514431012 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m437830257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, (String_t*)_stringLiteral1864861238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t1982102915 * L_5 = (UnityAction_1_t1982102915 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, (Delegate_t1188392813 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t1982102915 * L_7 = (UnityAction_1_t1982102915 *)__this->get_Delegate_0();
		ObjectU5BU5D_t2843939325* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t1982102915 *)L_7);
		((  void (*) (UnityAction_1_t1982102915 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t1982102915 *)L_7, (float)((*(float*)((float*)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2510486607_gshared (InvokableCall_1_t1514431012 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t1982102915 * L_0 = (UnityAction_1_t1982102915 *)__this->get_Delegate_0();
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t1982102915 * L_3 = (UnityAction_1_t1982102915 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m1720187399_gshared (InvokableCall_1_t2672850562 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m1720187399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m1036035291((BaseInvokableCall_t2703961024 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t2672850562 *)__this);
		((  void (*) (InvokableCall_1_t2672850562 *, UnityAction_1_t3140522465 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2672850562 *)__this, (UnityAction_1_t3140522465 *)((UnityAction_1_t3140522465 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m883760907_gshared (InvokableCall_1_t2672850562 * __this, UnityAction_1_t3140522465 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m3648827834((BaseInvokableCall_t2703961024 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3140522465 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t2672850562 *)__this);
		((  void (*) (InvokableCall_1_t2672850562 *, UnityAction_1_t3140522465 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2672850562 *)__this, (UnityAction_1_t3140522465 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m2959642168_gshared (InvokableCall_1_t2672850562 * __this, UnityAction_1_t3140522465 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3140522465 * V_0 = NULL;
	UnityAction_1_t3140522465 * V_1 = NULL;
	{
		UnityAction_1_t3140522465 * L_0 = (UnityAction_1_t3140522465 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3140522465 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3140522465 * L_1 = V_0;
		V_1 = (UnityAction_1_t3140522465 *)L_1;
		UnityAction_1_t3140522465 ** L_2 = (UnityAction_1_t3140522465 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3140522465 * L_3 = V_1;
		UnityAction_1_t3140522465 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3140522465 * L_6 = V_0;
		UnityAction_1_t3140522465 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3140522465 *>((UnityAction_1_t3140522465 **)L_2, (UnityAction_1_t3140522465 *)((UnityAction_1_t3140522465 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3140522465 *)L_6);
		V_0 = (UnityAction_1_t3140522465 *)L_7;
		UnityAction_1_t3140522465 * L_8 = V_0;
		UnityAction_1_t3140522465 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3140522465 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3140522465 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3502350317_gshared (InvokableCall_1_t2672850562 * __this, UnityAction_1_t3140522465 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3140522465 * V_0 = NULL;
	UnityAction_1_t3140522465 * V_1 = NULL;
	{
		UnityAction_1_t3140522465 * L_0 = (UnityAction_1_t3140522465 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3140522465 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3140522465 * L_1 = V_0;
		V_1 = (UnityAction_1_t3140522465 *)L_1;
		UnityAction_1_t3140522465 ** L_2 = (UnityAction_1_t3140522465 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3140522465 * L_3 = V_1;
		UnityAction_1_t3140522465 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3140522465 * L_6 = V_0;
		UnityAction_1_t3140522465 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3140522465 *>((UnityAction_1_t3140522465 **)L_2, (UnityAction_1_t3140522465 *)((UnityAction_1_t3140522465 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3140522465 *)L_6);
		V_0 = (UnityAction_1_t3140522465 *)L_7;
		UnityAction_1_t3140522465 * L_8 = V_0;
		UnityAction_1_t3140522465 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3140522465 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3140522465 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m3583307112_gshared (InvokableCall_1_t2672850562 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m3583307112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, (String_t*)_stringLiteral1864861238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3140522465 * L_5 = (UnityAction_1_t3140522465 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, (Delegate_t1188392813 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3140522465 * L_7 = (UnityAction_1_t3140522465 *)__this->get_Delegate_0();
		ObjectU5BU5D_t2843939325* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3140522465 *)L_7);
		((  void (*) (UnityAction_1_t3140522465 *, Color_t2555686324 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3140522465 *)L_7, (Color_t2555686324 )((*(Color_t2555686324 *)((Color_t2555686324 *)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m1086553984_gshared (InvokableCall_1_t2672850562 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3140522465 * L_0 = (UnityAction_1_t3140522465 *)__this->get_Delegate_0();
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3140522465 * L_3 = (UnityAction_1_t3140522465 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m3027854863_gshared (InvokableCall_1_t2273393761 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m3027854863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m1036035291((BaseInvokableCall_t2703961024 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t2273393761 *)__this);
		((  void (*) (InvokableCall_1_t2273393761 *, UnityAction_1_t2741065664 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2273393761 *)__this, (UnityAction_1_t2741065664 *)((UnityAction_1_t2741065664 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2827613418_gshared (InvokableCall_1_t2273393761 * __this, UnityAction_1_t2741065664 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m3648827834((BaseInvokableCall_t2703961024 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t2741065664 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t2273393761 *)__this);
		((  void (*) (InvokableCall_1_t2273393761 *, UnityAction_1_t2741065664 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2273393761 *)__this, (UnityAction_1_t2741065664 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m1796580605_gshared (InvokableCall_1_t2273393761 * __this, UnityAction_1_t2741065664 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t2741065664 * V_0 = NULL;
	UnityAction_1_t2741065664 * V_1 = NULL;
	{
		UnityAction_1_t2741065664 * L_0 = (UnityAction_1_t2741065664 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t2741065664 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t2741065664 * L_1 = V_0;
		V_1 = (UnityAction_1_t2741065664 *)L_1;
		UnityAction_1_t2741065664 ** L_2 = (UnityAction_1_t2741065664 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t2741065664 * L_3 = V_1;
		UnityAction_1_t2741065664 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t2741065664 * L_6 = V_0;
		UnityAction_1_t2741065664 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t2741065664 *>((UnityAction_1_t2741065664 **)L_2, (UnityAction_1_t2741065664 *)((UnityAction_1_t2741065664 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t2741065664 *)L_6);
		V_0 = (UnityAction_1_t2741065664 *)L_7;
		UnityAction_1_t2741065664 * L_8 = V_0;
		UnityAction_1_t2741065664 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t2741065664 *)L_8) == ((Il2CppObject*)(UnityAction_1_t2741065664 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3978975990_gshared (InvokableCall_1_t2273393761 * __this, UnityAction_1_t2741065664 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t2741065664 * V_0 = NULL;
	UnityAction_1_t2741065664 * V_1 = NULL;
	{
		UnityAction_1_t2741065664 * L_0 = (UnityAction_1_t2741065664 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t2741065664 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t2741065664 * L_1 = V_0;
		V_1 = (UnityAction_1_t2741065664 *)L_1;
		UnityAction_1_t2741065664 ** L_2 = (UnityAction_1_t2741065664 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t2741065664 * L_3 = V_1;
		UnityAction_1_t2741065664 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, (Delegate_t1188392813 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t2741065664 * L_6 = V_0;
		UnityAction_1_t2741065664 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t2741065664 *>((UnityAction_1_t2741065664 **)L_2, (UnityAction_1_t2741065664 *)((UnityAction_1_t2741065664 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t2741065664 *)L_6);
		V_0 = (UnityAction_1_t2741065664 *)L_7;
		UnityAction_1_t2741065664 * L_8 = V_0;
		UnityAction_1_t2741065664 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t2741065664 *)L_8) == ((Il2CppObject*)(UnityAction_1_t2741065664 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m2430730927_gshared (InvokableCall_1_t2273393761 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m2430730927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, (String_t*)_stringLiteral1864861238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t2741065664 * L_5 = (UnityAction_1_t2741065664 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, (Delegate_t1188392813 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t2741065664 * L_7 = (UnityAction_1_t2741065664 *)__this->get_Delegate_0();
		ObjectU5BU5D_t2843939325* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t2741065664 *)L_7);
		((  void (*) (UnityAction_1_t2741065664 *, Vector2_t2156229523 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t2741065664 *)L_7, (Vector2_t2156229523 )((*(Vector2_t2156229523 *)((Vector2_t2156229523 *)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2125539965_gshared (InvokableCall_1_t2273393761 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t2741065664 * L_0 = (UnityAction_1_t2741065664 *)__this->get_Delegate_0();
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t2741065664 * L_3 = (UnityAction_1_t2741065664 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m162480713_gshared (InvokableCall_2_t362407658 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m162480713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m1036035291((BaseInvokableCall_t2703961024 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3283971887 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m4103345430_gshared (InvokableCall_2_t362407658 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m4103345430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, (String_t*)_stringLiteral1864861238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t2843939325* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		UnityAction_2_t3283971887 * L_8 = (UnityAction_2_t3283971887 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, (Delegate_t1188392813 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3283971887 * L_10 = (UnityAction_2_t3283971887 *)__this->get_Delegate_0();
		ObjectU5BU5D_t2843939325* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t2843939325* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3283971887 *)L_10);
		((  void (*) (UnityAction_2_t3283971887 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_2_t3283971887 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m619659795_gshared (InvokableCall_2_t362407658 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t3283971887 * L_0 = (UnityAction_2_t3283971887 *)__this->get_Delegate_0();
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t3283971887 * L_3 = (UnityAction_2_t3283971887 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_3__ctor_m1759759471_gshared (InvokableCall_3_t4059188962 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3__ctor_m1759759471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m1036035291((BaseInvokableCall_t2703961024 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_3_t1557236713 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_3_Invoke_m4268591549_gshared (InvokableCall_3_t4059188962 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3_Invoke_m4268591549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, (String_t*)_stringLiteral1864861238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t2843939325* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t2843939325* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_3_t1557236713 * L_11 = (UnityAction_3_t1557236713 *)__this->get_Delegate_0();
		bool L_12 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, (Delegate_t1188392813 *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0060;
		}
	}
	{
		UnityAction_3_t1557236713 * L_13 = (UnityAction_3_t1557236713 *)__this->get_Delegate_0();
		ObjectU5BU5D_t2843939325* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 0;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		ObjectU5BU5D_t2843939325* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 1;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t2843939325* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 2;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck((UnityAction_3_t1557236713 *)L_13);
		((  void (*) (UnityAction_3_t1557236713 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_3_t1557236713 *)L_13, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0060:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_3_Find_m1876666590_gshared (InvokableCall_3_t4059188962 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_3_t1557236713 * L_0 = (UnityAction_3_t1557236713 *)__this->get_Delegate_0();
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_3_t1557236713 * L_3 = (UnityAction_3_t1557236713 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_4__ctor_m2023811662_gshared (InvokableCall_4_t2756980746 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4__ctor_m2023811662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2703961024 *)__this);
		BaseInvokableCall__ctor_m1036035291((BaseInvokableCall_t2703961024 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_4_t682480391 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_4_Invoke_m3539026103_gshared (InvokableCall_4_t2756980746 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4_Invoke_m3539026103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t2843939325* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)4)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t132251570 * L_1 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_1, (String_t*)_stringLiteral1864861238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t2843939325* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t2843939325* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t2843939325* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t2843939325* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 3;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_4_t682480391 * L_14 = (UnityAction_4_t682480391 *)__this->get_Delegate_0();
		bool L_15 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, (Delegate_t1188392813 *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0070;
		}
	}
	{
		UnityAction_4_t682480391 * L_16 = (UnityAction_4_t682480391 *)__this->get_Delegate_0();
		ObjectU5BU5D_t2843939325* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 0;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t2843939325* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 1;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		ObjectU5BU5D_t2843939325* L_23 = ___args0;
		NullCheck(L_23);
		int32_t L_24 = 2;
		Il2CppObject * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		ObjectU5BU5D_t2843939325* L_26 = ___args0;
		NullCheck(L_26);
		int32_t L_27 = 3;
		Il2CppObject * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((UnityAction_4_t682480391 *)L_16);
		((  void (*) (UnityAction_4_t682480391 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnityAction_4_t682480391 *)L_16, (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), (Il2CppObject *)((Il2CppObject *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_0070:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_4_Find_m4153917558_gshared (InvokableCall_4_t2756980746 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_4_t682480391 * L_0 = (UnityAction_4_t682480391 *)__this->get_Delegate_0();
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_4_t682480391 * L_3 = (UnityAction_4_t682480391 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3007623985_gshared (UnityAction_1_t682124106 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3205374586_gshared (UnityAction_1_t682124106 * __this, bool ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3205374586((UnityAction_1_t682124106 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Boolean>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m917793246_gshared (UnityAction_1_t682124106 * __this, bool ___arg00, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m917793246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m2555381368_gshared (UnityAction_1_t682124106 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m4246074269_gshared (UnityAction_1_t3535781894 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m286788042_gshared (UnityAction_1_t3535781894 * __this, int32_t ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m286788042((UnityAction_1_t3535781894 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Int32>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2958527564_gshared (UnityAction_1_t3535781894 * __this, int32_t ___arg00, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2958527564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m36082240_gshared (UnityAction_1_t3535781894 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2948818879_gshared (UnityAction_1_t3664942305 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m85073476_gshared (UnityAction_1_t3664942305 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m85073476((UnityAction_1_t3664942305 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m1877898761_gshared (UnityAction_1_t3664942305 * __this, Il2CppObject * ___arg00, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg00;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m2002601517_gshared (UnityAction_1_t3664942305 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m336053009_gshared (UnityAction_1_t1982102915 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1880497053_gshared (UnityAction_1_t1982102915 * __this, float ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m1880497053((UnityAction_1_t1982102915 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Single>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m118448792_gshared (UnityAction_1_t1982102915 * __this, float ___arg00, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m118448792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t1397266774_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m312813767_gshared (UnityAction_1_t1982102915 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2796929162_gshared (UnityAction_1_t3140522465 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2491965359_gshared (UnityAction_1_t3140522465 * __this, Color_t2555686324  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2491965359((UnityAction_1_t3140522465 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2555686324  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2555686324  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Color>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m1854409024_gshared (UnityAction_1_t3140522465 * __this, Color_t2555686324  ___arg00, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m1854409024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2555686324_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3794558788_gshared (UnityAction_1_t3140522465 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3692564582_gshared (UnityAction_1_t2933211702 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3689685399_gshared (UnityAction_1_t2933211702 * __this, Scene_t2348375561  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3689685399((UnityAction_1_t2933211702 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t2348375561  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t2348375561  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m1034040374_gshared (UnityAction_1_t2933211702 * __this, Scene_t2348375561  ___arg00, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m1034040374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Scene_t2348375561_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3110418531_gshared (UnityAction_1_t2933211702 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m508631773_gshared (UnityAction_1_t2741065664 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2871728_gshared (UnityAction_1_t2741065664 * __this, Vector2_t2156229523  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2871728((UnityAction_1_t2741065664 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2156229523  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2156229523  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2863020861_gshared (UnityAction_1_t2741065664 * __this, Vector2_t2156229523  ___arg00, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2863020861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3648199316_gshared (UnityAction_1_t2741065664 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m3522155603_gshared (UnityAction_2_t3283971887 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2615882976_gshared (UnityAction_2_t3283971887 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m2615882976((UnityAction_2_t3283971887 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,System.Object>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2050629662_gshared (UnityAction_2_t3283971887 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m611263908_gshared (UnityAction_2_t3283971887 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m3364748402_gshared (UnityAction_2_t2165061829 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1297655681_gshared (UnityAction_2_t2165061829 * __this, Scene_t2348375561  ___arg00, int32_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1297655681((UnityAction_2_t2165061829 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t2348375561  ___arg00, int32_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t2348375561  ___arg00, int32_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m3595933178_gshared (UnityAction_2_t2165061829 * __this, Scene_t2348375561  ___arg00, int32_t ___arg11, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m3595933178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t2348375561_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(LoadSceneMode_t3251202195_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m3654482891_gshared (UnityAction_2_t2165061829 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m740642639_gshared (UnityAction_2_t1262235195 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m770027984_gshared (UnityAction_2_t1262235195 * __this, Scene_t2348375561  ___arg00, Scene_t2348375561  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m770027984((UnityAction_2_t1262235195 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t2348375561  ___arg00, Scene_t2348375561  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t2348375561  ___arg00, Scene_t2348375561  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m453999018_gshared (UnityAction_2_t1262235195 * __this, Scene_t2348375561  ___arg00, Scene_t2348375561  ___arg11, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m453999018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t2348375561_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(Scene_t2348375561_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m1433391987_gshared (UnityAction_2_t1262235195 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_3__ctor_m290677339_gshared (UnityAction_3_t1557236713 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m2312220652_gshared (UnityAction_3_t1557236713 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_3_Invoke_m2312220652((UnityAction_3_t1557236713 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_3_BeginInvoke_m1952876974_gshared (UnityAction_3_t1557236713 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, AsyncCallback_t3962456242 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_3_EndInvoke_m1136343158_gshared (UnityAction_3_t1557236713 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_4__ctor_m476821447_gshared (UnityAction_4_t682480391 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
extern "C"  void UnityAction_4_Invoke_m1593023405_gshared (UnityAction_4_t682480391 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_4_Invoke_m1593023405((UnityAction_4_t682480391 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, ___arg33, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_4_BeginInvoke_m578834885_gshared (UnityAction_4_t682480391 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, AsyncCallback_t3962456242 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	__d_args[3] = ___arg33;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_4_EndInvoke_m326333003_gshared (UnityAction_4_t682480391 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::.ctor()
extern "C"  void UnityEvent_1__ctor_m3777630589_gshared (UnityEvent_1_t978947469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m3777630589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase__ctor_m1087366165((UnityEventBase_t3960448221 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m2847988282_gshared (UnityEvent_1_t978947469 * __this, UnityAction_1_t682124106 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t682124106 * L_0 = ___call0;
		BaseInvokableCall_t2703961024 * L_1 = ((  BaseInvokableCall_t2703961024 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t682124106 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t682124106 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_AddCall_m3632990696((UnityEventBase_t3960448221 *)__this, (BaseInvokableCall_t2703961024 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m389670056_gshared (UnityEvent_1_t978947469 * __this, UnityAction_1_t682124106 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t682124106 * L_0 = ___call0;
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t682124106 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_RemoveListener_m748247232((UnityEventBase_t3960448221 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Boolean>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2660664345_gshared (UnityEvent_1_t978947469 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2660664345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3940880105* L_2 = (TypeU5BU5D_t3940880105*)((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3940880105*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m4189390352_gshared (UnityEvent_1_t978947469 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t214452203 * L_2 = (InvokableCall_1_t214452203 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t214452203 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2703961024 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m3141471262_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t682124106 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		UnityAction_1_t682124106 * L_0 = ___action0;
		InvokableCall_1_t214452203 * L_1 = (InvokableCall_1_t214452203 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t214452203 *, UnityAction_1_t682124106 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t682124106 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2703961024 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2703961024 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m933614109_gshared (UnityEvent_1_t978947469 * __this, bool ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		bool L_1 = ___arg00;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_Invoke_m3022009851((UnityEventBase_t3960448221 *)__this, (ObjectU5BU5D_t2843939325*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2474648315_gshared (UnityEvent_1_t3832605257 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m2474648315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase__ctor_m1087366165((UnityEventBase_t3960448221 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m3990091128_gshared (UnityEvent_1_t3832605257 * __this, UnityAction_1_t3535781894 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3535781894 * L_0 = ___call0;
		BaseInvokableCall_t2703961024 * L_1 = ((  BaseInvokableCall_t2703961024 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3535781894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3535781894 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_AddCall_m3632990696((UnityEventBase_t3960448221 *)__this, (BaseInvokableCall_t2703961024 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m3507445688_gshared (UnityEvent_1_t3832605257 * __this, UnityAction_1_t3535781894 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3535781894 * L_0 = ___call0;
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3535781894 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_RemoveListener_m748247232((UnityEventBase_t3960448221 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Int32>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m649722414_gshared (UnityEvent_1_t3832605257 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m649722414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3940880105* L_2 = (TypeU5BU5D_t3940880105*)((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3940880105*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m977786818_gshared (UnityEvent_1_t3832605257 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t3068109991 * L_2 = (InvokableCall_1_t3068109991 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t3068109991 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2703961024 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m1085214430_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t3535781894 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		UnityAction_1_t3535781894 * L_0 = ___action0;
		InvokableCall_1_t3068109991 * L_1 = (InvokableCall_1_t3068109991 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t3068109991 *, UnityAction_1_t3535781894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3535781894 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2703961024 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2703961024 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m117476416_gshared (UnityEvent_1_t3832605257 * __this, int32_t ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		int32_t L_1 = ___arg00;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_Invoke_m3022009851((UnityEventBase_t3960448221 *)__this, (ObjectU5BU5D_t2843939325*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C"  void UnityEvent_1__ctor_m4234511999_gshared (UnityEvent_1_t3961765668 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m4234511999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase__ctor_m1087366165((UnityEventBase_t3960448221 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m2542153311_gshared (UnityEvent_1_t3961765668 * __this, UnityAction_1_t3664942305 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3664942305 * L_0 = ___call0;
		BaseInvokableCall_t2703961024 * L_1 = ((  BaseInvokableCall_t2703961024 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3664942305 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3664942305 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_AddCall_m3632990696((UnityEventBase_t3960448221 *)__this, (BaseInvokableCall_t2703961024 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m609742345_gshared (UnityEvent_1_t3961765668 * __this, UnityAction_1_t3664942305 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3664942305 * L_0 = ___call0;
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3664942305 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_RemoveListener_m748247232((UnityEventBase_t3960448221 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m1537875845_gshared (UnityEvent_1_t3961765668 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m1537875845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3940880105* L_2 = (TypeU5BU5D_t3940880105*)((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3940880105*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m574988313_gshared (UnityEvent_1_t3961765668 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t3197270402 * L_2 = (InvokableCall_1_t3197270402 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t3197270402 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2703961024 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m2351167509_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t3664942305 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		UnityAction_1_t3664942305 * L_0 = ___action0;
		InvokableCall_1_t3197270402 * L_1 = (InvokableCall_1_t3197270402 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t3197270402 *, UnityAction_1_t3664942305 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3664942305 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2703961024 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2703961024 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m4196549529_gshared (UnityEvent_1_t3961765668 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		Il2CppObject * L_1 = ___arg00;
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_Invoke_m3022009851((UnityEventBase_t3960448221 *)__this, (ObjectU5BU5D_t2843939325*)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2218582587_gshared (UnityEvent_1_t2278926278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m2218582587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase__ctor_m1087366165((UnityEventBase_t3960448221 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m3008008915_gshared (UnityEvent_1_t2278926278 * __this, UnityAction_1_t1982102915 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t1982102915 * L_0 = ___call0;
		BaseInvokableCall_t2703961024 * L_1 = ((  BaseInvokableCall_t2703961024 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t1982102915 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t1982102915 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_AddCall_m3632990696((UnityEventBase_t3960448221 *)__this, (BaseInvokableCall_t2703961024 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m4190968495_gshared (UnityEvent_1_t2278926278 * __this, UnityAction_1_t1982102915 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t1982102915 * L_0 = ___call0;
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t1982102915 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_RemoveListener_m748247232((UnityEventBase_t3960448221 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Single>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m1665304028_gshared (UnityEvent_1_t2278926278 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m1665304028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3940880105* L_2 = (TypeU5BU5D_t3940880105*)((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3940880105*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m1599200742_gshared (UnityEvent_1_t2278926278 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t1514431012 * L_2 = (InvokableCall_1_t1514431012 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t1514431012 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2703961024 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m1196210497_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t1982102915 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		UnityAction_1_t1982102915 * L_0 = ___action0;
		InvokableCall_1_t1514431012 * L_1 = (InvokableCall_1_t1514431012 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t1514431012 *, UnityAction_1_t1982102915 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t1982102915 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2703961024 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2703961024 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m3400677460_gshared (UnityEvent_1_t2278926278 * __this, float ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		float L_1 = ___arg00;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_Invoke_m3022009851((UnityEventBase_t3960448221 *)__this, (ObjectU5BU5D_t2843939325*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern "C"  void UnityEvent_1__ctor_m1293792034_gshared (UnityEvent_1_t3437345828 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m1293792034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase__ctor_m1087366165((UnityEventBase_t3960448221 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m1590149461_gshared (UnityEvent_1_t3437345828 * __this, UnityAction_1_t3140522465 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3140522465 * L_0 = ___call0;
		BaseInvokableCall_t2703961024 * L_1 = ((  BaseInvokableCall_t2703961024 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3140522465 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3140522465 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_AddCall_m3632990696((UnityEventBase_t3960448221 *)__this, (BaseInvokableCall_t2703961024 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m3543237118_gshared (UnityEvent_1_t3437345828 * __this, UnityAction_1_t3140522465 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3140522465 * L_0 = ___call0;
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3140522465 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_RemoveListener_m748247232((UnityEventBase_t3960448221 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m4095274380_gshared (UnityEvent_1_t3437345828 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m4095274380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3940880105* L_2 = (TypeU5BU5D_t3940880105*)((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3940880105*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m2274265507_gshared (UnityEvent_1_t3437345828 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2672850562 * L_2 = (InvokableCall_1_t2672850562 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2672850562 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2703961024 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m3570057907_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t3140522465 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		UnityAction_1_t3140522465 * L_0 = ___action0;
		InvokableCall_1_t2672850562 * L_1 = (InvokableCall_1_t2672850562 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2672850562 *, UnityAction_1_t3140522465 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3140522465 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2703961024 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2703961024 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m3884411426_gshared (UnityEvent_1_t3437345828 * __this, Color_t2555686324  ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		Color_t2555686324  L_1 = ___arg00;
		Color_t2555686324  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_Invoke_m3022009851((UnityEventBase_t3960448221 *)__this, (ObjectU5BU5D_t2843939325*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::.ctor()
extern "C"  void UnityEvent_1__ctor_m3675246889_gshared (UnityEvent_1_t3037889027 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m3675246889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase__ctor_m1087366165((UnityEventBase_t3960448221 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m428330015_gshared (UnityEvent_1_t3037889027 * __this, UnityAction_1_t2741065664 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t2741065664 * L_0 = ___call0;
		BaseInvokableCall_t2703961024 * L_1 = ((  BaseInvokableCall_t2703961024 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t2741065664 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t2741065664 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_AddCall_m3632990696((UnityEventBase_t3960448221 *)__this, (BaseInvokableCall_t2703961024 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m315868041_gshared (UnityEvent_1_t3037889027 * __this, UnityAction_1_t2741065664 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t2741065664 * L_0 = ___call0;
		NullCheck((Delegate_t1188392813 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888((Delegate_t1188392813 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t2741065664 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, (Delegate_t1188392813 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_RemoveListener_m748247232((UnityEventBase_t3960448221 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m3637444959_gshared (UnityEvent_1_t3037889027 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m3637444959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3940880105* L_2 = (TypeU5BU5D_t3940880105*)((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3940880105*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m1591937171_gshared (UnityEvent_1_t3037889027 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2273393761 * L_2 = (InvokableCall_1_t2273393761 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2273393761 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2703961024 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_1_GetDelegate_m3233580170_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t2741065664 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		UnityAction_1_t2741065664 * L_0 = ___action0;
		InvokableCall_1_t2273393761 * L_1 = (InvokableCall_1_t2273393761 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2273393761 *, UnityAction_1_t2741065664 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t2741065664 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2703961024 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2703961024 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m3432495026_gshared (UnityEvent_1_t3037889027 * __this, Vector2_t2156229523  ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		Vector2_t2156229523  L_1 = ___arg00;
		Vector2_t2156229523  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = (ObjectU5BU5D_t2843939325*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase_Invoke_m3022009851((UnityEventBase_t3960448221 *)__this, (ObjectU5BU5D_t2843939325*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_2__ctor_m3404211184_gshared (UnityEvent_2_t614268397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2__ctor_m3404211184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2)));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase__ctor_m1087366165((UnityEventBase_t3960448221 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m933094994_gshared (UnityEvent_2_t614268397 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2_FindMethod_Impl_m933094994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3940880105* L_2 = (TypeU5BU5D_t3940880105*)((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t3940880105* L_4 = (TypeU5BU5D_t3940880105*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3940880105*)L_4, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_6;
		goto IL_002e;
	}

IL_002e:
	{
		MethodInfo_t * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_2_GetDelegate_m4088973080_gshared (UnityEvent_2_t614268397 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_2_t362407658 * L_2 = (InvokableCall_2_t362407658 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (InvokableCall_2_t362407658 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (BaseInvokableCall_t2703961024 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_3__ctor_m3742571125_gshared (UnityEvent_3_t2404744798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3__ctor_m3742571125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)3)));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase__ctor_m1087366165((UnityEventBase_t3960448221 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_3_FindMethod_Impl_m156112408_gshared (UnityEvent_3_t2404744798 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3_FindMethod_Impl_m156112408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3940880105* L_2 = (TypeU5BU5D_t3940880105*)((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t3940880105* L_4 = (TypeU5BU5D_t3940880105*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t3940880105* L_6 = (TypeU5BU5D_t3940880105*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		MethodInfo_t * L_8 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3940880105*)L_6, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_8;
		goto IL_003b;
	}

IL_003b:
	{
		MethodInfo_t * L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_3_GetDelegate_m3568004668_gshared (UnityEvent_3_t2404744798 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_3_t4059188962 * L_2 = (InvokableCall_3_t4059188962 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_3_t4059188962 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2703961024 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_4__ctor_m1543618728_gshared (UnityEvent_4_t4085588227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4__ctor_m1543618728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4)));
		NullCheck((UnityEventBase_t3960448221 *)__this);
		UnityEventBase__ctor_m1087366165((UnityEventBase_t3960448221 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_4_FindMethod_Impl_m96988021_gshared (UnityEvent_4_t4085588227 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4_FindMethod_Impl_m96988021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3940880105* L_2 = (TypeU5BU5D_t3940880105*)((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t3940880105* L_4 = (TypeU5BU5D_t3940880105*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t3940880105* L_6 = (TypeU5BU5D_t3940880105*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		TypeU5BU5D_t3940880105* L_8 = (TypeU5BU5D_t3940880105*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_9);
		MethodInfo_t * L_10 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3940880105*)L_8, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_10;
		goto IL_0048;
	}

IL_0048:
	{
		MethodInfo_t * L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_4_GetDelegate_m1058124614_gshared (UnityEvent_4_t4085588227 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_4_t2756980746 * L_2 = (InvokableCall_4_t2756980746 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (InvokableCall_4_t2756980746 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2703961024 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void EventFunction_1__ctor_m4292798223_gshared (EventFunction_1_t1764640198 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
extern "C"  void EventFunction_1_Invoke_m2429482587_gshared (EventFunction_1_t1764640198 * __this, Il2CppObject * ___handler0, BaseEventData_t3903027533 * ___eventData1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		EventFunction_1_Invoke_m2429482587((EventFunction_1_t1764640198 *)__this->get_prev_9(),___handler0, ___eventData1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___handler0, BaseEventData_t3903027533 * ___eventData1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___handler0, ___eventData1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___handler0, BaseEventData_t3903027533 * ___eventData1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___handler0, ___eventData1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, BaseEventData_t3903027533 * ___eventData1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___handler0, ___eventData1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::BeginInvoke(T1,UnityEngine.EventSystems.BaseEventData,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EventFunction_1_BeginInvoke_m117707366_gshared (EventFunction_1_t1764640198 * __this, Il2CppObject * ___handler0, BaseEventData_t3903027533 * ___eventData1, AsyncCallback_t3962456242 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___handler0;
	__d_args[1] = ___eventData1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void EventFunction_1_EndInvoke_m1395098989_gshared (EventFunction_1_t1764640198 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C"  void IndexedSet_1__ctor_m2250384602_gshared (IndexedSet_1_t234526808 * __this, const MethodInfo* method)
{
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_List_0(L_0);
		Dictionary_2_t3384741 * L_1 = (Dictionary_2_t3384741 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (Dictionary_2_t3384741 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		__this->set_m_Dictionary_1(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C"  void IndexedSet_1_Add_m459949375_gshared (IndexedSet_1_t234526808 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_m_List_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((List_1_t257213610 *)L_0);
		((  void (*) (List_1_t257213610 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t257213610 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Dictionary_2_t3384741 * L_2 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_3 = ___item0;
		List_1_t257213610 * L_4 = (List_1_t257213610 *)__this->get_m_List_0();
		NullCheck((List_1_t257213610 *)L_4);
		int32_t L_5 = ((  int32_t (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t257213610 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t3384741 *)L_2);
		((  void (*) (Dictionary_2_t3384741 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t3384741 *)L_2, (Il2CppObject *)L_3, (int32_t)((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::AddUnique(T)
extern "C"  bool IndexedSet_1_AddUnique_m861843892_gshared (IndexedSet_1_t234526808 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Dictionary_2_t3384741 * L_0 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3384741 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3384741 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t3384741 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0045;
	}

IL_0019:
	{
		List_1_t257213610 * L_3 = (List_1_t257213610 *)__this->get_m_List_0();
		Il2CppObject * L_4 = ___item0;
		NullCheck((List_1_t257213610 *)L_3);
		((  void (*) (List_1_t257213610 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t257213610 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Dictionary_2_t3384741 * L_5 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_6 = ___item0;
		List_1_t257213610 * L_7 = (List_1_t257213610 *)__this->get_m_List_0();
		NullCheck((List_1_t257213610 *)L_7);
		int32_t L_8 = ((  int32_t (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t257213610 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t3384741 *)L_5);
		((  void (*) (Dictionary_2_t3384741 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t3384741 *)L_5, (Il2CppObject *)L_6, (int32_t)((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (bool)1;
		goto IL_0045;
	}

IL_0045:
	{
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C"  bool IndexedSet_1_Remove_m4118314453_gshared (IndexedSet_1_t234526808 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = (int32_t)(-1);
		Dictionary_2_t3384741 * L_0 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3384741 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3384741 *, Il2CppObject *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t3384741 *)L_0, (Il2CppObject *)L_1, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_002b;
	}

IL_001d:
	{
		int32_t L_3 = V_0;
		NullCheck((IndexedSet_1_t234526808 *)__this);
		((  void (*) (IndexedSet_1_t234526808 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((IndexedSet_1_t234526808 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_1 = (bool)1;
		goto IL_002b;
	}

IL_002b:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* IndexedSet_1_GetEnumerator_m3750514392_gshared (IndexedSet_1_t234526808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_GetEnumerator_m3750514392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t3489357830 * L_0 = (NotImplementedException_t3489357830 *)il2cpp_codegen_object_new(NotImplementedException_t3489357830_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m3058704252(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m190983904_gshared (IndexedSet_1_t234526808 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((IndexedSet_1_t234526808 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (IndexedSet_1_t234526808 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((IndexedSet_1_t234526808 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_0 = (Il2CppObject *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C"  void IndexedSet_1_Clear_m4036265083_gshared (IndexedSet_1_t234526808 * __this, const MethodInfo* method)
{
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_m_List_0();
		NullCheck((List_1_t257213610 *)L_0);
		((  void (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t257213610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		Dictionary_2_t3384741 * L_1 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		NullCheck((Dictionary_2_t3384741 *)L_1);
		((  void (*) (Dictionary_2_t3384741 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3384741 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C"  bool IndexedSet_1_Contains_m1525966688_gshared (IndexedSet_1_t234526808 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Dictionary_2_t3384741 * L_0 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3384741 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3384741 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t3384741 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (bool)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void IndexedSet_1_CopyTo_m4232548259_gshared (IndexedSet_1_t234526808 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_m_List_0();
		ObjectU5BU5D_t2843939325* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck((List_1_t257213610 *)L_0);
		((  void (*) (List_1_t257213610 *, ObjectU5BU5D_t2843939325*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t257213610 *)L_0, (ObjectU5BU5D_t2843939325*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C"  int32_t IndexedSet_1_get_Count_m2591381675_gshared (IndexedSet_1_t234526808 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_m_List_0();
		NullCheck((List_1_t257213610 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t257213610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C"  bool IndexedSet_1_get_IsReadOnly_m1939064765_gshared (IndexedSet_1_t234526808 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0008;
	}

IL_0008:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C"  int32_t IndexedSet_1_IndexOf_m241693686_gshared (IndexedSet_1_t234526808 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = (int32_t)(-1);
		Dictionary_2_t3384741 * L_0 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3384741 *)L_0);
		((  bool (*) (Dictionary_2_t3384741 *, Il2CppObject *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t3384741 *)L_0, (Il2CppObject *)L_1, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_2 = V_0;
		V_1 = (int32_t)L_2;
		goto IL_0019;
	}

IL_0019:
	{
		int32_t L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C"  void IndexedSet_1_Insert_m1432638049_gshared (IndexedSet_1_t234526808 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_Insert_m1432638049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2494070935(L_0, (String_t*)_stringLiteral3926843441, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void IndexedSet_1_RemoveAt_m3002732320_gshared (IndexedSet_1_t234526808 * __this, int32_t ___index0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t257213610 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t257213610 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t257213610 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (Il2CppObject *)L_2;
		Dictionary_2_t3384741 * L_3 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_4 = V_0;
		NullCheck((Dictionary_2_t3384741 *)L_3);
		((  bool (*) (Dictionary_2_t3384741 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t3384741 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		int32_t L_5 = ___index0;
		List_1_t257213610 * L_6 = (List_1_t257213610 *)__this->get_m_List_0();
		NullCheck((List_1_t257213610 *)L_6);
		int32_t L_7 = ((  int32_t (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t257213610 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)((int32_t)L_7-(int32_t)1))))))
		{
			goto IL_003f;
		}
	}
	{
		List_1_t257213610 * L_8 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_9 = ___index0;
		NullCheck((List_1_t257213610 *)L_8);
		((  void (*) (List_1_t257213610 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t257213610 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		goto IL_0082;
	}

IL_003f:
	{
		List_1_t257213610 * L_10 = (List_1_t257213610 *)__this->get_m_List_0();
		NullCheck((List_1_t257213610 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t257213610 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_1 = (int32_t)((int32_t)((int32_t)L_11-(int32_t)1));
		List_1_t257213610 * L_12 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_13 = V_1;
		NullCheck((List_1_t257213610 *)L_12);
		Il2CppObject * L_14 = ((  Il2CppObject * (*) (List_1_t257213610 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t257213610 *)L_12, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_2 = (Il2CppObject *)L_14;
		List_1_t257213610 * L_15 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_16 = ___index0;
		Il2CppObject * L_17 = V_2;
		NullCheck((List_1_t257213610 *)L_15);
		((  void (*) (List_1_t257213610 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t257213610 *)L_15, (int32_t)L_16, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Dictionary_2_t3384741 * L_18 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_19 = V_2;
		int32_t L_20 = ___index0;
		NullCheck((Dictionary_2_t3384741 *)L_18);
		((  void (*) (Dictionary_2_t3384741 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t3384741 *)L_18, (Il2CppObject *)L_19, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		List_1_t257213610 * L_21 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_22 = V_1;
		NullCheck((List_1_t257213610 *)L_21);
		((  void (*) (List_1_t257213610 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t257213610 *)L_21, (int32_t)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
	}

IL_0082:
	{
		return;
	}
}
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * IndexedSet_1_get_Item_m3913508799_gshared (IndexedSet_1_t234526808 * __this, int32_t ___index0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t257213610 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t257213610 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t257213610 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (Il2CppObject *)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void IndexedSet_1_set_Item_m4214546195_gshared (IndexedSet_1_t234526808 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t257213610 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t257213610 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t257213610 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (Il2CppObject *)L_2;
		Dictionary_2_t3384741 * L_3 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_4 = V_0;
		NullCheck((Dictionary_2_t3384741 *)L_3);
		((  bool (*) (Dictionary_2_t3384741 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t3384741 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		List_1_t257213610 * L_5 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_6 = ___index0;
		Il2CppObject * L_7 = ___value1;
		NullCheck((List_1_t257213610 *)L_5);
		((  void (*) (List_1_t257213610 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t257213610 *)L_5, (int32_t)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Dictionary_2_t3384741 * L_8 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_9 = V_0;
		int32_t L_10 = ___index0;
		NullCheck((Dictionary_2_t3384741 *)L_8);
		((  void (*) (Dictionary_2_t3384741 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t3384741 *)L_8, (Il2CppObject *)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C"  void IndexedSet_1_RemoveAll_m3453409986_gshared (IndexedSet_1_t234526808 * __this, Predicate_1_t3905400288 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (int32_t)0;
		goto IL_0034;
	}

IL_0008:
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_1 = V_0;
		NullCheck((List_1_t257213610 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t257213610 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t257213610 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_2;
		Predicate_1_t3905400288 * L_3 = ___match0;
		Il2CppObject * L_4 = V_1;
		NullCheck((Predicate_1_t3905400288 *)L_3);
		bool L_5 = ((  bool (*) (Predicate_1_t3905400288 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Predicate_1_t3905400288 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_6 = V_1;
		NullCheck((IndexedSet_1_t234526808 *)__this);
		((  bool (*) (IndexedSet_1_t234526808 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((IndexedSet_1_t234526808 *)__this, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		goto IL_0033;
	}

IL_002f:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0033:
	{
	}

IL_0034:
	{
		int32_t L_8 = V_0;
		List_1_t257213610 * L_9 = (List_1_t257213610 *)__this->get_m_List_0();
		NullCheck((List_1_t257213610 *)L_9);
		int32_t L_10 = ((  int32_t (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t257213610 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C"  void IndexedSet_1_Sort_m2612539420_gshared (IndexedSet_1_t234526808 * __this, Comparison_1_t2855037343 * ___sortLayoutFunction0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_m_List_0();
		Comparison_1_t2855037343 * L_1 = ___sortLayoutFunction0;
		NullCheck((List_1_t257213610 *)L_0);
		((  void (*) (List_1_t257213610 *, Comparison_1_t2855037343 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t257213610 *)L_0, (Comparison_1_t2855037343 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (int32_t)0;
		goto IL_0034;
	}

IL_0014:
	{
		List_1_t257213610 * L_2 = (List_1_t257213610 *)__this->get_m_List_0();
		int32_t L_3 = V_0;
		NullCheck((List_1_t257213610 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (List_1_t257213610 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t257213610 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_4;
		Dictionary_2_t3384741 * L_5 = (Dictionary_2_t3384741 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck((Dictionary_2_t3384741 *)L_5);
		((  void (*) (Dictionary_2_t3384741 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t3384741 *)L_5, (Il2CppObject *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_9 = V_0;
		List_1_t257213610 * L_10 = (List_1_t257213610 *)__this->get_m_List_0();
		NullCheck((List_1_t257213610 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t257213610 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3001242744_gshared (U3CStartU3Ec__Iterator0_t3860393442 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m524356752_gshared (U3CStartU3Ec__Iterator0_t3860393442 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m524356752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t3860393442 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t3860393442 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t3860393442 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d5;
			}
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		ColorTween_t809614380 * L_2 = (ColorTween_t809614380 *)__this->get_address_of_tweenInfo_0();
		bool L_3 = ColorTween_ValidTarget_m376919233((ColorTween_t809614380 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_010f;
	}

IL_003d:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00d6;
	}

IL_004d:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t809614380 * L_5 = (ColorTween_t809614380 *)__this->get_address_of_tweenInfo_0();
		bool L_6 = ColorTween_get_ignoreTimeScale_m1133957174((ColorTween_t809614380 *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t3860393442 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t3860393442 *)(__this));
			goto IL_0075;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_m3673896426(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t3860393442 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		float L_8 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t3860393442 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)((float)G_B8_1+(float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t809614380 * L_10 = (ColorTween_t809614380 *)__this->get_address_of_tweenInfo_0();
		float L_11 = ColorTween_get_duration_m3264097060((ColorTween_t809614380 *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m4133291925(NULL /*static, unused*/, (float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		ColorTween_t809614380 * L_13 = (ColorTween_t809614380 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		ColorTween_TweenValue_m3895102629((ColorTween_t809614380 *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
	}

IL_00d6:
	{
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t809614380 * L_17 = (ColorTween_t809614380 *)__this->get_address_of_tweenInfo_0();
		float L_18 = ColorTween_get_duration_m3264097060((ColorTween_t809614380 *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		ColorTween_t809614380 * L_19 = (ColorTween_t809614380 *)__this->get_address_of_tweenInfo_0();
		ColorTween_TweenValue_m3895102629((ColorTween_t809614380 *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2852443338_gshared (U3CStartU3Ec__Iterator0_t3860393442 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3282639877_gshared (U3CStartU3Ec__Iterator0_t3860393442 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m261027331_gshared (U3CStartU3Ec__Iterator0_t3860393442 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m3175110837_gshared (U3CStartU3Ec__Iterator0_t3860393442 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m3175110837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m2366347741_gshared (U3CStartU3Ec__Iterator0_t30141770 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m4270440387_gshared (U3CStartU3Ec__Iterator0_t30141770 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m4270440387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t30141770 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t30141770 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t30141770 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d5;
			}
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		FloatTween_t1274330004 * L_2 = (FloatTween_t1274330004 *)__this->get_address_of_tweenInfo_0();
		bool L_3 = FloatTween_ValidTarget_m885246038((FloatTween_t1274330004 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_010f;
	}

IL_003d:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00d6;
	}

IL_004d:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t1274330004 * L_5 = (FloatTween_t1274330004 *)__this->get_address_of_tweenInfo_0();
		bool L_6 = FloatTween_get_ignoreTimeScale_m322812475((FloatTween_t1274330004 *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t30141770 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t30141770 *)(__this));
			goto IL_0075;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_m3673896426(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t30141770 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		float L_8 = Time_get_deltaTime_m3562456068(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t30141770 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)((float)G_B8_1+(float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t1274330004 * L_10 = (FloatTween_t1274330004 *)__this->get_address_of_tweenInfo_0();
		float L_11 = FloatTween_get_duration_m1227071020((FloatTween_t1274330004 *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m4133291925(NULL /*static, unused*/, (float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		FloatTween_t1274330004 * L_13 = (FloatTween_t1274330004 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		FloatTween_TweenValue_m52237061((FloatTween_t1274330004 *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
	}

IL_00d6:
	{
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t1274330004 * L_17 = (FloatTween_t1274330004 *)__this->get_address_of_tweenInfo_0();
		float L_18 = FloatTween_get_duration_m1227071020((FloatTween_t1274330004 *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		FloatTween_t1274330004 * L_19 = (FloatTween_t1274330004 *)__this->get_address_of_tweenInfo_0();
		FloatTween_TweenValue_m52237061((FloatTween_t1274330004 *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3156493053_gshared (U3CStartU3Ec__Iterator0_t30141770 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1677159983_gshared (U3CStartU3Ec__Iterator0_t30141770 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3800412744_gshared (U3CStartU3Ec__Iterator0_t30141770 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m656428886_gshared (U3CStartU3Ec__Iterator0_t30141770 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m656428886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m340723704_gshared (TweenRunner_1_t3055525458 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C"  Il2CppObject * TweenRunner_1_Start_m817364799_gshared (Il2CppObject * __this /* static, unused */, ColorTween_t809614380  ___tweenInfo0, const MethodInfo* method)
{
	U3CStartU3Ec__Iterator0_t3860393442 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CStartU3Ec__Iterator0_t3860393442 * L_0 = (U3CStartU3Ec__Iterator0_t3860393442 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t3860393442 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t3860393442 *)L_0;
		U3CStartU3Ec__Iterator0_t3860393442 * L_1 = V_0;
		ColorTween_t809614380  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t3860393442 * L_3 = V_0;
		V_1 = (Il2CppObject *)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m3026112660_gshared (TweenRunner_1_t3055525458 * __this, MonoBehaviour_t3962482529 * ___coroutineContainer0, const MethodInfo* method)
{
	{
		MonoBehaviour_t3962482529 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern "C"  void TweenRunner_1_StartTween_m2247690200_gshared (TweenRunner_1_t3055525458 * __this, ColorTween_t809614380  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m2247690200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t3962482529 * L_0 = (MonoBehaviour_t3962482529 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3661709751(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1132744560, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0022:
	{
		NullCheck((TweenRunner_1_t3055525458 *)__this);
		((  void (*) (TweenRunner_1_t3055525458 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((TweenRunner_1_t3055525458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		MonoBehaviour_t3962482529 * L_2 = (MonoBehaviour_t3962482529 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t1923634451 *)L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m2648350745((Component_t1923634451 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_t1113636619 *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_m921367020((GameObject_t1113636619 *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		ColorTween_TweenValue_m3895102629((ColorTween_t809614380 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0055:
	{
		ColorTween_t809614380  L_5 = ___info0;
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, ColorTween_t809614380 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (ColorTween_t809614380 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		MonoBehaviour_t3962482529 * L_7 = (MonoBehaviour_t3962482529 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t3962482529 *)L_7);
		MonoBehaviour_StartCoroutine_m4001331470((MonoBehaviour_t3962482529 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StopTween()
extern "C"  void TweenRunner_1_StopTween_m1830357468_gshared (TweenRunner_1_t3055525458 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_t3962482529 * L_1 = (MonoBehaviour_t3962482529 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t3962482529 *)L_1);
		MonoBehaviour_StopCoroutine_m1038915083((MonoBehaviour_t3962482529 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		__this->set_m_Tween_1((Il2CppObject *)NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m3053831591_gshared (TweenRunner_1_t3520241082 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
extern "C"  Il2CppObject * TweenRunner_1_Start_m3757154622_gshared (Il2CppObject * __this /* static, unused */, FloatTween_t1274330004  ___tweenInfo0, const MethodInfo* method)
{
	U3CStartU3Ec__Iterator0_t30141770 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CStartU3Ec__Iterator0_t30141770 * L_0 = (U3CStartU3Ec__Iterator0_t30141770 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t30141770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t30141770 *)L_0;
		U3CStartU3Ec__Iterator0_t30141770 * L_1 = V_0;
		FloatTween_t1274330004  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t30141770 * L_3 = V_0;
		V_1 = (Il2CppObject *)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m1266084429_gshared (TweenRunner_1_t3520241082 * __this, MonoBehaviour_t3962482529 * ___coroutineContainer0, const MethodInfo* method)
{
	{
		MonoBehaviour_t3962482529 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
extern "C"  void TweenRunner_1_StartTween_m1055628540_gshared (TweenRunner_1_t3520241082 * __this, FloatTween_t1274330004  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m1055628540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t3962482529 * L_0 = (MonoBehaviour_t3962482529 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, (Object_t631007953 *)L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3661709751(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1132744560, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0022:
	{
		NullCheck((TweenRunner_1_t3520241082 *)__this);
		((  void (*) (TweenRunner_1_t3520241082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((TweenRunner_1_t3520241082 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		MonoBehaviour_t3962482529 * L_2 = (MonoBehaviour_t3962482529 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t1923634451 *)L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m2648350745((Component_t1923634451 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_t1113636619 *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_m921367020((GameObject_t1113636619 *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		FloatTween_TweenValue_m52237061((FloatTween_t1274330004 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0055:
	{
		FloatTween_t1274330004  L_5 = ___info0;
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, FloatTween_t1274330004 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (FloatTween_t1274330004 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		MonoBehaviour_t3962482529 * L_7 = (MonoBehaviour_t3962482529 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t3962482529 *)L_7);
		MonoBehaviour_StartCoroutine_m4001331470((MonoBehaviour_t3962482529 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StopTween()
extern "C"  void TweenRunner_1_StopTween_m3457627707_gshared (TweenRunner_1_t3520241082 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_t3962482529 * L_1 = (MonoBehaviour_t3962482529 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t3962482529 *)L_1);
		MonoBehaviour_StopCoroutine_m1038915083((MonoBehaviour_t3962482529 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		__this->set_m_Tween_1((Il2CppObject *)NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C"  List_1_t128053199 * ListPool_1_Get_m2031605680_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t128053199 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t4122643707 * L_0 = ((ListPool_1_t3980534944_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t4122643707 *)L_0);
		List_1_t128053199 * L_1 = ((  List_1_t128053199 * (*) (ObjectPool_1_t4122643707 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t4122643707 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t128053199 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t128053199 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m188599205_gshared (Il2CppObject * __this /* static, unused */, List_1_t128053199 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t4122643707 * L_0 = ((ListPool_1_t3980534944_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t128053199 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t4122643707 *)L_0);
		((  void (*) (ObjectPool_1_t4122643707 *, List_1_t128053199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t4122643707 *)L_0, (List_1_t128053199 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C"  void ListPool_1__cctor_m647010813_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t712889340 * L_1 = (UnityAction_1_t712889340 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t712889340 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t4122643707 * L_2 = (ObjectPool_1_t4122643707 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t4122643707 *, UnityAction_1_t712889340 *, UnityAction_1_t712889340 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t712889340 *)NULL, (UnityAction_1_t712889340 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t3980534944_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m1565128702_gshared (Il2CppObject * __this /* static, unused */, List_1_t128053199 * ___l0, const MethodInfo* method)
{
	{
		List_1_t128053199 * L_0 = ___l0;
		NullCheck((List_1_t128053199 *)L_0);
		((  void (*) (List_1_t128053199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t128053199 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
extern "C"  List_1_t257213610 * ListPool_1_Get_m1670010485_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t257213610 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t4251804118 * L_0 = ((ListPool_1_t4109695355_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t4251804118 *)L_0);
		List_1_t257213610 * L_1 = ((  List_1_t257213610 * (*) (ObjectPool_1_t4251804118 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t4251804118 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t257213610 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t257213610 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m957266927_gshared (Il2CppObject * __this /* static, unused */, List_1_t257213610 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t4251804118 * L_0 = ((ListPool_1_t4109695355_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t257213610 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t4251804118 *)L_0);
		((  void (*) (ObjectPool_1_t4251804118 *, List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t4251804118 *)L_0, (List_1_t257213610 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::.cctor()
extern "C"  void ListPool_1__cctor_m1477269088_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t842049751 * L_1 = (UnityAction_1_t842049751 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t842049751 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t4251804118 * L_2 = (ObjectPool_1_t4251804118 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t4251804118 *, UnityAction_1_t842049751 *, UnityAction_1_t842049751 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t842049751 *)NULL, (UnityAction_1_t842049751 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t4109695355_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m2790550420_gshared (Il2CppObject * __this /* static, unused */, List_1_t257213610 * ___l0, const MethodInfo* method)
{
	{
		List_1_t257213610 * L_0 = ___l0;
		NullCheck((List_1_t257213610 *)L_0);
		((  void (*) (List_1_t257213610 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t257213610 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
extern "C"  List_1_t4072576034 * ListPool_1_Get_m2875520964_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t4072576034 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3772199246 * L_0 = ((ListPool_1_t3630090483_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3772199246 *)L_0);
		List_1_t4072576034 * L_1 = ((  List_1_t4072576034 * (*) (ObjectPool_1_t3772199246 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t3772199246 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t4072576034 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t4072576034 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m2857821093_gshared (Il2CppObject * __this /* static, unused */, List_1_t4072576034 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3772199246 * L_0 = ((ListPool_1_t3630090483_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t4072576034 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3772199246 *)L_0);
		((  void (*) (ObjectPool_1_t3772199246 *, List_1_t4072576034 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3772199246 *)L_0, (List_1_t4072576034 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
extern "C"  void ListPool_1__cctor_m1390066271_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t362444879 * L_1 = (UnityAction_1_t362444879 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t362444879 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t3772199246 * L_2 = (ObjectPool_1_t3772199246 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t3772199246 *, UnityAction_1_t362444879 *, UnityAction_1_t362444879 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t362444879 *)NULL, (UnityAction_1_t362444879 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t3630090483_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m3132766965_gshared (Il2CppObject * __this /* static, unused */, List_1_t4072576034 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4072576034 * L_0 = ___l0;
		NullCheck((List_1_t4072576034 *)L_0);
		((  void (*) (List_1_t4072576034 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t4072576034 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
extern "C"  List_1_t1234605051 * ListPool_1_Get_m738675669_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t1234605051 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t934228263 * L_0 = ((ListPool_1_t792119500_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t934228263 *)L_0);
		List_1_t1234605051 * L_1 = ((  List_1_t1234605051 * (*) (ObjectPool_1_t934228263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t934228263 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1234605051 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t1234605051 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1246825787_gshared (Il2CppObject * __this /* static, unused */, List_1_t1234605051 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t934228263 * L_0 = ((ListPool_1_t792119500_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1234605051 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t934228263 *)L_0);
		((  void (*) (ObjectPool_1_t934228263 *, List_1_t1234605051 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t934228263 *)L_0, (List_1_t1234605051 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C"  void ListPool_1__cctor_m995356616_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t1819441192 * L_1 = (UnityAction_1_t1819441192 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t1819441192 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t934228263 * L_2 = (ObjectPool_1_t934228263 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t934228263 *, UnityAction_1_t1819441192 *, UnityAction_1_t1819441192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t1819441192 *)NULL, (UnityAction_1_t1819441192 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t792119500_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m1647198399_gshared (Il2CppObject * __this /* static, unused */, List_1_t1234605051 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1234605051 * L_0 = ___l0;
		NullCheck((List_1_t1234605051 *)L_0);
		((  void (*) (List_1_t1234605051 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1234605051 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C"  List_1_t3628304265 * ListPool_1_Get_m3176650548_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t3628304265 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3327927477 * L_0 = ((ListPool_1_t3185818714_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3327927477 *)L_0);
		List_1_t3628304265 * L_1 = ((  List_1_t3628304265 * (*) (ObjectPool_1_t3327927477 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t3327927477 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t3628304265 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t3628304265 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m591299672_gshared (Il2CppObject * __this /* static, unused */, List_1_t3628304265 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3327927477 * L_0 = ((ListPool_1_t3185818714_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t3628304265 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3327927477 *)L_0);
		((  void (*) (ObjectPool_1_t3327927477 *, List_1_t3628304265 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3327927477 *)L_0, (List_1_t3628304265 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C"  void ListPool_1__cctor_m3480273184_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t4213140406 * L_1 = (UnityAction_1_t4213140406 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t4213140406 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t3327927477 * L_2 = (ObjectPool_1_t3327927477 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t3327927477 *, UnityAction_1_t4213140406 *, UnityAction_1_t4213140406 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t4213140406 *)NULL, (UnityAction_1_t4213140406 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t3185818714_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m579534218_gshared (Il2CppObject * __this /* static, unused */, List_1_t3628304265 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3628304265 * L_0 = ___l0;
		NullCheck((List_1_t3628304265 *)L_0);
		((  void (*) (List_1_t3628304265 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t3628304265 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
extern "C"  List_1_t899420910 * ListPool_1_Get_m3176649063_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t899420910 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t599044122 * L_0 = ((ListPool_1_t456935359_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t599044122 *)L_0);
		List_1_t899420910 * L_1 = ((  List_1_t899420910 * (*) (ObjectPool_1_t599044122 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t599044122 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t899420910 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t899420910 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m4113115349_gshared (Il2CppObject * __this /* static, unused */, List_1_t899420910 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t599044122 * L_0 = ((ListPool_1_t456935359_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t899420910 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t599044122 *)L_0);
		((  void (*) (ObjectPool_1_t599044122 *, List_1_t899420910 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t599044122 *)L_0, (List_1_t899420910 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
extern "C"  void ListPool_1__cctor_m4085211983_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t1484257051 * L_1 = (UnityAction_1_t1484257051 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t1484257051 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t599044122 * L_2 = (ObjectPool_1_t599044122 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t599044122 *, UnityAction_1_t1484257051 *, UnityAction_1_t1484257051 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t1484257051 *)NULL, (UnityAction_1_t1484257051 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t456935359_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m657844629_gshared (Il2CppObject * __this /* static, unused */, List_1_t899420910 * ___l0, const MethodInfo* method)
{
	{
		List_1_t899420910 * L_0 = ___l0;
		NullCheck((List_1_t899420910 *)L_0);
		((  void (*) (List_1_t899420910 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t899420910 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Get()
extern "C"  List_1_t496136383 * ListPool_1_Get_m3176656818_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t496136383 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t195759595 * L_0 = ((ListPool_1_t53650832_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t195759595 *)L_0);
		List_1_t496136383 * L_1 = ((  List_1_t496136383 * (*) (ObjectPool_1_t195759595 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t195759595 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t496136383 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t496136383 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1363449253_gshared (Il2CppObject * __this /* static, unused */, List_1_t496136383 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t195759595 * L_0 = ((ListPool_1_t53650832_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t496136383 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t195759595 *)L_0);
		((  void (*) (ObjectPool_1_t195759595 *, List_1_t496136383 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t195759595 *)L_0, (List_1_t496136383 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::.cctor()
extern "C"  void ListPool_1__cctor_m704263611_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t1080972524 * L_1 = (UnityAction_1_t1080972524 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t1080972524 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t195759595 * L_2 = (ObjectPool_1_t195759595 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t195759595 *, UnityAction_1_t1080972524 *, UnityAction_1_t1080972524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t1080972524 *)NULL, (UnityAction_1_t1080972524 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t53650832_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m2283646495_gshared (Il2CppObject * __this /* static, unused */, List_1_t496136383 * ___l0, const MethodInfo* method)
{
	{
		List_1_t496136383 * L_0 = ___l0;
		NullCheck((List_1_t496136383 *)L_0);
		((  void (*) (List_1_t496136383 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t496136383 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void ObjectPool_1__ctor_m2535233435_gshared (ObjectPool_1_t2779729376 * __this, UnityAction_1_t3664942305 * ___actionOnGet0, UnityAction_1_t3664942305 * ___actionOnRelease1, const MethodInfo* method)
{
	{
		Stack_1_t3923495619 * L_0 = (Stack_1_t3923495619 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Stack_1_t3923495619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_Stack_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3664942305 * L_1 = ___actionOnGet0;
		__this->set_m_ActionOnGet_1(L_1);
		UnityAction_1_t3664942305 * L_2 = ___actionOnRelease1;
		__this->set_m_ActionOnRelease_2(L_2);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C"  int32_t ObjectPool_1_get_countAll_m819305395_gshared (ObjectPool_1_t2779729376 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U3CcountAllU3Ek__BackingField_3();
		V_0 = (int32_t)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C"  void ObjectPool_1_set_countAll_m3507126863_gshared (ObjectPool_1_t2779729376 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CcountAllU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C"  int32_t ObjectPool_1_get_countActive_m807006650_gshared (ObjectPool_1_t2779729376 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		NullCheck((ObjectPool_1_t2779729376 *)__this);
		int32_t L_0 = ((  int32_t (*) (ObjectPool_1_t2779729376 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2779729376 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t2779729376 *)__this);
		int32_t L_1 = ((  int32_t (*) (ObjectPool_1_t2779729376 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ObjectPool_1_t2779729376 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C"  int32_t ObjectPool_1_get_countInactive_m526975942_gshared (ObjectPool_1_t2779729376 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Stack_1_t3923495619 * L_0 = (Stack_1_t3923495619 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3923495619 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t3923495619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t3923495619 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern "C"  Il2CppObject * ObjectPool_1_Get_m3351668383_gshared (ObjectPool_1_t2779729376 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Stack_1_t3923495619 * L_0 = (Stack_1_t3923495619 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3923495619 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t3923495619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t3923495619 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (Il2CppObject *)L_2;
		NullCheck((ObjectPool_1_t2779729376 *)__this);
		int32_t L_3 = ((  int32_t (*) (ObjectPool_1_t2779729376 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2779729376 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t2779729376 *)__this);
		((  void (*) (ObjectPool_1_t2779729376 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((ObjectPool_1_t2779729376 *)__this, (int32_t)((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		goto IL_003a;
	}

IL_002c:
	{
		Stack_1_t3923495619 * L_4 = (Stack_1_t3923495619 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3923495619 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Stack_1_t3923495619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Stack_1_t3923495619 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (Il2CppObject *)L_5;
	}

IL_003a:
	{
		UnityAction_1_t3664942305 * L_6 = (UnityAction_1_t3664942305 *)__this->get_m_ActionOnGet_1();
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		UnityAction_1_t3664942305 * L_7 = (UnityAction_1_t3664942305 *)__this->get_m_ActionOnGet_1();
		Il2CppObject * L_8 = V_0;
		NullCheck((UnityAction_1_t3664942305 *)L_7);
		((  void (*) (UnityAction_1_t3664942305 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_1_t3664942305 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0051:
	{
		Il2CppObject * L_9 = V_0;
		V_1 = (Il2CppObject *)L_9;
		goto IL_0058;
	}

IL_0058:
	{
		Il2CppObject * L_10 = V_1;
		return L_10;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern "C"  void ObjectPool_1_Release_m3263354170_gshared (ObjectPool_1_t2779729376 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_m3263354170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t3923495619 * L_0 = (Stack_1_t3923495619 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3923495619 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t3923495619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t3923495619 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		Stack_1_t3923495619 * L_2 = (Stack_1_t3923495619 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3923495619 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Stack_1_t3923495619 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Stack_1_t3923495619 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		Il2CppObject * L_4 = ___element0;
		bool L_5 = Object_ReferenceEquals_m610702577(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral46997234, /*hidden argument*/NULL);
	}

IL_003c:
	{
		UnityAction_1_t3664942305 * L_6 = (UnityAction_1_t3664942305 *)__this->get_m_ActionOnRelease_2();
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		UnityAction_1_t3664942305 * L_7 = (UnityAction_1_t3664942305 *)__this->get_m_ActionOnRelease_2();
		Il2CppObject * L_8 = ___element0;
		NullCheck((UnityAction_1_t3664942305 *)L_7);
		((  void (*) (UnityAction_1_t3664942305 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_1_t3664942305 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0053:
	{
		Stack_1_t3923495619 * L_9 = (Stack_1_t3923495619 *)__this->get_m_Stack_0();
		Il2CppObject * L_10 = ___element0;
		NullCheck((Stack_1_t3923495619 *)L_9);
		((  void (*) (Stack_1_t3923495619 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Stack_1_t3923495619 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
