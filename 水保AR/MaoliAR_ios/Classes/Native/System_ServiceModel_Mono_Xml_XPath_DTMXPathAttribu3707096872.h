﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.DTMXPathAttributeNode2
struct  DTMXPathAttributeNode2_t3707096873 
{
public:
	// System.Int32 Mono.Xml.XPath.DTMXPathAttributeNode2::OwnerElement
	int32_t ___OwnerElement_0;
	// System.Int32 Mono.Xml.XPath.DTMXPathAttributeNode2::NextAttribute
	int32_t ___NextAttribute_1;
	// System.Int32 Mono.Xml.XPath.DTMXPathAttributeNode2::LocalName
	int32_t ___LocalName_2;
	// System.Int32 Mono.Xml.XPath.DTMXPathAttributeNode2::NamespaceURI
	int32_t ___NamespaceURI_3;
	// System.Int32 Mono.Xml.XPath.DTMXPathAttributeNode2::Prefix
	int32_t ___Prefix_4;
	// System.Int32 Mono.Xml.XPath.DTMXPathAttributeNode2::Value
	int32_t ___Value_5;
	// System.Int32 Mono.Xml.XPath.DTMXPathAttributeNode2::LineNumber
	int32_t ___LineNumber_6;
	// System.Int32 Mono.Xml.XPath.DTMXPathAttributeNode2::LinePosition
	int32_t ___LinePosition_7;

public:
	inline static int32_t get_offset_of_OwnerElement_0() { return static_cast<int32_t>(offsetof(DTMXPathAttributeNode2_t3707096873, ___OwnerElement_0)); }
	inline int32_t get_OwnerElement_0() const { return ___OwnerElement_0; }
	inline int32_t* get_address_of_OwnerElement_0() { return &___OwnerElement_0; }
	inline void set_OwnerElement_0(int32_t value)
	{
		___OwnerElement_0 = value;
	}

	inline static int32_t get_offset_of_NextAttribute_1() { return static_cast<int32_t>(offsetof(DTMXPathAttributeNode2_t3707096873, ___NextAttribute_1)); }
	inline int32_t get_NextAttribute_1() const { return ___NextAttribute_1; }
	inline int32_t* get_address_of_NextAttribute_1() { return &___NextAttribute_1; }
	inline void set_NextAttribute_1(int32_t value)
	{
		___NextAttribute_1 = value;
	}

	inline static int32_t get_offset_of_LocalName_2() { return static_cast<int32_t>(offsetof(DTMXPathAttributeNode2_t3707096873, ___LocalName_2)); }
	inline int32_t get_LocalName_2() const { return ___LocalName_2; }
	inline int32_t* get_address_of_LocalName_2() { return &___LocalName_2; }
	inline void set_LocalName_2(int32_t value)
	{
		___LocalName_2 = value;
	}

	inline static int32_t get_offset_of_NamespaceURI_3() { return static_cast<int32_t>(offsetof(DTMXPathAttributeNode2_t3707096873, ___NamespaceURI_3)); }
	inline int32_t get_NamespaceURI_3() const { return ___NamespaceURI_3; }
	inline int32_t* get_address_of_NamespaceURI_3() { return &___NamespaceURI_3; }
	inline void set_NamespaceURI_3(int32_t value)
	{
		___NamespaceURI_3 = value;
	}

	inline static int32_t get_offset_of_Prefix_4() { return static_cast<int32_t>(offsetof(DTMXPathAttributeNode2_t3707096873, ___Prefix_4)); }
	inline int32_t get_Prefix_4() const { return ___Prefix_4; }
	inline int32_t* get_address_of_Prefix_4() { return &___Prefix_4; }
	inline void set_Prefix_4(int32_t value)
	{
		___Prefix_4 = value;
	}

	inline static int32_t get_offset_of_Value_5() { return static_cast<int32_t>(offsetof(DTMXPathAttributeNode2_t3707096873, ___Value_5)); }
	inline int32_t get_Value_5() const { return ___Value_5; }
	inline int32_t* get_address_of_Value_5() { return &___Value_5; }
	inline void set_Value_5(int32_t value)
	{
		___Value_5 = value;
	}

	inline static int32_t get_offset_of_LineNumber_6() { return static_cast<int32_t>(offsetof(DTMXPathAttributeNode2_t3707096873, ___LineNumber_6)); }
	inline int32_t get_LineNumber_6() const { return ___LineNumber_6; }
	inline int32_t* get_address_of_LineNumber_6() { return &___LineNumber_6; }
	inline void set_LineNumber_6(int32_t value)
	{
		___LineNumber_6 = value;
	}

	inline static int32_t get_offset_of_LinePosition_7() { return static_cast<int32_t>(offsetof(DTMXPathAttributeNode2_t3707096873, ___LinePosition_7)); }
	inline int32_t get_LinePosition_7() const { return ___LinePosition_7; }
	inline int32_t* get_address_of_LinePosition_7() { return &___LinePosition_7; }
	inline void set_LinePosition_7(int32_t value)
	{
		___LinePosition_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
