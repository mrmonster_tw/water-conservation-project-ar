﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.HttpContext
struct HttpContext_t1969259010;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpServerUtility
struct  HttpServerUtility_t3850680178  : public Il2CppObject
{
public:
	// System.Web.HttpContext System.Web.HttpServerUtility::context
	HttpContext_t1969259010 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(HttpServerUtility_t3850680178, ___context_0)); }
	inline HttpContext_t1969259010 * get_context_0() const { return ___context_0; }
	inline HttpContext_t1969259010 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(HttpContext_t1969259010 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier(&___context_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
