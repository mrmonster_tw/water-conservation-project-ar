﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.OperationContractAttribute
struct  OperationContractAttribute_t3102514551  : public Attribute_t861562559
{
public:
	// System.String System.ServiceModel.OperationContractAttribute::action
	String_t* ___action_0;
	// System.String System.ServiceModel.OperationContractAttribute::reply_action
	String_t* ___reply_action_1;
	// System.String System.ServiceModel.OperationContractAttribute::name
	String_t* ___name_2;
	// System.Boolean System.ServiceModel.OperationContractAttribute::is_initiating
	bool ___is_initiating_3;
	// System.Boolean System.ServiceModel.OperationContractAttribute::is_terminating
	bool ___is_terminating_4;
	// System.Boolean System.ServiceModel.OperationContractAttribute::is_oneway
	bool ___is_oneway_5;
	// System.Boolean System.ServiceModel.OperationContractAttribute::is_async
	bool ___is_async_6;
	// System.Net.Security.ProtectionLevel System.ServiceModel.OperationContractAttribute::protection_level
	int32_t ___protection_level_7;
	// System.Boolean System.ServiceModel.OperationContractAttribute::has_protection_level
	bool ___has_protection_level_8;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(OperationContractAttribute_t3102514551, ___action_0)); }
	inline String_t* get_action_0() const { return ___action_0; }
	inline String_t** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(String_t* value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier(&___action_0, value);
	}

	inline static int32_t get_offset_of_reply_action_1() { return static_cast<int32_t>(offsetof(OperationContractAttribute_t3102514551, ___reply_action_1)); }
	inline String_t* get_reply_action_1() const { return ___reply_action_1; }
	inline String_t** get_address_of_reply_action_1() { return &___reply_action_1; }
	inline void set_reply_action_1(String_t* value)
	{
		___reply_action_1 = value;
		Il2CppCodeGenWriteBarrier(&___reply_action_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(OperationContractAttribute_t3102514551, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_is_initiating_3() { return static_cast<int32_t>(offsetof(OperationContractAttribute_t3102514551, ___is_initiating_3)); }
	inline bool get_is_initiating_3() const { return ___is_initiating_3; }
	inline bool* get_address_of_is_initiating_3() { return &___is_initiating_3; }
	inline void set_is_initiating_3(bool value)
	{
		___is_initiating_3 = value;
	}

	inline static int32_t get_offset_of_is_terminating_4() { return static_cast<int32_t>(offsetof(OperationContractAttribute_t3102514551, ___is_terminating_4)); }
	inline bool get_is_terminating_4() const { return ___is_terminating_4; }
	inline bool* get_address_of_is_terminating_4() { return &___is_terminating_4; }
	inline void set_is_terminating_4(bool value)
	{
		___is_terminating_4 = value;
	}

	inline static int32_t get_offset_of_is_oneway_5() { return static_cast<int32_t>(offsetof(OperationContractAttribute_t3102514551, ___is_oneway_5)); }
	inline bool get_is_oneway_5() const { return ___is_oneway_5; }
	inline bool* get_address_of_is_oneway_5() { return &___is_oneway_5; }
	inline void set_is_oneway_5(bool value)
	{
		___is_oneway_5 = value;
	}

	inline static int32_t get_offset_of_is_async_6() { return static_cast<int32_t>(offsetof(OperationContractAttribute_t3102514551, ___is_async_6)); }
	inline bool get_is_async_6() const { return ___is_async_6; }
	inline bool* get_address_of_is_async_6() { return &___is_async_6; }
	inline void set_is_async_6(bool value)
	{
		___is_async_6 = value;
	}

	inline static int32_t get_offset_of_protection_level_7() { return static_cast<int32_t>(offsetof(OperationContractAttribute_t3102514551, ___protection_level_7)); }
	inline int32_t get_protection_level_7() const { return ___protection_level_7; }
	inline int32_t* get_address_of_protection_level_7() { return &___protection_level_7; }
	inline void set_protection_level_7(int32_t value)
	{
		___protection_level_7 = value;
	}

	inline static int32_t get_offset_of_has_protection_level_8() { return static_cast<int32_t>(offsetof(OperationContractAttribute_t3102514551, ___has_protection_level_8)); }
	inline bool get_has_protection_level_8() const { return ___has_protection_level_8; }
	inline bool* get_address_of_has_protection_level_8() { return &___has_protection_level_8; }
	inline void set_has_protection_level_8(bool value)
	{
		___has_protection_level_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
