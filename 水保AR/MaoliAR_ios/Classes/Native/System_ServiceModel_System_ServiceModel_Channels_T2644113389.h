﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_XmlBinaryW1730638232.h"

// System.Collections.Generic.List`1<System.Xml.XmlDictionaryString>
struct List_1_t681227712;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpBinaryFrameManager/MyXmlBinaryWriterSession
struct  MyXmlBinaryWriterSession_t2644113389  : public XmlBinaryWriterSession_t1730638232
{
public:
	// System.Collections.Generic.List`1<System.Xml.XmlDictionaryString> System.ServiceModel.Channels.TcpBinaryFrameManager/MyXmlBinaryWriterSession::List
	List_1_t681227712 * ___List_1;

public:
	inline static int32_t get_offset_of_List_1() { return static_cast<int32_t>(offsetof(MyXmlBinaryWriterSession_t2644113389, ___List_1)); }
	inline List_1_t681227712 * get_List_1() const { return ___List_1; }
	inline List_1_t681227712 ** get_address_of_List_1() { return &___List_1; }
	inline void set_List_1(List_1_t681227712 * value)
	{
		___List_1 = value;
		Il2CppCodeGenWriteBarrier(&___List_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
