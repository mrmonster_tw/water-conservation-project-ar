﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// Level_Controller
struct Level_Controller_t1433348427;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level_03_Controller
struct  Level_03_Controller_t697859203  : public MonoBehaviour_t3962482529
{
public:
	// Level_Controller Level_03_Controller::lvManager
	Level_Controller_t1433348427 * ___lvManager_2;
	// UnityEngine.GameObject[] Level_03_Controller::all_bad_car
	GameObjectU5BU5D_t3328599146* ___all_bad_car_3;
	// System.Boolean Level_03_Controller::do_once
	bool ___do_once_4;
	// UnityEngine.GameObject Level_03_Controller::win_panel
	GameObject_t1113636619 * ___win_panel_5;

public:
	inline static int32_t get_offset_of_lvManager_2() { return static_cast<int32_t>(offsetof(Level_03_Controller_t697859203, ___lvManager_2)); }
	inline Level_Controller_t1433348427 * get_lvManager_2() const { return ___lvManager_2; }
	inline Level_Controller_t1433348427 ** get_address_of_lvManager_2() { return &___lvManager_2; }
	inline void set_lvManager_2(Level_Controller_t1433348427 * value)
	{
		___lvManager_2 = value;
		Il2CppCodeGenWriteBarrier(&___lvManager_2, value);
	}

	inline static int32_t get_offset_of_all_bad_car_3() { return static_cast<int32_t>(offsetof(Level_03_Controller_t697859203, ___all_bad_car_3)); }
	inline GameObjectU5BU5D_t3328599146* get_all_bad_car_3() const { return ___all_bad_car_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_all_bad_car_3() { return &___all_bad_car_3; }
	inline void set_all_bad_car_3(GameObjectU5BU5D_t3328599146* value)
	{
		___all_bad_car_3 = value;
		Il2CppCodeGenWriteBarrier(&___all_bad_car_3, value);
	}

	inline static int32_t get_offset_of_do_once_4() { return static_cast<int32_t>(offsetof(Level_03_Controller_t697859203, ___do_once_4)); }
	inline bool get_do_once_4() const { return ___do_once_4; }
	inline bool* get_address_of_do_once_4() { return &___do_once_4; }
	inline void set_do_once_4(bool value)
	{
		___do_once_4 = value;
	}

	inline static int32_t get_offset_of_win_panel_5() { return static_cast<int32_t>(offsetof(Level_03_Controller_t697859203, ___win_panel_5)); }
	inline GameObject_t1113636619 * get_win_panel_5() const { return ___win_panel_5; }
	inline GameObject_t1113636619 ** get_address_of_win_panel_5() { return &___win_panel_5; }
	inline void set_win_panel_5(GameObject_t1113636619 * value)
	{
		___win_panel_5 = value;
		Il2CppCodeGenWriteBarrier(&___win_panel_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
