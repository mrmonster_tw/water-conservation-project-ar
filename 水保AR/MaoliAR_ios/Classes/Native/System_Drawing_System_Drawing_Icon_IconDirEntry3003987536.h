﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.Icon/IconDirEntry
struct  IconDirEntry_t3003987536 
{
public:
	// System.Byte System.Drawing.Icon/IconDirEntry::width
	uint8_t ___width_0;
	// System.Byte System.Drawing.Icon/IconDirEntry::height
	uint8_t ___height_1;
	// System.Byte System.Drawing.Icon/IconDirEntry::colorCount
	uint8_t ___colorCount_2;
	// System.Byte System.Drawing.Icon/IconDirEntry::reserved
	uint8_t ___reserved_3;
	// System.UInt16 System.Drawing.Icon/IconDirEntry::planes
	uint16_t ___planes_4;
	// System.UInt16 System.Drawing.Icon/IconDirEntry::bitCount
	uint16_t ___bitCount_5;
	// System.UInt32 System.Drawing.Icon/IconDirEntry::bytesInRes
	uint32_t ___bytesInRes_6;
	// System.UInt32 System.Drawing.Icon/IconDirEntry::imageOffset
	uint32_t ___imageOffset_7;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(IconDirEntry_t3003987536, ___width_0)); }
	inline uint8_t get_width_0() const { return ___width_0; }
	inline uint8_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(uint8_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(IconDirEntry_t3003987536, ___height_1)); }
	inline uint8_t get_height_1() const { return ___height_1; }
	inline uint8_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(uint8_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_colorCount_2() { return static_cast<int32_t>(offsetof(IconDirEntry_t3003987536, ___colorCount_2)); }
	inline uint8_t get_colorCount_2() const { return ___colorCount_2; }
	inline uint8_t* get_address_of_colorCount_2() { return &___colorCount_2; }
	inline void set_colorCount_2(uint8_t value)
	{
		___colorCount_2 = value;
	}

	inline static int32_t get_offset_of_reserved_3() { return static_cast<int32_t>(offsetof(IconDirEntry_t3003987536, ___reserved_3)); }
	inline uint8_t get_reserved_3() const { return ___reserved_3; }
	inline uint8_t* get_address_of_reserved_3() { return &___reserved_3; }
	inline void set_reserved_3(uint8_t value)
	{
		___reserved_3 = value;
	}

	inline static int32_t get_offset_of_planes_4() { return static_cast<int32_t>(offsetof(IconDirEntry_t3003987536, ___planes_4)); }
	inline uint16_t get_planes_4() const { return ___planes_4; }
	inline uint16_t* get_address_of_planes_4() { return &___planes_4; }
	inline void set_planes_4(uint16_t value)
	{
		___planes_4 = value;
	}

	inline static int32_t get_offset_of_bitCount_5() { return static_cast<int32_t>(offsetof(IconDirEntry_t3003987536, ___bitCount_5)); }
	inline uint16_t get_bitCount_5() const { return ___bitCount_5; }
	inline uint16_t* get_address_of_bitCount_5() { return &___bitCount_5; }
	inline void set_bitCount_5(uint16_t value)
	{
		___bitCount_5 = value;
	}

	inline static int32_t get_offset_of_bytesInRes_6() { return static_cast<int32_t>(offsetof(IconDirEntry_t3003987536, ___bytesInRes_6)); }
	inline uint32_t get_bytesInRes_6() const { return ___bytesInRes_6; }
	inline uint32_t* get_address_of_bytesInRes_6() { return &___bytesInRes_6; }
	inline void set_bytesInRes_6(uint32_t value)
	{
		___bytesInRes_6 = value;
	}

	inline static int32_t get_offset_of_imageOffset_7() { return static_cast<int32_t>(offsetof(IconDirEntry_t3003987536, ___imageOffset_7)); }
	inline uint32_t get_imageOffset_7() const { return ___imageOffset_7; }
	inline uint32_t* get_address_of_imageOffset_7() { return &___imageOffset_7; }
	inline void set_imageOffset_7(uint32_t value)
	{
		___imageOffset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
