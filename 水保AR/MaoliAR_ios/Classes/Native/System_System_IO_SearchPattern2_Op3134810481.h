﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_System_IO_SearchPattern2_OpCode3581930920.h"

// System.String
struct String_t;
// System.IO.SearchPattern2/Op
struct Op_t3134810481;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.SearchPattern2/Op
struct  Op_t3134810481  : public Il2CppObject
{
public:
	// System.IO.SearchPattern2/OpCode System.IO.SearchPattern2/Op::Code
	int32_t ___Code_0;
	// System.String System.IO.SearchPattern2/Op::Argument
	String_t* ___Argument_1;
	// System.IO.SearchPattern2/Op System.IO.SearchPattern2/Op::Next
	Op_t3134810481 * ___Next_2;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(Op_t3134810481, ___Code_0)); }
	inline int32_t get_Code_0() const { return ___Code_0; }
	inline int32_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(int32_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Argument_1() { return static_cast<int32_t>(offsetof(Op_t3134810481, ___Argument_1)); }
	inline String_t* get_Argument_1() const { return ___Argument_1; }
	inline String_t** get_address_of_Argument_1() { return &___Argument_1; }
	inline void set_Argument_1(String_t* value)
	{
		___Argument_1 = value;
		Il2CppCodeGenWriteBarrier(&___Argument_1, value);
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Op_t3134810481, ___Next_2)); }
	inline Op_t3134810481 * get_Next_2() const { return ___Next_2; }
	inline Op_t3134810481 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Op_t3134810481 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier(&___Next_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
