﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.Type
struct Type_t;
// System.Web.UI.ControlBuilderAttribute
struct ControlBuilderAttribute_t1654838440;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ControlBuilderAttribute
struct  ControlBuilderAttribute_t1654838440  : public Attribute_t861562559
{
public:
	// System.Type System.Web.UI.ControlBuilderAttribute::builderType
	Type_t * ___builderType_0;

public:
	inline static int32_t get_offset_of_builderType_0() { return static_cast<int32_t>(offsetof(ControlBuilderAttribute_t1654838440, ___builderType_0)); }
	inline Type_t * get_builderType_0() const { return ___builderType_0; }
	inline Type_t ** get_address_of_builderType_0() { return &___builderType_0; }
	inline void set_builderType_0(Type_t * value)
	{
		___builderType_0 = value;
		Il2CppCodeGenWriteBarrier(&___builderType_0, value);
	}
};

struct ControlBuilderAttribute_t1654838440_StaticFields
{
public:
	// System.Web.UI.ControlBuilderAttribute System.Web.UI.ControlBuilderAttribute::Default
	ControlBuilderAttribute_t1654838440 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(ControlBuilderAttribute_t1654838440_StaticFields, ___Default_1)); }
	inline ControlBuilderAttribute_t1654838440 * get_Default_1() const { return ___Default_1; }
	inline ControlBuilderAttribute_t1654838440 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(ControlBuilderAttribute_t1654838440 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier(&___Default_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
