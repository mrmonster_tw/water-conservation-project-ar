﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Emitter942443340.h"

// System.Xml.XmlWriter
struct XmlWriter_t127905479;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XmlWriterEmitter
struct  XmlWriterEmitter_t503944025  : public Emitter_t942443340
{
public:
	// System.Xml.XmlWriter Mono.Xml.Xsl.XmlWriterEmitter::writer
	XmlWriter_t127905479 * ___writer_0;

public:
	inline static int32_t get_offset_of_writer_0() { return static_cast<int32_t>(offsetof(XmlWriterEmitter_t503944025, ___writer_0)); }
	inline XmlWriter_t127905479 * get_writer_0() const { return ___writer_0; }
	inline XmlWriter_t127905479 ** get_address_of_writer_0() { return &___writer_0; }
	inline void set_writer_0(XmlWriter_t127905479 * value)
	{
		___writer_0 = value;
		Il2CppCodeGenWriteBarrier(&___writer_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
