﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_TextWriter3478189236.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.String
struct String_t;
// System.Web.UI.HtmlTextWriter/AddedStyle[]
struct AddedStyleU5BU5D_t1124216118;
// System.Web.UI.HtmlTextWriter/AddedAttr[]
struct AddedAttrU5BU5D_t125943289;
// System.Web.UI.HtmlTextWriter/AddedTag[]
struct AddedTagU5BU5D_t1683055369;
// System.Web.UI.HtmlTextWriter/HtmlTag[]
struct HtmlTagU5BU5D_t4103470321;
// System.Web.UI.HtmlTextWriter/HtmlAttribute[]
struct HtmlAttributeU5BU5D_t4208143799;
// System.Web.UI.HtmlTextWriter/HtmlStyle[]
struct HtmlStyleU5BU5D_t1518568670;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlTextWriter
struct  HtmlTextWriter_t1404937429  : public TextWriter_t3478189236
{
public:
	// System.Int32 System.Web.UI.HtmlTextWriter::indent
	int32_t ___indent_7;
	// System.IO.TextWriter System.Web.UI.HtmlTextWriter::b
	TextWriter_t3478189236 * ___b_8;
	// System.String System.Web.UI.HtmlTextWriter::tab_string
	String_t* ___tab_string_9;
	// System.Boolean System.Web.UI.HtmlTextWriter::newline
	bool ___newline_10;
	// System.Web.UI.HtmlTextWriter/AddedStyle[] System.Web.UI.HtmlTextWriter::styles
	AddedStyleU5BU5D_t1124216118* ___styles_11;
	// System.Web.UI.HtmlTextWriter/AddedAttr[] System.Web.UI.HtmlTextWriter::attrs
	AddedAttrU5BU5D_t125943289* ___attrs_12;
	// System.Web.UI.HtmlTextWriter/AddedTag[] System.Web.UI.HtmlTextWriter::tagstack
	AddedTagU5BU5D_t1683055369* ___tagstack_13;
	// System.Int32 System.Web.UI.HtmlTextWriter::styles_pos
	int32_t ___styles_pos_14;
	// System.Int32 System.Web.UI.HtmlTextWriter::attrs_pos
	int32_t ___attrs_pos_15;
	// System.Int32 System.Web.UI.HtmlTextWriter::tagstack_pos
	int32_t ___tagstack_pos_16;

public:
	inline static int32_t get_offset_of_indent_7() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___indent_7)); }
	inline int32_t get_indent_7() const { return ___indent_7; }
	inline int32_t* get_address_of_indent_7() { return &___indent_7; }
	inline void set_indent_7(int32_t value)
	{
		___indent_7 = value;
	}

	inline static int32_t get_offset_of_b_8() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___b_8)); }
	inline TextWriter_t3478189236 * get_b_8() const { return ___b_8; }
	inline TextWriter_t3478189236 ** get_address_of_b_8() { return &___b_8; }
	inline void set_b_8(TextWriter_t3478189236 * value)
	{
		___b_8 = value;
		Il2CppCodeGenWriteBarrier(&___b_8, value);
	}

	inline static int32_t get_offset_of_tab_string_9() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___tab_string_9)); }
	inline String_t* get_tab_string_9() const { return ___tab_string_9; }
	inline String_t** get_address_of_tab_string_9() { return &___tab_string_9; }
	inline void set_tab_string_9(String_t* value)
	{
		___tab_string_9 = value;
		Il2CppCodeGenWriteBarrier(&___tab_string_9, value);
	}

	inline static int32_t get_offset_of_newline_10() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___newline_10)); }
	inline bool get_newline_10() const { return ___newline_10; }
	inline bool* get_address_of_newline_10() { return &___newline_10; }
	inline void set_newline_10(bool value)
	{
		___newline_10 = value;
	}

	inline static int32_t get_offset_of_styles_11() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___styles_11)); }
	inline AddedStyleU5BU5D_t1124216118* get_styles_11() const { return ___styles_11; }
	inline AddedStyleU5BU5D_t1124216118** get_address_of_styles_11() { return &___styles_11; }
	inline void set_styles_11(AddedStyleU5BU5D_t1124216118* value)
	{
		___styles_11 = value;
		Il2CppCodeGenWriteBarrier(&___styles_11, value);
	}

	inline static int32_t get_offset_of_attrs_12() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___attrs_12)); }
	inline AddedAttrU5BU5D_t125943289* get_attrs_12() const { return ___attrs_12; }
	inline AddedAttrU5BU5D_t125943289** get_address_of_attrs_12() { return &___attrs_12; }
	inline void set_attrs_12(AddedAttrU5BU5D_t125943289* value)
	{
		___attrs_12 = value;
		Il2CppCodeGenWriteBarrier(&___attrs_12, value);
	}

	inline static int32_t get_offset_of_tagstack_13() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___tagstack_13)); }
	inline AddedTagU5BU5D_t1683055369* get_tagstack_13() const { return ___tagstack_13; }
	inline AddedTagU5BU5D_t1683055369** get_address_of_tagstack_13() { return &___tagstack_13; }
	inline void set_tagstack_13(AddedTagU5BU5D_t1683055369* value)
	{
		___tagstack_13 = value;
		Il2CppCodeGenWriteBarrier(&___tagstack_13, value);
	}

	inline static int32_t get_offset_of_styles_pos_14() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___styles_pos_14)); }
	inline int32_t get_styles_pos_14() const { return ___styles_pos_14; }
	inline int32_t* get_address_of_styles_pos_14() { return &___styles_pos_14; }
	inline void set_styles_pos_14(int32_t value)
	{
		___styles_pos_14 = value;
	}

	inline static int32_t get_offset_of_attrs_pos_15() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___attrs_pos_15)); }
	inline int32_t get_attrs_pos_15() const { return ___attrs_pos_15; }
	inline int32_t* get_address_of_attrs_pos_15() { return &___attrs_pos_15; }
	inline void set_attrs_pos_15(int32_t value)
	{
		___attrs_pos_15 = value;
	}

	inline static int32_t get_offset_of_tagstack_pos_16() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429, ___tagstack_pos_16)); }
	inline int32_t get_tagstack_pos_16() const { return ___tagstack_pos_16; }
	inline int32_t* get_address_of_tagstack_pos_16() { return &___tagstack_pos_16; }
	inline void set_tagstack_pos_16(int32_t value)
	{
		___tagstack_pos_16 = value;
	}
};

struct HtmlTextWriter_t1404937429_StaticFields
{
public:
	// System.Collections.Hashtable System.Web.UI.HtmlTextWriter::_tagTable
	Hashtable_t1853889766 * ____tagTable_4;
	// System.Collections.Hashtable System.Web.UI.HtmlTextWriter::_attributeTable
	Hashtable_t1853889766 * ____attributeTable_5;
	// System.Collections.Hashtable System.Web.UI.HtmlTextWriter::_styleTable
	Hashtable_t1853889766 * ____styleTable_6;
	// System.Web.UI.HtmlTextWriter/HtmlTag[] System.Web.UI.HtmlTextWriter::tags
	HtmlTagU5BU5D_t4103470321* ___tags_17;
	// System.Web.UI.HtmlTextWriter/HtmlAttribute[] System.Web.UI.HtmlTextWriter::htmlattrs
	HtmlAttributeU5BU5D_t4208143799* ___htmlattrs_18;
	// System.Web.UI.HtmlTextWriter/HtmlStyle[] System.Web.UI.HtmlTextWriter::htmlstyles
	HtmlStyleU5BU5D_t1518568670* ___htmlstyles_19;

public:
	inline static int32_t get_offset_of__tagTable_4() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429_StaticFields, ____tagTable_4)); }
	inline Hashtable_t1853889766 * get__tagTable_4() const { return ____tagTable_4; }
	inline Hashtable_t1853889766 ** get_address_of__tagTable_4() { return &____tagTable_4; }
	inline void set__tagTable_4(Hashtable_t1853889766 * value)
	{
		____tagTable_4 = value;
		Il2CppCodeGenWriteBarrier(&____tagTable_4, value);
	}

	inline static int32_t get_offset_of__attributeTable_5() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429_StaticFields, ____attributeTable_5)); }
	inline Hashtable_t1853889766 * get__attributeTable_5() const { return ____attributeTable_5; }
	inline Hashtable_t1853889766 ** get_address_of__attributeTable_5() { return &____attributeTable_5; }
	inline void set__attributeTable_5(Hashtable_t1853889766 * value)
	{
		____attributeTable_5 = value;
		Il2CppCodeGenWriteBarrier(&____attributeTable_5, value);
	}

	inline static int32_t get_offset_of__styleTable_6() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429_StaticFields, ____styleTable_6)); }
	inline Hashtable_t1853889766 * get__styleTable_6() const { return ____styleTable_6; }
	inline Hashtable_t1853889766 ** get_address_of__styleTable_6() { return &____styleTable_6; }
	inline void set__styleTable_6(Hashtable_t1853889766 * value)
	{
		____styleTable_6 = value;
		Il2CppCodeGenWriteBarrier(&____styleTable_6, value);
	}

	inline static int32_t get_offset_of_tags_17() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429_StaticFields, ___tags_17)); }
	inline HtmlTagU5BU5D_t4103470321* get_tags_17() const { return ___tags_17; }
	inline HtmlTagU5BU5D_t4103470321** get_address_of_tags_17() { return &___tags_17; }
	inline void set_tags_17(HtmlTagU5BU5D_t4103470321* value)
	{
		___tags_17 = value;
		Il2CppCodeGenWriteBarrier(&___tags_17, value);
	}

	inline static int32_t get_offset_of_htmlattrs_18() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429_StaticFields, ___htmlattrs_18)); }
	inline HtmlAttributeU5BU5D_t4208143799* get_htmlattrs_18() const { return ___htmlattrs_18; }
	inline HtmlAttributeU5BU5D_t4208143799** get_address_of_htmlattrs_18() { return &___htmlattrs_18; }
	inline void set_htmlattrs_18(HtmlAttributeU5BU5D_t4208143799* value)
	{
		___htmlattrs_18 = value;
		Il2CppCodeGenWriteBarrier(&___htmlattrs_18, value);
	}

	inline static int32_t get_offset_of_htmlstyles_19() { return static_cast<int32_t>(offsetof(HtmlTextWriter_t1404937429_StaticFields, ___htmlstyles_19)); }
	inline HtmlStyleU5BU5D_t1518568670* get_htmlstyles_19() const { return ___htmlstyles_19; }
	inline HtmlStyleU5BU5D_t1518568670** get_address_of_htmlstyles_19() { return &___htmlstyles_19; }
	inline void set_htmlstyles_19(HtmlStyleU5BU5D_t1518568670* value)
	{
		___htmlstyles_19 = value;
		Il2CppCodeGenWriteBarrier(&___htmlstyles_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
