﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Runtime.Serialization.KnownTypeCollection
struct KnownTypeCollection_t3509697551;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<System.Runtime.Serialization.DataMemberInfo>
struct List_1_t2172617328;
// System.Collections.Generic.Dictionary`2<System.Type,System.Xml.XmlQualifiedName>
struct Dictionary_2_t910034080;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationMap
struct  SerializationMap_t4088073813  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.KnownTypeCollection System.Runtime.Serialization.SerializationMap::KnownTypes
	KnownTypeCollection_t3509697551 * ___KnownTypes_0;
	// System.Type System.Runtime.Serialization.SerializationMap::RuntimeType
	Type_t * ___RuntimeType_1;
	// System.Boolean System.Runtime.Serialization.SerializationMap::IsReference
	bool ___IsReference_2;
	// System.Collections.Generic.List`1<System.Runtime.Serialization.DataMemberInfo> System.Runtime.Serialization.SerializationMap::Members
	List_1_t2172617328 * ___Members_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Xml.XmlQualifiedName> System.Runtime.Serialization.SerializationMap::qname_table
	Dictionary_2_t910034080 * ___qname_table_4;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.SerializationMap::<XmlName>k__BackingField
	XmlQualifiedName_t2760654312 * ___U3CXmlNameU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_KnownTypes_0() { return static_cast<int32_t>(offsetof(SerializationMap_t4088073813, ___KnownTypes_0)); }
	inline KnownTypeCollection_t3509697551 * get_KnownTypes_0() const { return ___KnownTypes_0; }
	inline KnownTypeCollection_t3509697551 ** get_address_of_KnownTypes_0() { return &___KnownTypes_0; }
	inline void set_KnownTypes_0(KnownTypeCollection_t3509697551 * value)
	{
		___KnownTypes_0 = value;
		Il2CppCodeGenWriteBarrier(&___KnownTypes_0, value);
	}

	inline static int32_t get_offset_of_RuntimeType_1() { return static_cast<int32_t>(offsetof(SerializationMap_t4088073813, ___RuntimeType_1)); }
	inline Type_t * get_RuntimeType_1() const { return ___RuntimeType_1; }
	inline Type_t ** get_address_of_RuntimeType_1() { return &___RuntimeType_1; }
	inline void set_RuntimeType_1(Type_t * value)
	{
		___RuntimeType_1 = value;
		Il2CppCodeGenWriteBarrier(&___RuntimeType_1, value);
	}

	inline static int32_t get_offset_of_IsReference_2() { return static_cast<int32_t>(offsetof(SerializationMap_t4088073813, ___IsReference_2)); }
	inline bool get_IsReference_2() const { return ___IsReference_2; }
	inline bool* get_address_of_IsReference_2() { return &___IsReference_2; }
	inline void set_IsReference_2(bool value)
	{
		___IsReference_2 = value;
	}

	inline static int32_t get_offset_of_Members_3() { return static_cast<int32_t>(offsetof(SerializationMap_t4088073813, ___Members_3)); }
	inline List_1_t2172617328 * get_Members_3() const { return ___Members_3; }
	inline List_1_t2172617328 ** get_address_of_Members_3() { return &___Members_3; }
	inline void set_Members_3(List_1_t2172617328 * value)
	{
		___Members_3 = value;
		Il2CppCodeGenWriteBarrier(&___Members_3, value);
	}

	inline static int32_t get_offset_of_qname_table_4() { return static_cast<int32_t>(offsetof(SerializationMap_t4088073813, ___qname_table_4)); }
	inline Dictionary_2_t910034080 * get_qname_table_4() const { return ___qname_table_4; }
	inline Dictionary_2_t910034080 ** get_address_of_qname_table_4() { return &___qname_table_4; }
	inline void set_qname_table_4(Dictionary_2_t910034080 * value)
	{
		___qname_table_4 = value;
		Il2CppCodeGenWriteBarrier(&___qname_table_4, value);
	}

	inline static int32_t get_offset_of_U3CXmlNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SerializationMap_t4088073813, ___U3CXmlNameU3Ek__BackingField_5)); }
	inline XmlQualifiedName_t2760654312 * get_U3CXmlNameU3Ek__BackingField_5() const { return ___U3CXmlNameU3Ek__BackingField_5; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_U3CXmlNameU3Ek__BackingField_5() { return &___U3CXmlNameU3Ek__BackingField_5; }
	inline void set_U3CXmlNameU3Ek__BackingField_5(XmlQualifiedName_t2760654312 * value)
	{
		___U3CXmlNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CXmlNameU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
