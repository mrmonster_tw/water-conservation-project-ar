﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpCacheVaryByParams
struct  HttpCacheVaryByParams_t2818058713  : public Il2CppObject
{
public:
	// System.Boolean System.Web.HttpCacheVaryByParams::ignore_parms
	bool ___ignore_parms_0;
	// System.Collections.Hashtable System.Web.HttpCacheVaryByParams::parms
	Hashtable_t1853889766 * ___parms_1;

public:
	inline static int32_t get_offset_of_ignore_parms_0() { return static_cast<int32_t>(offsetof(HttpCacheVaryByParams_t2818058713, ___ignore_parms_0)); }
	inline bool get_ignore_parms_0() const { return ___ignore_parms_0; }
	inline bool* get_address_of_ignore_parms_0() { return &___ignore_parms_0; }
	inline void set_ignore_parms_0(bool value)
	{
		___ignore_parms_0 = value;
	}

	inline static int32_t get_offset_of_parms_1() { return static_cast<int32_t>(offsetof(HttpCacheVaryByParams_t2818058713, ___parms_1)); }
	inline Hashtable_t1853889766 * get_parms_1() const { return ___parms_1; }
	inline Hashtable_t1853889766 ** get_address_of_parms_1() { return &___parms_1; }
	inline void set_parms_1(Hashtable_t1853889766 * value)
	{
		___parms_1 = value;
		Il2CppCodeGenWriteBarrier(&___parms_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
