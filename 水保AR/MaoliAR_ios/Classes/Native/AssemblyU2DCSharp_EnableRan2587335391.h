﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// CameraFilterPack_Atmosphere_Rain_Pro
struct CameraFilterPack_Atmosphere_Rain_Pro_t3269395514;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnableRan
struct  EnableRan_t2587335391  : public MonoBehaviour_t3962482529
{
public:
	// CameraFilterPack_Atmosphere_Rain_Pro EnableRan::rain
	CameraFilterPack_Atmosphere_Rain_Pro_t3269395514 * ___rain_2;
	// System.Boolean EnableRan::OnMeshRenderOpen
	bool ___OnMeshRenderOpen_3;

public:
	inline static int32_t get_offset_of_rain_2() { return static_cast<int32_t>(offsetof(EnableRan_t2587335391, ___rain_2)); }
	inline CameraFilterPack_Atmosphere_Rain_Pro_t3269395514 * get_rain_2() const { return ___rain_2; }
	inline CameraFilterPack_Atmosphere_Rain_Pro_t3269395514 ** get_address_of_rain_2() { return &___rain_2; }
	inline void set_rain_2(CameraFilterPack_Atmosphere_Rain_Pro_t3269395514 * value)
	{
		___rain_2 = value;
		Il2CppCodeGenWriteBarrier(&___rain_2, value);
	}

	inline static int32_t get_offset_of_OnMeshRenderOpen_3() { return static_cast<int32_t>(offsetof(EnableRan_t2587335391, ___OnMeshRenderOpen_3)); }
	inline bool get_OnMeshRenderOpen_3() const { return ___OnMeshRenderOpen_3; }
	inline bool* get_address_of_OnMeshRenderOpen_3() { return &___OnMeshRenderOpen_3; }
	inline void set_OnMeshRenderOpen_3(bool value)
	{
		___OnMeshRenderOpen_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
