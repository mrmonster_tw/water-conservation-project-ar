﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_Descriptio1296153790.h"
#include "System_System_Net_Security_ProtectionLevel2559578148.h"

// System.String
struct String_t;
// System.ServiceModel.Description.MessagePropertyDescriptionCollection
struct MessagePropertyDescriptionCollection_t2693848676;
// System.ServiceModel.Description.MessageBodyDescription
struct MessageBodyDescription_t2541387169;
// System.ServiceModel.Description.MessageHeaderDescriptionCollection
struct MessageHeaderDescriptionCollection_t4093932642;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.MessageDescription
struct  MessageDescription_t512795677  : public Il2CppObject
{
public:
	// System.String System.ServiceModel.Description.MessageDescription::action
	String_t* ___action_0;
	// System.ServiceModel.Description.MessageDirection System.ServiceModel.Description.MessageDescription::direction
	int32_t ___direction_1;
	// System.ServiceModel.Description.MessagePropertyDescriptionCollection System.ServiceModel.Description.MessageDescription::properties
	MessagePropertyDescriptionCollection_t2693848676 * ___properties_2;
	// System.ServiceModel.Description.MessageBodyDescription System.ServiceModel.Description.MessageDescription::body
	MessageBodyDescription_t2541387169 * ___body_3;
	// System.ServiceModel.Description.MessageHeaderDescriptionCollection System.ServiceModel.Description.MessageDescription::headers
	MessageHeaderDescriptionCollection_t4093932642 * ___headers_4;
	// System.Type System.ServiceModel.Description.MessageDescription::message_type
	Type_t * ___message_type_5;
	// System.Boolean System.ServiceModel.Description.MessageDescription::has_protection_level
	bool ___has_protection_level_6;
	// System.Net.Security.ProtectionLevel System.ServiceModel.Description.MessageDescription::protection_level
	int32_t ___protection_level_7;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(MessageDescription_t512795677, ___action_0)); }
	inline String_t* get_action_0() const { return ___action_0; }
	inline String_t** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(String_t* value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier(&___action_0, value);
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(MessageDescription_t512795677, ___direction_1)); }
	inline int32_t get_direction_1() const { return ___direction_1; }
	inline int32_t* get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(int32_t value)
	{
		___direction_1 = value;
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(MessageDescription_t512795677, ___properties_2)); }
	inline MessagePropertyDescriptionCollection_t2693848676 * get_properties_2() const { return ___properties_2; }
	inline MessagePropertyDescriptionCollection_t2693848676 ** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(MessagePropertyDescriptionCollection_t2693848676 * value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier(&___properties_2, value);
	}

	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(MessageDescription_t512795677, ___body_3)); }
	inline MessageBodyDescription_t2541387169 * get_body_3() const { return ___body_3; }
	inline MessageBodyDescription_t2541387169 ** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(MessageBodyDescription_t2541387169 * value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier(&___body_3, value);
	}

	inline static int32_t get_offset_of_headers_4() { return static_cast<int32_t>(offsetof(MessageDescription_t512795677, ___headers_4)); }
	inline MessageHeaderDescriptionCollection_t4093932642 * get_headers_4() const { return ___headers_4; }
	inline MessageHeaderDescriptionCollection_t4093932642 ** get_address_of_headers_4() { return &___headers_4; }
	inline void set_headers_4(MessageHeaderDescriptionCollection_t4093932642 * value)
	{
		___headers_4 = value;
		Il2CppCodeGenWriteBarrier(&___headers_4, value);
	}

	inline static int32_t get_offset_of_message_type_5() { return static_cast<int32_t>(offsetof(MessageDescription_t512795677, ___message_type_5)); }
	inline Type_t * get_message_type_5() const { return ___message_type_5; }
	inline Type_t ** get_address_of_message_type_5() { return &___message_type_5; }
	inline void set_message_type_5(Type_t * value)
	{
		___message_type_5 = value;
		Il2CppCodeGenWriteBarrier(&___message_type_5, value);
	}

	inline static int32_t get_offset_of_has_protection_level_6() { return static_cast<int32_t>(offsetof(MessageDescription_t512795677, ___has_protection_level_6)); }
	inline bool get_has_protection_level_6() const { return ___has_protection_level_6; }
	inline bool* get_address_of_has_protection_level_6() { return &___has_protection_level_6; }
	inline void set_has_protection_level_6(bool value)
	{
		___has_protection_level_6 = value;
	}

	inline static int32_t get_offset_of_protection_level_7() { return static_cast<int32_t>(offsetof(MessageDescription_t512795677, ___protection_level_7)); }
	inline int32_t get_protection_level_7() const { return ___protection_level_7; }
	inline int32_t* get_address_of_protection_level_7() { return &___protection_level_7; }
	inline void set_protection_level_7(int32_t value)
	{
		___protection_level_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
