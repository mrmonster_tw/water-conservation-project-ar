﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_ComponentModel_CategoryAttribute39585132.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.WebCategoryAttribute
struct  WebCategoryAttribute_t1186646113  : public CategoryAttribute_t39585132
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
