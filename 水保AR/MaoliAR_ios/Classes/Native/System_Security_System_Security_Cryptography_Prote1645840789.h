﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Security_System_Security_Cryptography_Prote2680138268.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.ProtectedData
struct  ProtectedData_t1645840789  : public Il2CppObject
{
public:

public:
};

struct ProtectedData_t1645840789_StaticFields
{
public:
	// System.Security.Cryptography.ProtectedData/DataProtectionImplementation System.Security.Cryptography.ProtectedData::impl
	int32_t ___impl_0;

public:
	inline static int32_t get_offset_of_impl_0() { return static_cast<int32_t>(offsetof(ProtectedData_t1645840789_StaticFields, ___impl_0)); }
	inline int32_t get_impl_0() const { return ___impl_0; }
	inline int32_t* get_address_of_impl_0() { return &___impl_0; }
	inline void set_impl_0(int32_t value)
	{
		___impl_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
