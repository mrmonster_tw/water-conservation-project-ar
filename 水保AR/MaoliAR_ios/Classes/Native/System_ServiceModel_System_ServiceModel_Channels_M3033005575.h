﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M3063398011.h"

// System.Text.Encoding
struct Encoding_t1523322056;
// System.ServiceModel.Channels.MessageVersion
struct MessageVersion_t1958933598;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MtomMessageEncoder
struct  MtomMessageEncoder_t3033005575  : public MessageEncoder_t3063398011
{
public:
	// System.Text.Encoding System.ServiceModel.Channels.MtomMessageEncoder::encoding
	Encoding_t1523322056 * ___encoding_0;
	// System.ServiceModel.Channels.MessageVersion System.ServiceModel.Channels.MtomMessageEncoder::version
	MessageVersion_t1958933598 * ___version_1;

public:
	inline static int32_t get_offset_of_encoding_0() { return static_cast<int32_t>(offsetof(MtomMessageEncoder_t3033005575, ___encoding_0)); }
	inline Encoding_t1523322056 * get_encoding_0() const { return ___encoding_0; }
	inline Encoding_t1523322056 ** get_address_of_encoding_0() { return &___encoding_0; }
	inline void set_encoding_0(Encoding_t1523322056 * value)
	{
		___encoding_0 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_0, value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(MtomMessageEncoder_t3033005575, ___version_1)); }
	inline MessageVersion_t1958933598 * get_version_1() const { return ___version_1; }
	inline MessageVersion_t1958933598 ** get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(MessageVersion_t1958933598 * value)
	{
		___version_1 = value;
		Il2CppCodeGenWriteBarrier(&___version_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
