﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream1273022909.h"
#include "System_Core_System_IO_Pipes_PipeDirection2244332536.h"
#include "System_Core_System_IO_Pipes_PipeTransmissionMode3285869980.h"

// Microsoft.Win32.SafeHandles.SafePipeHandle
struct SafePipeHandle_t1989113880;
// System.IO.Stream
struct Stream_t1273022909;
// System.Func`4<System.Byte[],System.Int32,System.Int32,System.Int32>
struct Func_4_t752956844;
// System.Action`3<System.Byte[],System.Int32,System.Int32>
struct Action_3_t1284011258;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Pipes.PipeStream
struct  PipeStream_t871053121  : public Stream_t1273022909
{
public:
	// System.IO.Pipes.PipeDirection System.IO.Pipes.PipeStream::direction
	int32_t ___direction_2;
	// System.IO.Pipes.PipeTransmissionMode System.IO.Pipes.PipeStream::transmission_mode
	int32_t ___transmission_mode_3;
	// System.IO.Pipes.PipeTransmissionMode System.IO.Pipes.PipeStream::read_trans_mode
	int32_t ___read_trans_mode_4;
	// System.Int32 System.IO.Pipes.PipeStream::buffer_size
	int32_t ___buffer_size_5;
	// Microsoft.Win32.SafeHandles.SafePipeHandle System.IO.Pipes.PipeStream::handle
	SafePipeHandle_t1989113880 * ___handle_6;
	// System.IO.Stream System.IO.Pipes.PipeStream::stream
	Stream_t1273022909 * ___stream_7;
	// System.Func`4<System.Byte[],System.Int32,System.Int32,System.Int32> System.IO.Pipes.PipeStream::read_delegate
	Func_4_t752956844 * ___read_delegate_8;
	// System.Action`3<System.Byte[],System.Int32,System.Int32> System.IO.Pipes.PipeStream::write_delegate
	Action_3_t1284011258 * ___write_delegate_9;
	// System.Boolean System.IO.Pipes.PipeStream::<IsAsync>k__BackingField
	bool ___U3CIsAsyncU3Ek__BackingField_10;
	// System.Boolean System.IO.Pipes.PipeStream::<IsConnected>k__BackingField
	bool ___U3CIsConnectedU3Ek__BackingField_11;
	// System.Boolean System.IO.Pipes.PipeStream::<IsHandleExposed>k__BackingField
	bool ___U3CIsHandleExposedU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_direction_2() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___direction_2)); }
	inline int32_t get_direction_2() const { return ___direction_2; }
	inline int32_t* get_address_of_direction_2() { return &___direction_2; }
	inline void set_direction_2(int32_t value)
	{
		___direction_2 = value;
	}

	inline static int32_t get_offset_of_transmission_mode_3() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___transmission_mode_3)); }
	inline int32_t get_transmission_mode_3() const { return ___transmission_mode_3; }
	inline int32_t* get_address_of_transmission_mode_3() { return &___transmission_mode_3; }
	inline void set_transmission_mode_3(int32_t value)
	{
		___transmission_mode_3 = value;
	}

	inline static int32_t get_offset_of_read_trans_mode_4() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___read_trans_mode_4)); }
	inline int32_t get_read_trans_mode_4() const { return ___read_trans_mode_4; }
	inline int32_t* get_address_of_read_trans_mode_4() { return &___read_trans_mode_4; }
	inline void set_read_trans_mode_4(int32_t value)
	{
		___read_trans_mode_4 = value;
	}

	inline static int32_t get_offset_of_buffer_size_5() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___buffer_size_5)); }
	inline int32_t get_buffer_size_5() const { return ___buffer_size_5; }
	inline int32_t* get_address_of_buffer_size_5() { return &___buffer_size_5; }
	inline void set_buffer_size_5(int32_t value)
	{
		___buffer_size_5 = value;
	}

	inline static int32_t get_offset_of_handle_6() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___handle_6)); }
	inline SafePipeHandle_t1989113880 * get_handle_6() const { return ___handle_6; }
	inline SafePipeHandle_t1989113880 ** get_address_of_handle_6() { return &___handle_6; }
	inline void set_handle_6(SafePipeHandle_t1989113880 * value)
	{
		___handle_6 = value;
		Il2CppCodeGenWriteBarrier(&___handle_6, value);
	}

	inline static int32_t get_offset_of_stream_7() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___stream_7)); }
	inline Stream_t1273022909 * get_stream_7() const { return ___stream_7; }
	inline Stream_t1273022909 ** get_address_of_stream_7() { return &___stream_7; }
	inline void set_stream_7(Stream_t1273022909 * value)
	{
		___stream_7 = value;
		Il2CppCodeGenWriteBarrier(&___stream_7, value);
	}

	inline static int32_t get_offset_of_read_delegate_8() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___read_delegate_8)); }
	inline Func_4_t752956844 * get_read_delegate_8() const { return ___read_delegate_8; }
	inline Func_4_t752956844 ** get_address_of_read_delegate_8() { return &___read_delegate_8; }
	inline void set_read_delegate_8(Func_4_t752956844 * value)
	{
		___read_delegate_8 = value;
		Il2CppCodeGenWriteBarrier(&___read_delegate_8, value);
	}

	inline static int32_t get_offset_of_write_delegate_9() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___write_delegate_9)); }
	inline Action_3_t1284011258 * get_write_delegate_9() const { return ___write_delegate_9; }
	inline Action_3_t1284011258 ** get_address_of_write_delegate_9() { return &___write_delegate_9; }
	inline void set_write_delegate_9(Action_3_t1284011258 * value)
	{
		___write_delegate_9 = value;
		Il2CppCodeGenWriteBarrier(&___write_delegate_9, value);
	}

	inline static int32_t get_offset_of_U3CIsAsyncU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___U3CIsAsyncU3Ek__BackingField_10)); }
	inline bool get_U3CIsAsyncU3Ek__BackingField_10() const { return ___U3CIsAsyncU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsAsyncU3Ek__BackingField_10() { return &___U3CIsAsyncU3Ek__BackingField_10; }
	inline void set_U3CIsAsyncU3Ek__BackingField_10(bool value)
	{
		___U3CIsAsyncU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CIsConnectedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___U3CIsConnectedU3Ek__BackingField_11)); }
	inline bool get_U3CIsConnectedU3Ek__BackingField_11() const { return ___U3CIsConnectedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsConnectedU3Ek__BackingField_11() { return &___U3CIsConnectedU3Ek__BackingField_11; }
	inline void set_U3CIsConnectedU3Ek__BackingField_11(bool value)
	{
		___U3CIsConnectedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CIsHandleExposedU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PipeStream_t871053121, ___U3CIsHandleExposedU3Ek__BackingField_12)); }
	inline bool get_U3CIsHandleExposedU3Ek__BackingField_12() const { return ___U3CIsHandleExposedU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsHandleExposedU3Ek__BackingField_12() { return &___U3CIsHandleExposedU3Ek__BackingField_12; }
	inline void set_U3CIsHandleExposedU3Ek__BackingField_12(bool value)
	{
		___U3CIsHandleExposedU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
