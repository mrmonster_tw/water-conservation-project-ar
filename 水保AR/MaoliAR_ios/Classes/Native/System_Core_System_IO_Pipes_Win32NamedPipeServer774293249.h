﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Core_System_IO_Pipes_Win32NamedPipe1933459707.h"

// Microsoft.Win32.SafeHandles.SafePipeHandle
struct SafePipeHandle_t1989113880;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Pipes.Win32NamedPipeServer
struct  Win32NamedPipeServer_t774293249  : public Win32NamedPipe_t1933459707
{
public:
	// Microsoft.Win32.SafeHandles.SafePipeHandle System.IO.Pipes.Win32NamedPipeServer::handle
	SafePipeHandle_t1989113880 * ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(Win32NamedPipeServer_t774293249, ___handle_0)); }
	inline SafePipeHandle_t1989113880 * get_handle_0() const { return ___handle_0; }
	inline SafePipeHandle_t1989113880 ** get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(SafePipeHandle_t1989113880 * value)
	{
		___handle_0 = value;
		Il2CppCodeGenWriteBarrier(&___handle_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
