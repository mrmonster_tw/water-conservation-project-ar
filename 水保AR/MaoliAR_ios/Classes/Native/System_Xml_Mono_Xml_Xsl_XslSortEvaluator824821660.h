﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XPath.XPathExpression
struct XPathExpression_t1723793351;
// Mono.Xml.Xsl.Sort[]
struct SortU5BU5D_t2079756477;
// System.Xml.XPath.XPathSorter[]
struct XPathSorterU5BU5D_t4263134841;
// System.Xml.XPath.XPathSorters
struct XPathSorters_t698127628;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XslSortEvaluator
struct  XslSortEvaluator_t824821660  : public Il2CppObject
{
public:
	// System.Xml.XPath.XPathExpression Mono.Xml.Xsl.XslSortEvaluator::select
	XPathExpression_t1723793351 * ___select_0;
	// Mono.Xml.Xsl.Sort[] Mono.Xml.Xsl.XslSortEvaluator::sorterTemplates
	SortU5BU5D_t2079756477* ___sorterTemplates_1;
	// System.Xml.XPath.XPathSorter[] Mono.Xml.Xsl.XslSortEvaluator::sorters
	XPathSorterU5BU5D_t4263134841* ___sorters_2;
	// System.Xml.XPath.XPathSorters Mono.Xml.Xsl.XslSortEvaluator::sortRunner
	XPathSorters_t698127628 * ___sortRunner_3;
	// System.Boolean Mono.Xml.Xsl.XslSortEvaluator::isSorterContextDependent
	bool ___isSorterContextDependent_4;

public:
	inline static int32_t get_offset_of_select_0() { return static_cast<int32_t>(offsetof(XslSortEvaluator_t824821660, ___select_0)); }
	inline XPathExpression_t1723793351 * get_select_0() const { return ___select_0; }
	inline XPathExpression_t1723793351 ** get_address_of_select_0() { return &___select_0; }
	inline void set_select_0(XPathExpression_t1723793351 * value)
	{
		___select_0 = value;
		Il2CppCodeGenWriteBarrier(&___select_0, value);
	}

	inline static int32_t get_offset_of_sorterTemplates_1() { return static_cast<int32_t>(offsetof(XslSortEvaluator_t824821660, ___sorterTemplates_1)); }
	inline SortU5BU5D_t2079756477* get_sorterTemplates_1() const { return ___sorterTemplates_1; }
	inline SortU5BU5D_t2079756477** get_address_of_sorterTemplates_1() { return &___sorterTemplates_1; }
	inline void set_sorterTemplates_1(SortU5BU5D_t2079756477* value)
	{
		___sorterTemplates_1 = value;
		Il2CppCodeGenWriteBarrier(&___sorterTemplates_1, value);
	}

	inline static int32_t get_offset_of_sorters_2() { return static_cast<int32_t>(offsetof(XslSortEvaluator_t824821660, ___sorters_2)); }
	inline XPathSorterU5BU5D_t4263134841* get_sorters_2() const { return ___sorters_2; }
	inline XPathSorterU5BU5D_t4263134841** get_address_of_sorters_2() { return &___sorters_2; }
	inline void set_sorters_2(XPathSorterU5BU5D_t4263134841* value)
	{
		___sorters_2 = value;
		Il2CppCodeGenWriteBarrier(&___sorters_2, value);
	}

	inline static int32_t get_offset_of_sortRunner_3() { return static_cast<int32_t>(offsetof(XslSortEvaluator_t824821660, ___sortRunner_3)); }
	inline XPathSorters_t698127628 * get_sortRunner_3() const { return ___sortRunner_3; }
	inline XPathSorters_t698127628 ** get_address_of_sortRunner_3() { return &___sortRunner_3; }
	inline void set_sortRunner_3(XPathSorters_t698127628 * value)
	{
		___sortRunner_3 = value;
		Il2CppCodeGenWriteBarrier(&___sortRunner_3, value);
	}

	inline static int32_t get_offset_of_isSorterContextDependent_4() { return static_cast<int32_t>(offsetof(XslSortEvaluator_t824821660, ___isSorterContextDependent_4)); }
	inline bool get_isSorterContextDependent_4() const { return ___isSorterContextDependent_4; }
	inline bool* get_address_of_isSorterContextDependent_4() { return &___isSorterContextDependent_4; }
	inline void set_isSorterContextDependent_4(bool value)
	{
		___isSorterContextDependent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
