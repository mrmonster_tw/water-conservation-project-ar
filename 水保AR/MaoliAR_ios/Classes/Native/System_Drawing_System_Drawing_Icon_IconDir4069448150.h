﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"

// System.Drawing.Icon/IconDirEntry[]
struct IconDirEntryU5BU5D_t1389106801;
struct IconDirEntry_t3003987536 ;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.Icon/IconDir
struct  IconDir_t4069448150 
{
public:
	// System.UInt16 System.Drawing.Icon/IconDir::idReserved
	uint16_t ___idReserved_0;
	// System.UInt16 System.Drawing.Icon/IconDir::idType
	uint16_t ___idType_1;
	// System.UInt16 System.Drawing.Icon/IconDir::idCount
	uint16_t ___idCount_2;
	// System.Drawing.Icon/IconDirEntry[] System.Drawing.Icon/IconDir::idEntries
	IconDirEntryU5BU5D_t1389106801* ___idEntries_3;

public:
	inline static int32_t get_offset_of_idReserved_0() { return static_cast<int32_t>(offsetof(IconDir_t4069448150, ___idReserved_0)); }
	inline uint16_t get_idReserved_0() const { return ___idReserved_0; }
	inline uint16_t* get_address_of_idReserved_0() { return &___idReserved_0; }
	inline void set_idReserved_0(uint16_t value)
	{
		___idReserved_0 = value;
	}

	inline static int32_t get_offset_of_idType_1() { return static_cast<int32_t>(offsetof(IconDir_t4069448150, ___idType_1)); }
	inline uint16_t get_idType_1() const { return ___idType_1; }
	inline uint16_t* get_address_of_idType_1() { return &___idType_1; }
	inline void set_idType_1(uint16_t value)
	{
		___idType_1 = value;
	}

	inline static int32_t get_offset_of_idCount_2() { return static_cast<int32_t>(offsetof(IconDir_t4069448150, ___idCount_2)); }
	inline uint16_t get_idCount_2() const { return ___idCount_2; }
	inline uint16_t* get_address_of_idCount_2() { return &___idCount_2; }
	inline void set_idCount_2(uint16_t value)
	{
		___idCount_2 = value;
	}

	inline static int32_t get_offset_of_idEntries_3() { return static_cast<int32_t>(offsetof(IconDir_t4069448150, ___idEntries_3)); }
	inline IconDirEntryU5BU5D_t1389106801* get_idEntries_3() const { return ___idEntries_3; }
	inline IconDirEntryU5BU5D_t1389106801** get_address_of_idEntries_3() { return &___idEntries_3; }
	inline void set_idEntries_3(IconDirEntryU5BU5D_t1389106801* value)
	{
		___idEntries_3 = value;
		Il2CppCodeGenWriteBarrier(&___idEntries_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Drawing.Icon/IconDir
struct IconDir_t4069448150_marshaled_pinvoke
{
	uint16_t ___idReserved_0;
	uint16_t ___idType_1;
	uint16_t ___idCount_2;
	IconDirEntry_t3003987536 * ___idEntries_3;
};
// Native definition for COM marshalling of System.Drawing.Icon/IconDir
struct IconDir_t4069448150_marshaled_com
{
	uint16_t ___idReserved_0;
	uint16_t ___idType_1;
	uint16_t ___idCount_2;
	IconDirEntry_t3003987536 * ___idEntries_3;
};
