﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Me929740066.h"

// System.String
struct String_t;
// System.ServiceModel.FaultCode
struct FaultCode_t3080963035;
// System.ServiceModel.FaultReason
struct FaultReason_t2245999938;
// System.Object
struct Il2CppObject;
// System.Runtime.Serialization.XmlObjectSerializer
struct XmlObjectSerializer_t3967301761;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MessageFault/SimpleMessageFault
struct  SimpleMessageFault_t3639017112  : public MessageFault_t929740066
{
public:
	// System.Boolean System.ServiceModel.Channels.MessageFault/SimpleMessageFault::has_detail
	bool ___has_detail_3;
	// System.String System.ServiceModel.Channels.MessageFault/SimpleMessageFault::actor
	String_t* ___actor_4;
	// System.String System.ServiceModel.Channels.MessageFault/SimpleMessageFault::node
	String_t* ___node_5;
	// System.ServiceModel.FaultCode System.ServiceModel.Channels.MessageFault/SimpleMessageFault::code
	FaultCode_t3080963035 * ___code_6;
	// System.ServiceModel.FaultReason System.ServiceModel.Channels.MessageFault/SimpleMessageFault::reason
	FaultReason_t2245999938 * ___reason_7;
	// System.Object System.ServiceModel.Channels.MessageFault/SimpleMessageFault::detail
	Il2CppObject * ___detail_8;
	// System.Runtime.Serialization.XmlObjectSerializer System.ServiceModel.Channels.MessageFault/SimpleMessageFault::formatter
	XmlObjectSerializer_t3967301761 * ___formatter_9;

public:
	inline static int32_t get_offset_of_has_detail_3() { return static_cast<int32_t>(offsetof(SimpleMessageFault_t3639017112, ___has_detail_3)); }
	inline bool get_has_detail_3() const { return ___has_detail_3; }
	inline bool* get_address_of_has_detail_3() { return &___has_detail_3; }
	inline void set_has_detail_3(bool value)
	{
		___has_detail_3 = value;
	}

	inline static int32_t get_offset_of_actor_4() { return static_cast<int32_t>(offsetof(SimpleMessageFault_t3639017112, ___actor_4)); }
	inline String_t* get_actor_4() const { return ___actor_4; }
	inline String_t** get_address_of_actor_4() { return &___actor_4; }
	inline void set_actor_4(String_t* value)
	{
		___actor_4 = value;
		Il2CppCodeGenWriteBarrier(&___actor_4, value);
	}

	inline static int32_t get_offset_of_node_5() { return static_cast<int32_t>(offsetof(SimpleMessageFault_t3639017112, ___node_5)); }
	inline String_t* get_node_5() const { return ___node_5; }
	inline String_t** get_address_of_node_5() { return &___node_5; }
	inline void set_node_5(String_t* value)
	{
		___node_5 = value;
		Il2CppCodeGenWriteBarrier(&___node_5, value);
	}

	inline static int32_t get_offset_of_code_6() { return static_cast<int32_t>(offsetof(SimpleMessageFault_t3639017112, ___code_6)); }
	inline FaultCode_t3080963035 * get_code_6() const { return ___code_6; }
	inline FaultCode_t3080963035 ** get_address_of_code_6() { return &___code_6; }
	inline void set_code_6(FaultCode_t3080963035 * value)
	{
		___code_6 = value;
		Il2CppCodeGenWriteBarrier(&___code_6, value);
	}

	inline static int32_t get_offset_of_reason_7() { return static_cast<int32_t>(offsetof(SimpleMessageFault_t3639017112, ___reason_7)); }
	inline FaultReason_t2245999938 * get_reason_7() const { return ___reason_7; }
	inline FaultReason_t2245999938 ** get_address_of_reason_7() { return &___reason_7; }
	inline void set_reason_7(FaultReason_t2245999938 * value)
	{
		___reason_7 = value;
		Il2CppCodeGenWriteBarrier(&___reason_7, value);
	}

	inline static int32_t get_offset_of_detail_8() { return static_cast<int32_t>(offsetof(SimpleMessageFault_t3639017112, ___detail_8)); }
	inline Il2CppObject * get_detail_8() const { return ___detail_8; }
	inline Il2CppObject ** get_address_of_detail_8() { return &___detail_8; }
	inline void set_detail_8(Il2CppObject * value)
	{
		___detail_8 = value;
		Il2CppCodeGenWriteBarrier(&___detail_8, value);
	}

	inline static int32_t get_offset_of_formatter_9() { return static_cast<int32_t>(offsetof(SimpleMessageFault_t3639017112, ___formatter_9)); }
	inline XmlObjectSerializer_t3967301761 * get_formatter_9() const { return ___formatter_9; }
	inline XmlObjectSerializer_t3967301761 ** get_address_of_formatter_9() { return &___formatter_9; }
	inline void set_formatter_9(XmlObjectSerializer_t3967301761 * value)
	{
		___formatter_9 = value;
		Il2CppCodeGenWriteBarrier(&___formatter_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
