﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslNumber_XslNu2815695937.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/RomanItem
struct  RomanItem_t2026450318  : public FormatItem_t2815695937
{
public:
	// System.Boolean Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/RomanItem::uc
	bool ___uc_1;

public:
	inline static int32_t get_offset_of_uc_1() { return static_cast<int32_t>(offsetof(RomanItem_t2026450318, ___uc_1)); }
	inline bool get_uc_1() const { return ___uc_1; }
	inline bool* get_address_of_uc_1() { return &___uc_1; }
	inline void set_uc_1(bool value)
	{
		___uc_1 = value;
	}
};

struct RomanItem_t2026450318_StaticFields
{
public:
	// System.String[] Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/RomanItem::ucrDigits
	StringU5BU5D_t1281789340* ___ucrDigits_2;
	// System.String[] Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/RomanItem::lcrDigits
	StringU5BU5D_t1281789340* ___lcrDigits_3;
	// System.Int32[] Mono.Xml.Xsl.Operations.XslNumber/XslNumberFormatter/RomanItem::decValues
	Int32U5BU5D_t385246372* ___decValues_4;

public:
	inline static int32_t get_offset_of_ucrDigits_2() { return static_cast<int32_t>(offsetof(RomanItem_t2026450318_StaticFields, ___ucrDigits_2)); }
	inline StringU5BU5D_t1281789340* get_ucrDigits_2() const { return ___ucrDigits_2; }
	inline StringU5BU5D_t1281789340** get_address_of_ucrDigits_2() { return &___ucrDigits_2; }
	inline void set_ucrDigits_2(StringU5BU5D_t1281789340* value)
	{
		___ucrDigits_2 = value;
		Il2CppCodeGenWriteBarrier(&___ucrDigits_2, value);
	}

	inline static int32_t get_offset_of_lcrDigits_3() { return static_cast<int32_t>(offsetof(RomanItem_t2026450318_StaticFields, ___lcrDigits_3)); }
	inline StringU5BU5D_t1281789340* get_lcrDigits_3() const { return ___lcrDigits_3; }
	inline StringU5BU5D_t1281789340** get_address_of_lcrDigits_3() { return &___lcrDigits_3; }
	inline void set_lcrDigits_3(StringU5BU5D_t1281789340* value)
	{
		___lcrDigits_3 = value;
		Il2CppCodeGenWriteBarrier(&___lcrDigits_3, value);
	}

	inline static int32_t get_offset_of_decValues_4() { return static_cast<int32_t>(offsetof(RomanItem_t2026450318_StaticFields, ___decValues_4)); }
	inline Int32U5BU5D_t385246372* get_decValues_4() const { return ___decValues_4; }
	inline Int32U5BU5D_t385246372** get_address_of_decValues_4() { return &___decValues_4; }
	inline void set_decValues_4(Int32U5BU5D_t385246372* value)
	{
		___decValues_4 = value;
		Il2CppCodeGenWriteBarrier(&___decValues_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
