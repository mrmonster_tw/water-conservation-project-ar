﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryDictionaryWriter/<LookupPrefix>c__AnonStorey0
struct  U3CLookupPrefixU3Ec__AnonStorey0_t4273334409  : public Il2CppObject
{
public:
	// System.String System.Xml.XmlBinaryDictionaryWriter/<LookupPrefix>c__AnonStorey0::ns
	String_t* ___ns_0;

public:
	inline static int32_t get_offset_of_ns_0() { return static_cast<int32_t>(offsetof(U3CLookupPrefixU3Ec__AnonStorey0_t4273334409, ___ns_0)); }
	inline String_t* get_ns_0() const { return ___ns_0; }
	inline String_t** get_address_of_ns_0() { return &___ns_0; }
	inline void set_ns_0(String_t* value)
	{
		___ns_0 = value;
		Il2CppCodeGenWriteBarrier(&___ns_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
