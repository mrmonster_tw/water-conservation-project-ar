﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement
struct  LocalServiceSecuritySettingsElement_t1052743477  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct LocalServiceSecuritySettingsElement_t1052743477_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::detect_replays
	ConfigurationProperty_t3590861854 * ___detect_replays_14;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::inactivity_timeout
	ConfigurationProperty_t3590861854 * ___inactivity_timeout_15;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::issued_cookie_lifetime
	ConfigurationProperty_t3590861854 * ___issued_cookie_lifetime_16;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::max_cached_cookies
	ConfigurationProperty_t3590861854 * ___max_cached_cookies_17;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::max_clock_skew
	ConfigurationProperty_t3590861854 * ___max_clock_skew_18;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::max_pending_sessions
	ConfigurationProperty_t3590861854 * ___max_pending_sessions_19;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::max_stateful_negotiations
	ConfigurationProperty_t3590861854 * ___max_stateful_negotiations_20;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::negotiation_timeout
	ConfigurationProperty_t3590861854 * ___negotiation_timeout_21;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::reconnect_transport_on_failure
	ConfigurationProperty_t3590861854 * ___reconnect_transport_on_failure_22;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::replay_cache_size
	ConfigurationProperty_t3590861854 * ___replay_cache_size_23;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::replay_window
	ConfigurationProperty_t3590861854 * ___replay_window_24;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::session_key_renewal_interval
	ConfigurationProperty_t3590861854 * ___session_key_renewal_interval_25;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::session_key_rollover_interval
	ConfigurationProperty_t3590861854 * ___session_key_rollover_interval_26;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.LocalServiceSecuritySettingsElement::timestamp_validity_duration
	ConfigurationProperty_t3590861854 * ___timestamp_validity_duration_27;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_detect_replays_14() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___detect_replays_14)); }
	inline ConfigurationProperty_t3590861854 * get_detect_replays_14() const { return ___detect_replays_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_detect_replays_14() { return &___detect_replays_14; }
	inline void set_detect_replays_14(ConfigurationProperty_t3590861854 * value)
	{
		___detect_replays_14 = value;
		Il2CppCodeGenWriteBarrier(&___detect_replays_14, value);
	}

	inline static int32_t get_offset_of_inactivity_timeout_15() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___inactivity_timeout_15)); }
	inline ConfigurationProperty_t3590861854 * get_inactivity_timeout_15() const { return ___inactivity_timeout_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_inactivity_timeout_15() { return &___inactivity_timeout_15; }
	inline void set_inactivity_timeout_15(ConfigurationProperty_t3590861854 * value)
	{
		___inactivity_timeout_15 = value;
		Il2CppCodeGenWriteBarrier(&___inactivity_timeout_15, value);
	}

	inline static int32_t get_offset_of_issued_cookie_lifetime_16() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___issued_cookie_lifetime_16)); }
	inline ConfigurationProperty_t3590861854 * get_issued_cookie_lifetime_16() const { return ___issued_cookie_lifetime_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_issued_cookie_lifetime_16() { return &___issued_cookie_lifetime_16; }
	inline void set_issued_cookie_lifetime_16(ConfigurationProperty_t3590861854 * value)
	{
		___issued_cookie_lifetime_16 = value;
		Il2CppCodeGenWriteBarrier(&___issued_cookie_lifetime_16, value);
	}

	inline static int32_t get_offset_of_max_cached_cookies_17() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___max_cached_cookies_17)); }
	inline ConfigurationProperty_t3590861854 * get_max_cached_cookies_17() const { return ___max_cached_cookies_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_cached_cookies_17() { return &___max_cached_cookies_17; }
	inline void set_max_cached_cookies_17(ConfigurationProperty_t3590861854 * value)
	{
		___max_cached_cookies_17 = value;
		Il2CppCodeGenWriteBarrier(&___max_cached_cookies_17, value);
	}

	inline static int32_t get_offset_of_max_clock_skew_18() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___max_clock_skew_18)); }
	inline ConfigurationProperty_t3590861854 * get_max_clock_skew_18() const { return ___max_clock_skew_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_clock_skew_18() { return &___max_clock_skew_18; }
	inline void set_max_clock_skew_18(ConfigurationProperty_t3590861854 * value)
	{
		___max_clock_skew_18 = value;
		Il2CppCodeGenWriteBarrier(&___max_clock_skew_18, value);
	}

	inline static int32_t get_offset_of_max_pending_sessions_19() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___max_pending_sessions_19)); }
	inline ConfigurationProperty_t3590861854 * get_max_pending_sessions_19() const { return ___max_pending_sessions_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_pending_sessions_19() { return &___max_pending_sessions_19; }
	inline void set_max_pending_sessions_19(ConfigurationProperty_t3590861854 * value)
	{
		___max_pending_sessions_19 = value;
		Il2CppCodeGenWriteBarrier(&___max_pending_sessions_19, value);
	}

	inline static int32_t get_offset_of_max_stateful_negotiations_20() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___max_stateful_negotiations_20)); }
	inline ConfigurationProperty_t3590861854 * get_max_stateful_negotiations_20() const { return ___max_stateful_negotiations_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_max_stateful_negotiations_20() { return &___max_stateful_negotiations_20; }
	inline void set_max_stateful_negotiations_20(ConfigurationProperty_t3590861854 * value)
	{
		___max_stateful_negotiations_20 = value;
		Il2CppCodeGenWriteBarrier(&___max_stateful_negotiations_20, value);
	}

	inline static int32_t get_offset_of_negotiation_timeout_21() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___negotiation_timeout_21)); }
	inline ConfigurationProperty_t3590861854 * get_negotiation_timeout_21() const { return ___negotiation_timeout_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_negotiation_timeout_21() { return &___negotiation_timeout_21; }
	inline void set_negotiation_timeout_21(ConfigurationProperty_t3590861854 * value)
	{
		___negotiation_timeout_21 = value;
		Il2CppCodeGenWriteBarrier(&___negotiation_timeout_21, value);
	}

	inline static int32_t get_offset_of_reconnect_transport_on_failure_22() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___reconnect_transport_on_failure_22)); }
	inline ConfigurationProperty_t3590861854 * get_reconnect_transport_on_failure_22() const { return ___reconnect_transport_on_failure_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_reconnect_transport_on_failure_22() { return &___reconnect_transport_on_failure_22; }
	inline void set_reconnect_transport_on_failure_22(ConfigurationProperty_t3590861854 * value)
	{
		___reconnect_transport_on_failure_22 = value;
		Il2CppCodeGenWriteBarrier(&___reconnect_transport_on_failure_22, value);
	}

	inline static int32_t get_offset_of_replay_cache_size_23() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___replay_cache_size_23)); }
	inline ConfigurationProperty_t3590861854 * get_replay_cache_size_23() const { return ___replay_cache_size_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_replay_cache_size_23() { return &___replay_cache_size_23; }
	inline void set_replay_cache_size_23(ConfigurationProperty_t3590861854 * value)
	{
		___replay_cache_size_23 = value;
		Il2CppCodeGenWriteBarrier(&___replay_cache_size_23, value);
	}

	inline static int32_t get_offset_of_replay_window_24() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___replay_window_24)); }
	inline ConfigurationProperty_t3590861854 * get_replay_window_24() const { return ___replay_window_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_replay_window_24() { return &___replay_window_24; }
	inline void set_replay_window_24(ConfigurationProperty_t3590861854 * value)
	{
		___replay_window_24 = value;
		Il2CppCodeGenWriteBarrier(&___replay_window_24, value);
	}

	inline static int32_t get_offset_of_session_key_renewal_interval_25() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___session_key_renewal_interval_25)); }
	inline ConfigurationProperty_t3590861854 * get_session_key_renewal_interval_25() const { return ___session_key_renewal_interval_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_session_key_renewal_interval_25() { return &___session_key_renewal_interval_25; }
	inline void set_session_key_renewal_interval_25(ConfigurationProperty_t3590861854 * value)
	{
		___session_key_renewal_interval_25 = value;
		Il2CppCodeGenWriteBarrier(&___session_key_renewal_interval_25, value);
	}

	inline static int32_t get_offset_of_session_key_rollover_interval_26() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___session_key_rollover_interval_26)); }
	inline ConfigurationProperty_t3590861854 * get_session_key_rollover_interval_26() const { return ___session_key_rollover_interval_26; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_session_key_rollover_interval_26() { return &___session_key_rollover_interval_26; }
	inline void set_session_key_rollover_interval_26(ConfigurationProperty_t3590861854 * value)
	{
		___session_key_rollover_interval_26 = value;
		Il2CppCodeGenWriteBarrier(&___session_key_rollover_interval_26, value);
	}

	inline static int32_t get_offset_of_timestamp_validity_duration_27() { return static_cast<int32_t>(offsetof(LocalServiceSecuritySettingsElement_t1052743477_StaticFields, ___timestamp_validity_duration_27)); }
	inline ConfigurationProperty_t3590861854 * get_timestamp_validity_duration_27() const { return ___timestamp_validity_duration_27; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_timestamp_validity_duration_27() { return &___timestamp_validity_duration_27; }
	inline void set_timestamp_validity_duration_27(ConfigurationProperty_t3590861854 * value)
	{
		___timestamp_validity_duration_27 = value;
		Il2CppCodeGenWriteBarrier(&___timestamp_validity_duration_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
