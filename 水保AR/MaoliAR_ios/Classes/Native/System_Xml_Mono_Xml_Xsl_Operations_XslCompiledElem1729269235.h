﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslOperation2153241355.h"

// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t787956054;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslCompiledElementBase
struct  XslCompiledElementBase_t1729269235  : public XslOperation_t2153241355
{
public:
	// System.Int32 Mono.Xml.Xsl.Operations.XslCompiledElementBase::lineNumber
	int32_t ___lineNumber_0;
	// System.Int32 Mono.Xml.Xsl.Operations.XslCompiledElementBase::linePosition
	int32_t ___linePosition_1;
	// System.Xml.XPath.XPathNavigator Mono.Xml.Xsl.Operations.XslCompiledElementBase::debugInput
	XPathNavigator_t787956054 * ___debugInput_2;

public:
	inline static int32_t get_offset_of_lineNumber_0() { return static_cast<int32_t>(offsetof(XslCompiledElementBase_t1729269235, ___lineNumber_0)); }
	inline int32_t get_lineNumber_0() const { return ___lineNumber_0; }
	inline int32_t* get_address_of_lineNumber_0() { return &___lineNumber_0; }
	inline void set_lineNumber_0(int32_t value)
	{
		___lineNumber_0 = value;
	}

	inline static int32_t get_offset_of_linePosition_1() { return static_cast<int32_t>(offsetof(XslCompiledElementBase_t1729269235, ___linePosition_1)); }
	inline int32_t get_linePosition_1() const { return ___linePosition_1; }
	inline int32_t* get_address_of_linePosition_1() { return &___linePosition_1; }
	inline void set_linePosition_1(int32_t value)
	{
		___linePosition_1 = value;
	}

	inline static int32_t get_offset_of_debugInput_2() { return static_cast<int32_t>(offsetof(XslCompiledElementBase_t1729269235, ___debugInput_2)); }
	inline XPathNavigator_t787956054 * get_debugInput_2() const { return ___debugInput_2; }
	inline XPathNavigator_t787956054 ** get_address_of_debugInput_2() { return &___debugInput_2; }
	inline void set_debugInput_2(XPathNavigator_t787956054 * value)
	{
		___debugInput_2 = value;
		Il2CppCodeGenWriteBarrier(&___debugInput_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
