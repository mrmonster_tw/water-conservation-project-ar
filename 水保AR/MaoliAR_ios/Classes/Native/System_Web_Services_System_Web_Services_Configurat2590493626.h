﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Configuration.DiagnosticsElement
struct  DiagnosticsElement_t2590493626  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct DiagnosticsElement_t2590493626_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Web.Services.Configuration.DiagnosticsElement::suppressReturningExceptionsProp
	ConfigurationProperty_t3590861854 * ___suppressReturningExceptionsProp_13;
	// System.Configuration.ConfigurationPropertyCollection System.Web.Services.Configuration.DiagnosticsElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_14;

public:
	inline static int32_t get_offset_of_suppressReturningExceptionsProp_13() { return static_cast<int32_t>(offsetof(DiagnosticsElement_t2590493626_StaticFields, ___suppressReturningExceptionsProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_suppressReturningExceptionsProp_13() const { return ___suppressReturningExceptionsProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_suppressReturningExceptionsProp_13() { return &___suppressReturningExceptionsProp_13; }
	inline void set_suppressReturningExceptionsProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___suppressReturningExceptionsProp_13 = value;
		Il2CppCodeGenWriteBarrier(&___suppressReturningExceptionsProp_13, value);
	}

	inline static int32_t get_offset_of_properties_14() { return static_cast<int32_t>(offsetof(DiagnosticsElement_t2590493626_StaticFields, ___properties_14)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_14() const { return ___properties_14; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_14() { return &___properties_14; }
	inline void set_properties_14(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_14 = value;
		Il2CppCodeGenWriteBarrier(&___properties_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
