﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Byte
struct Byte_t1134296376;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpResponseStream/BlockManager
struct  BlockManager_t141966076  : public Il2CppObject
{
public:
	// System.Byte* System.Web.HttpResponseStream/BlockManager::data
	uint8_t* ___data_0;
	// System.Int32 System.Web.HttpResponseStream/BlockManager::position
	int32_t ___position_1;
	// System.Int32 System.Web.HttpResponseStream/BlockManager::block_size
	int32_t ___block_size_2;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(BlockManager_t141966076, ___data_0)); }
	inline uint8_t* get_data_0() const { return ___data_0; }
	inline uint8_t** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(uint8_t* value)
	{
		___data_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(BlockManager_t141966076, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_block_size_2() { return static_cast<int32_t>(offsetof(BlockManager_t141966076, ___block_size_2)); }
	inline int32_t get_block_size_2() const { return ___block_size_2; }
	inline int32_t* get_address_of_block_size_2() { return &___block_size_2; }
	inline void set_block_size_2(int32_t value)
	{
		___block_size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
