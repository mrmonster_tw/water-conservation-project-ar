﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_RestFulsClass_RestFulClass2200423901.h"
#include "AssemblyU2DCSharp_GetTickets_IPAddress1620270522.h"
#include "AssemblyU2DCSharp_GetTickets_PlayerSetting3195188508.h"
#include "AssemblyU2DCSharp_GetTickets_ComponetFuncScript187458.h"

// GetTickets
struct GetTickets_t911946028;
// System.String
struct String_t;
// System.DateTime[]
struct DateTimeU5BU5D_t1184652292;
// UnityEngine.Animator
struct Animator_t434523843;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Predicate`1<UnityEngine.Animator>
struct Predicate_1_t1259817967;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetTickets
struct  GetTickets_t911946028  : public RestFulClass_t2200423901
{
public:
	// GetTickets/IPAddress GetTickets::address
	int32_t ___address_2;
	// System.Int32 GetTickets::hour
	int32_t ___hour_6;
	// System.Int32 GetTickets::minute
	int32_t ___minute_7;
	// GetTickets/PlayerSetting GetTickets::playerSetting
	PlayerSetting_t3195188508  ___playerSetting_8;
	// GetTickets/ComponetFuncScript GetTickets::componetFuncScript
	ComponetFuncScript_t187458  ___componetFuncScript_9;
	// System.DateTime[] GetTickets::d3D
	DateTimeU5BU5D_t1184652292* ___d3D_10;
	// System.DateTime[] GetTickets::d4D
	DateTimeU5BU5D_t1184652292* ___d4D_11;
	// System.DateTime[] GetTickets::dnoD
	DateTimeU5BU5D_t1184652292* ___dnoD_12;
	// UnityEngine.Animator GetTickets::Connecting
	Animator_t434523843 * ___Connecting_13;
	// System.Collections.Generic.List`1<System.String> GetTickets::checkExpiredresult
	List_1_t3319525431 * ___checkExpiredresult_14;
	// UnityEngine.GameObject GetTickets::thanks
	GameObject_t1113636619 * ___thanks_16;
	// System.String GetTickets::yourUnQuestionedMode
	String_t* ___yourUnQuestionedMode_17;
	// System.Int32 GetTickets::yourUnQuestionedNumber
	int32_t ___yourUnQuestionedNumber_18;
	// System.Boolean GetTickets::sureInsert
	bool ___sureInsert_19;
	// System.Single GetTickets::waitScendAndAnimationTime
	float ___waitScendAndAnimationTime_20;
	// System.Boolean GetTickets::isquitting
	bool ___isquitting_21;
	// System.Boolean GetTickets::Shot
	bool ___Shot_22;
	// UnityEngine.Texture2D GetTickets::tex
	Texture2D_t3840446185 * ___tex_23;

public:
	inline static int32_t get_offset_of_address_2() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___address_2)); }
	inline int32_t get_address_2() const { return ___address_2; }
	inline int32_t* get_address_of_address_2() { return &___address_2; }
	inline void set_address_2(int32_t value)
	{
		___address_2 = value;
	}

	inline static int32_t get_offset_of_hour_6() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___hour_6)); }
	inline int32_t get_hour_6() const { return ___hour_6; }
	inline int32_t* get_address_of_hour_6() { return &___hour_6; }
	inline void set_hour_6(int32_t value)
	{
		___hour_6 = value;
	}

	inline static int32_t get_offset_of_minute_7() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___minute_7)); }
	inline int32_t get_minute_7() const { return ___minute_7; }
	inline int32_t* get_address_of_minute_7() { return &___minute_7; }
	inline void set_minute_7(int32_t value)
	{
		___minute_7 = value;
	}

	inline static int32_t get_offset_of_playerSetting_8() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___playerSetting_8)); }
	inline PlayerSetting_t3195188508  get_playerSetting_8() const { return ___playerSetting_8; }
	inline PlayerSetting_t3195188508 * get_address_of_playerSetting_8() { return &___playerSetting_8; }
	inline void set_playerSetting_8(PlayerSetting_t3195188508  value)
	{
		___playerSetting_8 = value;
	}

	inline static int32_t get_offset_of_componetFuncScript_9() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___componetFuncScript_9)); }
	inline ComponetFuncScript_t187458  get_componetFuncScript_9() const { return ___componetFuncScript_9; }
	inline ComponetFuncScript_t187458 * get_address_of_componetFuncScript_9() { return &___componetFuncScript_9; }
	inline void set_componetFuncScript_9(ComponetFuncScript_t187458  value)
	{
		___componetFuncScript_9 = value;
	}

	inline static int32_t get_offset_of_d3D_10() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___d3D_10)); }
	inline DateTimeU5BU5D_t1184652292* get_d3D_10() const { return ___d3D_10; }
	inline DateTimeU5BU5D_t1184652292** get_address_of_d3D_10() { return &___d3D_10; }
	inline void set_d3D_10(DateTimeU5BU5D_t1184652292* value)
	{
		___d3D_10 = value;
		Il2CppCodeGenWriteBarrier(&___d3D_10, value);
	}

	inline static int32_t get_offset_of_d4D_11() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___d4D_11)); }
	inline DateTimeU5BU5D_t1184652292* get_d4D_11() const { return ___d4D_11; }
	inline DateTimeU5BU5D_t1184652292** get_address_of_d4D_11() { return &___d4D_11; }
	inline void set_d4D_11(DateTimeU5BU5D_t1184652292* value)
	{
		___d4D_11 = value;
		Il2CppCodeGenWriteBarrier(&___d4D_11, value);
	}

	inline static int32_t get_offset_of_dnoD_12() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___dnoD_12)); }
	inline DateTimeU5BU5D_t1184652292* get_dnoD_12() const { return ___dnoD_12; }
	inline DateTimeU5BU5D_t1184652292** get_address_of_dnoD_12() { return &___dnoD_12; }
	inline void set_dnoD_12(DateTimeU5BU5D_t1184652292* value)
	{
		___dnoD_12 = value;
		Il2CppCodeGenWriteBarrier(&___dnoD_12, value);
	}

	inline static int32_t get_offset_of_Connecting_13() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___Connecting_13)); }
	inline Animator_t434523843 * get_Connecting_13() const { return ___Connecting_13; }
	inline Animator_t434523843 ** get_address_of_Connecting_13() { return &___Connecting_13; }
	inline void set_Connecting_13(Animator_t434523843 * value)
	{
		___Connecting_13 = value;
		Il2CppCodeGenWriteBarrier(&___Connecting_13, value);
	}

	inline static int32_t get_offset_of_checkExpiredresult_14() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___checkExpiredresult_14)); }
	inline List_1_t3319525431 * get_checkExpiredresult_14() const { return ___checkExpiredresult_14; }
	inline List_1_t3319525431 ** get_address_of_checkExpiredresult_14() { return &___checkExpiredresult_14; }
	inline void set_checkExpiredresult_14(List_1_t3319525431 * value)
	{
		___checkExpiredresult_14 = value;
		Il2CppCodeGenWriteBarrier(&___checkExpiredresult_14, value);
	}

	inline static int32_t get_offset_of_thanks_16() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___thanks_16)); }
	inline GameObject_t1113636619 * get_thanks_16() const { return ___thanks_16; }
	inline GameObject_t1113636619 ** get_address_of_thanks_16() { return &___thanks_16; }
	inline void set_thanks_16(GameObject_t1113636619 * value)
	{
		___thanks_16 = value;
		Il2CppCodeGenWriteBarrier(&___thanks_16, value);
	}

	inline static int32_t get_offset_of_yourUnQuestionedMode_17() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___yourUnQuestionedMode_17)); }
	inline String_t* get_yourUnQuestionedMode_17() const { return ___yourUnQuestionedMode_17; }
	inline String_t** get_address_of_yourUnQuestionedMode_17() { return &___yourUnQuestionedMode_17; }
	inline void set_yourUnQuestionedMode_17(String_t* value)
	{
		___yourUnQuestionedMode_17 = value;
		Il2CppCodeGenWriteBarrier(&___yourUnQuestionedMode_17, value);
	}

	inline static int32_t get_offset_of_yourUnQuestionedNumber_18() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___yourUnQuestionedNumber_18)); }
	inline int32_t get_yourUnQuestionedNumber_18() const { return ___yourUnQuestionedNumber_18; }
	inline int32_t* get_address_of_yourUnQuestionedNumber_18() { return &___yourUnQuestionedNumber_18; }
	inline void set_yourUnQuestionedNumber_18(int32_t value)
	{
		___yourUnQuestionedNumber_18 = value;
	}

	inline static int32_t get_offset_of_sureInsert_19() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___sureInsert_19)); }
	inline bool get_sureInsert_19() const { return ___sureInsert_19; }
	inline bool* get_address_of_sureInsert_19() { return &___sureInsert_19; }
	inline void set_sureInsert_19(bool value)
	{
		___sureInsert_19 = value;
	}

	inline static int32_t get_offset_of_waitScendAndAnimationTime_20() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___waitScendAndAnimationTime_20)); }
	inline float get_waitScendAndAnimationTime_20() const { return ___waitScendAndAnimationTime_20; }
	inline float* get_address_of_waitScendAndAnimationTime_20() { return &___waitScendAndAnimationTime_20; }
	inline void set_waitScendAndAnimationTime_20(float value)
	{
		___waitScendAndAnimationTime_20 = value;
	}

	inline static int32_t get_offset_of_isquitting_21() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___isquitting_21)); }
	inline bool get_isquitting_21() const { return ___isquitting_21; }
	inline bool* get_address_of_isquitting_21() { return &___isquitting_21; }
	inline void set_isquitting_21(bool value)
	{
		___isquitting_21 = value;
	}

	inline static int32_t get_offset_of_Shot_22() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___Shot_22)); }
	inline bool get_Shot_22() const { return ___Shot_22; }
	inline bool* get_address_of_Shot_22() { return &___Shot_22; }
	inline void set_Shot_22(bool value)
	{
		___Shot_22 = value;
	}

	inline static int32_t get_offset_of_tex_23() { return static_cast<int32_t>(offsetof(GetTickets_t911946028, ___tex_23)); }
	inline Texture2D_t3840446185 * get_tex_23() const { return ___tex_23; }
	inline Texture2D_t3840446185 ** get_address_of_tex_23() { return &___tex_23; }
	inline void set_tex_23(Texture2D_t3840446185 * value)
	{
		___tex_23 = value;
		Il2CppCodeGenWriteBarrier(&___tex_23, value);
	}
};

struct GetTickets_t911946028_StaticFields
{
public:
	// GetTickets GetTickets::This
	GetTickets_t911946028 * ___This_3;
	// System.String GetTickets::Mode
	String_t* ___Mode_4;
	// System.String GetTickets::PlayerUUID
	String_t* ___PlayerUUID_5;
	// System.Boolean[] GetTickets::checkingAny
	BooleanU5BU5D_t2897418192* ___checkingAny_15;
	// System.Predicate`1<UnityEngine.Animator> GetTickets::<>f__am$cache0
	Predicate_1_t1259817967 * ___U3CU3Ef__amU24cache0_24;

public:
	inline static int32_t get_offset_of_This_3() { return static_cast<int32_t>(offsetof(GetTickets_t911946028_StaticFields, ___This_3)); }
	inline GetTickets_t911946028 * get_This_3() const { return ___This_3; }
	inline GetTickets_t911946028 ** get_address_of_This_3() { return &___This_3; }
	inline void set_This_3(GetTickets_t911946028 * value)
	{
		___This_3 = value;
		Il2CppCodeGenWriteBarrier(&___This_3, value);
	}

	inline static int32_t get_offset_of_Mode_4() { return static_cast<int32_t>(offsetof(GetTickets_t911946028_StaticFields, ___Mode_4)); }
	inline String_t* get_Mode_4() const { return ___Mode_4; }
	inline String_t** get_address_of_Mode_4() { return &___Mode_4; }
	inline void set_Mode_4(String_t* value)
	{
		___Mode_4 = value;
		Il2CppCodeGenWriteBarrier(&___Mode_4, value);
	}

	inline static int32_t get_offset_of_PlayerUUID_5() { return static_cast<int32_t>(offsetof(GetTickets_t911946028_StaticFields, ___PlayerUUID_5)); }
	inline String_t* get_PlayerUUID_5() const { return ___PlayerUUID_5; }
	inline String_t** get_address_of_PlayerUUID_5() { return &___PlayerUUID_5; }
	inline void set_PlayerUUID_5(String_t* value)
	{
		___PlayerUUID_5 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerUUID_5, value);
	}

	inline static int32_t get_offset_of_checkingAny_15() { return static_cast<int32_t>(offsetof(GetTickets_t911946028_StaticFields, ___checkingAny_15)); }
	inline BooleanU5BU5D_t2897418192* get_checkingAny_15() const { return ___checkingAny_15; }
	inline BooleanU5BU5D_t2897418192** get_address_of_checkingAny_15() { return &___checkingAny_15; }
	inline void set_checkingAny_15(BooleanU5BU5D_t2897418192* value)
	{
		___checkingAny_15 = value;
		Il2CppCodeGenWriteBarrier(&___checkingAny_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_24() { return static_cast<int32_t>(offsetof(GetTickets_t911946028_StaticFields, ___U3CU3Ef__amU24cache0_24)); }
	inline Predicate_1_t1259817967 * get_U3CU3Ef__amU24cache0_24() const { return ___U3CU3Ef__amU24cache0_24; }
	inline Predicate_1_t1259817967 ** get_address_of_U3CU3Ef__amU24cache0_24() { return &___U3CU3Ef__amU24cache0_24; }
	inline void set_U3CU3Ef__amU24cache0_24(Predicate_1_t1259817967 * value)
	{
		___U3CU3Ef__amU24cache0_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
