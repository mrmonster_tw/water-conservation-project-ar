﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.Triplet
struct  Triplet_t2158800263  : public Il2CppObject
{
public:
	// System.Object System.Web.UI.Triplet::First
	Il2CppObject * ___First_0;
	// System.Object System.Web.UI.Triplet::Second
	Il2CppObject * ___Second_1;
	// System.Object System.Web.UI.Triplet::Third
	Il2CppObject * ___Third_2;

public:
	inline static int32_t get_offset_of_First_0() { return static_cast<int32_t>(offsetof(Triplet_t2158800263, ___First_0)); }
	inline Il2CppObject * get_First_0() const { return ___First_0; }
	inline Il2CppObject ** get_address_of_First_0() { return &___First_0; }
	inline void set_First_0(Il2CppObject * value)
	{
		___First_0 = value;
		Il2CppCodeGenWriteBarrier(&___First_0, value);
	}

	inline static int32_t get_offset_of_Second_1() { return static_cast<int32_t>(offsetof(Triplet_t2158800263, ___Second_1)); }
	inline Il2CppObject * get_Second_1() const { return ___Second_1; }
	inline Il2CppObject ** get_address_of_Second_1() { return &___Second_1; }
	inline void set_Second_1(Il2CppObject * value)
	{
		___Second_1 = value;
		Il2CppCodeGenWriteBarrier(&___Second_1, value);
	}

	inline static int32_t get_offset_of_Third_2() { return static_cast<int32_t>(offsetof(Triplet_t2158800263, ___Third_2)); }
	inline Il2CppObject * get_Third_2() const { return ___Third_2; }
	inline Il2CppObject ** get_address_of_Third_2() { return &___Third_2; }
	inline void set_Third_2(Il2CppObject * value)
	{
		___Third_2 = value;
		Il2CppCodeGenWriteBarrier(&___Third_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
