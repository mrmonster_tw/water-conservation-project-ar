﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IO.TextReader
struct TextReader_t283511965;
// System.Text.StringBuilder
struct StringBuilder_t1712802186;
// System.String
struct String_t;
// System.Collections.Stack
struct Stack_t2329662280;
// System.Security.Cryptography.MD5
struct MD5_t3177620429;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.AspTokenizer
struct  AspTokenizer_t992095833  : public Il2CppObject
{
public:
	// System.IO.TextReader System.Web.Compilation.AspTokenizer::sr
	TextReader_t283511965 * ___sr_1;
	// System.Int32 System.Web.Compilation.AspTokenizer::current_token
	int32_t ___current_token_2;
	// System.Text.StringBuilder System.Web.Compilation.AspTokenizer::sb
	StringBuilder_t1712802186 * ___sb_3;
	// System.Text.StringBuilder System.Web.Compilation.AspTokenizer::odds
	StringBuilder_t1712802186 * ___odds_4;
	// System.Int32 System.Web.Compilation.AspTokenizer::col
	int32_t ___col_5;
	// System.Int32 System.Web.Compilation.AspTokenizer::line
	int32_t ___line_6;
	// System.Int32 System.Web.Compilation.AspTokenizer::begcol
	int32_t ___begcol_7;
	// System.Int32 System.Web.Compilation.AspTokenizer::begline
	int32_t ___begline_8;
	// System.Int32 System.Web.Compilation.AspTokenizer::position
	int32_t ___position_9;
	// System.Boolean System.Web.Compilation.AspTokenizer::inTag
	bool ___inTag_10;
	// System.Boolean System.Web.Compilation.AspTokenizer::expectAttrValue
	bool ___expectAttrValue_11;
	// System.Boolean System.Web.Compilation.AspTokenizer::alternatingQuotes
	bool ___alternatingQuotes_12;
	// System.Boolean System.Web.Compilation.AspTokenizer::hasPutBack
	bool ___hasPutBack_13;
	// System.Boolean System.Web.Compilation.AspTokenizer::verbatim
	bool ___verbatim_14;
	// System.Boolean System.Web.Compilation.AspTokenizer::have_value
	bool ___have_value_15;
	// System.Boolean System.Web.Compilation.AspTokenizer::have_unget
	bool ___have_unget_16;
	// System.Int32 System.Web.Compilation.AspTokenizer::unget_value
	int32_t ___unget_value_17;
	// System.String System.Web.Compilation.AspTokenizer::val
	String_t* ___val_18;
	// System.Collections.Stack System.Web.Compilation.AspTokenizer::putBackBuffer
	Stack_t2329662280 * ___putBackBuffer_19;
	// System.Security.Cryptography.MD5 System.Web.Compilation.AspTokenizer::checksum
	MD5_t3177620429 * ___checksum_20;
	// System.Char[] System.Web.Compilation.AspTokenizer::checksum_buf
	CharU5BU5D_t3528271667* ___checksum_buf_21;
	// System.Int32 System.Web.Compilation.AspTokenizer::checksum_buf_pos
	int32_t ___checksum_buf_pos_22;

public:
	inline static int32_t get_offset_of_sr_1() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___sr_1)); }
	inline TextReader_t283511965 * get_sr_1() const { return ___sr_1; }
	inline TextReader_t283511965 ** get_address_of_sr_1() { return &___sr_1; }
	inline void set_sr_1(TextReader_t283511965 * value)
	{
		___sr_1 = value;
		Il2CppCodeGenWriteBarrier(&___sr_1, value);
	}

	inline static int32_t get_offset_of_current_token_2() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___current_token_2)); }
	inline int32_t get_current_token_2() const { return ___current_token_2; }
	inline int32_t* get_address_of_current_token_2() { return &___current_token_2; }
	inline void set_current_token_2(int32_t value)
	{
		___current_token_2 = value;
	}

	inline static int32_t get_offset_of_sb_3() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___sb_3)); }
	inline StringBuilder_t1712802186 * get_sb_3() const { return ___sb_3; }
	inline StringBuilder_t1712802186 ** get_address_of_sb_3() { return &___sb_3; }
	inline void set_sb_3(StringBuilder_t1712802186 * value)
	{
		___sb_3 = value;
		Il2CppCodeGenWriteBarrier(&___sb_3, value);
	}

	inline static int32_t get_offset_of_odds_4() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___odds_4)); }
	inline StringBuilder_t1712802186 * get_odds_4() const { return ___odds_4; }
	inline StringBuilder_t1712802186 ** get_address_of_odds_4() { return &___odds_4; }
	inline void set_odds_4(StringBuilder_t1712802186 * value)
	{
		___odds_4 = value;
		Il2CppCodeGenWriteBarrier(&___odds_4, value);
	}

	inline static int32_t get_offset_of_col_5() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___col_5)); }
	inline int32_t get_col_5() const { return ___col_5; }
	inline int32_t* get_address_of_col_5() { return &___col_5; }
	inline void set_col_5(int32_t value)
	{
		___col_5 = value;
	}

	inline static int32_t get_offset_of_line_6() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___line_6)); }
	inline int32_t get_line_6() const { return ___line_6; }
	inline int32_t* get_address_of_line_6() { return &___line_6; }
	inline void set_line_6(int32_t value)
	{
		___line_6 = value;
	}

	inline static int32_t get_offset_of_begcol_7() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___begcol_7)); }
	inline int32_t get_begcol_7() const { return ___begcol_7; }
	inline int32_t* get_address_of_begcol_7() { return &___begcol_7; }
	inline void set_begcol_7(int32_t value)
	{
		___begcol_7 = value;
	}

	inline static int32_t get_offset_of_begline_8() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___begline_8)); }
	inline int32_t get_begline_8() const { return ___begline_8; }
	inline int32_t* get_address_of_begline_8() { return &___begline_8; }
	inline void set_begline_8(int32_t value)
	{
		___begline_8 = value;
	}

	inline static int32_t get_offset_of_position_9() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___position_9)); }
	inline int32_t get_position_9() const { return ___position_9; }
	inline int32_t* get_address_of_position_9() { return &___position_9; }
	inline void set_position_9(int32_t value)
	{
		___position_9 = value;
	}

	inline static int32_t get_offset_of_inTag_10() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___inTag_10)); }
	inline bool get_inTag_10() const { return ___inTag_10; }
	inline bool* get_address_of_inTag_10() { return &___inTag_10; }
	inline void set_inTag_10(bool value)
	{
		___inTag_10 = value;
	}

	inline static int32_t get_offset_of_expectAttrValue_11() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___expectAttrValue_11)); }
	inline bool get_expectAttrValue_11() const { return ___expectAttrValue_11; }
	inline bool* get_address_of_expectAttrValue_11() { return &___expectAttrValue_11; }
	inline void set_expectAttrValue_11(bool value)
	{
		___expectAttrValue_11 = value;
	}

	inline static int32_t get_offset_of_alternatingQuotes_12() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___alternatingQuotes_12)); }
	inline bool get_alternatingQuotes_12() const { return ___alternatingQuotes_12; }
	inline bool* get_address_of_alternatingQuotes_12() { return &___alternatingQuotes_12; }
	inline void set_alternatingQuotes_12(bool value)
	{
		___alternatingQuotes_12 = value;
	}

	inline static int32_t get_offset_of_hasPutBack_13() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___hasPutBack_13)); }
	inline bool get_hasPutBack_13() const { return ___hasPutBack_13; }
	inline bool* get_address_of_hasPutBack_13() { return &___hasPutBack_13; }
	inline void set_hasPutBack_13(bool value)
	{
		___hasPutBack_13 = value;
	}

	inline static int32_t get_offset_of_verbatim_14() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___verbatim_14)); }
	inline bool get_verbatim_14() const { return ___verbatim_14; }
	inline bool* get_address_of_verbatim_14() { return &___verbatim_14; }
	inline void set_verbatim_14(bool value)
	{
		___verbatim_14 = value;
	}

	inline static int32_t get_offset_of_have_value_15() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___have_value_15)); }
	inline bool get_have_value_15() const { return ___have_value_15; }
	inline bool* get_address_of_have_value_15() { return &___have_value_15; }
	inline void set_have_value_15(bool value)
	{
		___have_value_15 = value;
	}

	inline static int32_t get_offset_of_have_unget_16() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___have_unget_16)); }
	inline bool get_have_unget_16() const { return ___have_unget_16; }
	inline bool* get_address_of_have_unget_16() { return &___have_unget_16; }
	inline void set_have_unget_16(bool value)
	{
		___have_unget_16 = value;
	}

	inline static int32_t get_offset_of_unget_value_17() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___unget_value_17)); }
	inline int32_t get_unget_value_17() const { return ___unget_value_17; }
	inline int32_t* get_address_of_unget_value_17() { return &___unget_value_17; }
	inline void set_unget_value_17(int32_t value)
	{
		___unget_value_17 = value;
	}

	inline static int32_t get_offset_of_val_18() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___val_18)); }
	inline String_t* get_val_18() const { return ___val_18; }
	inline String_t** get_address_of_val_18() { return &___val_18; }
	inline void set_val_18(String_t* value)
	{
		___val_18 = value;
		Il2CppCodeGenWriteBarrier(&___val_18, value);
	}

	inline static int32_t get_offset_of_putBackBuffer_19() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___putBackBuffer_19)); }
	inline Stack_t2329662280 * get_putBackBuffer_19() const { return ___putBackBuffer_19; }
	inline Stack_t2329662280 ** get_address_of_putBackBuffer_19() { return &___putBackBuffer_19; }
	inline void set_putBackBuffer_19(Stack_t2329662280 * value)
	{
		___putBackBuffer_19 = value;
		Il2CppCodeGenWriteBarrier(&___putBackBuffer_19, value);
	}

	inline static int32_t get_offset_of_checksum_20() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___checksum_20)); }
	inline MD5_t3177620429 * get_checksum_20() const { return ___checksum_20; }
	inline MD5_t3177620429 ** get_address_of_checksum_20() { return &___checksum_20; }
	inline void set_checksum_20(MD5_t3177620429 * value)
	{
		___checksum_20 = value;
		Il2CppCodeGenWriteBarrier(&___checksum_20, value);
	}

	inline static int32_t get_offset_of_checksum_buf_21() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___checksum_buf_21)); }
	inline CharU5BU5D_t3528271667* get_checksum_buf_21() const { return ___checksum_buf_21; }
	inline CharU5BU5D_t3528271667** get_address_of_checksum_buf_21() { return &___checksum_buf_21; }
	inline void set_checksum_buf_21(CharU5BU5D_t3528271667* value)
	{
		___checksum_buf_21 = value;
		Il2CppCodeGenWriteBarrier(&___checksum_buf_21, value);
	}

	inline static int32_t get_offset_of_checksum_buf_pos_22() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833, ___checksum_buf_pos_22)); }
	inline int32_t get_checksum_buf_pos_22() const { return ___checksum_buf_pos_22; }
	inline int32_t* get_address_of_checksum_buf_pos_22() { return &___checksum_buf_pos_22; }
	inline void set_checksum_buf_pos_22(int32_t value)
	{
		___checksum_buf_pos_22 = value;
	}
};

struct AspTokenizer_t992095833_StaticFields
{
public:
	// System.Char[] System.Web.Compilation.AspTokenizer::lfcr
	CharU5BU5D_t3528271667* ___lfcr_0;

public:
	inline static int32_t get_offset_of_lfcr_0() { return static_cast<int32_t>(offsetof(AspTokenizer_t992095833_StaticFields, ___lfcr_0)); }
	inline CharU5BU5D_t3528271667* get_lfcr_0() const { return ___lfcr_0; }
	inline CharU5BU5D_t3528271667** get_address_of_lfcr_0() { return &___lfcr_0; }
	inline void set_lfcr_0(CharU5BU5D_t3528271667* value)
	{
		___lfcr_0 = value;
		Il2CppCodeGenWriteBarrier(&___lfcr_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
