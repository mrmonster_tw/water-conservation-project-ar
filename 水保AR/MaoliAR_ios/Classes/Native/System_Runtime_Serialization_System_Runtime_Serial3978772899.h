﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Runtime_Serial3967301761.h"

// System.Type
struct Type_t;
// System.Runtime.Serialization.KnownTypeCollection
struct KnownTypeCollection_t3509697551;
// System.Runtime.Serialization.IDataContractSurrogate
struct IDataContractSurrogate_t3505381635;
// System.Xml.XmlDictionaryString
struct XmlDictionaryString_t3504120266;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DataContractSerializer
struct  DataContractSerializer_t3978772899  : public XmlObjectSerializer_t3967301761
{
public:
	// System.Type System.Runtime.Serialization.DataContractSerializer::type
	Type_t * ___type_1;
	// System.Boolean System.Runtime.Serialization.DataContractSerializer::ignore_ext
	bool ___ignore_ext_2;
	// System.Runtime.Serialization.KnownTypeCollection System.Runtime.Serialization.DataContractSerializer::known_types
	KnownTypeCollection_t3509697551 * ___known_types_3;
	// System.Runtime.Serialization.IDataContractSurrogate System.Runtime.Serialization.DataContractSerializer::surrogate
	Il2CppObject * ___surrogate_4;
	// System.Int32 System.Runtime.Serialization.DataContractSerializer::max_items
	int32_t ___max_items_5;
	// System.Boolean System.Runtime.Serialization.DataContractSerializer::names_filled
	bool ___names_filled_6;
	// System.Xml.XmlDictionaryString System.Runtime.Serialization.DataContractSerializer::root_name
	XmlDictionaryString_t3504120266 * ___root_name_7;
	// System.Xml.XmlDictionaryString System.Runtime.Serialization.DataContractSerializer::root_ns
	XmlDictionaryString_t3504120266 * ___root_ns_8;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DataContractSerializer_t3978772899, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_ignore_ext_2() { return static_cast<int32_t>(offsetof(DataContractSerializer_t3978772899, ___ignore_ext_2)); }
	inline bool get_ignore_ext_2() const { return ___ignore_ext_2; }
	inline bool* get_address_of_ignore_ext_2() { return &___ignore_ext_2; }
	inline void set_ignore_ext_2(bool value)
	{
		___ignore_ext_2 = value;
	}

	inline static int32_t get_offset_of_known_types_3() { return static_cast<int32_t>(offsetof(DataContractSerializer_t3978772899, ___known_types_3)); }
	inline KnownTypeCollection_t3509697551 * get_known_types_3() const { return ___known_types_3; }
	inline KnownTypeCollection_t3509697551 ** get_address_of_known_types_3() { return &___known_types_3; }
	inline void set_known_types_3(KnownTypeCollection_t3509697551 * value)
	{
		___known_types_3 = value;
		Il2CppCodeGenWriteBarrier(&___known_types_3, value);
	}

	inline static int32_t get_offset_of_surrogate_4() { return static_cast<int32_t>(offsetof(DataContractSerializer_t3978772899, ___surrogate_4)); }
	inline Il2CppObject * get_surrogate_4() const { return ___surrogate_4; }
	inline Il2CppObject ** get_address_of_surrogate_4() { return &___surrogate_4; }
	inline void set_surrogate_4(Il2CppObject * value)
	{
		___surrogate_4 = value;
		Il2CppCodeGenWriteBarrier(&___surrogate_4, value);
	}

	inline static int32_t get_offset_of_max_items_5() { return static_cast<int32_t>(offsetof(DataContractSerializer_t3978772899, ___max_items_5)); }
	inline int32_t get_max_items_5() const { return ___max_items_5; }
	inline int32_t* get_address_of_max_items_5() { return &___max_items_5; }
	inline void set_max_items_5(int32_t value)
	{
		___max_items_5 = value;
	}

	inline static int32_t get_offset_of_names_filled_6() { return static_cast<int32_t>(offsetof(DataContractSerializer_t3978772899, ___names_filled_6)); }
	inline bool get_names_filled_6() const { return ___names_filled_6; }
	inline bool* get_address_of_names_filled_6() { return &___names_filled_6; }
	inline void set_names_filled_6(bool value)
	{
		___names_filled_6 = value;
	}

	inline static int32_t get_offset_of_root_name_7() { return static_cast<int32_t>(offsetof(DataContractSerializer_t3978772899, ___root_name_7)); }
	inline XmlDictionaryString_t3504120266 * get_root_name_7() const { return ___root_name_7; }
	inline XmlDictionaryString_t3504120266 ** get_address_of_root_name_7() { return &___root_name_7; }
	inline void set_root_name_7(XmlDictionaryString_t3504120266 * value)
	{
		___root_name_7 = value;
		Il2CppCodeGenWriteBarrier(&___root_name_7, value);
	}

	inline static int32_t get_offset_of_root_ns_8() { return static_cast<int32_t>(offsetof(DataContractSerializer_t3978772899, ___root_ns_8)); }
	inline XmlDictionaryString_t3504120266 * get_root_ns_8() const { return ___root_ns_8; }
	inline XmlDictionaryString_t3504120266 ** get_address_of_root_ns_8() { return &___root_ns_8; }
	inline void set_root_ns_8(XmlDictionaryString_t3504120266 * value)
	{
		___root_ns_8 = value;
		Il2CppCodeGenWriteBarrier(&___root_ns_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
