﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Uri
struct Uri_t100236324;
// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.EndpointAddress10
struct  EndpointAddress10_t2657313230  : public Il2CppObject
{
public:
	// System.ServiceModel.EndpointAddress System.ServiceModel.EndpointAddress10::address
	EndpointAddress_t3119842923 * ___address_1;

public:
	inline static int32_t get_offset_of_address_1() { return static_cast<int32_t>(offsetof(EndpointAddress10_t2657313230, ___address_1)); }
	inline EndpointAddress_t3119842923 * get_address_1() const { return ___address_1; }
	inline EndpointAddress_t3119842923 ** get_address_of_address_1() { return &___address_1; }
	inline void set_address_1(EndpointAddress_t3119842923 * value)
	{
		___address_1 = value;
		Il2CppCodeGenWriteBarrier(&___address_1, value);
	}
};

struct EndpointAddress10_t2657313230_StaticFields
{
public:
	// System.Uri System.ServiceModel.EndpointAddress10::w3c_anonymous
	Uri_t100236324 * ___w3c_anonymous_0;

public:
	inline static int32_t get_offset_of_w3c_anonymous_0() { return static_cast<int32_t>(offsetof(EndpointAddress10_t2657313230_StaticFields, ___w3c_anonymous_0)); }
	inline Uri_t100236324 * get_w3c_anonymous_0() const { return ___w3c_anonymous_0; }
	inline Uri_t100236324 ** get_address_of_w3c_anonymous_0() { return &___w3c_anonymous_0; }
	inline void set_w3c_anonymous_0(Uri_t100236324 * value)
	{
		___w3c_anonymous_0 = value;
		Il2CppCodeGenWriteBarrier(&___w3c_anonymous_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
