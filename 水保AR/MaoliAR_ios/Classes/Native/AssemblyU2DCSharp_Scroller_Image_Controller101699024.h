﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Scroller_Image_Controller
struct  Scroller_Image_Controller_t101699024  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Scroller_Image_Controller::Panel_True
	GameObject_t1113636619 * ___Panel_True_2;
	// UnityEngine.GameObject Scroller_Image_Controller::Panel_False
	GameObject_t1113636619 * ___Panel_False_3;

public:
	inline static int32_t get_offset_of_Panel_True_2() { return static_cast<int32_t>(offsetof(Scroller_Image_Controller_t101699024, ___Panel_True_2)); }
	inline GameObject_t1113636619 * get_Panel_True_2() const { return ___Panel_True_2; }
	inline GameObject_t1113636619 ** get_address_of_Panel_True_2() { return &___Panel_True_2; }
	inline void set_Panel_True_2(GameObject_t1113636619 * value)
	{
		___Panel_True_2 = value;
		Il2CppCodeGenWriteBarrier(&___Panel_True_2, value);
	}

	inline static int32_t get_offset_of_Panel_False_3() { return static_cast<int32_t>(offsetof(Scroller_Image_Controller_t101699024, ___Panel_False_3)); }
	inline GameObject_t1113636619 * get_Panel_False_3() const { return ___Panel_False_3; }
	inline GameObject_t1113636619 ** get_address_of_Panel_False_3() { return &___Panel_False_3; }
	inline void set_Panel_False_3(GameObject_t1113636619 * value)
	{
		___Panel_False_3 = value;
		Il2CppCodeGenWriteBarrier(&___Panel_False_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
