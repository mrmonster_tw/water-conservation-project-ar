﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Web.Configuration.CompilationSection
struct CompilationSection_t1832220892;
// System.Web.VirtualPath
struct VirtualPath_t4270372584;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BuildProvider
struct  BuildProvider_t3736381005  : public Il2CppObject
{
public:
	// System.Collections.ArrayList System.Web.Compilation.BuildProvider::ref_assemblies
	ArrayList_t2718874744 * ___ref_assemblies_0;
	// System.Web.Configuration.CompilationSection System.Web.Compilation.BuildProvider::compilationSection
	CompilationSection_t1832220892 * ___compilationSection_1;
	// System.Web.VirtualPath System.Web.Compilation.BuildProvider::vpath
	VirtualPath_t4270372584 * ___vpath_2;

public:
	inline static int32_t get_offset_of_ref_assemblies_0() { return static_cast<int32_t>(offsetof(BuildProvider_t3736381005, ___ref_assemblies_0)); }
	inline ArrayList_t2718874744 * get_ref_assemblies_0() const { return ___ref_assemblies_0; }
	inline ArrayList_t2718874744 ** get_address_of_ref_assemblies_0() { return &___ref_assemblies_0; }
	inline void set_ref_assemblies_0(ArrayList_t2718874744 * value)
	{
		___ref_assemblies_0 = value;
		Il2CppCodeGenWriteBarrier(&___ref_assemblies_0, value);
	}

	inline static int32_t get_offset_of_compilationSection_1() { return static_cast<int32_t>(offsetof(BuildProvider_t3736381005, ___compilationSection_1)); }
	inline CompilationSection_t1832220892 * get_compilationSection_1() const { return ___compilationSection_1; }
	inline CompilationSection_t1832220892 ** get_address_of_compilationSection_1() { return &___compilationSection_1; }
	inline void set_compilationSection_1(CompilationSection_t1832220892 * value)
	{
		___compilationSection_1 = value;
		Il2CppCodeGenWriteBarrier(&___compilationSection_1, value);
	}

	inline static int32_t get_offset_of_vpath_2() { return static_cast<int32_t>(offsetof(BuildProvider_t3736381005, ___vpath_2)); }
	inline VirtualPath_t4270372584 * get_vpath_2() const { return ___vpath_2; }
	inline VirtualPath_t4270372584 ** get_address_of_vpath_2() { return &___vpath_2; }
	inline void set_vpath_2(VirtualPath_t4270372584 * value)
	{
		___vpath_2 = value;
		Il2CppCodeGenWriteBarrier(&___vpath_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
