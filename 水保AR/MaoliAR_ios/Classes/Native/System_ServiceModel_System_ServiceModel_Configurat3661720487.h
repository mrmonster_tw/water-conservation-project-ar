﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3318566633.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Configuration.NamedPipeTransportSecurityElement
struct  NamedPipeTransportSecurityElement_t3661720487  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct NamedPipeTransportSecurityElement_t3661720487_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.ServiceModel.Configuration.NamedPipeTransportSecurityElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.ServiceModel.Configuration.NamedPipeTransportSecurityElement::protection_level
	ConfigurationProperty_t3590861854 * ___protection_level_14;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(NamedPipeTransportSecurityElement_t3661720487_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier(&___properties_13, value);
	}

	inline static int32_t get_offset_of_protection_level_14() { return static_cast<int32_t>(offsetof(NamedPipeTransportSecurityElement_t3661720487_StaticFields, ___protection_level_14)); }
	inline ConfigurationProperty_t3590861854 * get_protection_level_14() const { return ___protection_level_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_protection_level_14() { return &___protection_level_14; }
	inline void set_protection_level_14(ConfigurationProperty_t3590861854 * value)
	{
		___protection_level_14 = value;
		Il2CppCodeGenWriteBarrier(&___protection_level_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
