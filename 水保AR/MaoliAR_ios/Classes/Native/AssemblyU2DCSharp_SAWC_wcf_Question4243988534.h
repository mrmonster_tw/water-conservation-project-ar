﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Runtime.Serialization.ExtensionDataObject
struct ExtensionDataObject_t285311256;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SAWC.wcf.Question
struct  Question_t4243988534  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.ExtensionDataObject SAWC.wcf.Question::extensionDataField
	ExtensionDataObject_t285311256 * ___extensionDataField_0;
	// System.String SAWC.wcf.Question::AgeField
	String_t* ___AgeField_1;
	// System.String SAWC.wcf.Question::IDField
	String_t* ___IDField_2;
	// System.Int32 SAWC.wcf.Question::NumberField
	int32_t ___NumberField_3;
	// System.String SAWC.wcf.Question::OpinionField
	String_t* ___OpinionField_4;
	// System.Int32 SAWC.wcf.Question::ScoreField
	int32_t ___ScoreField_5;
	// System.String SAWC.wcf.Question::SexField
	String_t* ___SexField_6;
	// System.String SAWC.wcf.Question::TypeField
	String_t* ___TypeField_7;

public:
	inline static int32_t get_offset_of_extensionDataField_0() { return static_cast<int32_t>(offsetof(Question_t4243988534, ___extensionDataField_0)); }
	inline ExtensionDataObject_t285311256 * get_extensionDataField_0() const { return ___extensionDataField_0; }
	inline ExtensionDataObject_t285311256 ** get_address_of_extensionDataField_0() { return &___extensionDataField_0; }
	inline void set_extensionDataField_0(ExtensionDataObject_t285311256 * value)
	{
		___extensionDataField_0 = value;
		Il2CppCodeGenWriteBarrier(&___extensionDataField_0, value);
	}

	inline static int32_t get_offset_of_AgeField_1() { return static_cast<int32_t>(offsetof(Question_t4243988534, ___AgeField_1)); }
	inline String_t* get_AgeField_1() const { return ___AgeField_1; }
	inline String_t** get_address_of_AgeField_1() { return &___AgeField_1; }
	inline void set_AgeField_1(String_t* value)
	{
		___AgeField_1 = value;
		Il2CppCodeGenWriteBarrier(&___AgeField_1, value);
	}

	inline static int32_t get_offset_of_IDField_2() { return static_cast<int32_t>(offsetof(Question_t4243988534, ___IDField_2)); }
	inline String_t* get_IDField_2() const { return ___IDField_2; }
	inline String_t** get_address_of_IDField_2() { return &___IDField_2; }
	inline void set_IDField_2(String_t* value)
	{
		___IDField_2 = value;
		Il2CppCodeGenWriteBarrier(&___IDField_2, value);
	}

	inline static int32_t get_offset_of_NumberField_3() { return static_cast<int32_t>(offsetof(Question_t4243988534, ___NumberField_3)); }
	inline int32_t get_NumberField_3() const { return ___NumberField_3; }
	inline int32_t* get_address_of_NumberField_3() { return &___NumberField_3; }
	inline void set_NumberField_3(int32_t value)
	{
		___NumberField_3 = value;
	}

	inline static int32_t get_offset_of_OpinionField_4() { return static_cast<int32_t>(offsetof(Question_t4243988534, ___OpinionField_4)); }
	inline String_t* get_OpinionField_4() const { return ___OpinionField_4; }
	inline String_t** get_address_of_OpinionField_4() { return &___OpinionField_4; }
	inline void set_OpinionField_4(String_t* value)
	{
		___OpinionField_4 = value;
		Il2CppCodeGenWriteBarrier(&___OpinionField_4, value);
	}

	inline static int32_t get_offset_of_ScoreField_5() { return static_cast<int32_t>(offsetof(Question_t4243988534, ___ScoreField_5)); }
	inline int32_t get_ScoreField_5() const { return ___ScoreField_5; }
	inline int32_t* get_address_of_ScoreField_5() { return &___ScoreField_5; }
	inline void set_ScoreField_5(int32_t value)
	{
		___ScoreField_5 = value;
	}

	inline static int32_t get_offset_of_SexField_6() { return static_cast<int32_t>(offsetof(Question_t4243988534, ___SexField_6)); }
	inline String_t* get_SexField_6() const { return ___SexField_6; }
	inline String_t** get_address_of_SexField_6() { return &___SexField_6; }
	inline void set_SexField_6(String_t* value)
	{
		___SexField_6 = value;
		Il2CppCodeGenWriteBarrier(&___SexField_6, value);
	}

	inline static int32_t get_offset_of_TypeField_7() { return static_cast<int32_t>(offsetof(Question_t4243988534, ___TypeField_7)); }
	inline String_t* get_TypeField_7() const { return ___TypeField_7; }
	inline String_t** get_address_of_TypeField_7() { return &___TypeField_7; }
	inline void set_TypeField_7(String_t* value)
	{
		___TypeField_7 = value;
		Il2CppCodeGenWriteBarrier(&___TypeField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
