﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Xml.XmlDictionaryWriter
struct XmlDictionaryWriter_t1958650860;
// System.Runtime.Serialization.KnownTypeCollection
struct KnownTypeCollection_t3509697551;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.Hashtable
struct Hashtable_t1853889766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.XmlFormatterSerializer
struct  XmlFormatterSerializer_t79074219  : public Il2CppObject
{
public:
	// System.Xml.XmlDictionaryWriter System.Runtime.Serialization.XmlFormatterSerializer::writer
	XmlDictionaryWriter_t1958650860 * ___writer_0;
	// System.Runtime.Serialization.KnownTypeCollection System.Runtime.Serialization.XmlFormatterSerializer::types
	KnownTypeCollection_t3509697551 * ___types_1;
	// System.Boolean System.Runtime.Serialization.XmlFormatterSerializer::ignore_unknown
	bool ___ignore_unknown_2;
	// System.Int32 System.Runtime.Serialization.XmlFormatterSerializer::max_items
	int32_t ___max_items_3;
	// System.Collections.ArrayList System.Runtime.Serialization.XmlFormatterSerializer::objects
	ArrayList_t2718874744 * ___objects_4;
	// System.Collections.Hashtable System.Runtime.Serialization.XmlFormatterSerializer::references
	Hashtable_t1853889766 * ___references_5;

public:
	inline static int32_t get_offset_of_writer_0() { return static_cast<int32_t>(offsetof(XmlFormatterSerializer_t79074219, ___writer_0)); }
	inline XmlDictionaryWriter_t1958650860 * get_writer_0() const { return ___writer_0; }
	inline XmlDictionaryWriter_t1958650860 ** get_address_of_writer_0() { return &___writer_0; }
	inline void set_writer_0(XmlDictionaryWriter_t1958650860 * value)
	{
		___writer_0 = value;
		Il2CppCodeGenWriteBarrier(&___writer_0, value);
	}

	inline static int32_t get_offset_of_types_1() { return static_cast<int32_t>(offsetof(XmlFormatterSerializer_t79074219, ___types_1)); }
	inline KnownTypeCollection_t3509697551 * get_types_1() const { return ___types_1; }
	inline KnownTypeCollection_t3509697551 ** get_address_of_types_1() { return &___types_1; }
	inline void set_types_1(KnownTypeCollection_t3509697551 * value)
	{
		___types_1 = value;
		Il2CppCodeGenWriteBarrier(&___types_1, value);
	}

	inline static int32_t get_offset_of_ignore_unknown_2() { return static_cast<int32_t>(offsetof(XmlFormatterSerializer_t79074219, ___ignore_unknown_2)); }
	inline bool get_ignore_unknown_2() const { return ___ignore_unknown_2; }
	inline bool* get_address_of_ignore_unknown_2() { return &___ignore_unknown_2; }
	inline void set_ignore_unknown_2(bool value)
	{
		___ignore_unknown_2 = value;
	}

	inline static int32_t get_offset_of_max_items_3() { return static_cast<int32_t>(offsetof(XmlFormatterSerializer_t79074219, ___max_items_3)); }
	inline int32_t get_max_items_3() const { return ___max_items_3; }
	inline int32_t* get_address_of_max_items_3() { return &___max_items_3; }
	inline void set_max_items_3(int32_t value)
	{
		___max_items_3 = value;
	}

	inline static int32_t get_offset_of_objects_4() { return static_cast<int32_t>(offsetof(XmlFormatterSerializer_t79074219, ___objects_4)); }
	inline ArrayList_t2718874744 * get_objects_4() const { return ___objects_4; }
	inline ArrayList_t2718874744 ** get_address_of_objects_4() { return &___objects_4; }
	inline void set_objects_4(ArrayList_t2718874744 * value)
	{
		___objects_4 = value;
		Il2CppCodeGenWriteBarrier(&___objects_4, value);
	}

	inline static int32_t get_offset_of_references_5() { return static_cast<int32_t>(offsetof(XmlFormatterSerializer_t79074219, ___references_5)); }
	inline Hashtable_t1853889766 * get_references_5() const { return ___references_5; }
	inline Hashtable_t1853889766 ** get_address_of_references_5() { return &___references_5; }
	inline void set_references_5(Hashtable_t1853889766 * value)
	{
		___references_5 = value;
		Il2CppCodeGenWriteBarrier(&___references_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
