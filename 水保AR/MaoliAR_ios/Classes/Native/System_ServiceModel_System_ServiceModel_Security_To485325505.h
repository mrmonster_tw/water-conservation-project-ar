﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Security_To589097440.h"

// System.ServiceModel.Security.ServiceCredentialsSecurityTokenManager
struct ServiceCredentialsSecurityTokenManager_t557944679;
// System.ServiceModel.Security.Tokens.SpnegoAuthenticatorCommunicationObject
struct SpnegoAuthenticatorCommunicationObject_t2143488888;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.SpnegoSecurityTokenAuthenticator
struct  SpnegoSecurityTokenAuthenticator_t485325505  : public CommunicationSecurityTokenAuthenticator_t589097440
{
public:
	// System.ServiceModel.Security.ServiceCredentialsSecurityTokenManager System.ServiceModel.Security.Tokens.SpnegoSecurityTokenAuthenticator::manager
	ServiceCredentialsSecurityTokenManager_t557944679 * ___manager_1;
	// System.ServiceModel.Security.Tokens.SpnegoAuthenticatorCommunicationObject System.ServiceModel.Security.Tokens.SpnegoSecurityTokenAuthenticator::comm
	SpnegoAuthenticatorCommunicationObject_t2143488888 * ___comm_2;

public:
	inline static int32_t get_offset_of_manager_1() { return static_cast<int32_t>(offsetof(SpnegoSecurityTokenAuthenticator_t485325505, ___manager_1)); }
	inline ServiceCredentialsSecurityTokenManager_t557944679 * get_manager_1() const { return ___manager_1; }
	inline ServiceCredentialsSecurityTokenManager_t557944679 ** get_address_of_manager_1() { return &___manager_1; }
	inline void set_manager_1(ServiceCredentialsSecurityTokenManager_t557944679 * value)
	{
		___manager_1 = value;
		Il2CppCodeGenWriteBarrier(&___manager_1, value);
	}

	inline static int32_t get_offset_of_comm_2() { return static_cast<int32_t>(offsetof(SpnegoSecurityTokenAuthenticator_t485325505, ___comm_2)); }
	inline SpnegoAuthenticatorCommunicationObject_t2143488888 * get_comm_2() const { return ___comm_2; }
	inline SpnegoAuthenticatorCommunicationObject_t2143488888 ** get_address_of_comm_2() { return &___comm_2; }
	inline void set_comm_2(SpnegoAuthenticatorCommunicationObject_t2143488888 * value)
	{
		___comm_2 = value;
		Il2CppCodeGenWriteBarrier(&___comm_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
