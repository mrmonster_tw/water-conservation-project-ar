﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_Control3006474639.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.LiteralControl
struct  LiteralControl_t249601277  : public Control_t3006474639
{
public:
	// System.String System.Web.UI.LiteralControl::_text
	String_t* ____text_28;

public:
	inline static int32_t get_offset_of__text_28() { return static_cast<int32_t>(offsetof(LiteralControl_t249601277, ____text_28)); }
	inline String_t* get__text_28() const { return ____text_28; }
	inline String_t** get_address_of__text_28() { return &____text_28; }
	inline void set__text_28(String_t* value)
	{
		____text_28 = value;
		Il2CppCodeGenWriteBarrier(&____text_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
