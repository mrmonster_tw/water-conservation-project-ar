﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// leve4To2D
struct  leve4To2D_t1062310264  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean leve4To2D::NowChange
	bool ___NowChange_2;
	// UnityEngine.GameObject leve4To2D::bloodUI
	GameObject_t1113636619 * ___bloodUI_3;
	// UnityEngine.SpriteRenderer leve4To2D::SR
	SpriteRenderer_t3235626157 * ___SR_4;
	// UnityEngine.GameObject leve4To2D::MC
	GameObject_t1113636619 * ___MC_5;
	// System.Boolean leve4To2D::once
	bool ___once_6;

public:
	inline static int32_t get_offset_of_NowChange_2() { return static_cast<int32_t>(offsetof(leve4To2D_t1062310264, ___NowChange_2)); }
	inline bool get_NowChange_2() const { return ___NowChange_2; }
	inline bool* get_address_of_NowChange_2() { return &___NowChange_2; }
	inline void set_NowChange_2(bool value)
	{
		___NowChange_2 = value;
	}

	inline static int32_t get_offset_of_bloodUI_3() { return static_cast<int32_t>(offsetof(leve4To2D_t1062310264, ___bloodUI_3)); }
	inline GameObject_t1113636619 * get_bloodUI_3() const { return ___bloodUI_3; }
	inline GameObject_t1113636619 ** get_address_of_bloodUI_3() { return &___bloodUI_3; }
	inline void set_bloodUI_3(GameObject_t1113636619 * value)
	{
		___bloodUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___bloodUI_3, value);
	}

	inline static int32_t get_offset_of_SR_4() { return static_cast<int32_t>(offsetof(leve4To2D_t1062310264, ___SR_4)); }
	inline SpriteRenderer_t3235626157 * get_SR_4() const { return ___SR_4; }
	inline SpriteRenderer_t3235626157 ** get_address_of_SR_4() { return &___SR_4; }
	inline void set_SR_4(SpriteRenderer_t3235626157 * value)
	{
		___SR_4 = value;
		Il2CppCodeGenWriteBarrier(&___SR_4, value);
	}

	inline static int32_t get_offset_of_MC_5() { return static_cast<int32_t>(offsetof(leve4To2D_t1062310264, ___MC_5)); }
	inline GameObject_t1113636619 * get_MC_5() const { return ___MC_5; }
	inline GameObject_t1113636619 ** get_address_of_MC_5() { return &___MC_5; }
	inline void set_MC_5(GameObject_t1113636619 * value)
	{
		___MC_5 = value;
		Il2CppCodeGenWriteBarrier(&___MC_5, value);
	}

	inline static int32_t get_offset_of_once_6() { return static_cast<int32_t>(offsetof(leve4To2D_t1062310264, ___once_6)); }
	inline bool get_once_6() const { return ___once_6; }
	inline bool* get_address_of_once_6() { return &___once_6; }
	inline void set_once_6(bool value)
	{
		___once_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
