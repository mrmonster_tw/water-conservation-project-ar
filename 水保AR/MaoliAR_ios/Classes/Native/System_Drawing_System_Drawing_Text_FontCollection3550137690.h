﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_IntPtr840150181.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Drawing.Text.FontCollection
struct  FontCollection_t3550137690  : public Il2CppObject
{
public:
	// System.IntPtr System.Drawing.Text.FontCollection::nativeFontCollection
	IntPtr_t ___nativeFontCollection_0;

public:
	inline static int32_t get_offset_of_nativeFontCollection_0() { return static_cast<int32_t>(offsetof(FontCollection_t3550137690, ___nativeFontCollection_0)); }
	inline IntPtr_t get_nativeFontCollection_0() const { return ___nativeFontCollection_0; }
	inline IntPtr_t* get_address_of_nativeFontCollection_0() { return &___nativeFontCollection_0; }
	inline void set_nativeFontCollection_0(IntPtr_t value)
	{
		___nativeFontCollection_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
