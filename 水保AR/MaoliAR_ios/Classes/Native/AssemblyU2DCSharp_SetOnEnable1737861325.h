﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Object
struct Object_t631007953;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetOnEnable
struct  SetOnEnable_t1737861325  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Object SetOnEnable::setObj
	Object_t631007953 * ___setObj_2;

public:
	inline static int32_t get_offset_of_setObj_2() { return static_cast<int32_t>(offsetof(SetOnEnable_t1737861325, ___setObj_2)); }
	inline Object_t631007953 * get_setObj_2() const { return ___setObj_2; }
	inline Object_t631007953 ** get_address_of_setObj_2() { return &___setObj_2; }
	inline void set_setObj_2(Object_t631007953 * value)
	{
		___setObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___setObj_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
