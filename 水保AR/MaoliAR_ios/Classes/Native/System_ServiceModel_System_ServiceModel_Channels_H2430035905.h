﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_I2309025479.h"

// System.Collections.Generic.List`1<System.ServiceModel.Channels.IReplyChannel>
struct List_1_t1033727036;
// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;
// System.ServiceModel.Channels.HttpListenerManager
struct HttpListenerManager_t555124713;
// System.ServiceModel.Channels.HttpTransportBindingElement
struct HttpTransportBindingElement_t2392894562;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpChannelListenerBase`1<System.ServiceModel.Channels.IReplyChannel>
struct  HttpChannelListenerBase_1_t2430035905  : public InternalChannelListenerBase_1_t2309025479
{
public:
	// System.Collections.Generic.List`1<TChannel> System.ServiceModel.Channels.HttpChannelListenerBase`1::channels
	List_1_t1033727036 * ___channels_16;
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.HttpChannelListenerBase`1::encoder
	MessageEncoder_t3063398011 * ___encoder_17;
	// System.ServiceModel.Channels.HttpListenerManager System.ServiceModel.Channels.HttpChannelListenerBase`1::httpChannelManager
	HttpListenerManager_t555124713 * ___httpChannelManager_18;
	// System.ServiceModel.Channels.HttpTransportBindingElement System.ServiceModel.Channels.HttpChannelListenerBase`1::<Source>k__BackingField
	HttpTransportBindingElement_t2392894562 * ___U3CSourceU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_channels_16() { return static_cast<int32_t>(offsetof(HttpChannelListenerBase_1_t2430035905, ___channels_16)); }
	inline List_1_t1033727036 * get_channels_16() const { return ___channels_16; }
	inline List_1_t1033727036 ** get_address_of_channels_16() { return &___channels_16; }
	inline void set_channels_16(List_1_t1033727036 * value)
	{
		___channels_16 = value;
		Il2CppCodeGenWriteBarrier(&___channels_16, value);
	}

	inline static int32_t get_offset_of_encoder_17() { return static_cast<int32_t>(offsetof(HttpChannelListenerBase_1_t2430035905, ___encoder_17)); }
	inline MessageEncoder_t3063398011 * get_encoder_17() const { return ___encoder_17; }
	inline MessageEncoder_t3063398011 ** get_address_of_encoder_17() { return &___encoder_17; }
	inline void set_encoder_17(MessageEncoder_t3063398011 * value)
	{
		___encoder_17 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_17, value);
	}

	inline static int32_t get_offset_of_httpChannelManager_18() { return static_cast<int32_t>(offsetof(HttpChannelListenerBase_1_t2430035905, ___httpChannelManager_18)); }
	inline HttpListenerManager_t555124713 * get_httpChannelManager_18() const { return ___httpChannelManager_18; }
	inline HttpListenerManager_t555124713 ** get_address_of_httpChannelManager_18() { return &___httpChannelManager_18; }
	inline void set_httpChannelManager_18(HttpListenerManager_t555124713 * value)
	{
		___httpChannelManager_18 = value;
		Il2CppCodeGenWriteBarrier(&___httpChannelManager_18, value);
	}

	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(HttpChannelListenerBase_1_t2430035905, ___U3CSourceU3Ek__BackingField_19)); }
	inline HttpTransportBindingElement_t2392894562 * get_U3CSourceU3Ek__BackingField_19() const { return ___U3CSourceU3Ek__BackingField_19; }
	inline HttpTransportBindingElement_t2392894562 ** get_address_of_U3CSourceU3Ek__BackingField_19() { return &___U3CSourceU3Ek__BackingField_19; }
	inline void set_U3CSourceU3Ek__BackingField_19(HttpTransportBindingElement_t2392894562 * value)
	{
		___U3CSourceU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSourceU3Ek__BackingField_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
