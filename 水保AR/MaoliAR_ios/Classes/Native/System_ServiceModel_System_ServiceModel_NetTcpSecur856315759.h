﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_ServiceModel_System_ServiceModel_SecurityMo1299321141.h"

// System.ServiceModel.MessageSecurityOverTcp
struct MessageSecurityOverTcp_t2089208958;
// System.ServiceModel.TcpTransportSecurity
struct TcpTransportSecurity_t3663278697;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.NetTcpSecurity
struct  NetTcpSecurity_t856315759  : public Il2CppObject
{
public:
	// System.ServiceModel.MessageSecurityOverTcp System.ServiceModel.NetTcpSecurity::message
	MessageSecurityOverTcp_t2089208958 * ___message_0;
	// System.ServiceModel.SecurityMode System.ServiceModel.NetTcpSecurity::mode
	int32_t ___mode_1;
	// System.ServiceModel.TcpTransportSecurity System.ServiceModel.NetTcpSecurity::transport
	TcpTransportSecurity_t3663278697 * ___transport_2;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(NetTcpSecurity_t856315759, ___message_0)); }
	inline MessageSecurityOverTcp_t2089208958 * get_message_0() const { return ___message_0; }
	inline MessageSecurityOverTcp_t2089208958 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(MessageSecurityOverTcp_t2089208958 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier(&___message_0, value);
	}

	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(NetTcpSecurity_t856315759, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_transport_2() { return static_cast<int32_t>(offsetof(NetTcpSecurity_t856315759, ___transport_2)); }
	inline TcpTransportSecurity_t3663278697 * get_transport_2() const { return ___transport_2; }
	inline TcpTransportSecurity_t3663278697 ** get_address_of_transport_2() { return &___transport_2; }
	inline void set_transport_2(TcpTransportSecurity_t3663278697 * value)
	{
		___transport_2 = value;
		Il2CppCodeGenWriteBarrier(&___transport_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
