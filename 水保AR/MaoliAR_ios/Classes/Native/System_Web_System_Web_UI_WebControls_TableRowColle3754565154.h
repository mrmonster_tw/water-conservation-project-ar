﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Web.UI.ControlCollection
struct ControlCollection_t4212191938;
// System.Web.UI.WebControls.Table
struct Table_t574440801;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.WebControls.TableRowCollection
struct  TableRowCollection_t3754565154  : public Il2CppObject
{
public:
	// System.Web.UI.ControlCollection System.Web.UI.WebControls.TableRowCollection::cc
	ControlCollection_t4212191938 * ___cc_0;
	// System.Web.UI.WebControls.Table System.Web.UI.WebControls.TableRowCollection::owner
	Table_t574440801 * ___owner_1;

public:
	inline static int32_t get_offset_of_cc_0() { return static_cast<int32_t>(offsetof(TableRowCollection_t3754565154, ___cc_0)); }
	inline ControlCollection_t4212191938 * get_cc_0() const { return ___cc_0; }
	inline ControlCollection_t4212191938 ** get_address_of_cc_0() { return &___cc_0; }
	inline void set_cc_0(ControlCollection_t4212191938 * value)
	{
		___cc_0 = value;
		Il2CppCodeGenWriteBarrier(&___cc_0, value);
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(TableRowCollection_t3754565154, ___owner_1)); }
	inline Table_t574440801 * get_owner_1() const { return ___owner_1; }
	inline Table_t574440801 ** get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(Table_t574440801 * value)
	{
		___owner_1 = value;
		Il2CppCodeGenWriteBarrier(&___owner_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
