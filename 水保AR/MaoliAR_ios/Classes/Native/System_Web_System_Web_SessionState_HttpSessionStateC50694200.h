﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "System_Web_System_Web_SessionState_SessionStateMod3002328156.h"
#include "System_Web_System_Web_HttpCookieMode2360372362.h"

// System.String
struct String_t;
// System.Web.HttpStaticObjectsCollection
struct HttpStaticObjectsCollection_t518175362;
// System.Web.SessionState.ISessionStateItemCollection
struct ISessionStateItemCollection_t2279755297;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.SessionState.HttpSessionStateContainer
struct  HttpSessionStateContainer_t50694200  : public Il2CppObject
{
public:
	// System.String System.Web.SessionState.HttpSessionStateContainer::id
	String_t* ___id_0;
	// System.Web.HttpStaticObjectsCollection System.Web.SessionState.HttpSessionStateContainer::staticObjects
	HttpStaticObjectsCollection_t518175362 * ___staticObjects_1;
	// System.Int32 System.Web.SessionState.HttpSessionStateContainer::timeout
	int32_t ___timeout_2;
	// System.Boolean System.Web.SessionState.HttpSessionStateContainer::newSession
	bool ___newSession_3;
	// System.Boolean System.Web.SessionState.HttpSessionStateContainer::isCookieless
	bool ___isCookieless_4;
	// System.Web.SessionState.SessionStateMode System.Web.SessionState.HttpSessionStateContainer::mode
	int32_t ___mode_5;
	// System.Boolean System.Web.SessionState.HttpSessionStateContainer::isReadOnly
	bool ___isReadOnly_6;
	// System.Boolean System.Web.SessionState.HttpSessionStateContainer::abandoned
	bool ___abandoned_7;
	// System.Web.SessionState.ISessionStateItemCollection System.Web.SessionState.HttpSessionStateContainer::sessionItems
	Il2CppObject * ___sessionItems_8;
	// System.Web.HttpCookieMode System.Web.SessionState.HttpSessionStateContainer::cookieMode
	int32_t ___cookieMode_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_staticObjects_1() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___staticObjects_1)); }
	inline HttpStaticObjectsCollection_t518175362 * get_staticObjects_1() const { return ___staticObjects_1; }
	inline HttpStaticObjectsCollection_t518175362 ** get_address_of_staticObjects_1() { return &___staticObjects_1; }
	inline void set_staticObjects_1(HttpStaticObjectsCollection_t518175362 * value)
	{
		___staticObjects_1 = value;
		Il2CppCodeGenWriteBarrier(&___staticObjects_1, value);
	}

	inline static int32_t get_offset_of_timeout_2() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___timeout_2)); }
	inline int32_t get_timeout_2() const { return ___timeout_2; }
	inline int32_t* get_address_of_timeout_2() { return &___timeout_2; }
	inline void set_timeout_2(int32_t value)
	{
		___timeout_2 = value;
	}

	inline static int32_t get_offset_of_newSession_3() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___newSession_3)); }
	inline bool get_newSession_3() const { return ___newSession_3; }
	inline bool* get_address_of_newSession_3() { return &___newSession_3; }
	inline void set_newSession_3(bool value)
	{
		___newSession_3 = value;
	}

	inline static int32_t get_offset_of_isCookieless_4() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___isCookieless_4)); }
	inline bool get_isCookieless_4() const { return ___isCookieless_4; }
	inline bool* get_address_of_isCookieless_4() { return &___isCookieless_4; }
	inline void set_isCookieless_4(bool value)
	{
		___isCookieless_4 = value;
	}

	inline static int32_t get_offset_of_mode_5() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___mode_5)); }
	inline int32_t get_mode_5() const { return ___mode_5; }
	inline int32_t* get_address_of_mode_5() { return &___mode_5; }
	inline void set_mode_5(int32_t value)
	{
		___mode_5 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_6() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___isReadOnly_6)); }
	inline bool get_isReadOnly_6() const { return ___isReadOnly_6; }
	inline bool* get_address_of_isReadOnly_6() { return &___isReadOnly_6; }
	inline void set_isReadOnly_6(bool value)
	{
		___isReadOnly_6 = value;
	}

	inline static int32_t get_offset_of_abandoned_7() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___abandoned_7)); }
	inline bool get_abandoned_7() const { return ___abandoned_7; }
	inline bool* get_address_of_abandoned_7() { return &___abandoned_7; }
	inline void set_abandoned_7(bool value)
	{
		___abandoned_7 = value;
	}

	inline static int32_t get_offset_of_sessionItems_8() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___sessionItems_8)); }
	inline Il2CppObject * get_sessionItems_8() const { return ___sessionItems_8; }
	inline Il2CppObject ** get_address_of_sessionItems_8() { return &___sessionItems_8; }
	inline void set_sessionItems_8(Il2CppObject * value)
	{
		___sessionItems_8 = value;
		Il2CppCodeGenWriteBarrier(&___sessionItems_8, value);
	}

	inline static int32_t get_offset_of_cookieMode_9() { return static_cast<int32_t>(offsetof(HttpSessionStateContainer_t50694200, ___cookieMode_9)); }
	inline int32_t get_cookieMode_9() const { return ___cookieMode_9; }
	inline int32_t* get_address_of_cookieMode_9() { return &___cookieMode_9; }
	inline void set_cookieMode_9(int32_t value)
	{
		___cookieMode_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
