﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ServiceThrottlingBehavior
struct  ServiceThrottlingBehavior_t2743341039  : public Il2CppObject
{
public:
	// System.Int32 System.ServiceModel.Description.ServiceThrottlingBehavior::max_calls
	int32_t ___max_calls_0;
	// System.Int32 System.ServiceModel.Description.ServiceThrottlingBehavior::max_conn
	int32_t ___max_conn_1;
	// System.Int32 System.ServiceModel.Description.ServiceThrottlingBehavior::max_instance
	int32_t ___max_instance_2;

public:
	inline static int32_t get_offset_of_max_calls_0() { return static_cast<int32_t>(offsetof(ServiceThrottlingBehavior_t2743341039, ___max_calls_0)); }
	inline int32_t get_max_calls_0() const { return ___max_calls_0; }
	inline int32_t* get_address_of_max_calls_0() { return &___max_calls_0; }
	inline void set_max_calls_0(int32_t value)
	{
		___max_calls_0 = value;
	}

	inline static int32_t get_offset_of_max_conn_1() { return static_cast<int32_t>(offsetof(ServiceThrottlingBehavior_t2743341039, ___max_conn_1)); }
	inline int32_t get_max_conn_1() const { return ___max_conn_1; }
	inline int32_t* get_address_of_max_conn_1() { return &___max_conn_1; }
	inline void set_max_conn_1(int32_t value)
	{
		___max_conn_1 = value;
	}

	inline static int32_t get_offset_of_max_instance_2() { return static_cast<int32_t>(offsetof(ServiceThrottlingBehavior_t2743341039, ___max_instance_2)); }
	inline int32_t get_max_instance_2() const { return ___max_instance_2; }
	inline int32_t* get_address_of_max_instance_2() { return &___max_instance_2; }
	inline void set_max_instance_2(int32_t value)
	{
		___max_instance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
