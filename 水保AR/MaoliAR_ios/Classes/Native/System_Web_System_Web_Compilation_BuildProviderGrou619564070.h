﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_Generic_List_1_gen913488451.h"

// System.String
struct String_t;
// System.Web.Compilation.CompilerType
struct CompilerType_t2533482247;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.BuildProviderGroup
struct  BuildProviderGroup_t619564070  : public List_1_t913488451
{
public:
	// System.String System.Web.Compilation.BuildProviderGroup::<NamePrefix>k__BackingField
	String_t* ___U3CNamePrefixU3Ek__BackingField_5;
	// System.Boolean System.Web.Compilation.BuildProviderGroup::<Standalone>k__BackingField
	bool ___U3CStandaloneU3Ek__BackingField_6;
	// System.Boolean System.Web.Compilation.BuildProviderGroup::<Application>k__BackingField
	bool ___U3CApplicationU3Ek__BackingField_7;
	// System.Boolean System.Web.Compilation.BuildProviderGroup::<Master>k__BackingField
	bool ___U3CMasterU3Ek__BackingField_8;
	// System.Web.Compilation.CompilerType System.Web.Compilation.BuildProviderGroup::<CompilerType>k__BackingField
	CompilerType_t2533482247 * ___U3CCompilerTypeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CNamePrefixU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BuildProviderGroup_t619564070, ___U3CNamePrefixU3Ek__BackingField_5)); }
	inline String_t* get_U3CNamePrefixU3Ek__BackingField_5() const { return ___U3CNamePrefixU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CNamePrefixU3Ek__BackingField_5() { return &___U3CNamePrefixU3Ek__BackingField_5; }
	inline void set_U3CNamePrefixU3Ek__BackingField_5(String_t* value)
	{
		___U3CNamePrefixU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNamePrefixU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CStandaloneU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BuildProviderGroup_t619564070, ___U3CStandaloneU3Ek__BackingField_6)); }
	inline bool get_U3CStandaloneU3Ek__BackingField_6() const { return ___U3CStandaloneU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CStandaloneU3Ek__BackingField_6() { return &___U3CStandaloneU3Ek__BackingField_6; }
	inline void set_U3CStandaloneU3Ek__BackingField_6(bool value)
	{
		___U3CStandaloneU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CApplicationU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BuildProviderGroup_t619564070, ___U3CApplicationU3Ek__BackingField_7)); }
	inline bool get_U3CApplicationU3Ek__BackingField_7() const { return ___U3CApplicationU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CApplicationU3Ek__BackingField_7() { return &___U3CApplicationU3Ek__BackingField_7; }
	inline void set_U3CApplicationU3Ek__BackingField_7(bool value)
	{
		___U3CApplicationU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CMasterU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BuildProviderGroup_t619564070, ___U3CMasterU3Ek__BackingField_8)); }
	inline bool get_U3CMasterU3Ek__BackingField_8() const { return ___U3CMasterU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CMasterU3Ek__BackingField_8() { return &___U3CMasterU3Ek__BackingField_8; }
	inline void set_U3CMasterU3Ek__BackingField_8(bool value)
	{
		___U3CMasterU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CCompilerTypeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BuildProviderGroup_t619564070, ___U3CCompilerTypeU3Ek__BackingField_9)); }
	inline CompilerType_t2533482247 * get_U3CCompilerTypeU3Ek__BackingField_9() const { return ___U3CCompilerTypeU3Ek__BackingField_9; }
	inline CompilerType_t2533482247 ** get_address_of_U3CCompilerTypeU3Ek__BackingField_9() { return &___U3CCompilerTypeU3Ek__BackingField_9; }
	inline void set_U3CCompilerTypeU3Ek__BackingField_9(CompilerType_t2533482247 * value)
	{
		___U3CCompilerTypeU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCompilerTypeU3Ek__BackingField_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
