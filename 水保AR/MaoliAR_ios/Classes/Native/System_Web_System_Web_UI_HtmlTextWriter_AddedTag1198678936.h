﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "System_Web_System_Web_UI_HtmlTextWriterTag4267392773.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.HtmlTextWriter/AddedTag
struct  AddedTag_t1198678936 
{
public:
	// System.String System.Web.UI.HtmlTextWriter/AddedTag::name
	String_t* ___name_0;
	// System.Web.UI.HtmlTextWriterTag System.Web.UI.HtmlTextWriter/AddedTag::key
	int32_t ___key_1;
	// System.Boolean System.Web.UI.HtmlTextWriter/AddedTag::ignore
	bool ___ignore_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AddedTag_t1198678936, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(AddedTag_t1198678936, ___key_1)); }
	inline int32_t get_key_1() const { return ___key_1; }
	inline int32_t* get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(int32_t value)
	{
		___key_1 = value;
	}

	inline static int32_t get_offset_of_ignore_2() { return static_cast<int32_t>(offsetof(AddedTag_t1198678936, ___ignore_2)); }
	inline bool get_ignore_2() const { return ___ignore_2; }
	inline bool* get_address_of_ignore_2() { return &___ignore_2; }
	inline void set_ignore_2(bool value)
	{
		___ignore_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Web.UI.HtmlTextWriter/AddedTag
struct AddedTag_t1198678936_marshaled_pinvoke
{
	char* ___name_0;
	int32_t ___key_1;
	int32_t ___ignore_2;
};
// Native definition for COM marshalling of System.Web.UI.HtmlTextWriter/AddedTag
struct AddedTag_t1198678936_marshaled_com
{
	Il2CppChar* ___name_0;
	int32_t ___key_1;
	int32_t ___ignore_2;
};
