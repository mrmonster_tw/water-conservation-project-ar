﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Compilation.ResourceExpressionFields
struct  ResourceExpressionFields_t4288169951  : public Il2CppObject
{
public:
	// System.String System.Web.Compilation.ResourceExpressionFields::classKey
	String_t* ___classKey_0;
	// System.String System.Web.Compilation.ResourceExpressionFields::resourceKey
	String_t* ___resourceKey_1;

public:
	inline static int32_t get_offset_of_classKey_0() { return static_cast<int32_t>(offsetof(ResourceExpressionFields_t4288169951, ___classKey_0)); }
	inline String_t* get_classKey_0() const { return ___classKey_0; }
	inline String_t** get_address_of_classKey_0() { return &___classKey_0; }
	inline void set_classKey_0(String_t* value)
	{
		___classKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___classKey_0, value);
	}

	inline static int32_t get_offset_of_resourceKey_1() { return static_cast<int32_t>(offsetof(ResourceExpressionFields_t4288169951, ___resourceKey_1)); }
	inline String_t* get_resourceKey_1() const { return ___resourceKey_1; }
	inline String_t** get_address_of_resourceKey_1() { return &___resourceKey_1; }
	inline void set_resourceKey_1(String_t* value)
	{
		___resourceKey_1 = value;
		Il2CppCodeGenWriteBarrier(&___resourceKey_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
