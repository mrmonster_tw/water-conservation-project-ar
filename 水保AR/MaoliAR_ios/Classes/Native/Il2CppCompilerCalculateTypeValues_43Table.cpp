﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFrom2785373562.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilde1777433883.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainInitia1789741059.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab495369070.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker765719473.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeh3611421852.h"
#include "Vuforia_UnityExtensions_Vuforia_CylinderTargetAbst1240762998.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSet3286034874.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSet_StorageTyp3460620509.h"
#include "Vuforia_UnityExtensions_Vuforia_DatabaseLoadARCont1526318335.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSetTrackableBe3430730379.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleData1039179782.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleIntData221481125.h"
#include "Vuforia_UnityExtensions_Vuforia_OrientedBoundingBo2769728497.h"
#include "Vuforia_UnityExtensions_Vuforia_OrientedBoundingBo4089508388.h"
#include "Vuforia_UnityExtensions_Vuforia_BehaviourComponentF616131333.h"
#include "Vuforia_UnityExtensions_Vuforia_BehaviourComponent1133355857.h"
#include "Vuforia_UnityExtensions_Vuforia_CloudRecoImageTarg1528709233.h"
#include "Vuforia_UnityExtensions_Vuforia_CylinderTargetImpl1765561451.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetType2834081427.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetData2650985503.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder2430893908.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_F46289180.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDeviceImpl301920651.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDeviceImpl_Ca409245341.h"
#include "Vuforia_UnityExtensions_Vuforia_DataSetImpl2094717509.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageImpl1502331638.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder4174262652.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetImpl2025897970.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl4028644236.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetImpl2100637008.h"
#include "Vuforia_UnityExtensions_Vuforia_NullWebCamTexAdapto740403440.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtil1756161826.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtil3846470484.h"
#include "Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor489323229.h"
#include "Vuforia_UnityExtensions_Vuforia_PremiumObjectFacto3495665550.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl407376483.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3114383412.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3800049328.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2901758114.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3709259836.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_980376481.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1835118957.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2883825721.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1300500262.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1799993495.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3603203054.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3925829072.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2744445717.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1300926610.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_489181770.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1414459591.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_574185949.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_946804616.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2218716262.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRendererImpl821052510.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRendererImp3153772864.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnityImpl933790584.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceImpl1244782987.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilde1656443109.h"
#include "Vuforia_UnityExtensions_Vuforia_PropImpl3704727797.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracka2707361041.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker651952228.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl1410587152.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_Up2731840355.h"
#include "Vuforia_UnityExtensions_Vuforia_TypeMapping3272509632.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor3430449046.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptorIm4146576487.h"
#include "Vuforia_UnityExtensions_Vuforia_WordImpl2494369133.h"
#include "Vuforia_UnityExtensions_Vuforia_WordPrefabCreation3691738946.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManagerImpl1706254019.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResultImpl709252498.h"
#include "Vuforia_UnityExtensions_Vuforia_WordListImpl4149586592.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNullWrapper2343315499.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNativeWrapp2621726075.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaWrapper2763746413.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr818896732.h"
#include "Vuforia_UnityExtensions_Vuforia_PropAbstractBehavi2080236229.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke1238706968.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManager1982749557.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManagerImpl1869359784.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl2888108813.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_Ta884522839.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4300 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4301 = { sizeof (ReconstructionFromTargetAbstractBehaviour_t2785373562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4301[2] = 
{
	ReconstructionFromTargetAbstractBehaviour_t2785373562::get_offset_of_mReconstructionFromTarget_2(),
	ReconstructionFromTargetAbstractBehaviour_t2785373562::get_offset_of_mReconstructionBehaviour_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4302 = { sizeof (SmartTerrainBuilder_t1777433883), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4303 = { sizeof (SmartTerrainInitializationInfo_t1789741059)+ sizeof (Il2CppObject), sizeof(SmartTerrainInitializationInfo_t1789741059 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4304 = { sizeof (SmartTerrainTrackableBehaviour_t495369070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4304[4] = 
{
	SmartTerrainTrackableBehaviour_t495369070::get_offset_of_mSmartTerrainTrackable_10(),
	SmartTerrainTrackableBehaviour_t495369070::get_offset_of_mDisableAutomaticUpdates_11(),
	SmartTerrainTrackableBehaviour_t495369070::get_offset_of_mMeshFilterToUpdate_12(),
	SmartTerrainTrackableBehaviour_t495369070::get_offset_of_mMeshColliderToUpdate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4305 = { sizeof (SmartTerrainTrackerARController_t765719473), -1, sizeof(SmartTerrainTrackerARController_t765719473_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4305[9] = 
{
	SmartTerrainTrackerARController_t765719473::get_offset_of_mAutoInitTracker_1(),
	SmartTerrainTrackerARController_t765719473::get_offset_of_mAutoStartTracker_2(),
	SmartTerrainTrackerARController_t765719473::get_offset_of_mAutoInitBuilder_3(),
	SmartTerrainTrackerARController_t765719473::get_offset_of_mSceneUnitsToMillimeter_4(),
	SmartTerrainTrackerARController_t765719473::get_offset_of_mTrackerStarted_5(),
	SmartTerrainTrackerARController_t765719473::get_offset_of_mTrackerWasActiveBeforePause_6(),
	SmartTerrainTrackerARController_t765719473::get_offset_of_mTrackerWasActiveBeforeDisabling_7(),
	SmartTerrainTrackerARController_t765719473_StaticFields::get_offset_of_mInstance_8(),
	SmartTerrainTrackerARController_t765719473_StaticFields::get_offset_of_mPadlock_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4306 = { sizeof (SurfaceAbstractBehaviour_t3611421852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4306[1] = 
{
	SurfaceAbstractBehaviour_t3611421852::get_offset_of_mSurface_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4307 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4308 = { sizeof (CylinderTargetAbstractBehaviour_t1240762998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4308[10] = 
{
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mCylinderTarget_20(),
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mTopDiameterRatio_21(),
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mBottomDiameterRatio_22(),
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mSideLength_23(),
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mTopDiameter_24(),
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mBottomDiameter_25(),
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mFrameIndex_26(),
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mUpdateFrameIndex_27(),
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mFutureScale_28(),
	CylinderTargetAbstractBehaviour_t1240762998::get_offset_of_mLastTransformScale_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4309 = { sizeof (DataSet_t3286034874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4310 = { sizeof (StorageType_t3460620509)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4310[4] = 
{
	StorageType_t3460620509::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4311 = { sizeof (DatabaseLoadARController_t1526318335), -1, sizeof(DatabaseLoadARController_t1526318335_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4311[6] = 
{
	DatabaseLoadARController_t1526318335::get_offset_of_mDatasetsLoaded_1(),
	DatabaseLoadARController_t1526318335::get_offset_of_mExternalDatasetRoots_2(),
	DatabaseLoadARController_t1526318335::get_offset_of_mDataSetsToLoad_3(),
	DatabaseLoadARController_t1526318335::get_offset_of_mDataSetsToActivate_4(),
	DatabaseLoadARController_t1526318335_StaticFields::get_offset_of_mInstance_5(),
	DatabaseLoadARController_t1526318335_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4312 = { sizeof (DataSetTrackableBehaviour_t3430730379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4312[10] = 
{
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mDataSetPath_10(),
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mExtendedTracking_11(),
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mInitializeSmartTerrain_12(),
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mReconstructionToInitialize_13(),
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mSmartTerrainOccluderBoundsMin_14(),
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mSmartTerrainOccluderBoundsMax_15(),
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mIsSmartTerrainOccluderOffset_16(),
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mSmartTerrainOccluderOffset_17(),
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mSmartTerrainOccluderRotation_18(),
	DataSetTrackableBehaviour_t3430730379::get_offset_of_mAutoSetOccluderFromTargetSize_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4313 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4314 = { sizeof (RectangleData_t1039179782)+ sizeof (Il2CppObject), sizeof(RectangleData_t1039179782 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4314[4] = 
{
	RectangleData_t1039179782::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t1039179782::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t1039179782::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleData_t1039179782::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4315 = { sizeof (RectangleIntData_t221481125)+ sizeof (Il2CppObject), sizeof(RectangleIntData_t221481125 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4315[4] = 
{
	RectangleIntData_t221481125::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t221481125::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t221481125::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RectangleIntData_t221481125::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4316 = { sizeof (OrientedBoundingBox_t2769728497)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox_t2769728497 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4316[3] = 
{
	OrientedBoundingBox_t2769728497::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox_t2769728497::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox_t2769728497::get_offset_of_U3CRotationU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4317 = { sizeof (OrientedBoundingBox3D_t4089508388)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox3D_t4089508388 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4317[3] = 
{
	OrientedBoundingBox3D_t4089508388::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox3D_t4089508388::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	OrientedBoundingBox3D_t4089508388::get_offset_of_U3CRotationYU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4318 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4319 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4320 = { sizeof (BehaviourComponentFactory_t616131333), -1, sizeof(BehaviourComponentFactory_t616131333_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4320[1] = 
{
	BehaviourComponentFactory_t616131333_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4321 = { sizeof (NullBehaviourComponentFactory_t1133355857), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4322 = { sizeof (CloudRecoImageTargetImpl_t1528709233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4322[1] = 
{
	CloudRecoImageTargetImpl_t1528709233::get_offset_of_mSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4323 = { sizeof (CylinderTargetImpl_t1765561451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4323[3] = 
{
	CylinderTargetImpl_t1765561451::get_offset_of_mSideLength_4(),
	CylinderTargetImpl_t1765561451::get_offset_of_mTopDiameter_5(),
	CylinderTargetImpl_t1765561451::get_offset_of_mBottomDiameter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4324 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4325 = { sizeof (ImageTargetType_t2834081427)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4325[4] = 
{
	ImageTargetType_t2834081427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4326 = { sizeof (ImageTargetData_t2650985503)+ sizeof (Il2CppObject), sizeof(ImageTargetData_t2650985503 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4326[2] = 
{
	ImageTargetData_t2650985503::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageTargetData_t2650985503::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4327 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4328 = { sizeof (ImageTargetBuilder_t2430893908), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4329 = { sizeof (FrameQuality_t46289180)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4329[5] = 
{
	FrameQuality_t46289180::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4330 = { sizeof (CameraDeviceImpl_t301920651), -1, sizeof(CameraDeviceImpl_t301920651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4330[12] = 
{
	CameraDeviceImpl_t301920651::get_offset_of_mCameraImages_1(),
	CameraDeviceImpl_t301920651::get_offset_of_mForcedCameraFormats_2(),
	CameraDeviceImpl_t301920651_StaticFields::get_offset_of_mWebCam_3(),
	CameraDeviceImpl_t301920651::get_offset_of_mCameraReady_4(),
	CameraDeviceImpl_t301920651::get_offset_of_mIsDirty_5(),
	CameraDeviceImpl_t301920651::get_offset_of_mActualCameraDirection_6(),
	CameraDeviceImpl_t301920651::get_offset_of_mSelectedCameraDirection_7(),
	CameraDeviceImpl_t301920651::get_offset_of_mCameraDeviceMode_8(),
	CameraDeviceImpl_t301920651::get_offset_of_mVideoModeData_9(),
	CameraDeviceImpl_t301920651::get_offset_of_mVideoModeDataNeedsUpdate_10(),
	CameraDeviceImpl_t301920651::get_offset_of_mHasCameraDeviceModeBeenSet_11(),
	CameraDeviceImpl_t301920651::get_offset_of_mCameraActive_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4331 = { sizeof (CameraFieldData_t409245341)+ sizeof (Il2CppObject), sizeof(CameraFieldData_t409245341_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4331[3] = 
{
	CameraFieldData_t409245341::get_offset_of_KeyValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraFieldData_t409245341::get_offset_of_DataType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraFieldData_t409245341::get_offset_of_Unused_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4332 = { sizeof (DataSetImpl_t2094717509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4332[5] = 
{
	DataSetImpl_t2094717509::get_offset_of_mDataSetPtr_0(),
	DataSetImpl_t2094717509::get_offset_of_mPath_1(),
	DataSetImpl_t2094717509::get_offset_of_mStorageType_2(),
	DataSetImpl_t2094717509::get_offset_of_mTrackablesDict_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4333 = { sizeof (ImageImpl_t1502331638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4333[10] = 
{
	ImageImpl_t1502331638::get_offset_of_mWidth_0(),
	ImageImpl_t1502331638::get_offset_of_mHeight_1(),
	ImageImpl_t1502331638::get_offset_of_mStride_2(),
	ImageImpl_t1502331638::get_offset_of_mBufferWidth_3(),
	ImageImpl_t1502331638::get_offset_of_mBufferHeight_4(),
	ImageImpl_t1502331638::get_offset_of_mPixelFormat_5(),
	ImageImpl_t1502331638::get_offset_of_mData_6(),
	ImageImpl_t1502331638::get_offset_of_mUnmanagedData_7(),
	ImageImpl_t1502331638::get_offset_of_mDataSet_8(),
	ImageImpl_t1502331638::get_offset_of_mPixel32_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4334 = { sizeof (ImageTargetBuilderImpl_t4174262652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4334[2] = 
{
	ImageTargetBuilderImpl_t4174262652::get_offset_of_mTrackableSource_0(),
	ImageTargetBuilderImpl_t4174262652::get_offset_of_mIsScanning_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4335 = { sizeof (ImageTargetImpl_t2025897970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4335[2] = 
{
	ImageTargetImpl_t2025897970::get_offset_of_mImageTargetType_4(),
	ImageTargetImpl_t2025897970::get_offset_of_mVirtualButtons_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4336 = { sizeof (ObjectTrackerImpl_t4028644236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4336[4] = 
{
	ObjectTrackerImpl_t4028644236::get_offset_of_mActiveDataSets_1(),
	ObjectTrackerImpl_t4028644236::get_offset_of_mDataSets_2(),
	ObjectTrackerImpl_t4028644236::get_offset_of_mImageTargetBuilder_3(),
	ObjectTrackerImpl_t4028644236::get_offset_of_mTargetFinder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4337 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4338 = { sizeof (MultiTargetImpl_t2100637008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4339 = { sizeof (NullWebCamTexAdaptor_t740403440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4339[5] = 
{
	NullWebCamTexAdaptor_t740403440::get_offset_of_mTexture_0(),
	NullWebCamTexAdaptor_t740403440::get_offset_of_mPseudoPlaying_1(),
	NullWebCamTexAdaptor_t740403440::get_offset_of_mMsBetweenFrames_2(),
	NullWebCamTexAdaptor_t740403440::get_offset_of_mLastFrame_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4340 = { sizeof (PlayModeEditorUtility_t1756161826), -1, sizeof(PlayModeEditorUtility_t1756161826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4340[1] = 
{
	PlayModeEditorUtility_t1756161826_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4341 = { sizeof (NullPlayModeEditorUtility_t3846470484), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4342 = { sizeof (PremiumObjectFactory_t489323229), -1, sizeof(PremiumObjectFactory_t489323229_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4342[1] = 
{
	PremiumObjectFactory_t489323229_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4343 = { sizeof (NullPremiumObjectFactory_t3495665550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4344 = { sizeof (VuforiaManagerImpl_t407376483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4344[25] = 
{
	VuforiaManagerImpl_t407376483::get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_1(),
	VuforiaManagerImpl_t407376483::get_offset_of_mWorldCenterMode_2(),
	VuforiaManagerImpl_t407376483::get_offset_of_mWorldCenter_3(),
	VuforiaManagerImpl_t407376483::get_offset_of_mVuMarkWorldCenter_4(),
	VuforiaManagerImpl_t407376483::get_offset_of_mARCameraTransform_5(),
	VuforiaManagerImpl_t407376483::get_offset_of_mCentralAnchorPoint_6(),
	VuforiaManagerImpl_t407376483::get_offset_of_mParentAnchorPoint_7(),
	VuforiaManagerImpl_t407376483::get_offset_of_mTrackableResultDataArray_8(),
	VuforiaManagerImpl_t407376483::get_offset_of_mWordDataArray_9(),
	VuforiaManagerImpl_t407376483::get_offset_of_mWordResultDataArray_10(),
	VuforiaManagerImpl_t407376483::get_offset_of_mVuMarkDataArray_11(),
	VuforiaManagerImpl_t407376483::get_offset_of_mVuMarkResultDataArray_12(),
	VuforiaManagerImpl_t407376483::get_offset_of_mTrackableFoundQueue_13(),
	VuforiaManagerImpl_t407376483::get_offset_of_mImageHeaderData_14(),
	VuforiaManagerImpl_t407376483::get_offset_of_mNumImageHeaders_15(),
	VuforiaManagerImpl_t407376483::get_offset_of_mInjectedFrameIdx_16(),
	VuforiaManagerImpl_t407376483::get_offset_of_mLastProcessedFrameStatePtr_17(),
	VuforiaManagerImpl_t407376483::get_offset_of_mInitialized_18(),
	VuforiaManagerImpl_t407376483::get_offset_of_mPaused_19(),
	VuforiaManagerImpl_t407376483::get_offset_of_mFrameState_20(),
	VuforiaManagerImpl_t407376483::get_offset_of_mAutoRotationState_21(),
	VuforiaManagerImpl_t407376483::get_offset_of_mVideoBackgroundNeedsRedrawing_22(),
	VuforiaManagerImpl_t407376483::get_offset_of_mDiscardStatesForRendering_23(),
	VuforiaManagerImpl_t407376483::get_offset_of_mLastFrameIdx_24(),
	VuforiaManagerImpl_t407376483::get_offset_of_mIsSeeThroughDevice_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4345 = { sizeof (PoseData_t3114383412)+ sizeof (Il2CppObject), sizeof(PoseData_t3114383412 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4345[3] = 
{
	PoseData_t3114383412::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseData_t3114383412::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseData_t3114383412::get_offset_of_csInteger_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4346 = { sizeof (TrackableResultData_t3800049328)+ sizeof (Il2CppObject), sizeof(TrackableResultData_t3800049328 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4346[4] = 
{
	TrackableResultData_t3800049328::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t3800049328::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t3800049328::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableResultData_t3800049328::get_offset_of_id_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4347 = { sizeof (VirtualButtonData_t2901758114)+ sizeof (Il2CppObject), sizeof(VirtualButtonData_t2901758114 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4347[2] = 
{
	VirtualButtonData_t2901758114::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VirtualButtonData_t2901758114::get_offset_of_isPressed_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4348 = { sizeof (Obb2D_t3709259836)+ sizeof (Il2CppObject), sizeof(Obb2D_t3709259836 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4348[4] = 
{
	Obb2D_t3709259836::get_offset_of_center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t3709259836::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t3709259836::get_offset_of_rotation_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb2D_t3709259836::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4349 = { sizeof (Obb3D_t980376481)+ sizeof (Il2CppObject), sizeof(Obb3D_t980376481 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4349[4] = 
{
	Obb3D_t980376481::get_offset_of_center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t980376481::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t980376481::get_offset_of_rotationZ_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Obb3D_t980376481::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4350 = { sizeof (WordResultData_t1835118957)+ sizeof (Il2CppObject), sizeof(WordResultData_t1835118957 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4350[5] = 
{
	WordResultData_t1835118957::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t1835118957::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t1835118957::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t1835118957::get_offset_of_id_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordResultData_t1835118957::get_offset_of_orientedBoundingBox_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4351 = { sizeof (WordData_t2883825721)+ sizeof (Il2CppObject), sizeof(WordData_t2883825721 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4351[4] = 
{
	WordData_t2883825721::get_offset_of_stringValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t2883825721::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t2883825721::get_offset_of_size_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordData_t2883825721::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4352 = { sizeof (ImageHeaderData_t1300500262)+ sizeof (Il2CppObject), sizeof(ImageHeaderData_t1300500262 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4352[9] = 
{
	ImageHeaderData_t1300500262::get_offset_of_data_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t1300500262::get_offset_of_width_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t1300500262::get_offset_of_height_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t1300500262::get_offset_of_stride_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t1300500262::get_offset_of_bufferWidth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t1300500262::get_offset_of_bufferHeight_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t1300500262::get_offset_of_format_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t1300500262::get_offset_of_reallocate_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageHeaderData_t1300500262::get_offset_of_updated_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4353 = { sizeof (MeshData_t1799993495)+ sizeof (Il2CppObject), sizeof(MeshData_t1799993495 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4353[8] = 
{
	MeshData_t1799993495::get_offset_of_positionsArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1799993495::get_offset_of_normalsArray_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1799993495::get_offset_of_texCoordsArray_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1799993495::get_offset_of_triangleIdxArray_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1799993495::get_offset_of_numVertexValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1799993495::get_offset_of_hasNormals_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1799993495::get_offset_of_hasTexCoords_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MeshData_t1799993495::get_offset_of_numTriangleIndices_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4354 = { sizeof (InstanceIdData_t3603203054)+ sizeof (Il2CppObject), sizeof(InstanceIdData_t3603203054 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4354[5] = 
{
	InstanceIdData_t3603203054::get_offset_of_numericValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InstanceIdData_t3603203054::get_offset_of_buffer_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InstanceIdData_t3603203054::get_offset_of_reserved_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InstanceIdData_t3603203054::get_offset_of_dataLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InstanceIdData_t3603203054::get_offset_of_dataType_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4355 = { sizeof (VuMarkTargetData_t3925829072)+ sizeof (Il2CppObject), sizeof(VuMarkTargetData_t3925829072 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4355[5] = 
{
	VuMarkTargetData_t3925829072::get_offset_of_instanceId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetData_t3925829072::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetData_t3925829072::get_offset_of_templateId_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetData_t3925829072::get_offset_of_size_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetData_t3925829072::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4356 = { sizeof (SmartTerrainRevisionData_t2744445717)+ sizeof (Il2CppObject), sizeof(SmartTerrainRevisionData_t2744445717 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4356[2] = 
{
	SmartTerrainRevisionData_t2744445717::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SmartTerrainRevisionData_t2744445717::get_offset_of_revision_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4357 = { sizeof (SurfaceData_t1300926610)+ sizeof (Il2CppObject), sizeof(SurfaceData_t1300926610 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4357[9] = 
{
	SurfaceData_t1300926610::get_offset_of_meshBoundaryArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1300926610::get_offset_of_meshData_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1300926610::get_offset_of_navMeshData_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1300926610::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1300926610::get_offset_of_localPose_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1300926610::get_offset_of_id_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1300926610::get_offset_of_parentID_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1300926610::get_offset_of_numBoundaryIndices_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SurfaceData_t1300926610::get_offset_of_revision_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4358 = { sizeof (PropData_t489181770)+ sizeof (Il2CppObject), sizeof(PropData_t489181770 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4358[8] = 
{
	PropData_t489181770::get_offset_of_meshData_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t489181770::get_offset_of_id_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t489181770::get_offset_of_parentID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t489181770::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t489181770::get_offset_of_localPosition_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t489181770::get_offset_of_localPose_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t489181770::get_offset_of_revision_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropData_t489181770::get_offset_of_unused_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4359 = { sizeof (VuMarkTargetResultData_t1414459591)+ sizeof (Il2CppObject), sizeof(VuMarkTargetResultData_t1414459591 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4359[6] = 
{
	VuMarkTargetResultData_t1414459591::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t1414459591::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t1414459591::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t1414459591::get_offset_of_targetID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t1414459591::get_offset_of_resultID_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VuMarkTargetResultData_t1414459591::get_offset_of_unused_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4360 = { sizeof (FrameState_t574185949)+ sizeof (Il2CppObject), sizeof(FrameState_t574185949 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4360[22] = 
{
	FrameState_t574185949::get_offset_of_trackableDataArray_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_vbDataArray_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_wordResultArray_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_newWordDataArray_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_vuMarkResultArray_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_newVuMarkDataArray_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_propTrackableDataArray_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_smartTerrainRevisionsArray_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_updatedSurfacesArray_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_updatedPropsArray_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numTrackableResults_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numVirtualButtonResults_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_frameIndex_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numWordResults_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numNewWords_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numVuMarkResults_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numNewVuMarks_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numPropTrackableResults_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numSmartTerrainRevisions_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numUpdatedSurfaces_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_numUpdatedProps_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameState_t574185949::get_offset_of_deviceTrackableId_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4361 = { sizeof (AutoRotationState_t946804616)+ sizeof (Il2CppObject), sizeof(AutoRotationState_t946804616_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4361[5] = 
{
	AutoRotationState_t946804616::get_offset_of_setOnPause_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t946804616::get_offset_of_autorotateToPortrait_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t946804616::get_offset_of_autorotateToPortraitUpsideDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t946804616::get_offset_of_autorotateToLandscapeLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t946804616::get_offset_of_autorotateToLandscapeRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4362 = { sizeof (U3CU3Ec__DisplayClass86_0_t2218716262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4362[1] = 
{
	U3CU3Ec__DisplayClass86_0_t2218716262::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4363 = { sizeof (VuforiaRendererImpl_t821052510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4363[6] = 
{
	VuforiaRendererImpl_t821052510::get_offset_of_mVideoBGConfig_1(),
	VuforiaRendererImpl_t821052510::get_offset_of_mVideoBGConfigSet_2(),
	VuforiaRendererImpl_t821052510::get_offset_of_mVideoBackgroundTexture_3(),
	VuforiaRendererImpl_t821052510::get_offset_of_mBackgroundTextureHasChanged_4(),
	VuforiaRendererImpl_t821052510::get_offset_of_mLastSetReflection_5(),
	VuforiaRendererImpl_t821052510::get_offset_of_mNativeRenderingCallback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4364 = { sizeof (RenderEvent_t3153772864)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4364[7] = 
{
	RenderEvent_t3153772864::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4365 = { sizeof (VuforiaUnityImpl_t933790584), -1, sizeof(VuforiaUnityImpl_t933790584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4365[5] = 
{
	0,
	0,
	0,
	VuforiaUnityImpl_t933790584_StaticFields::get_offset_of_mRendererDirty_3(),
	VuforiaUnityImpl_t933790584_StaticFields::get_offset_of_mWrapperType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4366 = { sizeof (SurfaceImpl_t1244782987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4366[5] = 
{
	SurfaceImpl_t1244782987::get_offset_of_mNavMesh_7(),
	SurfaceImpl_t1244782987::get_offset_of_mMeshBoundaries_8(),
	SurfaceImpl_t1244782987::get_offset_of_mBoundingBox_9(),
	SurfaceImpl_t1244782987::get_offset_of_mSurfaceArea_10(),
	SurfaceImpl_t1244782987::get_offset_of_mAreaNeedsUpdate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4367 = { sizeof (SmartTerrainBuilderImpl_t1656443109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4367[2] = 
{
	SmartTerrainBuilderImpl_t1656443109::get_offset_of_mReconstructionBehaviours_0(),
	SmartTerrainBuilderImpl_t1656443109::get_offset_of_mIsInitialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4368 = { sizeof (PropImpl_t3704727797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4368[1] = 
{
	PropImpl_t3704727797::get_offset_of_mOrientedBoundingBox3D_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4369 = { sizeof (SmartTerrainTrackableImpl_t2707361041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4369[5] = 
{
	SmartTerrainTrackableImpl_t2707361041::get_offset_of_mChildren_2(),
	SmartTerrainTrackableImpl_t2707361041::get_offset_of_mMesh_3(),
	SmartTerrainTrackableImpl_t2707361041::get_offset_of_mMeshRevision_4(),
	SmartTerrainTrackableImpl_t2707361041::get_offset_of_mLocalPose_5(),
	SmartTerrainTrackableImpl_t2707361041::get_offset_of_U3CParentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4370 = { sizeof (SmartTerrainTrackerImpl_t651952228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4370[2] = 
{
	SmartTerrainTrackerImpl_t651952228::get_offset_of_mScaleToMillimeter_1(),
	SmartTerrainTrackerImpl_t651952228::get_offset_of_mSmartTerrainBuilder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4371 = { sizeof (TextTrackerImpl_t1410587152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4371[1] = 
{
	TextTrackerImpl_t1410587152::get_offset_of_mWordList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4372 = { sizeof (UpDirection_t2731840355)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4372[5] = 
{
	UpDirection_t2731840355::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4373 = { sizeof (TypeMapping_t3272509632), -1, sizeof(TypeMapping_t3272509632_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4373[1] = 
{
	TypeMapping_t3272509632_StaticFields::get_offset_of_sTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4374 = { sizeof (WebCamTexAdaptor_t3430449046), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4375 = { sizeof (WebCamTexAdaptorImpl_t4146576487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4375[2] = 
{
	WebCamTexAdaptorImpl_t4146576487::get_offset_of_mWebCamTexture_0(),
	WebCamTexAdaptorImpl_t4146576487::get_offset_of_mCheckCameraPermissions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4376 = { sizeof (WordImpl_t2494369133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4376[5] = 
{
	WordImpl_t2494369133::get_offset_of_mText_2(),
	WordImpl_t2494369133::get_offset_of_mSize_3(),
	WordImpl_t2494369133::get_offset_of_mLetterMask_4(),
	WordImpl_t2494369133::get_offset_of_mLetterImageHeader_5(),
	WordImpl_t2494369133::get_offset_of_mLetterBoundingBoxes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4377 = { sizeof (WordPrefabCreationMode_t3691738946)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4377[3] = 
{
	WordPrefabCreationMode_t3691738946::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4378 = { sizeof (WordManagerImpl_t1706254019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4378[12] = 
{
	WordManagerImpl_t1706254019::get_offset_of_mTrackedWords_0(),
	WordManagerImpl_t1706254019::get_offset_of_mNewWords_1(),
	WordManagerImpl_t1706254019::get_offset_of_mLostWords_2(),
	WordManagerImpl_t1706254019::get_offset_of_mActiveWordBehaviours_3(),
	WordManagerImpl_t1706254019::get_offset_of_mWordBehavioursMarkedForDeletion_4(),
	WordManagerImpl_t1706254019::get_offset_of_mWaitingQueue_5(),
	0,
	WordManagerImpl_t1706254019::get_offset_of_mWordBehaviours_7(),
	WordManagerImpl_t1706254019::get_offset_of_mAutomaticTemplate_8(),
	WordManagerImpl_t1706254019::get_offset_of_mMaxInstances_9(),
	WordManagerImpl_t1706254019::get_offset_of_mWordPrefabCreationMode_10(),
	WordManagerImpl_t1706254019::get_offset_of_mVuforiaBehaviour_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4379 = { sizeof (WordResultImpl_t709252498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4379[5] = 
{
	WordResultImpl_t709252498::get_offset_of_mObb_0(),
	WordResultImpl_t709252498::get_offset_of_mPosition_1(),
	WordResultImpl_t709252498::get_offset_of_mOrientation_2(),
	WordResultImpl_t709252498::get_offset_of_mWord_3(),
	WordResultImpl_t709252498::get_offset_of_mStatus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4380 = { sizeof (WordListImpl_t4149586592), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4381 = { sizeof (VuforiaNullWrapper_t2343315499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4382 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4383 = { sizeof (VuforiaNativeWrapper_t2621726075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4384 = { sizeof (VuforiaWrapper_t2763746413), -1, sizeof(VuforiaWrapper_t2763746413_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4384[2] = 
{
	VuforiaWrapper_t2763746413_StaticFields::get_offset_of_sWrapper_0(),
	VuforiaWrapper_t2763746413_StaticFields::get_offset_of_sCamIndependentWrapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4385 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4386 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4387 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4388 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4389 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4390 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4391 = { sizeof (ReconstructionAbstractBehaviour_t818896732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4391[22] = 
{
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mHasInitialized_2(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mSmartTerrainEventHandlers_3(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mOnInitialized_4(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mOnPropCreated_5(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mOnPropUpdated_6(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mOnPropDeleted_7(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mOnSurfaceCreated_8(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mOnSurfaceUpdated_9(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mOnSurfaceDeleted_10(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mInitializedInEditor_11(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mMaximumExtentEnabled_12(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mMaximumExtent_13(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mAutomaticStart_14(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mNavMeshUpdates_15(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mNavMeshPadding_16(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mReconstruction_17(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mSurfaces_18(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mActiveSurfaceBehaviours_19(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mProps_20(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mActivePropBehaviours_21(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22(),
	ReconstructionAbstractBehaviour_t818896732::get_offset_of_mIgnoreNextUpdate_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4392 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4393 = { sizeof (PropAbstractBehaviour_t2080236229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4393[2] = 
{
	PropAbstractBehaviour_t2080236229::get_offset_of_mProp_14(),
	PropAbstractBehaviour_t2080236229::get_offset_of_mBoxColliderToUpdate_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4394 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4395 = { sizeof (SmartTerrainTracker_t1238706968), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4396 = { sizeof (StateManager_t1982749557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4397 = { sizeof (StateManagerImpl_t1869359784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4397[9] = 
{
	StateManagerImpl_t1869359784::get_offset_of_mTrackableBehaviours_0(),
	StateManagerImpl_t1869359784::get_offset_of_mAutomaticallyCreatedBehaviours_1(),
	StateManagerImpl_t1869359784::get_offset_of_mBehavioursMarkedForDeletion_2(),
	StateManagerImpl_t1869359784::get_offset_of_mActiveTrackableBehaviours_3(),
	StateManagerImpl_t1869359784::get_offset_of_mWordManager_4(),
	StateManagerImpl_t1869359784::get_offset_of_mVuMarkManager_5(),
	StateManagerImpl_t1869359784::get_offset_of_mDeviceTrackingManager_6(),
	StateManagerImpl_t1869359784::get_offset_of_mCameraPositioningHelper_7(),
	StateManagerImpl_t1869359784::get_offset_of_mExtendedTrackingManager_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4398 = { sizeof (TargetFinderImpl_t2888108813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4398[4] = 
{
	TargetFinderImpl_t2888108813::get_offset_of_mTargetFinderStatePtr_0(),
	TargetFinderImpl_t2888108813::get_offset_of_mTargetFinderState_1(),
	TargetFinderImpl_t2888108813::get_offset_of_mNewResults_2(),
	TargetFinderImpl_t2888108813::get_offset_of_mImageTargets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4399 = { sizeof (TargetFinderState_t884522839)+ sizeof (Il2CppObject), sizeof(TargetFinderState_t884522839 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4399[4] = 
{
	TargetFinderState_t884522839::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t884522839::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t884522839::get_offset_of_ResultCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t884522839::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
