﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_TextWriter3478189236.h"

// System.Web.HttpResponseStream
struct HttpResponseStream_t38354555;
// System.Web.HttpResponse
struct HttpResponse_t1542361753;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpWriter
struct  HttpWriter_t2060157611  : public TextWriter_t3478189236
{
public:
	// System.Web.HttpResponseStream System.Web.HttpWriter::output_stream
	HttpResponseStream_t38354555 * ___output_stream_4;
	// System.Web.HttpResponse System.Web.HttpWriter::response
	HttpResponse_t1542361753 * ___response_5;
	// System.Text.Encoding System.Web.HttpWriter::encoding
	Encoding_t1523322056 * ___encoding_6;
	// System.Byte[] System.Web.HttpWriter::_bytebuffer
	ByteU5BU5D_t4116647657* ____bytebuffer_7;
	// System.Char[] System.Web.HttpWriter::chars
	CharU5BU5D_t3528271667* ___chars_8;

public:
	inline static int32_t get_offset_of_output_stream_4() { return static_cast<int32_t>(offsetof(HttpWriter_t2060157611, ___output_stream_4)); }
	inline HttpResponseStream_t38354555 * get_output_stream_4() const { return ___output_stream_4; }
	inline HttpResponseStream_t38354555 ** get_address_of_output_stream_4() { return &___output_stream_4; }
	inline void set_output_stream_4(HttpResponseStream_t38354555 * value)
	{
		___output_stream_4 = value;
		Il2CppCodeGenWriteBarrier(&___output_stream_4, value);
	}

	inline static int32_t get_offset_of_response_5() { return static_cast<int32_t>(offsetof(HttpWriter_t2060157611, ___response_5)); }
	inline HttpResponse_t1542361753 * get_response_5() const { return ___response_5; }
	inline HttpResponse_t1542361753 ** get_address_of_response_5() { return &___response_5; }
	inline void set_response_5(HttpResponse_t1542361753 * value)
	{
		___response_5 = value;
		Il2CppCodeGenWriteBarrier(&___response_5, value);
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(HttpWriter_t2060157611, ___encoding_6)); }
	inline Encoding_t1523322056 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t1523322056 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t1523322056 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_6, value);
	}

	inline static int32_t get_offset_of__bytebuffer_7() { return static_cast<int32_t>(offsetof(HttpWriter_t2060157611, ____bytebuffer_7)); }
	inline ByteU5BU5D_t4116647657* get__bytebuffer_7() const { return ____bytebuffer_7; }
	inline ByteU5BU5D_t4116647657** get_address_of__bytebuffer_7() { return &____bytebuffer_7; }
	inline void set__bytebuffer_7(ByteU5BU5D_t4116647657* value)
	{
		____bytebuffer_7 = value;
		Il2CppCodeGenWriteBarrier(&____bytebuffer_7, value);
	}

	inline static int32_t get_offset_of_chars_8() { return static_cast<int32_t>(offsetof(HttpWriter_t2060157611, ___chars_8)); }
	inline CharU5BU5D_t3528271667* get_chars_8() const { return ___chars_8; }
	inline CharU5BU5D_t3528271667** get_address_of_chars_8() { return &___chars_8; }
	inline void set_chars_8(CharU5BU5D_t3528271667* value)
	{
		___chars_8 = value;
		Il2CppCodeGenWriteBarrier(&___chars_8, value);
	}
};

struct HttpWriter_t2060157611_StaticFields
{
public:
	// System.Char[] System.Web.HttpWriter::newline
	CharU5BU5D_t3528271667* ___newline_9;

public:
	inline static int32_t get_offset_of_newline_9() { return static_cast<int32_t>(offsetof(HttpWriter_t2060157611_StaticFields, ___newline_9)); }
	inline CharU5BU5D_t3528271667* get_newline_9() const { return ___newline_9; }
	inline CharU5BU5D_t3528271667** get_address_of_newline_9() { return &___newline_9; }
	inline void set_newline_9(CharU5BU5D_t3528271667* value)
	{
		___newline_9 = value;
		Il2CppCodeGenWriteBarrier(&___newline_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
