﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_IO_Stream1273022909.h"

// System.Web.HttpResponseStream/Bucket
struct Bucket_t1453203164;
// System.Web.HttpResponse
struct HttpResponse_t1542361753;
// System.IO.Stream
struct Stream_t1273022909;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.HttpResponseStream
struct  HttpResponseStream_t38354555  : public Stream_t1273022909
{
public:
	// System.Web.HttpResponseStream/Bucket System.Web.HttpResponseStream::first_bucket
	Bucket_t1453203164 * ___first_bucket_2;
	// System.Web.HttpResponseStream/Bucket System.Web.HttpResponseStream::cur_bucket
	Bucket_t1453203164 * ___cur_bucket_3;
	// System.Web.HttpResponse System.Web.HttpResponseStream::response
	HttpResponse_t1542361753 * ___response_4;
	// System.Int64 System.Web.HttpResponseStream::total
	int64_t ___total_5;
	// System.IO.Stream System.Web.HttpResponseStream::filter
	Stream_t1273022909 * ___filter_6;
	// System.Byte[] System.Web.HttpResponseStream::chunk_buffer
	ByteU5BU5D_t4116647657* ___chunk_buffer_7;
	// System.Boolean System.Web.HttpResponseStream::filtering
	bool ___filtering_8;

public:
	inline static int32_t get_offset_of_first_bucket_2() { return static_cast<int32_t>(offsetof(HttpResponseStream_t38354555, ___first_bucket_2)); }
	inline Bucket_t1453203164 * get_first_bucket_2() const { return ___first_bucket_2; }
	inline Bucket_t1453203164 ** get_address_of_first_bucket_2() { return &___first_bucket_2; }
	inline void set_first_bucket_2(Bucket_t1453203164 * value)
	{
		___first_bucket_2 = value;
		Il2CppCodeGenWriteBarrier(&___first_bucket_2, value);
	}

	inline static int32_t get_offset_of_cur_bucket_3() { return static_cast<int32_t>(offsetof(HttpResponseStream_t38354555, ___cur_bucket_3)); }
	inline Bucket_t1453203164 * get_cur_bucket_3() const { return ___cur_bucket_3; }
	inline Bucket_t1453203164 ** get_address_of_cur_bucket_3() { return &___cur_bucket_3; }
	inline void set_cur_bucket_3(Bucket_t1453203164 * value)
	{
		___cur_bucket_3 = value;
		Il2CppCodeGenWriteBarrier(&___cur_bucket_3, value);
	}

	inline static int32_t get_offset_of_response_4() { return static_cast<int32_t>(offsetof(HttpResponseStream_t38354555, ___response_4)); }
	inline HttpResponse_t1542361753 * get_response_4() const { return ___response_4; }
	inline HttpResponse_t1542361753 ** get_address_of_response_4() { return &___response_4; }
	inline void set_response_4(HttpResponse_t1542361753 * value)
	{
		___response_4 = value;
		Il2CppCodeGenWriteBarrier(&___response_4, value);
	}

	inline static int32_t get_offset_of_total_5() { return static_cast<int32_t>(offsetof(HttpResponseStream_t38354555, ___total_5)); }
	inline int64_t get_total_5() const { return ___total_5; }
	inline int64_t* get_address_of_total_5() { return &___total_5; }
	inline void set_total_5(int64_t value)
	{
		___total_5 = value;
	}

	inline static int32_t get_offset_of_filter_6() { return static_cast<int32_t>(offsetof(HttpResponseStream_t38354555, ___filter_6)); }
	inline Stream_t1273022909 * get_filter_6() const { return ___filter_6; }
	inline Stream_t1273022909 ** get_address_of_filter_6() { return &___filter_6; }
	inline void set_filter_6(Stream_t1273022909 * value)
	{
		___filter_6 = value;
		Il2CppCodeGenWriteBarrier(&___filter_6, value);
	}

	inline static int32_t get_offset_of_chunk_buffer_7() { return static_cast<int32_t>(offsetof(HttpResponseStream_t38354555, ___chunk_buffer_7)); }
	inline ByteU5BU5D_t4116647657* get_chunk_buffer_7() const { return ___chunk_buffer_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_chunk_buffer_7() { return &___chunk_buffer_7; }
	inline void set_chunk_buffer_7(ByteU5BU5D_t4116647657* value)
	{
		___chunk_buffer_7 = value;
		Il2CppCodeGenWriteBarrier(&___chunk_buffer_7, value);
	}

	inline static int32_t get_offset_of_filtering_8() { return static_cast<int32_t>(offsetof(HttpResponseStream_t38354555, ___filtering_8)); }
	inline bool get_filtering_8() const { return ___filtering_8; }
	inline bool* get_address_of_filtering_8() { return &___filtering_8; }
	inline void set_filtering_8(bool value)
	{
		___filtering_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
