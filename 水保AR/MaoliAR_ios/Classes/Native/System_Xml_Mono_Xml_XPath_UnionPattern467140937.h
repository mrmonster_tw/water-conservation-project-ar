﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_XPath_Pattern1136864796.h"

// Mono.Xml.XPath.Pattern
struct Pattern_t1136864796;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.UnionPattern
struct  UnionPattern_t467140937  : public Pattern_t1136864796
{
public:
	// Mono.Xml.XPath.Pattern Mono.Xml.XPath.UnionPattern::p0
	Pattern_t1136864796 * ___p0_0;
	// Mono.Xml.XPath.Pattern Mono.Xml.XPath.UnionPattern::p1
	Pattern_t1136864796 * ___p1_1;

public:
	inline static int32_t get_offset_of_p0_0() { return static_cast<int32_t>(offsetof(UnionPattern_t467140937, ___p0_0)); }
	inline Pattern_t1136864796 * get_p0_0() const { return ___p0_0; }
	inline Pattern_t1136864796 ** get_address_of_p0_0() { return &___p0_0; }
	inline void set_p0_0(Pattern_t1136864796 * value)
	{
		___p0_0 = value;
		Il2CppCodeGenWriteBarrier(&___p0_0, value);
	}

	inline static int32_t get_offset_of_p1_1() { return static_cast<int32_t>(offsetof(UnionPattern_t467140937, ___p1_1)); }
	inline Pattern_t1136864796 * get_p1_1() const { return ___p1_1; }
	inline Pattern_t1136864796 ** get_address_of_p1_1() { return &___p1_1; }
	inline void set_p1_1(Pattern_t1136864796 * value)
	{
		___p1_1 = value;
		Il2CppCodeGenWriteBarrier(&___p1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
