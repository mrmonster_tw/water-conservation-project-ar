﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_R2746353389.h"

// System.ServiceModel.EndpointAddress
struct EndpointAddress_t3119842923;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.InternalReplyChannelBase
struct  InternalReplyChannelBase_t1191478253  : public ReplyChannelBase_t2746353389
{
public:
	// System.ServiceModel.EndpointAddress System.ServiceModel.Channels.InternalReplyChannelBase::local_address
	EndpointAddress_t3119842923 * ___local_address_14;

public:
	inline static int32_t get_offset_of_local_address_14() { return static_cast<int32_t>(offsetof(InternalReplyChannelBase_t1191478253, ___local_address_14)); }
	inline EndpointAddress_t3119842923 * get_local_address_14() const { return ___local_address_14; }
	inline EndpointAddress_t3119842923 ** get_address_of_local_address_14() { return &___local_address_14; }
	inline void set_local_address_14(EndpointAddress_t3119842923 * value)
	{
		___local_address_14 = value;
		Il2CppCodeGenWriteBarrier(&___local_address_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
