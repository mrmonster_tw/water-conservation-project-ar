﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.IO.FileSystemInfo
struct FileSystemInfo_t3745885336;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.KeventFileData
struct  KeventFileData_t3537069659  : public Il2CppObject
{
public:
	// System.IO.FileSystemInfo System.IO.KeventFileData::fsi
	FileSystemInfo_t3745885336 * ___fsi_0;
	// System.DateTime System.IO.KeventFileData::LastAccessTime
	DateTime_t3738529785  ___LastAccessTime_1;
	// System.DateTime System.IO.KeventFileData::LastWriteTime
	DateTime_t3738529785  ___LastWriteTime_2;

public:
	inline static int32_t get_offset_of_fsi_0() { return static_cast<int32_t>(offsetof(KeventFileData_t3537069659, ___fsi_0)); }
	inline FileSystemInfo_t3745885336 * get_fsi_0() const { return ___fsi_0; }
	inline FileSystemInfo_t3745885336 ** get_address_of_fsi_0() { return &___fsi_0; }
	inline void set_fsi_0(FileSystemInfo_t3745885336 * value)
	{
		___fsi_0 = value;
		Il2CppCodeGenWriteBarrier(&___fsi_0, value);
	}

	inline static int32_t get_offset_of_LastAccessTime_1() { return static_cast<int32_t>(offsetof(KeventFileData_t3537069659, ___LastAccessTime_1)); }
	inline DateTime_t3738529785  get_LastAccessTime_1() const { return ___LastAccessTime_1; }
	inline DateTime_t3738529785 * get_address_of_LastAccessTime_1() { return &___LastAccessTime_1; }
	inline void set_LastAccessTime_1(DateTime_t3738529785  value)
	{
		___LastAccessTime_1 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_2() { return static_cast<int32_t>(offsetof(KeventFileData_t3537069659, ___LastWriteTime_2)); }
	inline DateTime_t3738529785  get_LastWriteTime_2() const { return ___LastWriteTime_2; }
	inline DateTime_t3738529785 * get_address_of_LastWriteTime_2() { return &___LastWriteTime_2; }
	inline void set_LastWriteTime_2(DateTime_t3738529785  value)
	{
		___LastWriteTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
