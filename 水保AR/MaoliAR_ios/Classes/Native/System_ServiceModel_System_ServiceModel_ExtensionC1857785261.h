﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_Collections_Generic_Syn3223601106.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.ExtensionCollection`1<System.Object>
struct  ExtensionCollection_1_t1857785261  : public SynchronizedCollection_1_t3223601106
{
public:
	// T System.ServiceModel.ExtensionCollection`1::owner
	Il2CppObject * ___owner_2;

public:
	inline static int32_t get_offset_of_owner_2() { return static_cast<int32_t>(offsetof(ExtensionCollection_1_t1857785261, ___owner_2)); }
	inline Il2CppObject * get_owner_2() const { return ___owner_2; }
	inline Il2CppObject ** get_address_of_owner_2() { return &___owner_2; }
	inline void set_owner_2(Il2CppObject * value)
	{
		___owner_2 = value;
		Il2CppCodeGenWriteBarrier(&___owner_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
