﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M3561136435.h"

// System.ServiceModel.Channels.BinaryMessageEncodingBindingElement
struct BinaryMessageEncodingBindingElement_t4154157131;
// System.ServiceModel.Channels.BinaryMessageEncoder
struct BinaryMessageEncoder_t3396373419;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.BinaryMessageEncoderFactory
struct  BinaryMessageEncoderFactory_t4125415926  : public MessageEncoderFactory_t3561136435
{
public:
	// System.ServiceModel.Channels.BinaryMessageEncodingBindingElement System.ServiceModel.Channels.BinaryMessageEncoderFactory::owner
	BinaryMessageEncodingBindingElement_t4154157131 * ___owner_0;
	// System.ServiceModel.Channels.BinaryMessageEncoder System.ServiceModel.Channels.BinaryMessageEncoderFactory::encoder
	BinaryMessageEncoder_t3396373419 * ___encoder_1;

public:
	inline static int32_t get_offset_of_owner_0() { return static_cast<int32_t>(offsetof(BinaryMessageEncoderFactory_t4125415926, ___owner_0)); }
	inline BinaryMessageEncodingBindingElement_t4154157131 * get_owner_0() const { return ___owner_0; }
	inline BinaryMessageEncodingBindingElement_t4154157131 ** get_address_of_owner_0() { return &___owner_0; }
	inline void set_owner_0(BinaryMessageEncodingBindingElement_t4154157131 * value)
	{
		___owner_0 = value;
		Il2CppCodeGenWriteBarrier(&___owner_0, value);
	}

	inline static int32_t get_offset_of_encoder_1() { return static_cast<int32_t>(offsetof(BinaryMessageEncoderFactory_t4125415926, ___encoder_1)); }
	inline BinaryMessageEncoder_t3396373419 * get_encoder_1() const { return ___encoder_1; }
	inline BinaryMessageEncoder_t3396373419 ** get_address_of_encoder_1() { return &___encoder_1; }
	inline void set_encoder_1(BinaryMessageEncoder_t3396373419 * value)
	{
		___encoder_1 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
