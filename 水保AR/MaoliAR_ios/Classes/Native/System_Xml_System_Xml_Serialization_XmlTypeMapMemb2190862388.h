﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_XmlTypeMapMembe634974464.h"

// System.Xml.Serialization.ListMap
struct ListMap_t4107479011;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberFlatList
struct  XmlTypeMapMemberFlatList_t2190862388  : public XmlTypeMapMemberExpandable_t634974464
{
public:
	// System.Xml.Serialization.ListMap System.Xml.Serialization.XmlTypeMapMemberFlatList::_listMap
	ListMap_t4107479011 * ____listMap_14;

public:
	inline static int32_t get_offset_of__listMap_14() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberFlatList_t2190862388, ____listMap_14)); }
	inline ListMap_t4107479011 * get__listMap_14() const { return ____listMap_14; }
	inline ListMap_t4107479011 ** get_address_of__listMap_14() { return &____listMap_14; }
	inline void set__listMap_14(ListMap_t4107479011 * value)
	{
		____listMap_14 = value;
		Il2CppCodeGenWriteBarrier(&____listMap_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
