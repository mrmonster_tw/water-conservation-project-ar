﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Serialization_SerializationC2472901294.h"
#include "System_Xml_System_Xml_Serialization_SerializationC1025918884.h"
#include "System_Xml_System_Xml_Serialization_GenerationResu2469053731.h"
#include "System_Xml_System_Xml_Serialization_SerializerInfo2293805917.h"
#include "System_Xml_System_Xml_Serialization_Hook924144422.h"
#include "System_Xml_System_Xml_Serialization_Select2941486351.h"
#include "System_Xml_System_Xml_Serialization_HookAction3799292133.h"
#include "System_Xml_System_Xml_Serialization_HookType3446337949.h"
#include "System_Xml_System_Xml_Serialization_SerializationS3488723117.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeSeriali4151017472.h"
#include "System_Xml_System_Xml_Serialization_SoapTypeSerial2273034474.h"
#include "System_Xml_System_Xml_Serialization_MembersSerializ468897103.h"
#include "System_Xml_System_Xml_Serialization_SoapAttributeAt495928767.h"
#include "System_Xml_System_Xml_Serialization_SoapAttributeO1295397514.h"
#include "System_Xml_System_Xml_Serialization_SoapAttributes929401768.h"
#include "System_Xml_System_Xml_Serialization_SoapCodeExport3304897474.h"
#include "System_Xml_System_Xml_Serialization_SoapMapCodeGen2241378902.h"
#include "System_Xml_System_Xml_Serialization_SoapElementAtt1210796251.h"
#include "System_Xml_System_Xml_Serialization_SoapEnumAttribu306263779.h"
#include "System_Xml_System_Xml_Serialization_SoapIgnoreAttr3723184581.h"
#include "System_Xml_System_Xml_Serialization_SoapIncludeAttr666096636.h"
#include "System_Xml_System_Xml_Serialization_SoapSchemaImpo1895423347.h"
#include "System_Xml_System_Xml_Serialization_SoapSchemaMembe859827560.h"
#include "System_Xml_System_Xml_Serialization_SoapReflectionI749884629.h"
#include "System_Xml_System_Xml_Serialization_SoapTypeAttrib2580020517.h"
#include "System_Xml_System_Xml_Serialization_TypeData476999220.h"
#include "System_Xml_System_Xml_Serialization_TypeMember914068052.h"
#include "System_Xml_System_Xml_Serialization_TypeTranslator3446962748.h"
#include "System_Xml_System_Xml_Serialization_UnreferencedOb1466260313.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyAttribut1449326428.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyElementA4038919363.h"
#include "System_Xml_System_Xml_Serialization_XmlAnyElementA1381893315.h"
#include "System_Xml_System_Xml_Serialization_XmlArrayAttrib1484124842.h"
#include "System_Xml_System_Xml_Serialization_XmlArrayItemAt3521089969.h"
#include "System_Xml_System_Xml_Serialization_XmlArrayItemAt3998742014.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeAt2511360870.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeEv1881679642.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeOv4195160900.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributes3116019767.h"
#include "System_Xml_System_Xml_Serialization_XmlCodeExporte4164579413.h"
#include "System_Xml_System_Xml_Serialization_XmlMapCodeGene3713090525.h"
#include "System_Xml_System_Xml_Serialization_XmlChoiceIdent1913802258.h"
#include "System_Xml_System_Xml_Serialization_XmlCustomForma2377520463.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttrib17472343.h"
#include "System_Xml_System_Xml_Serialization_XmlElementAttr2920573420.h"
#include "System_Xml_System_Xml_Serialization_XmlElementEventA71396081.h"
#include "System_Xml_System_Xml_Serialization_XmlEnumAttribut106705320.h"
#include "System_Xml_System_Xml_Serialization_XmlIgnoreAttri1428424057.h"
#include "System_Xml_System_Xml_Serialization_XmlIncludeAttr1446401819.h"
#include "System_Xml_System_Xml_Serialization_XmlMemberMappin113569223.h"
#include "System_Xml_System_Xml_Serialization_XmlMembersMapp4154751912.h"
#include "System_Xml_System_Xml_Serialization_XmlMapping1653394.h"
#include "System_Xml_System_Xml_Serialization_ObjectMap4068517277.h"
#include "System_Xml_System_Xml_Serialization_SerializationF3918594465.h"
#include "System_Xml_System_Xml_Serialization_XmlMappingAcce2719670221.h"
#include "System_Xml_System_Xml_Serialization_XmlNamespaceDec966425202.h"
#include "System_Xml_System_Xml_Serialization_XmlNodeEventAr1044138183.h"
#include "System_Xml_System_Xml_Serialization_XmlReflectionI1777580912.h"
#include "System_Xml_System_Xml_Serialization_XmlReflectionM1313238960.h"
#include "System_Xml_System_Xml_Serialization_XmlRootAttribu2306097217.h"
#include "System_Xml_System_Xml_Serialization_XmlSchemaEnume2583766800.h"
#include "System_Xml_System_Xml_Serialization_XmlSchemaImpor2852143304.h"
#include "System_Xml_System_Xml_Serialization_XmlSchemaImport630237321.h"
#include "System_Xml_System_Xml_Serialization_XmlSchemaProvi3872582200.h"
#include "System_Xml_System_Xml_Serialization_XmlSchemas3283371924.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati1625906606.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati1652069793.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati2777933997.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializatio574604497.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3556126492.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3797480032.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializatio231046833.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati1167033645.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializatio882063084.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializatio982275218.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati2444815868.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati4120928523.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati2600680195.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializer1117804635.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializer_3337767682.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializer_1938464320.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerI3900572101.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializerN2702737953.h"
#include "System_Xml_System_Xml_Serialization_XmlTextAttribut499390083.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeAttribut945254369.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapElem2741990046.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapElem2115305165.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb3739392753.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb2452898273.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb1199680526.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMembe176526733.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMembe634974464.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb2190862388.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb1417227289.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb3039835622.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb1954902185.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapping3915382669.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializabl1689765018.h"
#include "System_Xml_System_Xml_Serialization_ClassMap2947503525.h"
#include "System_Xml_System_Xml_Serialization_ListMap4107479011.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (SerializationCodeGenerator_t2472901294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[19] = 
{
	SerializationCodeGenerator_t2472901294::get_offset_of__typeMap_0(),
	SerializationCodeGenerator_t2472901294::get_offset_of__format_1(),
	SerializationCodeGenerator_t2472901294::get_offset_of__writer_2(),
	SerializationCodeGenerator_t2472901294::get_offset_of__tempVarId_3(),
	SerializationCodeGenerator_t2472901294::get_offset_of__indent_4(),
	SerializationCodeGenerator_t2472901294::get_offset_of__uniqueNames_5(),
	SerializationCodeGenerator_t2472901294::get_offset_of__methodId_6(),
	SerializationCodeGenerator_t2472901294::get_offset_of__config_7(),
	SerializationCodeGenerator_t2472901294::get_offset_of__mapsToGenerate_8(),
	SerializationCodeGenerator_t2472901294::get_offset_of__fixupCallbacks_9(),
	SerializationCodeGenerator_t2472901294::get_offset_of__referencedTypes_10(),
	SerializationCodeGenerator_t2472901294::get_offset_of__results_11(),
	SerializationCodeGenerator_t2472901294::get_offset_of__result_12(),
	SerializationCodeGenerator_t2472901294::get_offset_of__xmlMaps_13(),
	SerializationCodeGenerator_t2472901294::get_offset_of_classNames_14(),
	SerializationCodeGenerator_t2472901294::get_offset_of__listsToFill_15(),
	SerializationCodeGenerator_t2472901294::get_offset_of__hookVariables_16(),
	SerializationCodeGenerator_t2472901294::get_offset_of__hookContexts_17(),
	SerializationCodeGenerator_t2472901294::get_offset_of__hookOpenHooks_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (HookInfo_t1025918884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[4] = 
{
	HookInfo_t1025918884::get_offset_of_HookType_0(),
	HookInfo_t1025918884::get_offset_of_Type_1(),
	HookInfo_t1025918884::get_offset_of_Member_2(),
	HookInfo_t1025918884::get_offset_of_Direction_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (GenerationResult_t2469053731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[9] = 
{
	GenerationResult_t2469053731::get_offset_of_Mapping_0(),
	GenerationResult_t2469053731::get_offset_of_ReaderClassName_1(),
	GenerationResult_t2469053731::get_offset_of_ReadMethodName_2(),
	GenerationResult_t2469053731::get_offset_of_WriterClassName_3(),
	GenerationResult_t2469053731::get_offset_of_WriteMethodName_4(),
	GenerationResult_t2469053731::get_offset_of_Namespace_5(),
	GenerationResult_t2469053731::get_offset_of_SerializerClassName_6(),
	GenerationResult_t2469053731::get_offset_of_BaseSerializerClassName_7(),
	GenerationResult_t2469053731::get_offset_of_ImplementationClassName_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (SerializerInfo_t2293805917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[11] = 
{
	SerializerInfo_t2293805917::get_offset_of_ReaderClassName_0(),
	SerializerInfo_t2293805917::get_offset_of_WriterClassName_1(),
	SerializerInfo_t2293805917::get_offset_of_BaseSerializerClassName_2(),
	SerializerInfo_t2293805917::get_offset_of_ImplementationClassName_3(),
	SerializerInfo_t2293805917::get_offset_of_NoReader_4(),
	SerializerInfo_t2293805917::get_offset_of_NoWriter_5(),
	SerializerInfo_t2293805917::get_offset_of_GenerateAsInternal_6(),
	SerializerInfo_t2293805917::get_offset_of_Namespace_7(),
	SerializerInfo_t2293805917::get_offset_of_NamespaceImports_8(),
	SerializerInfo_t2293805917::get_offset_of_ReaderHooks_9(),
	SerializerInfo_t2293805917::get_offset_of_WriterHooks_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (Hook_t924144422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[5] = 
{
	Hook_t924144422::get_offset_of_HookType_0(),
	Hook_t924144422::get_offset_of_Select_1(),
	Hook_t924144422::get_offset_of_InsertBefore_2(),
	Hook_t924144422::get_offset_of_InsertAfter_3(),
	Hook_t924144422::get_offset_of_Replace_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (Select_t2941486351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[3] = 
{
	Select_t2941486351::get_offset_of_TypeName_0(),
	Select_t2941486351::get_offset_of_TypeAttributes_1(),
	Select_t2941486351::get_offset_of_TypeMember_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (HookAction_t3799292133)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2006[4] = 
{
	HookAction_t3799292133::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (HookType_t3446337949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2007[7] = 
{
	HookType_t3446337949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (SerializationSource_t3488723117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[3] = 
{
	SerializationSource_t3488723117::get_offset_of_includedTypes_0(),
	SerializationSource_t3488723117::get_offset_of_namspace_1(),
	SerializationSource_t3488723117::get_offset_of_canBeGenerated_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (XmlTypeSerializationSource_t4151017472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[3] = 
{
	XmlTypeSerializationSource_t4151017472::get_offset_of_attributeOverridesHash_3(),
	XmlTypeSerializationSource_t4151017472::get_offset_of_type_4(),
	XmlTypeSerializationSource_t4151017472::get_offset_of_rootHash_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (SoapTypeSerializationSource_t2273034474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[2] = 
{
	SoapTypeSerializationSource_t2273034474::get_offset_of_attributeOverridesHash_3(),
	SoapTypeSerializationSource_t2273034474::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (MembersSerializationSource_t468897103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[5] = 
{
	MembersSerializationSource_t468897103::get_offset_of_elementName_3(),
	MembersSerializationSource_t468897103::get_offset_of_hasWrapperElement_4(),
	MembersSerializationSource_t468897103::get_offset_of_membersHash_5(),
	MembersSerializationSource_t468897103::get_offset_of_writeAccessors_6(),
	MembersSerializationSource_t468897103::get_offset_of_literalFormat_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (SoapAttributeAttribute_t495928767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[3] = 
{
	SoapAttributeAttribute_t495928767::get_offset_of_attrName_0(),
	SoapAttributeAttribute_t495928767::get_offset_of_dataType_1(),
	SoapAttributeAttribute_t495928767::get_offset_of_ns_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (SoapAttributeOverrides_t1295397514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[1] = 
{
	SoapAttributeOverrides_t1295397514::get_offset_of_overrides_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (SoapAttributes_t929401768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[6] = 
{
	SoapAttributes_t929401768::get_offset_of_soapAttribute_0(),
	SoapAttributes_t929401768::get_offset_of_soapDefaultValue_1(),
	SoapAttributes_t929401768::get_offset_of_soapElement_2(),
	SoapAttributes_t929401768::get_offset_of_soapEnum_3(),
	SoapAttributes_t929401768::get_offset_of_soapIgnore_4(),
	SoapAttributes_t929401768::get_offset_of_soapType_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (SoapCodeExporter_t3304897474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (SoapMapCodeGenerator_t2241378902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (SoapElementAttribute_t1210796251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[3] = 
{
	SoapElementAttribute_t1210796251::get_offset_of_dataType_0(),
	SoapElementAttribute_t1210796251::get_offset_of_elementName_1(),
	SoapElementAttribute_t1210796251::get_offset_of_isNullable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (SoapEnumAttribute_t306263779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[1] = 
{
	SoapEnumAttribute_t306263779::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (SoapIgnoreAttribute_t3723184581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (SoapIncludeAttribute_t666096636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[1] = 
{
	SoapIncludeAttribute_t666096636::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (SoapSchemaImporter_t1895423347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[1] = 
{
	SoapSchemaImporter_t1895423347::get_offset_of__importer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (SoapSchemaMember_t859827560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[2] = 
{
	SoapSchemaMember_t859827560::get_offset_of_memberName_0(),
	SoapSchemaMember_t859827560::get_offset_of_memberType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (SoapReflectionImporter_t749884629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[5] = 
{
	SoapReflectionImporter_t749884629::get_offset_of_attributeOverrides_0(),
	SoapReflectionImporter_t749884629::get_offset_of_initialDefaultNamespace_1(),
	SoapReflectionImporter_t749884629::get_offset_of_includedTypes_2(),
	SoapReflectionImporter_t749884629::get_offset_of_relatedMaps_3(),
	SoapReflectionImporter_t749884629::get_offset_of_helper_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (SoapTypeAttribute_t2580020517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[3] = 
{
	SoapTypeAttribute_t2580020517::get_offset_of_ns_0(),
	SoapTypeAttribute_t2580020517::get_offset_of_typeName_1(),
	SoapTypeAttribute_t2580020517::get_offset_of_includeInSchema_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (TypeData_t476999220), -1, sizeof(TypeData_t476999220_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2025[16] = 
{
	TypeData_t476999220::get_offset_of_type_0(),
	TypeData_t476999220::get_offset_of_elementName_1(),
	TypeData_t476999220::get_offset_of_sType_2(),
	TypeData_t476999220::get_offset_of_listItemType_3(),
	TypeData_t476999220::get_offset_of_typeName_4(),
	TypeData_t476999220::get_offset_of_fullTypeName_5(),
	TypeData_t476999220::get_offset_of_csharpName_6(),
	TypeData_t476999220::get_offset_of_csharpFullName_7(),
	TypeData_t476999220::get_offset_of_listItemTypeData_8(),
	TypeData_t476999220::get_offset_of_listTypeData_9(),
	TypeData_t476999220::get_offset_of_mappedType_10(),
	TypeData_t476999220::get_offset_of_facet_11(),
	TypeData_t476999220::get_offset_of_hasPublicConstructor_12(),
	TypeData_t476999220::get_offset_of_nullableOverride_13(),
	TypeData_t476999220_StaticFields::get_offset_of_keywordsTable_14(),
	TypeData_t476999220_StaticFields::get_offset_of_keywords_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (TypeMember_t914068052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[2] = 
{
	TypeMember_t914068052::get_offset_of_type_0(),
	TypeMember_t914068052::get_offset_of_member_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (TypeTranslator_t3446962748), -1, sizeof(TypeTranslator_t3446962748_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2027[4] = 
{
	TypeTranslator_t3446962748_StaticFields::get_offset_of_nameCache_0(),
	TypeTranslator_t3446962748_StaticFields::get_offset_of_primitiveTypes_1(),
	TypeTranslator_t3446962748_StaticFields::get_offset_of_primitiveArrayTypes_2(),
	TypeTranslator_t3446962748_StaticFields::get_offset_of_nullableTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (UnreferencedObjectEventArgs_t1466260313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[2] = 
{
	UnreferencedObjectEventArgs_t1466260313::get_offset_of_unreferencedObject_1(),
	UnreferencedObjectEventArgs_t1466260313::get_offset_of_unreferencedId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (XmlAnyAttributeAttribute_t1449326428), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (XmlAnyElementAttribute_t4038919363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[4] = 
{
	XmlAnyElementAttribute_t4038919363::get_offset_of_elementName_0(),
	XmlAnyElementAttribute_t4038919363::get_offset_of_ns_1(),
	XmlAnyElementAttribute_t4038919363::get_offset_of_isNamespaceSpecified_2(),
	XmlAnyElementAttribute_t4038919363::get_offset_of_order_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (XmlAnyElementAttributes_t1381893315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (XmlArrayAttribute_t1484124842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[5] = 
{
	XmlArrayAttribute_t1484124842::get_offset_of_elementName_0(),
	XmlArrayAttribute_t1484124842::get_offset_of_form_1(),
	XmlArrayAttribute_t1484124842::get_offset_of_isNullable_2(),
	XmlArrayAttribute_t1484124842::get_offset_of_ns_3(),
	XmlArrayAttribute_t1484124842::get_offset_of_order_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (XmlArrayItemAttribute_t3521089969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[7] = 
{
	XmlArrayItemAttribute_t3521089969::get_offset_of_dataType_0(),
	XmlArrayItemAttribute_t3521089969::get_offset_of_elementName_1(),
	XmlArrayItemAttribute_t3521089969::get_offset_of_form_2(),
	XmlArrayItemAttribute_t3521089969::get_offset_of_ns_3(),
	XmlArrayItemAttribute_t3521089969::get_offset_of_isNullable_4(),
	XmlArrayItemAttribute_t3521089969::get_offset_of_nestingLevel_5(),
	XmlArrayItemAttribute_t3521089969::get_offset_of_type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (XmlArrayItemAttributes_t3998742014), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (XmlAttributeAttribute_t2511360870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[5] = 
{
	XmlAttributeAttribute_t2511360870::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t2511360870::get_offset_of_dataType_1(),
	XmlAttributeAttribute_t2511360870::get_offset_of_type_2(),
	XmlAttributeAttribute_t2511360870::get_offset_of_form_3(),
	XmlAttributeAttribute_t2511360870::get_offset_of_ns_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (XmlAttributeEventArgs_t1881679642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[5] = 
{
	XmlAttributeEventArgs_t1881679642::get_offset_of_attr_1(),
	XmlAttributeEventArgs_t1881679642::get_offset_of_lineNumber_2(),
	XmlAttributeEventArgs_t1881679642::get_offset_of_linePosition_3(),
	XmlAttributeEventArgs_t1881679642::get_offset_of_obj_4(),
	XmlAttributeEventArgs_t1881679642::get_offset_of_expectedAttributes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (XmlAttributeOverrides_t4195160900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[1] = 
{
	XmlAttributeOverrides_t4195160900::get_offset_of_overrides_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (XmlAttributes_t3116019767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[14] = 
{
	XmlAttributes_t3116019767::get_offset_of_xmlAnyAttribute_0(),
	XmlAttributes_t3116019767::get_offset_of_xmlAnyElements_1(),
	XmlAttributes_t3116019767::get_offset_of_xmlArray_2(),
	XmlAttributes_t3116019767::get_offset_of_xmlArrayItems_3(),
	XmlAttributes_t3116019767::get_offset_of_xmlAttribute_4(),
	XmlAttributes_t3116019767::get_offset_of_xmlChoiceIdentifier_5(),
	XmlAttributes_t3116019767::get_offset_of_xmlDefaultValue_6(),
	XmlAttributes_t3116019767::get_offset_of_xmlElements_7(),
	XmlAttributes_t3116019767::get_offset_of_xmlEnum_8(),
	XmlAttributes_t3116019767::get_offset_of_xmlIgnore_9(),
	XmlAttributes_t3116019767::get_offset_of_xmlns_10(),
	XmlAttributes_t3116019767::get_offset_of_xmlRoot_11(),
	XmlAttributes_t3116019767::get_offset_of_xmlText_12(),
	XmlAttributes_t3116019767::get_offset_of_xmlType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (XmlCodeExporter_t4164579413), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (XmlMapCodeGenerator_t3713090525), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (XmlChoiceIdentifierAttribute_t1913802258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[1] = 
{
	XmlChoiceIdentifierAttribute_t1913802258::get_offset_of_memberName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (XmlCustomFormatter_t2377520463), -1, sizeof(XmlCustomFormatter_t2377520463_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2042[4] = 
{
	XmlCustomFormatter_t2377520463_StaticFields::get_offset_of_U3CU3Ef__switchU24map51_0(),
	XmlCustomFormatter_t2377520463_StaticFields::get_offset_of_U3CU3Ef__switchU24map52_1(),
	XmlCustomFormatter_t2377520463_StaticFields::get_offset_of_U3CU3Ef__switchU24map53_2(),
	XmlCustomFormatter_t2377520463_StaticFields::get_offset_of_U3CU3Ef__switchU24map54_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (XmlElementAttribute_t17472343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[8] = 
{
	XmlElementAttribute_t17472343::get_offset_of_dataType_0(),
	XmlElementAttribute_t17472343::get_offset_of_elementName_1(),
	XmlElementAttribute_t17472343::get_offset_of_form_2(),
	XmlElementAttribute_t17472343::get_offset_of_ns_3(),
	XmlElementAttribute_t17472343::get_offset_of_isNullable_4(),
	XmlElementAttribute_t17472343::get_offset_of_isNullableSpecified_5(),
	XmlElementAttribute_t17472343::get_offset_of_type_6(),
	XmlElementAttribute_t17472343::get_offset_of_order_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (XmlElementAttributes_t2920573420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (XmlElementEventArgs_t71396081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[5] = 
{
	XmlElementEventArgs_t71396081::get_offset_of_attr_1(),
	XmlElementEventArgs_t71396081::get_offset_of_lineNumber_2(),
	XmlElementEventArgs_t71396081::get_offset_of_linePosition_3(),
	XmlElementEventArgs_t71396081::get_offset_of_obj_4(),
	XmlElementEventArgs_t71396081::get_offset_of_expectedElements_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (XmlEnumAttribute_t106705320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[1] = 
{
	XmlEnumAttribute_t106705320::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (XmlIgnoreAttribute_t1428424057), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (XmlIncludeAttribute_t1446401819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[1] = 
{
	XmlIncludeAttribute_t1446401819::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (XmlMemberMapping_t113569223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[6] = 
{
	XmlMemberMapping_t113569223::get_offset_of__mapMember_0(),
	XmlMemberMapping_t113569223::get_offset_of__elementName_1(),
	XmlMemberMapping_t113569223::get_offset_of__memberName_2(),
	XmlMemberMapping_t113569223::get_offset_of__namespace_3(),
	XmlMemberMapping_t113569223::get_offset_of__typeNamespace_4(),
	XmlMemberMapping_t113569223::get_offset_of__form_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (XmlMembersMapping_t4154751912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[2] = 
{
	XmlMembersMapping_t4154751912::get_offset_of__hasWrapperElement_7(),
	XmlMembersMapping_t4154751912::get_offset_of__mapping_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (XmlMapping_t1653394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[7] = 
{
	XmlMapping_t1653394::get_offset_of_map_0(),
	XmlMapping_t1653394::get_offset_of_relatedMaps_1(),
	XmlMapping_t1653394::get_offset_of_format_2(),
	XmlMapping_t1653394::get_offset_of_source_3(),
	XmlMapping_t1653394::get_offset_of__elementName_4(),
	XmlMapping_t1653394::get_offset_of__namespace_5(),
	XmlMapping_t1653394::get_offset_of_key_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (ObjectMap_t4068517277), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (SerializationFormat_t3918594465)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2053[3] = 
{
	SerializationFormat_t3918594465::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (XmlMappingAccess_t2719670221)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2054[4] = 
{
	XmlMappingAccess_t2719670221::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (XmlNamespaceDeclarationsAttribute_t966425202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (XmlNodeEventArgs_t1044138183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[8] = 
{
	XmlNodeEventArgs_t1044138183::get_offset_of_linenumber_1(),
	XmlNodeEventArgs_t1044138183::get_offset_of_lineposition_2(),
	XmlNodeEventArgs_t1044138183::get_offset_of_localname_3(),
	XmlNodeEventArgs_t1044138183::get_offset_of_name_4(),
	XmlNodeEventArgs_t1044138183::get_offset_of_nsuri_5(),
	XmlNodeEventArgs_t1044138183::get_offset_of_nodetype_6(),
	XmlNodeEventArgs_t1044138183::get_offset_of_source_7(),
	XmlNodeEventArgs_t1044138183::get_offset_of_text_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (XmlReflectionImporter_t1777580912), -1, sizeof(XmlReflectionImporter_t1777580912_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2057[9] = 
{
	XmlReflectionImporter_t1777580912::get_offset_of_initialDefaultNamespace_0(),
	XmlReflectionImporter_t1777580912::get_offset_of_attributeOverrides_1(),
	XmlReflectionImporter_t1777580912::get_offset_of_includedTypes_2(),
	XmlReflectionImporter_t1777580912::get_offset_of_helper_3(),
	XmlReflectionImporter_t1777580912::get_offset_of_arrayChoiceCount_4(),
	XmlReflectionImporter_t1777580912::get_offset_of_relatedMaps_5(),
	XmlReflectionImporter_t1777580912::get_offset_of_allowPrivateTypes_6(),
	XmlReflectionImporter_t1777580912_StaticFields::get_offset_of_errSimple_7(),
	XmlReflectionImporter_t1777580912_StaticFields::get_offset_of_errSimple2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (XmlReflectionMember_t1313238960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[7] = 
{
	XmlReflectionMember_t1313238960::get_offset_of_isReturnValue_0(),
	XmlReflectionMember_t1313238960::get_offset_of_memberName_1(),
	XmlReflectionMember_t1313238960::get_offset_of_memberType_2(),
	XmlReflectionMember_t1313238960::get_offset_of_overrideIsNullable_3(),
	XmlReflectionMember_t1313238960::get_offset_of_soapAttributes_4(),
	XmlReflectionMember_t1313238960::get_offset_of_xmlAttributes_5(),
	XmlReflectionMember_t1313238960::get_offset_of_declaringType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (XmlRootAttribute_t2306097217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[4] = 
{
	XmlRootAttribute_t2306097217::get_offset_of_dataType_0(),
	XmlRootAttribute_t2306097217::get_offset_of_elementName_1(),
	XmlRootAttribute_t2306097217::get_offset_of_isNullable_2(),
	XmlRootAttribute_t2306097217::get_offset_of_ns_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (XmlSchemaEnumerator_t2583766800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[1] = 
{
	XmlSchemaEnumerator_t2583766800::get_offset_of_e_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (XmlSchemaImporter_t2852143304), -1, sizeof(XmlSchemaImporter_t2852143304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2061[17] = 
{
	XmlSchemaImporter_t2852143304::get_offset_of_schemas_0(),
	XmlSchemaImporter_t2852143304::get_offset_of_typeIdentifiers_1(),
	XmlSchemaImporter_t2852143304::get_offset_of_elemIdentifiers_2(),
	XmlSchemaImporter_t2852143304::get_offset_of_mappedTypes_3(),
	XmlSchemaImporter_t2852143304::get_offset_of_primitiveDerivedMappedTypes_4(),
	XmlSchemaImporter_t2852143304::get_offset_of_dataMappedTypes_5(),
	XmlSchemaImporter_t2852143304::get_offset_of_pendingMaps_6(),
	XmlSchemaImporter_t2852143304::get_offset_of_sharedAnonymousTypes_7(),
	XmlSchemaImporter_t2852143304::get_offset_of_encodedFormat_8(),
	XmlSchemaImporter_t2852143304::get_offset_of_auxXmlRefImporter_9(),
	XmlSchemaImporter_t2852143304::get_offset_of_auxSoapRefImporter_10(),
	XmlSchemaImporter_t2852143304::get_offset_of_anyTypeImported_11(),
	XmlSchemaImporter_t2852143304::get_offset_of_options_12(),
	XmlSchemaImporter_t2852143304_StaticFields::get_offset_of_anyType_13(),
	XmlSchemaImporter_t2852143304_StaticFields::get_offset_of_arrayType_14(),
	XmlSchemaImporter_t2852143304_StaticFields::get_offset_of_arrayTypeRefName_15(),
	XmlSchemaImporter_t2852143304::get_offset_of_anyElement_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (MapFixup_t630237321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[3] = 
{
	MapFixup_t630237321::get_offset_of_Map_0(),
	MapFixup_t630237321::get_offset_of_SchemaType_1(),
	MapFixup_t630237321::get_offset_of_TypeName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (XmlSchemaProviderAttribute_t3872582200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[1] = 
{
	XmlSchemaProviderAttribute_t3872582200::get_offset_of__methodName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (XmlSchemas_t3283371924), -1, sizeof(XmlSchemas_t3283371924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2064[2] = 
{
	XmlSchemas_t3283371924_StaticFields::get_offset_of_msdataNS_1(),
	XmlSchemas_t3283371924::get_offset_of_table_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (XmlSerializationGeneratedCode_t1625906606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (XmlSerializationReader_t1652069793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[25] = 
{
	XmlSerializationReader_t1652069793::get_offset_of_document_0(),
	XmlSerializationReader_t1652069793::get_offset_of_reader_1(),
	XmlSerializationReader_t1652069793::get_offset_of_fixups_2(),
	XmlSerializationReader_t1652069793::get_offset_of_collFixups_3(),
	XmlSerializationReader_t1652069793::get_offset_of_collItemFixups_4(),
	XmlSerializationReader_t1652069793::get_offset_of_typesCallbacks_5(),
	XmlSerializationReader_t1652069793::get_offset_of_noIDTargets_6(),
	XmlSerializationReader_t1652069793::get_offset_of_targets_7(),
	XmlSerializationReader_t1652069793::get_offset_of_delayedListFixups_8(),
	XmlSerializationReader_t1652069793::get_offset_of_eventSource_9(),
	XmlSerializationReader_t1652069793::get_offset_of_delayedFixupId_10(),
	XmlSerializationReader_t1652069793::get_offset_of_referencedObjects_11(),
	XmlSerializationReader_t1652069793::get_offset_of_readCount_12(),
	XmlSerializationReader_t1652069793::get_offset_of_whileIterationCount_13(),
	XmlSerializationReader_t1652069793::get_offset_of_w3SchemaNS_14(),
	XmlSerializationReader_t1652069793::get_offset_of_w3InstanceNS_15(),
	XmlSerializationReader_t1652069793::get_offset_of_w3InstanceNS2000_16(),
	XmlSerializationReader_t1652069793::get_offset_of_w3InstanceNS1999_17(),
	XmlSerializationReader_t1652069793::get_offset_of_soapNS_18(),
	XmlSerializationReader_t1652069793::get_offset_of_wsdlNS_19(),
	XmlSerializationReader_t1652069793::get_offset_of_nullX_20(),
	XmlSerializationReader_t1652069793::get_offset_of_nil_21(),
	XmlSerializationReader_t1652069793::get_offset_of_typeX_22(),
	XmlSerializationReader_t1652069793::get_offset_of_arrayType_23(),
	XmlSerializationReader_t1652069793::get_offset_of_arrayQName_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (WriteCallbackInfo_t2777933997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[4] = 
{
	WriteCallbackInfo_t2777933997::get_offset_of_Type_0(),
	WriteCallbackInfo_t2777933997::get_offset_of_TypeName_1(),
	WriteCallbackInfo_t2777933997::get_offset_of_TypeNs_2(),
	WriteCallbackInfo_t2777933997::get_offset_of_Callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (CollectionFixup_t574604497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[4] = 
{
	CollectionFixup_t574604497::get_offset_of_callback_0(),
	CollectionFixup_t574604497::get_offset_of_collection_1(),
	CollectionFixup_t574604497::get_offset_of_collectionItems_2(),
	CollectionFixup_t574604497::get_offset_of_id_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (Fixup_t3556126492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[3] = 
{
	Fixup_t3556126492::get_offset_of_source_0(),
	Fixup_t3556126492::get_offset_of_ids_1(),
	Fixup_t3556126492::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (CollectionItemFixup_t3797480032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[3] = 
{
	CollectionItemFixup_t3797480032::get_offset_of_list_0(),
	CollectionItemFixup_t3797480032::get_offset_of_index_1(),
	CollectionItemFixup_t3797480032::get_offset_of_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (XmlSerializationReaderInterpreter_t231046833), -1, sizeof(XmlSerializationReaderInterpreter_t231046833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2071[4] = 
{
	XmlSerializationReaderInterpreter_t231046833::get_offset_of__typeMap_25(),
	XmlSerializationReaderInterpreter_t231046833::get_offset_of__format_26(),
	XmlSerializationReaderInterpreter_t231046833_StaticFields::get_offset_of_AnyType_27(),
	XmlSerializationReaderInterpreter_t231046833_StaticFields::get_offset_of_empty_array_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (FixupCallbackInfo_t1167033645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2072[3] = 
{
	FixupCallbackInfo_t1167033645::get_offset_of__sri_0(),
	FixupCallbackInfo_t1167033645::get_offset_of__map_1(),
	FixupCallbackInfo_t1167033645::get_offset_of__isValueList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (ReaderCallbackInfo_t882063084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[2] = 
{
	ReaderCallbackInfo_t882063084::get_offset_of__sri_0(),
	ReaderCallbackInfo_t882063084::get_offset_of__typeMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (XmlSerializationWriter_t982275218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2074[8] = 
{
	XmlSerializationWriter_t982275218::get_offset_of_idGenerator_0(),
	XmlSerializationWriter_t982275218::get_offset_of_qnameCount_1(),
	XmlSerializationWriter_t982275218::get_offset_of_topLevelElement_2(),
	XmlSerializationWriter_t982275218::get_offset_of_namespaces_3(),
	XmlSerializationWriter_t982275218::get_offset_of_writer_4(),
	XmlSerializationWriter_t982275218::get_offset_of_referencedElements_5(),
	XmlSerializationWriter_t982275218::get_offset_of_callbacks_6(),
	XmlSerializationWriter_t982275218::get_offset_of_serializedObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (WriteCallbackInfo_t2444815868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[4] = 
{
	WriteCallbackInfo_t2444815868::get_offset_of_Type_0(),
	WriteCallbackInfo_t2444815868::get_offset_of_TypeName_1(),
	WriteCallbackInfo_t2444815868::get_offset_of_TypeNs_2(),
	WriteCallbackInfo_t2444815868::get_offset_of_Callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (XmlSerializationWriterInterpreter_t4120928523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[2] = 
{
	XmlSerializationWriterInterpreter_t4120928523::get_offset_of__typeMap_8(),
	XmlSerializationWriterInterpreter_t4120928523::get_offset_of__format_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (CallbackInfo_t2600680195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[2] = 
{
	CallbackInfo_t2600680195::get_offset_of__swi_0(),
	CallbackInfo_t2600680195::get_offset_of__typeMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (XmlSerializer_t1117804635), -1, sizeof(XmlSerializer_t1117804635_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2078[12] = 
{
	XmlSerializer_t1117804635_StaticFields::get_offset_of_generationThreshold_0(),
	XmlSerializer_t1117804635_StaticFields::get_offset_of_backgroundGeneration_1(),
	XmlSerializer_t1117804635_StaticFields::get_offset_of_deleteTempFiles_2(),
	XmlSerializer_t1117804635_StaticFields::get_offset_of_generatorFallback_3(),
	XmlSerializer_t1117804635::get_offset_of_customSerializer_4(),
	XmlSerializer_t1117804635::get_offset_of_typeMapping_5(),
	XmlSerializer_t1117804635::get_offset_of_serializerData_6(),
	XmlSerializer_t1117804635_StaticFields::get_offset_of_serializerTypes_7(),
	XmlSerializer_t1117804635::get_offset_of_onUnknownAttribute_8(),
	XmlSerializer_t1117804635::get_offset_of_onUnknownElement_9(),
	XmlSerializer_t1117804635::get_offset_of_onUnknownNode_10(),
	XmlSerializer_t1117804635::get_offset_of_onUnreferencedObject_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (SerializerData_t3337767682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[7] = 
{
	SerializerData_t3337767682::get_offset_of_UsageCount_0(),
	SerializerData_t3337767682::get_offset_of_ReaderType_1(),
	SerializerData_t3337767682::get_offset_of_ReaderMethod_2(),
	SerializerData_t3337767682::get_offset_of_WriterType_3(),
	SerializerData_t3337767682::get_offset_of_WriterMethod_4(),
	SerializerData_t3337767682::get_offset_of_Batch_5(),
	SerializerData_t3337767682::get_offset_of_Implementation_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (GenerationBatch_t1938464320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2080[3] = 
{
	GenerationBatch_t1938464320::get_offset_of_Done_0(),
	GenerationBatch_t1938464320::get_offset_of_Maps_1(),
	GenerationBatch_t1938464320::get_offset_of_Datas_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (XmlSerializerImplementation_t3900572101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (XmlSerializerNamespaces_t2702737953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[1] = 
{
	XmlSerializerNamespaces_t2702737953::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (XmlTextAttribute_t499390083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2083[2] = 
{
	XmlTextAttribute_t499390083::get_offset_of_dataType_0(),
	XmlTextAttribute_t499390083::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (XmlTypeAttribute_t945254369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[3] = 
{
	XmlTypeAttribute_t945254369::get_offset_of_includeInSchema_0(),
	XmlTypeAttribute_t945254369::get_offset_of_ns_1(),
	XmlTypeAttribute_t945254369::get_offset_of_typeName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (XmlTypeMapElementInfo_t2741990046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[10] = 
{
	XmlTypeMapElementInfo_t2741990046::get_offset_of__elementName_0(),
	XmlTypeMapElementInfo_t2741990046::get_offset_of__namespace_1(),
	XmlTypeMapElementInfo_t2741990046::get_offset_of__form_2(),
	XmlTypeMapElementInfo_t2741990046::get_offset_of__member_3(),
	XmlTypeMapElementInfo_t2741990046::get_offset_of__choiceValue_4(),
	XmlTypeMapElementInfo_t2741990046::get_offset_of__isNullable_5(),
	XmlTypeMapElementInfo_t2741990046::get_offset_of__nestingLevel_6(),
	XmlTypeMapElementInfo_t2741990046::get_offset_of__mappedType_7(),
	XmlTypeMapElementInfo_t2741990046::get_offset_of__type_8(),
	XmlTypeMapElementInfo_t2741990046::get_offset_of__wrappedElement_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (XmlTypeMapElementInfoList_t2115305165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (XmlTypeMapMember_t3739392753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[9] = 
{
	XmlTypeMapMember_t3739392753::get_offset_of__name_0(),
	XmlTypeMapMember_t3739392753::get_offset_of__index_1(),
	XmlTypeMapMember_t3739392753::get_offset_of__globalIndex_2(),
	XmlTypeMapMember_t3739392753::get_offset_of__typeData_3(),
	XmlTypeMapMember_t3739392753::get_offset_of__member_4(),
	XmlTypeMapMember_t3739392753::get_offset_of__specifiedMember_5(),
	XmlTypeMapMember_t3739392753::get_offset_of__defaultValue_6(),
	XmlTypeMapMember_t3739392753::get_offset_of_documentation_7(),
	XmlTypeMapMember_t3739392753::get_offset_of__flags_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (XmlTypeMapMemberAttribute_t2452898273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[4] = 
{
	XmlTypeMapMemberAttribute_t2452898273::get_offset_of__attributeName_9(),
	XmlTypeMapMemberAttribute_t2452898273::get_offset_of__namespace_10(),
	XmlTypeMapMemberAttribute_t2452898273::get_offset_of__form_11(),
	XmlTypeMapMemberAttribute_t2452898273::get_offset_of__mappedType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (XmlTypeMapMemberElement_t1199680526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[4] = 
{
	XmlTypeMapMemberElement_t1199680526::get_offset_of__elementInfo_9(),
	XmlTypeMapMemberElement_t1199680526::get_offset_of__choiceMember_10(),
	XmlTypeMapMemberElement_t1199680526::get_offset_of__isTextCollector_11(),
	XmlTypeMapMemberElement_t1199680526::get_offset_of__choiceTypeData_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (XmlTypeMapMemberList_t176526733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (XmlTypeMapMemberExpandable_t634974464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[1] = 
{
	XmlTypeMapMemberExpandable_t634974464::get_offset_of__flatArrayIndex_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (XmlTypeMapMemberFlatList_t2190862388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[1] = 
{
	XmlTypeMapMemberFlatList_t2190862388::get_offset_of__listMap_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (XmlTypeMapMemberAnyElement_t1417227289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (XmlTypeMapMemberAnyAttribute_t3039835622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (XmlTypeMapMemberNamespaces_t1954902185), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (XmlTypeMapping_t3915382669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[10] = 
{
	XmlTypeMapping_t3915382669::get_offset_of_xmlType_7(),
	XmlTypeMapping_t3915382669::get_offset_of_xmlTypeNamespace_8(),
	XmlTypeMapping_t3915382669::get_offset_of_type_9(),
	XmlTypeMapping_t3915382669::get_offset_of_baseMap_10(),
	XmlTypeMapping_t3915382669::get_offset_of_multiReferenceType_11(),
	XmlTypeMapping_t3915382669::get_offset_of_isSimpleType_12(),
	XmlTypeMapping_t3915382669::get_offset_of_documentation_13(),
	XmlTypeMapping_t3915382669::get_offset_of_includeInSchema_14(),
	XmlTypeMapping_t3915382669::get_offset_of_isNullable_15(),
	XmlTypeMapping_t3915382669::get_offset_of__derivedTypes_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (XmlSerializableMapping_t1689765018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2097[3] = 
{
	XmlSerializableMapping_t1689765018::get_offset_of__schema_17(),
	XmlSerializableMapping_t1689765018::get_offset_of__schemaType_18(),
	XmlSerializableMapping_t1689765018::get_offset_of__schemaTypeName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (ClassMap_t2947503525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[16] = 
{
	ClassMap_t2947503525::get_offset_of__elements_0(),
	ClassMap_t2947503525::get_offset_of__elementMembers_1(),
	ClassMap_t2947503525::get_offset_of__attributeMembers_2(),
	ClassMap_t2947503525::get_offset_of__attributeMembersArray_3(),
	ClassMap_t2947503525::get_offset_of__elementsByIndex_4(),
	ClassMap_t2947503525::get_offset_of__flatLists_5(),
	ClassMap_t2947503525::get_offset_of__allMembers_6(),
	ClassMap_t2947503525::get_offset_of__membersWithDefault_7(),
	ClassMap_t2947503525::get_offset_of__listMembers_8(),
	ClassMap_t2947503525::get_offset_of__defaultAnyElement_9(),
	ClassMap_t2947503525::get_offset_of__defaultAnyAttribute_10(),
	ClassMap_t2947503525::get_offset_of__namespaceDeclarations_11(),
	ClassMap_t2947503525::get_offset_of__xmlTextCollector_12(),
	ClassMap_t2947503525::get_offset_of__returnMember_13(),
	ClassMap_t2947503525::get_offset_of__ignoreMemberNamespace_14(),
	ClassMap_t2947503525::get_offset_of__canBeSimpleType_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (ListMap_t4107479011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[4] = 
{
	ListMap_t4107479011::get_offset_of__itemInfo_0(),
	ListMap_t4107479011::get_offset_of__gotNestedMapping_1(),
	ListMap_t4107479011::get_offset_of__nestedArrayMapping_2(),
	ListMap_t4107479011::get_offset_of__choiceMember_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
