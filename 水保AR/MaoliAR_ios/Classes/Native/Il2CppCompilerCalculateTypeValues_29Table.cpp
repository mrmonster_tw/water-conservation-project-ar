﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Drawing_System_Drawing_Rectangle2269512193.h"
#include "System_Drawing_System_Drawing_SizeConverter3360787950.h"
#include "System_Drawing_System_Drawing_Size1711122972.h"
#include "System_Drawing_System_Drawing_SystemColors2352352385.h"
#include "System_Drawing_System_Drawing_Design_UITypeEditor2900915228.h"
#include "System_Drawing_System_Drawing_Drawing2D_FlushIntent390730309.h"
#include "System_Drawing_System_MonoTODOAttribute4131080581.h"
#include "System_Drawing_System_Drawing_Imaging_BitmapData288533671.h"
#include "System_Drawing_System_Drawing_Imaging_ColorPalette3652233547.h"
#include "System_Drawing_System_Drawing_Imaging_Encoder290915261.h"
#include "System_Drawing_System_Drawing_Imaging_EncoderParame138078747.h"
#include "System_Drawing_System_Drawing_Imaging_EncoderParam2680547867.h"
#include "System_Drawing_System_Drawing_Imaging_EncoderParam1032746077.h"
#include "System_Drawing_System_Drawing_Imaging_ImageCodecFl4044615128.h"
#include "System_Drawing_System_Drawing_Imaging_ImageCodecIn2037253290.h"
#include "System_Drawing_System_Drawing_Imaging_ImageFormat730671025.h"
#include "System_Drawing_System_Drawing_Imaging_ImageLockMod2145240228.h"
#include "System_Drawing_System_Drawing_Imaging_Metafile1029311082.h"
#include "System_Drawing_System_Drawing_Imaging_PixelFormat200935677.h"
#include "System_Drawing_System_Drawing_Text_FontCollection3550137690.h"
#include "System_Drawing_System_Drawing_Text_GenericFontFami3046638929.h"
#include "System_Drawing_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "System_Drawing_U3CPrivateImplementationDetailsU3E_3100175573.h"
#include "Mono_Web_U3CModuleU3E692745525.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingPlatform242231579.h"
#include "Mono_Web_Mono_Web_Util_SettingsMapping162400643.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingManager1609949884.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingWhatOperatio2264203516.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingWhatContents3035976441.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingWhat1462987973.h"
#include "System_Web_Services_U3CModuleU3E692745525.h"
#include "System_Web_Services_System_MonoTODOAttribute4131080581.h"
#include "System_Web_Services_System_Web_Services_WsiProfile1986678150.h"
#include "System_Web_Services_System_Web_Services_Configurat2590493626.h"
#include "System_Web_Services_System_Web_Services_Configurat3251766543.h"
#include "System_Web_Services_System_Web_Services_Configurat2134727492.h"
#include "System_Web_Services_System_Web_Services_Configurat1149416555.h"
#include "System_Web_Services_System_Web_Services_Configurati432830953.h"
#include "System_Web_Services_System_Web_Services_Configurati432867097.h"
#include "System_Web_Services_System_Web_Services_Configurat1164865643.h"
#include "System_Web_Services_System_Web_Services_Configurat3807070020.h"
#include "System_Web_Services_System_Web_Services_Configurat1340746787.h"
#include "System_Web_Services_System_Web_Services_Configurat1708535299.h"
#include "System_Web_Services_System_Web_Services_Configurat1519021719.h"
#include "System_Web_Services_System_Web_Services_Configurat2191173613.h"
#include "System_Web_Services_System_Web_Services_Configurati984573524.h"
#include "System_Web_Services_System_Web_Services_Configurat2632267290.h"
#include "System_Web_Services_System_Web_Services_Configurat2483648634.h"
#include "System_Web_Services_System_Web_Services_Configurat3725372622.h"
#include "System_Web_Services_System_Web_Services_Configurat3471468443.h"
#include "System_Web_Services_System_Web_Services_Configurat2070508364.h"
#include "System_Web_Services_System_Web_Services_Descriptio3952932040.h"
#include "System_Web_Services_System_Web_Services_Description274233127.h"
#include "System_Web_Services_System_Web_Services_Description546804031.h"
#include "System_Web_Services_System_Web_Services_Descriptio3596493475.h"
#include "System_Web_Services_System_Web_Services_Descriptio2785831862.h"
#include "System_Web_Services_System_Web_Services_Descriptio1261957259.h"
#include "System_Web_Services_System_Web_Services_Descriptio1636923877.h"
#include "System_Web_Services_System_Web_Services_Descriptio3081451370.h"
#include "System_Web_Services_System_Web_Services_Description690649395.h"
#include "System_Web_Services_System_Web_Services_Descriptio4104571634.h"
#include "System_Web_Services_System_Web_Services_Descriptio3689644165.h"
#include "System_Web_Services_System_Web_Services_Descriptio3614700311.h"
#include "System_Web_Services_System_Web_Services_Descriptio2972373571.h"
#include "System_Web_Services_System_Web_Services_Description796004249.h"
#include "System_Web_Services_System_Web_Services_Descriptio2249231492.h"
#include "System_Web_Services_System_Web_Services_Descriptio3136796011.h"
#include "System_Web_Services_System_Web_Services_Descriptio1729997838.h"
#include "System_Web_Services_System_Web_Services_Descriptio3764124827.h"
#include "System_Web_Services_System_Web_Services_Descriptio1124893136.h"
#include "System_Web_Services_System_Web_Services_Description466850992.h"
#include "System_Web_Services_System_Web_Services_Descriptio3925470281.h"
#include "System_Web_Services_System_Web_Services_Descriptio1634101190.h"
#include "System_Web_Services_System_Web_Services_Descriptio1220355597.h"
#include "System_Web_Services_System_Web_Services_Descriptio1199434016.h"
#include "System_Web_Services_System_Web_Services_Descriptio2537385574.h"
#include "System_Web_Services_System_Web_Services_Descriptio2808782420.h"
#include "System_Web_Services_System_Web_Services_Descriptio2466402210.h"
#include "System_Web_Services_System_Web_Services_Descriptio1010384202.h"
#include "System_Web_Services_System_Web_Services_Description314325322.h"
#include "System_Web_Services_System_Web_Services_Descriptio1644124013.h"
#include "System_Web_Services_System_Web_Services_Descriptio3429260361.h"
#include "System_Web_Services_System_Web_Services_Descriptio1601603675.h"
#include "System_Web_Services_System_Web_Services_Descriptio3586254674.h"
#include "System_Web_Services_System_Web_Services_Descriptio2042839389.h"
#include "System_Web_Services_System_Web_Services_Descriptio1012652750.h"
#include "System_Web_Services_System_Web_Services_Descriptio3992193545.h"
#include "System_Web_Services_System_Web_Services_Descriptio1439893716.h"
#include "System_Web_Services_System_Web_Services_Descriptio1318715061.h"
#include "System_Web_Services_System_Web_Services_Descriptio4151257856.h"
#include "System_Web_Services_System_Web_Services_Descriptio2517239564.h"
#include "System_Web_Services_System_Web_Services_Descriptio3119525474.h"
#include "System_Web_Services_System_Web_Services_Descriptio2738291908.h"
#include "System_Web_Services_System_Web_Services_Descriptio3850760970.h"
#include "System_Web_Services_System_Web_Services_Descriptio2672957899.h"
#include "System_Web_Services_System_Web_Services_Descriptio3495097492.h"
#include "System_Web_Services_System_Web_Services_Descriptio3693704363.h"
#include "System_Web_Services_System_Web_Services_Descriptio4141433730.h"
#include "System_Web_Services_System_Web_Services_Descriptio3443230295.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (Rectangle_t2269512193)+ sizeof (Il2CppObject), sizeof(Rectangle_t2269512193 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2900[4] = 
{
	Rectangle_t2269512193::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rectangle_t2269512193::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rectangle_t2269512193::get_offset_of_width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rectangle_t2269512193::get_offset_of_height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (SizeConverter_t3360787950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (Size_t1711122972)+ sizeof (Il2CppObject), sizeof(Size_t1711122972 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2902[2] = 
{
	Size_t1711122972::get_offset_of_width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Size_t1711122972::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (SystemColors_t2352352385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (UITypeEditor_t2900915228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (FlushIntention_t390730309)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2905[3] = 
{
	FlushIntention_t390730309::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (MonoTODOAttribute_t4131080588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[1] = 
{
	MonoTODOAttribute_t4131080588::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (BitmapData_t288533671), sizeof(BitmapData_t288533671_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2907[17] = 
{
	BitmapData_t288533671::get_offset_of_width_0(),
	BitmapData_t288533671::get_offset_of_height_1(),
	BitmapData_t288533671::get_offset_of_stride_2(),
	BitmapData_t288533671::get_offset_of_pixel_format_3(),
	BitmapData_t288533671::get_offset_of_scan0_4(),
	BitmapData_t288533671::get_offset_of_reserved_5(),
	BitmapData_t288533671::get_offset_of_palette_6(),
	BitmapData_t288533671::get_offset_of_property_count_7(),
	BitmapData_t288533671::get_offset_of_property_8(),
	BitmapData_t288533671::get_offset_of_dpi_horz_9(),
	BitmapData_t288533671::get_offset_of_dpi_vert_10(),
	BitmapData_t288533671::get_offset_of_image_flags_11(),
	BitmapData_t288533671::get_offset_of_left_12(),
	BitmapData_t288533671::get_offset_of_top_13(),
	BitmapData_t288533671::get_offset_of_x_14(),
	BitmapData_t288533671::get_offset_of_y_15(),
	BitmapData_t288533671::get_offset_of_transparent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (ColorPalette_t3652233547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[2] = 
{
	ColorPalette_t3652233547::get_offset_of_flags_0(),
	ColorPalette_t3652233547::get_offset_of_entries_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (Encoder_t290915261), -1, sizeof(Encoder_t290915261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2909[11] = 
{
	Encoder_t290915261::get_offset_of_guid_0(),
	Encoder_t290915261_StaticFields::get_offset_of_ChrominanceTable_1(),
	Encoder_t290915261_StaticFields::get_offset_of_ColorDepth_2(),
	Encoder_t290915261_StaticFields::get_offset_of_Compression_3(),
	Encoder_t290915261_StaticFields::get_offset_of_LuminanceTable_4(),
	Encoder_t290915261_StaticFields::get_offset_of_Quality_5(),
	Encoder_t290915261_StaticFields::get_offset_of_RenderMethod_6(),
	Encoder_t290915261_StaticFields::get_offset_of_SaveFlag_7(),
	Encoder_t290915261_StaticFields::get_offset_of_ScanMethod_8(),
	Encoder_t290915261_StaticFields::get_offset_of_Transformation_9(),
	Encoder_t290915261_StaticFields::get_offset_of_Version_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (EncoderParameter_t138078747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[4] = 
{
	EncoderParameter_t138078747::get_offset_of_encoder_0(),
	EncoderParameter_t138078747::get_offset_of_valuesCount_1(),
	EncoderParameter_t138078747::get_offset_of_type_2(),
	EncoderParameter_t138078747::get_offset_of_valuePtr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (EncoderParameters_t2680547867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[1] = 
{
	EncoderParameters_t2680547867::get_offset_of_parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (EncoderParameterValueType_t1032746077)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2912[9] = 
{
	EncoderParameterValueType_t1032746077::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (ImageCodecFlags_t4044615128)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2913[10] = 
{
	ImageCodecFlags_t4044615128::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (ImageCodecInfo_t2037253290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[11] = 
{
	ImageCodecInfo_t2037253290::get_offset_of_clsid_0(),
	ImageCodecInfo_t2037253290::get_offset_of_codecName_1(),
	ImageCodecInfo_t2037253290::get_offset_of_dllName_2(),
	ImageCodecInfo_t2037253290::get_offset_of_filenameExtension_3(),
	ImageCodecInfo_t2037253290::get_offset_of_flags_4(),
	ImageCodecInfo_t2037253290::get_offset_of_formatDescription_5(),
	ImageCodecInfo_t2037253290::get_offset_of_formatID_6(),
	ImageCodecInfo_t2037253290::get_offset_of_mimeType_7(),
	ImageCodecInfo_t2037253290::get_offset_of_signatureMasks_8(),
	ImageCodecInfo_t2037253290::get_offset_of_signaturePatterns_9(),
	ImageCodecInfo_t2037253290::get_offset_of_version_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (ImageFormat_t730671025), -1, sizeof(ImageFormat_t730671025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2915[13] = 
{
	ImageFormat_t730671025::get_offset_of_guid_0(),
	ImageFormat_t730671025::get_offset_of_name_1(),
	ImageFormat_t730671025_StaticFields::get_offset_of_locker_2(),
	ImageFormat_t730671025_StaticFields::get_offset_of_BmpImageFormat_3(),
	ImageFormat_t730671025_StaticFields::get_offset_of_EmfImageFormat_4(),
	ImageFormat_t730671025_StaticFields::get_offset_of_ExifImageFormat_5(),
	ImageFormat_t730671025_StaticFields::get_offset_of_GifImageFormat_6(),
	ImageFormat_t730671025_StaticFields::get_offset_of_TiffImageFormat_7(),
	ImageFormat_t730671025_StaticFields::get_offset_of_PngImageFormat_8(),
	ImageFormat_t730671025_StaticFields::get_offset_of_MemoryBmpImageFormat_9(),
	ImageFormat_t730671025_StaticFields::get_offset_of_IconImageFormat_10(),
	ImageFormat_t730671025_StaticFields::get_offset_of_JpegImageFormat_11(),
	ImageFormat_t730671025_StaticFields::get_offset_of_WmfImageFormat_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (ImageLockMode_t2145240228)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2916[5] = 
{
	ImageLockMode_t2145240228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (Metafile_t1029311082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (PixelFormat_t200935677)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2918[24] = 
{
	PixelFormat_t200935677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (FontCollection_t3550137690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[1] = 
{
	FontCollection_t3550137690::get_offset_of_nativeFontCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (GenericFontFamilies_t3046638929)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2920[4] = 
{
	GenericFontFamilies_t3046638929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2921[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (U24ArrayTypeU24700_t3100175573)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24700_t3100175573 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (U3CModuleU3E_t692745539), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (SettingsMappingPlatform_t242231579)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2925[3] = 
{
	SettingsMappingPlatform_t242231579::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (SettingsMapping_t162400643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[6] = 
{
	SettingsMapping_t162400643::get_offset_of__sectionTypeName_0(),
	SettingsMapping_t162400643::get_offset_of__sectionType_1(),
	SettingsMapping_t162400643::get_offset_of__mapperTypeName_2(),
	SettingsMapping_t162400643::get_offset_of__mapperType_3(),
	SettingsMapping_t162400643::get_offset_of__platform_4(),
	SettingsMapping_t162400643::get_offset_of__whats_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (SettingsMappingManager_t1609949884), -1, sizeof(SettingsMappingManager_t1609949884_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2927[7] = 
{
	SettingsMappingManager_t1609949884_StaticFields::get_offset_of_mapperLock_0(),
	SettingsMappingManager_t1609949884_StaticFields::get_offset_of__instance_1(),
	SettingsMappingManager_t1609949884_StaticFields::get_offset_of__mappingFile_2(),
	SettingsMappingManager_t1609949884::get_offset_of__mappers_3(),
	SettingsMappingManager_t1609949884_StaticFields::get_offset_of__mappedSections_4(),
	SettingsMappingManager_t1609949884_StaticFields::get_offset_of__myPlatform_5(),
	SettingsMappingManager_t1609949884_StaticFields::get_offset_of__runningOnWindows_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (SettingsMappingWhatOperation_t2264203516)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2928[5] = 
{
	SettingsMappingWhatOperation_t2264203516::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (SettingsMappingWhatContents_t3035976441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[2] = 
{
	SettingsMappingWhatContents_t3035976441::get_offset_of__operation_0(),
	SettingsMappingWhatContents_t3035976441::get_offset_of__attributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (SettingsMappingWhat_t1462987973), -1, sizeof(SettingsMappingWhat_t1462987973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2930[3] = 
{
	SettingsMappingWhat_t1462987973::get_offset_of__value_0(),
	SettingsMappingWhat_t1462987973::get_offset_of__contents_1(),
	SettingsMappingWhat_t1462987973_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (U3CModuleU3E_t692745540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (MonoTODOAttribute_t4131080589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[1] = 
{
	MonoTODOAttribute_t4131080589::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (WsiProfiles_t1986678150)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2933[3] = 
{
	WsiProfiles_t1986678150::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (DiagnosticsElement_t2590493626), -1, sizeof(DiagnosticsElement_t2590493626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2934[2] = 
{
	DiagnosticsElement_t2590493626_StaticFields::get_offset_of_suppressReturningExceptionsProp_13(),
	DiagnosticsElement_t2590493626_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (PriorityGroup_t3251766543)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2935[3] = 
{
	PriorityGroup_t3251766543::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (ProtocolElement_t2134727492), -1, sizeof(ProtocolElement_t2134727492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2936[2] = 
{
	ProtocolElement_t2134727492_StaticFields::get_offset_of_nameProp_13(),
	ProtocolElement_t2134727492_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (ProtocolElementCollection_t1149416555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (SoapEnvelopeProcessingElement_t432830953), -1, sizeof(SoapEnvelopeProcessingElement_t432830953_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2938[3] = 
{
	SoapEnvelopeProcessingElement_t432830953_StaticFields::get_offset_of_strictProp_13(),
	SoapEnvelopeProcessingElement_t432830953_StaticFields::get_offset_of_readTimeoutProp_14(),
	SoapEnvelopeProcessingElement_t432830953_StaticFields::get_offset_of_properties_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (SoapExtensionTypeElement_t432867097), -1, sizeof(SoapExtensionTypeElement_t432867097_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2939[4] = 
{
	SoapExtensionTypeElement_t432867097_StaticFields::get_offset_of_groupProp_13(),
	SoapExtensionTypeElement_t432867097_StaticFields::get_offset_of_priorityProp_14(),
	SoapExtensionTypeElement_t432867097_StaticFields::get_offset_of_typeProp_15(),
	SoapExtensionTypeElement_t432867097_StaticFields::get_offset_of_properties_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (SoapExtensionTypeElementCollection_t1164865643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (TypeElement_t3807070020), -1, sizeof(TypeElement_t3807070020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2941[2] = 
{
	TypeElement_t3807070020_StaticFields::get_offset_of_typeProp_13(),
	TypeElement_t3807070020_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (TypeElementCollection_t1340746787), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (TypeTypeConverter_t1708535299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (WebServiceProtocols_t1519021719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2944[9] = 
{
	WebServiceProtocols_t1519021719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (WebServicesSection_t2191173613), -1, sizeof(WebServicesSection_t2191173613_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2945[12] = 
{
	WebServicesSection_t2191173613_StaticFields::get_offset_of_conformanceWarningsProp_17(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_diagnosticsProp_18(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_protocolsProp_19(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_serviceDescriptionFormatExtensionTypesProp_20(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_soapEnvelopeProcessingProp_21(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_soapExtensionImporterTypesProp_22(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_soapExtensionReflectorTypesProp_23(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_soapExtensionTypesProp_24(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_soapServerProtocolFactoryProp_25(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_soapTransportImporterTypesProp_26(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_wsdlHelpGeneratorProp_27(),
	WebServicesSection_t2191173613_StaticFields::get_offset_of_properties_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (WsdlHelpGeneratorElement_t984573524), -1, sizeof(WsdlHelpGeneratorElement_t984573524_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2946[2] = 
{
	WsdlHelpGeneratorElement_t984573524_StaticFields::get_offset_of_hrefProp_13(),
	WsdlHelpGeneratorElement_t984573524_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (WsiProfilesElement_t2632267290), -1, sizeof(WsiProfilesElement_t2632267290_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2947[2] = 
{
	WsiProfilesElement_t2632267290_StaticFields::get_offset_of_nameProp_13(),
	WsiProfilesElement_t2632267290_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (WsiProfilesElementCollection_t2483648634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (XmlFormatExtensionAttribute_t3725372622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2949[3] = 
{
	XmlFormatExtensionAttribute_t3725372622::get_offset_of_elementName_0(),
	XmlFormatExtensionAttribute_t3725372622::get_offset_of_ns_1(),
	XmlFormatExtensionAttribute_t3725372622::get_offset_of_extensionPoints_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (XmlFormatExtensionPointAttribute_t3471468443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[2] = 
{
	XmlFormatExtensionPointAttribute_t3471468443::get_offset_of_allowElements_0(),
	XmlFormatExtensionPointAttribute_t3471468443::get_offset_of_memberName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (XmlFormatExtensionPrefixAttribute_t2070508364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2951[2] = 
{
	XmlFormatExtensionPrefixAttribute_t2070508364::get_offset_of_prefix_0(),
	XmlFormatExtensionPrefixAttribute_t2070508364::get_offset_of_ns_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (Binding_t3952932040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[4] = 
{
	Binding_t3952932040::get_offset_of_extensions_4(),
	Binding_t3952932040::get_offset_of_operations_5(),
	Binding_t3952932040::get_offset_of_serviceDescription_6(),
	Binding_t3952932040::get_offset_of_type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (BindingCollection_t274233127), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (DocumentableItem_t546804031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[3] = 
{
	DocumentableItem_t546804031::get_offset_of_docElement_0(),
	DocumentableItem_t546804031::get_offset_of_extAttributes_1(),
	DocumentableItem_t546804031::get_offset_of_namespaces_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (FaultBinding_t3596493475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[1] = 
{
	FaultBinding_t3596493475::get_offset_of_extensions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (FaultBindingCollection_t2785831862), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (HttpAddressBinding_t1261957259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[1] = 
{
	HttpAddressBinding_t1261957259::get_offset_of_location_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (HttpBinding_t1636923877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[1] = 
{
	HttpBinding_t1636923877::get_offset_of_verb_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (HttpGetProtocolImporter_t3081451370), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (HttpOperationBinding_t690649395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[1] = 
{
	HttpOperationBinding_t690649395::get_offset_of_location_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (HttpPostProtocolImporter_t4104571634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (HttpSimpleProtocolImporter_t3689644165), -1, sizeof(HttpSimpleProtocolImporter_t3689644165_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2962[7] = 
{
	HttpSimpleProtocolImporter_t3689644165::get_offset_of_httpBinding_19(),
	HttpSimpleProtocolImporter_t3689644165::get_offset_of_soapImporter_20(),
	HttpSimpleProtocolImporter_t3689644165::get_offset_of_xmlExporter_21(),
	HttpSimpleProtocolImporter_t3689644165::get_offset_of_xmlImporter_22(),
	HttpSimpleProtocolImporter_t3689644165::get_offset_of_memberIds_23(),
	HttpSimpleProtocolImporter_t3689644165::get_offset_of_xmlReflectionImporter_24(),
	HttpSimpleProtocolImporter_t3689644165_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (HttpUrlEncodedBinding_t3614700311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (HttpUrlReplacementBinding_t2972373571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (Import_t796004249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[4] = 
{
	Import_t796004249::get_offset_of_location_3(),
	Import_t796004249::get_offset_of_ns_4(),
	Import_t796004249::get_offset_of_serviceDescription_5(),
	Import_t796004249::get_offset_of_extensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (ImportCollection_t2249231492), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (InputBinding_t3136796011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[1] = 
{
	InputBinding_t3136796011::get_offset_of_extensions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (Message_t1729997838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[3] = 
{
	Message_t1729997838::get_offset_of_parts_4(),
	Message_t1729997838::get_offset_of_serviceDescription_5(),
	Message_t1729997838::get_offset_of_extensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (MessageBinding_t3764124827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[1] = 
{
	MessageBinding_t3764124827::get_offset_of_operationBinding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (MessageCollection_t1124893136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (MessagePart_t466850992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[4] = 
{
	MessagePart_t466850992::get_offset_of_element_4(),
	MessagePart_t466850992::get_offset_of_message_5(),
	MessagePart_t466850992::get_offset_of_type_6(),
	MessagePart_t466850992::get_offset_of_extensions_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (MessagePartCollection_t3925470281), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (MimeContentBinding_t1634101190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[1] = 
{
	MimeContentBinding_t1634101190::get_offset_of_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (MimeMultipartRelatedBinding_t1220355597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (MimePart_t1199434016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (MimeTextBinding_t2537385574), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (MimeXmlBinding_t2808782420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (NamedItem_t2466402210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[1] = 
{
	NamedItem_t2466402210::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (Operation_t1010384202), -1, sizeof(Operation_t1010384202_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2979[6] = 
{
	Operation_t1010384202::get_offset_of_faults_4(),
	Operation_t1010384202::get_offset_of_messages_5(),
	Operation_t1010384202::get_offset_of_parameterOrder_6(),
	Operation_t1010384202::get_offset_of_portType_7(),
	Operation_t1010384202::get_offset_of_extensions_8(),
	Operation_t1010384202_StaticFields::get_offset_of_wsChars_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (OperationBinding_t314325322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[5] = 
{
	OperationBinding_t314325322::get_offset_of_binding_4(),
	OperationBinding_t314325322::get_offset_of_extensions_5(),
	OperationBinding_t314325322::get_offset_of_faults_6(),
	OperationBinding_t314325322::get_offset_of_input_7(),
	OperationBinding_t314325322::get_offset_of_output_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (OperationBindingCollection_t1644124013), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (OperationCollection_t3429260361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (OperationFault_t1601603675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[1] = 
{
	OperationFault_t1601603675::get_offset_of_extensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (OperationFaultCollection_t3586254674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (OperationInput_t2042839389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[1] = 
{
	OperationInput_t2042839389::get_offset_of_extensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (OperationMessage_t1012652750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[2] = 
{
	OperationMessage_t1012652750::get_offset_of_message_4(),
	OperationMessage_t1012652750::get_offset_of_operation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (OperationMessageCollection_t3992193545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (OperationOutput_t1439893716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[1] = 
{
	OperationOutput_t1439893716::get_offset_of_extensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (OutputBinding_t1318715061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2989[1] = 
{
	OutputBinding_t1318715061::get_offset_of_extensions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (Port_t4151257856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2990[3] = 
{
	Port_t4151257856::get_offset_of_binding_4(),
	Port_t4151257856::get_offset_of_extensions_5(),
	Port_t4151257856::get_offset_of_service_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (PortCollection_t2517239564), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (PortType_t3119525474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2992[3] = 
{
	PortType_t3119525474::get_offset_of_operations_4(),
	PortType_t3119525474::get_offset_of_serviceDescription_5(),
	PortType_t3119525474::get_offset_of_extensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (PortTypeCollection_t2738291908), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (ProtocolImporter_t3850760970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2994[19] = 
{
	ProtocolImporter_t3850760970::get_offset_of_binding_0(),
	ProtocolImporter_t3850760970::get_offset_of_className_1(),
	ProtocolImporter_t3850760970::get_offset_of_classNames_2(),
	ProtocolImporter_t3850760970::get_offset_of_codeNamespace_3(),
	ProtocolImporter_t3850760970::get_offset_of_codeTypeDeclaration_4(),
	ProtocolImporter_t3850760970::get_offset_of_inputMessage_5(),
	ProtocolImporter_t3850760970::get_offset_of_methodName_6(),
	ProtocolImporter_t3850760970::get_offset_of_operation_7(),
	ProtocolImporter_t3850760970::get_offset_of_operationBinding_8(),
	ProtocolImporter_t3850760970::get_offset_of_outputMessage_9(),
	ProtocolImporter_t3850760970::get_offset_of_port_10(),
	ProtocolImporter_t3850760970::get_offset_of_portType_11(),
	ProtocolImporter_t3850760970::get_offset_of_service_12(),
	ProtocolImporter_t3850760970::get_offset_of_warnings_13(),
	ProtocolImporter_t3850760970::get_offset_of_descriptionImporter_14(),
	ProtocolImporter_t3850760970::get_offset_of_iinfo_15(),
	ProtocolImporter_t3850760970::get_offset_of_xmlSchemas_16(),
	ProtocolImporter_t3850760970::get_offset_of_soapSchemas_17(),
	ProtocolImporter_t3850760970::get_offset_of_asyncTypes_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (Service_t2672957899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2995[3] = 
{
	Service_t2672957899::get_offset_of_extensions_4(),
	Service_t2672957899::get_offset_of_ports_5(),
	Service_t2672957899::get_offset_of_serviceDescription_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (ServiceCollection_t3495097492), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (ServiceDescription_t3693704363), -1, sizeof(ServiceDescription_t3693704363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2997[9] = 
{
	ServiceDescription_t3693704363::get_offset_of_bindings_4(),
	ServiceDescription_t3693704363::get_offset_of_extensions_5(),
	ServiceDescription_t3693704363::get_offset_of_imports_6(),
	ServiceDescription_t3693704363::get_offset_of_messages_7(),
	ServiceDescription_t3693704363::get_offset_of_portTypes_8(),
	ServiceDescription_t3693704363::get_offset_of_services_9(),
	ServiceDescription_t3693704363::get_offset_of_targetNamespace_10(),
	ServiceDescription_t3693704363::get_offset_of_types_11(),
	ServiceDescription_t3693704363_StaticFields::get_offset_of_serializer_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (ServiceDescriptionSerializer_t4141433730), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (ServiceDescriptionBaseCollection_t3443230295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2999[2] = 
{
	ServiceDescriptionBaseCollection_t3443230295::get_offset_of_table_1(),
	ServiceDescriptionBaseCollection_t3443230295::get_offset_of_parent_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
