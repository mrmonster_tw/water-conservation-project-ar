﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IO.BinaryReader
struct BinaryReader_t2428077293;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryDictionaryReader/StreamSource
struct  StreamSource_t397184844  : public Il2CppObject
{
public:
	// System.IO.BinaryReader System.Xml.XmlBinaryDictionaryReader/StreamSource::reader
	BinaryReader_t2428077293 * ___reader_0;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(StreamSource_t397184844, ___reader_0)); }
	inline BinaryReader_t2428077293 * get_reader_0() const { return ___reader_0; }
	inline BinaryReader_t2428077293 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(BinaryReader_t2428077293 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier(&___reader_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
