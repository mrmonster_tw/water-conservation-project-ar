﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.HttpHandlersSection
struct  HttpHandlersSection_t1168259750  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct HttpHandlersSection_t1168259750_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.HttpHandlersSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.HttpHandlersSection::handlersProp
	ConfigurationProperty_t3590861854 * ___handlersProp_18;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(HttpHandlersSection_t1168259750_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier(&___properties_17, value);
	}

	inline static int32_t get_offset_of_handlersProp_18() { return static_cast<int32_t>(offsetof(HttpHandlersSection_t1168259750_StaticFields, ___handlersProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_handlersProp_18() const { return ___handlersProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_handlersProp_18() { return &___handlersProp_18; }
	inline void set_handlersProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___handlersProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___handlersProp_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
