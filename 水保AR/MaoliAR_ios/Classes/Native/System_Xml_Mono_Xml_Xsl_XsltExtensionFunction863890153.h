﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_XPFuncImpl459957616.h"

// System.Object
struct Il2CppObject;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.TypeCode[]
struct TypeCodeU5BU5D_t3797419438;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.XsltExtensionFunction
struct  XsltExtensionFunction_t863890153  : public XPFuncImpl_t459957616
{
public:
	// System.Object Mono.Xml.Xsl.XsltExtensionFunction::extension
	Il2CppObject * ___extension_4;
	// System.Reflection.MethodInfo Mono.Xml.Xsl.XsltExtensionFunction::method
	MethodInfo_t * ___method_5;
	// System.TypeCode[] Mono.Xml.Xsl.XsltExtensionFunction::typeCodes
	TypeCodeU5BU5D_t3797419438* ___typeCodes_6;

public:
	inline static int32_t get_offset_of_extension_4() { return static_cast<int32_t>(offsetof(XsltExtensionFunction_t863890153, ___extension_4)); }
	inline Il2CppObject * get_extension_4() const { return ___extension_4; }
	inline Il2CppObject ** get_address_of_extension_4() { return &___extension_4; }
	inline void set_extension_4(Il2CppObject * value)
	{
		___extension_4 = value;
		Il2CppCodeGenWriteBarrier(&___extension_4, value);
	}

	inline static int32_t get_offset_of_method_5() { return static_cast<int32_t>(offsetof(XsltExtensionFunction_t863890153, ___method_5)); }
	inline MethodInfo_t * get_method_5() const { return ___method_5; }
	inline MethodInfo_t ** get_address_of_method_5() { return &___method_5; }
	inline void set_method_5(MethodInfo_t * value)
	{
		___method_5 = value;
		Il2CppCodeGenWriteBarrier(&___method_5, value);
	}

	inline static int32_t get_offset_of_typeCodes_6() { return static_cast<int32_t>(offsetof(XsltExtensionFunction_t863890153, ___typeCodes_6)); }
	inline TypeCodeU5BU5D_t3797419438* get_typeCodes_6() const { return ___typeCodes_6; }
	inline TypeCodeU5BU5D_t3797419438** get_address_of_typeCodes_6() { return &___typeCodes_6; }
	inline void set_typeCodes_6(TypeCodeU5BU5D_t3797419438* value)
	{
		___typeCodes_6 = value;
		Il2CppCodeGenWriteBarrier(&___typeCodes_6, value);
	}
};

struct XsltExtensionFunction_t863890153_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XsltExtensionFunction::<>f__switch$map26
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map26_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Xsl.XsltExtensionFunction::<>f__switch$map27
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map27_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map26_7() { return static_cast<int32_t>(offsetof(XsltExtensionFunction_t863890153_StaticFields, ___U3CU3Ef__switchU24map26_7)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map26_7() const { return ___U3CU3Ef__switchU24map26_7; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map26_7() { return &___U3CU3Ef__switchU24map26_7; }
	inline void set_U3CU3Ef__switchU24map26_7(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map26_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map26_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map27_8() { return static_cast<int32_t>(offsetof(XsltExtensionFunction_t863890153_StaticFields, ___U3CU3Ef__switchU24map27_8)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map27_8() const { return ___U3CU3Ef__switchU24map27_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map27_8() { return &___U3CU3Ef__switchU24map27_8; }
	inline void set_U3CU3Ef__switchU24map27_8(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map27_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map27_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
