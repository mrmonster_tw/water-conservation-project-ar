﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_SerializationS3488723117.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.MembersSerializationSource
struct  MembersSerializationSource_t468897103  : public SerializationSource_t3488723117
{
public:
	// System.String System.Xml.Serialization.MembersSerializationSource::elementName
	String_t* ___elementName_3;
	// System.Boolean System.Xml.Serialization.MembersSerializationSource::hasWrapperElement
	bool ___hasWrapperElement_4;
	// System.String System.Xml.Serialization.MembersSerializationSource::membersHash
	String_t* ___membersHash_5;
	// System.Boolean System.Xml.Serialization.MembersSerializationSource::writeAccessors
	bool ___writeAccessors_6;
	// System.Boolean System.Xml.Serialization.MembersSerializationSource::literalFormat
	bool ___literalFormat_7;

public:
	inline static int32_t get_offset_of_elementName_3() { return static_cast<int32_t>(offsetof(MembersSerializationSource_t468897103, ___elementName_3)); }
	inline String_t* get_elementName_3() const { return ___elementName_3; }
	inline String_t** get_address_of_elementName_3() { return &___elementName_3; }
	inline void set_elementName_3(String_t* value)
	{
		___elementName_3 = value;
		Il2CppCodeGenWriteBarrier(&___elementName_3, value);
	}

	inline static int32_t get_offset_of_hasWrapperElement_4() { return static_cast<int32_t>(offsetof(MembersSerializationSource_t468897103, ___hasWrapperElement_4)); }
	inline bool get_hasWrapperElement_4() const { return ___hasWrapperElement_4; }
	inline bool* get_address_of_hasWrapperElement_4() { return &___hasWrapperElement_4; }
	inline void set_hasWrapperElement_4(bool value)
	{
		___hasWrapperElement_4 = value;
	}

	inline static int32_t get_offset_of_membersHash_5() { return static_cast<int32_t>(offsetof(MembersSerializationSource_t468897103, ___membersHash_5)); }
	inline String_t* get_membersHash_5() const { return ___membersHash_5; }
	inline String_t** get_address_of_membersHash_5() { return &___membersHash_5; }
	inline void set_membersHash_5(String_t* value)
	{
		___membersHash_5 = value;
		Il2CppCodeGenWriteBarrier(&___membersHash_5, value);
	}

	inline static int32_t get_offset_of_writeAccessors_6() { return static_cast<int32_t>(offsetof(MembersSerializationSource_t468897103, ___writeAccessors_6)); }
	inline bool get_writeAccessors_6() const { return ___writeAccessors_6; }
	inline bool* get_address_of_writeAccessors_6() { return &___writeAccessors_6; }
	inline void set_writeAccessors_6(bool value)
	{
		___writeAccessors_6 = value;
	}

	inline static int32_t get_offset_of_literalFormat_7() { return static_cast<int32_t>(offsetof(MembersSerializationSource_t468897103, ___literalFormat_7)); }
	inline bool get_literalFormat_7() const { return ___literalFormat_7; }
	inline bool* get_address_of_literalFormat_7() { return &___literalFormat_7; }
	inline void set_literalFormat_7(bool value)
	{
		___literalFormat_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
