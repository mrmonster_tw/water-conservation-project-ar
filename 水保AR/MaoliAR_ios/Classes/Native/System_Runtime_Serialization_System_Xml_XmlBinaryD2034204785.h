﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Runtime_Serialization_System_Xml_XmlDiction1044334689.h"
#include "System_Xml_System_Xml_ReadState944984020.h"
#include "System_Xml_System_Xml_XmlNodeType1672767151.h"

// System.Xml.XmlBinaryDictionaryReader/ISource
struct ISource_t775601569;
// System.Xml.IXmlDictionary
struct IXmlDictionary_t3978089843;
// System.Xml.XmlDictionaryReaderQuotas
struct XmlDictionaryReaderQuotas_t173030297;
// System.Xml.XmlBinaryReaderSession
struct XmlBinaryReaderSession_t3909229952;
// System.Xml.OnXmlDictionaryReaderClose
struct OnXmlDictionaryReaderClose_t3234185550;
// System.Xml.XmlParserContext
struct XmlParserContext_t2544895291;
// System.Xml.XmlBinaryDictionaryReader/NodeInfo
struct NodeInfo_t2956564772;
// System.Collections.Generic.List`1<System.Xml.XmlBinaryDictionaryReader/AttrNodeInfo>
struct List_1_t3058616959;
// System.Collections.Generic.List`1<System.Xml.XmlBinaryDictionaryReader/NodeInfo>
struct List_1_t133672218;
// System.Collections.Generic.List`1<System.Xml.XmlQualifiedName>
struct List_1_t4232729054;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Xml.XmlDictionaryString>
struct Dictionary_2_t2392833597;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Text.UTF8Encoding
struct UTF8Encoding_t3956466879;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryDictionaryReader
struct  XmlBinaryDictionaryReader_t2034204785  : public XmlDictionaryReader_t1044334689
{
public:
	// System.Xml.XmlBinaryDictionaryReader/ISource System.Xml.XmlBinaryDictionaryReader::source
	Il2CppObject * ___source_6;
	// System.Xml.IXmlDictionary System.Xml.XmlBinaryDictionaryReader::dictionary
	Il2CppObject * ___dictionary_7;
	// System.Xml.XmlDictionaryReaderQuotas System.Xml.XmlBinaryDictionaryReader::quota
	XmlDictionaryReaderQuotas_t173030297 * ___quota_8;
	// System.Xml.XmlBinaryReaderSession System.Xml.XmlBinaryDictionaryReader::session
	XmlBinaryReaderSession_t3909229952 * ___session_9;
	// System.Xml.OnXmlDictionaryReaderClose System.Xml.XmlBinaryDictionaryReader::on_close
	OnXmlDictionaryReaderClose_t3234185550 * ___on_close_10;
	// System.Xml.XmlParserContext System.Xml.XmlBinaryDictionaryReader::context
	XmlParserContext_t2544895291 * ___context_11;
	// System.Xml.ReadState System.Xml.XmlBinaryDictionaryReader::state
	int32_t ___state_12;
	// System.Xml.XmlBinaryDictionaryReader/NodeInfo System.Xml.XmlBinaryDictionaryReader::node
	NodeInfo_t2956564772 * ___node_13;
	// System.Xml.XmlBinaryDictionaryReader/NodeInfo System.Xml.XmlBinaryDictionaryReader::current
	NodeInfo_t2956564772 * ___current_14;
	// System.Collections.Generic.List`1<System.Xml.XmlBinaryDictionaryReader/AttrNodeInfo> System.Xml.XmlBinaryDictionaryReader::attributes
	List_1_t3058616959 * ___attributes_15;
	// System.Collections.Generic.List`1<System.Xml.XmlBinaryDictionaryReader/NodeInfo> System.Xml.XmlBinaryDictionaryReader::attr_values
	List_1_t133672218 * ___attr_values_16;
	// System.Collections.Generic.List`1<System.Xml.XmlBinaryDictionaryReader/NodeInfo> System.Xml.XmlBinaryDictionaryReader::node_stack
	List_1_t133672218 * ___node_stack_17;
	// System.Collections.Generic.List`1<System.Xml.XmlQualifiedName> System.Xml.XmlBinaryDictionaryReader::ns_store
	List_1_t4232729054 * ___ns_store_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Xml.XmlDictionaryString> System.Xml.XmlBinaryDictionaryReader::ns_dict_store
	Dictionary_2_t2392833597 * ___ns_dict_store_19;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader::attr_count
	int32_t ___attr_count_20;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader::attr_value_count
	int32_t ___attr_value_count_21;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader::current_attr
	int32_t ___current_attr_22;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader::depth
	int32_t ___depth_23;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader::ns_slot
	int32_t ___ns_slot_24;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader::next
	int32_t ___next_25;
	// System.Boolean System.Xml.XmlBinaryDictionaryReader::is_next_end_element
	bool ___is_next_end_element_26;
	// System.Byte[] System.Xml.XmlBinaryDictionaryReader::tmp_buffer
	ByteU5BU5D_t4116647657* ___tmp_buffer_27;
	// System.Text.UTF8Encoding System.Xml.XmlBinaryDictionaryReader::utf8enc
	UTF8Encoding_t3956466879 * ___utf8enc_28;
	// System.Int32 System.Xml.XmlBinaryDictionaryReader::array_item_remaining
	int32_t ___array_item_remaining_29;
	// System.Byte System.Xml.XmlBinaryDictionaryReader::array_item_type
	uint8_t ___array_item_type_30;
	// System.Xml.XmlNodeType System.Xml.XmlBinaryDictionaryReader::array_state
	int32_t ___array_state_31;

public:
	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___source_6)); }
	inline Il2CppObject * get_source_6() const { return ___source_6; }
	inline Il2CppObject ** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(Il2CppObject * value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier(&___source_6, value);
	}

	inline static int32_t get_offset_of_dictionary_7() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___dictionary_7)); }
	inline Il2CppObject * get_dictionary_7() const { return ___dictionary_7; }
	inline Il2CppObject ** get_address_of_dictionary_7() { return &___dictionary_7; }
	inline void set_dictionary_7(Il2CppObject * value)
	{
		___dictionary_7 = value;
		Il2CppCodeGenWriteBarrier(&___dictionary_7, value);
	}

	inline static int32_t get_offset_of_quota_8() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___quota_8)); }
	inline XmlDictionaryReaderQuotas_t173030297 * get_quota_8() const { return ___quota_8; }
	inline XmlDictionaryReaderQuotas_t173030297 ** get_address_of_quota_8() { return &___quota_8; }
	inline void set_quota_8(XmlDictionaryReaderQuotas_t173030297 * value)
	{
		___quota_8 = value;
		Il2CppCodeGenWriteBarrier(&___quota_8, value);
	}

	inline static int32_t get_offset_of_session_9() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___session_9)); }
	inline XmlBinaryReaderSession_t3909229952 * get_session_9() const { return ___session_9; }
	inline XmlBinaryReaderSession_t3909229952 ** get_address_of_session_9() { return &___session_9; }
	inline void set_session_9(XmlBinaryReaderSession_t3909229952 * value)
	{
		___session_9 = value;
		Il2CppCodeGenWriteBarrier(&___session_9, value);
	}

	inline static int32_t get_offset_of_on_close_10() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___on_close_10)); }
	inline OnXmlDictionaryReaderClose_t3234185550 * get_on_close_10() const { return ___on_close_10; }
	inline OnXmlDictionaryReaderClose_t3234185550 ** get_address_of_on_close_10() { return &___on_close_10; }
	inline void set_on_close_10(OnXmlDictionaryReaderClose_t3234185550 * value)
	{
		___on_close_10 = value;
		Il2CppCodeGenWriteBarrier(&___on_close_10, value);
	}

	inline static int32_t get_offset_of_context_11() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___context_11)); }
	inline XmlParserContext_t2544895291 * get_context_11() const { return ___context_11; }
	inline XmlParserContext_t2544895291 ** get_address_of_context_11() { return &___context_11; }
	inline void set_context_11(XmlParserContext_t2544895291 * value)
	{
		___context_11 = value;
		Il2CppCodeGenWriteBarrier(&___context_11, value);
	}

	inline static int32_t get_offset_of_state_12() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___state_12)); }
	inline int32_t get_state_12() const { return ___state_12; }
	inline int32_t* get_address_of_state_12() { return &___state_12; }
	inline void set_state_12(int32_t value)
	{
		___state_12 = value;
	}

	inline static int32_t get_offset_of_node_13() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___node_13)); }
	inline NodeInfo_t2956564772 * get_node_13() const { return ___node_13; }
	inline NodeInfo_t2956564772 ** get_address_of_node_13() { return &___node_13; }
	inline void set_node_13(NodeInfo_t2956564772 * value)
	{
		___node_13 = value;
		Il2CppCodeGenWriteBarrier(&___node_13, value);
	}

	inline static int32_t get_offset_of_current_14() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___current_14)); }
	inline NodeInfo_t2956564772 * get_current_14() const { return ___current_14; }
	inline NodeInfo_t2956564772 ** get_address_of_current_14() { return &___current_14; }
	inline void set_current_14(NodeInfo_t2956564772 * value)
	{
		___current_14 = value;
		Il2CppCodeGenWriteBarrier(&___current_14, value);
	}

	inline static int32_t get_offset_of_attributes_15() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___attributes_15)); }
	inline List_1_t3058616959 * get_attributes_15() const { return ___attributes_15; }
	inline List_1_t3058616959 ** get_address_of_attributes_15() { return &___attributes_15; }
	inline void set_attributes_15(List_1_t3058616959 * value)
	{
		___attributes_15 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_15, value);
	}

	inline static int32_t get_offset_of_attr_values_16() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___attr_values_16)); }
	inline List_1_t133672218 * get_attr_values_16() const { return ___attr_values_16; }
	inline List_1_t133672218 ** get_address_of_attr_values_16() { return &___attr_values_16; }
	inline void set_attr_values_16(List_1_t133672218 * value)
	{
		___attr_values_16 = value;
		Il2CppCodeGenWriteBarrier(&___attr_values_16, value);
	}

	inline static int32_t get_offset_of_node_stack_17() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___node_stack_17)); }
	inline List_1_t133672218 * get_node_stack_17() const { return ___node_stack_17; }
	inline List_1_t133672218 ** get_address_of_node_stack_17() { return &___node_stack_17; }
	inline void set_node_stack_17(List_1_t133672218 * value)
	{
		___node_stack_17 = value;
		Il2CppCodeGenWriteBarrier(&___node_stack_17, value);
	}

	inline static int32_t get_offset_of_ns_store_18() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___ns_store_18)); }
	inline List_1_t4232729054 * get_ns_store_18() const { return ___ns_store_18; }
	inline List_1_t4232729054 ** get_address_of_ns_store_18() { return &___ns_store_18; }
	inline void set_ns_store_18(List_1_t4232729054 * value)
	{
		___ns_store_18 = value;
		Il2CppCodeGenWriteBarrier(&___ns_store_18, value);
	}

	inline static int32_t get_offset_of_ns_dict_store_19() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___ns_dict_store_19)); }
	inline Dictionary_2_t2392833597 * get_ns_dict_store_19() const { return ___ns_dict_store_19; }
	inline Dictionary_2_t2392833597 ** get_address_of_ns_dict_store_19() { return &___ns_dict_store_19; }
	inline void set_ns_dict_store_19(Dictionary_2_t2392833597 * value)
	{
		___ns_dict_store_19 = value;
		Il2CppCodeGenWriteBarrier(&___ns_dict_store_19, value);
	}

	inline static int32_t get_offset_of_attr_count_20() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___attr_count_20)); }
	inline int32_t get_attr_count_20() const { return ___attr_count_20; }
	inline int32_t* get_address_of_attr_count_20() { return &___attr_count_20; }
	inline void set_attr_count_20(int32_t value)
	{
		___attr_count_20 = value;
	}

	inline static int32_t get_offset_of_attr_value_count_21() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___attr_value_count_21)); }
	inline int32_t get_attr_value_count_21() const { return ___attr_value_count_21; }
	inline int32_t* get_address_of_attr_value_count_21() { return &___attr_value_count_21; }
	inline void set_attr_value_count_21(int32_t value)
	{
		___attr_value_count_21 = value;
	}

	inline static int32_t get_offset_of_current_attr_22() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___current_attr_22)); }
	inline int32_t get_current_attr_22() const { return ___current_attr_22; }
	inline int32_t* get_address_of_current_attr_22() { return &___current_attr_22; }
	inline void set_current_attr_22(int32_t value)
	{
		___current_attr_22 = value;
	}

	inline static int32_t get_offset_of_depth_23() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___depth_23)); }
	inline int32_t get_depth_23() const { return ___depth_23; }
	inline int32_t* get_address_of_depth_23() { return &___depth_23; }
	inline void set_depth_23(int32_t value)
	{
		___depth_23 = value;
	}

	inline static int32_t get_offset_of_ns_slot_24() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___ns_slot_24)); }
	inline int32_t get_ns_slot_24() const { return ___ns_slot_24; }
	inline int32_t* get_address_of_ns_slot_24() { return &___ns_slot_24; }
	inline void set_ns_slot_24(int32_t value)
	{
		___ns_slot_24 = value;
	}

	inline static int32_t get_offset_of_next_25() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___next_25)); }
	inline int32_t get_next_25() const { return ___next_25; }
	inline int32_t* get_address_of_next_25() { return &___next_25; }
	inline void set_next_25(int32_t value)
	{
		___next_25 = value;
	}

	inline static int32_t get_offset_of_is_next_end_element_26() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___is_next_end_element_26)); }
	inline bool get_is_next_end_element_26() const { return ___is_next_end_element_26; }
	inline bool* get_address_of_is_next_end_element_26() { return &___is_next_end_element_26; }
	inline void set_is_next_end_element_26(bool value)
	{
		___is_next_end_element_26 = value;
	}

	inline static int32_t get_offset_of_tmp_buffer_27() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___tmp_buffer_27)); }
	inline ByteU5BU5D_t4116647657* get_tmp_buffer_27() const { return ___tmp_buffer_27; }
	inline ByteU5BU5D_t4116647657** get_address_of_tmp_buffer_27() { return &___tmp_buffer_27; }
	inline void set_tmp_buffer_27(ByteU5BU5D_t4116647657* value)
	{
		___tmp_buffer_27 = value;
		Il2CppCodeGenWriteBarrier(&___tmp_buffer_27, value);
	}

	inline static int32_t get_offset_of_utf8enc_28() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___utf8enc_28)); }
	inline UTF8Encoding_t3956466879 * get_utf8enc_28() const { return ___utf8enc_28; }
	inline UTF8Encoding_t3956466879 ** get_address_of_utf8enc_28() { return &___utf8enc_28; }
	inline void set_utf8enc_28(UTF8Encoding_t3956466879 * value)
	{
		___utf8enc_28 = value;
		Il2CppCodeGenWriteBarrier(&___utf8enc_28, value);
	}

	inline static int32_t get_offset_of_array_item_remaining_29() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___array_item_remaining_29)); }
	inline int32_t get_array_item_remaining_29() const { return ___array_item_remaining_29; }
	inline int32_t* get_address_of_array_item_remaining_29() { return &___array_item_remaining_29; }
	inline void set_array_item_remaining_29(int32_t value)
	{
		___array_item_remaining_29 = value;
	}

	inline static int32_t get_offset_of_array_item_type_30() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___array_item_type_30)); }
	inline uint8_t get_array_item_type_30() const { return ___array_item_type_30; }
	inline uint8_t* get_address_of_array_item_type_30() { return &___array_item_type_30; }
	inline void set_array_item_type_30(uint8_t value)
	{
		___array_item_type_30 = value;
	}

	inline static int32_t get_offset_of_array_state_31() { return static_cast<int32_t>(offsetof(XmlBinaryDictionaryReader_t2034204785, ___array_state_31)); }
	inline int32_t get_array_state_31() const { return ___array_state_31; }
	inline int32_t* get_address_of_array_state_31() { return &___array_state_31; }
	inline void set_array_state_31(int32_t value)
	{
		___array_state_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
