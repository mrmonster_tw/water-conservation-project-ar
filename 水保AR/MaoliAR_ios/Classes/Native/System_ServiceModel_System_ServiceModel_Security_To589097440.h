﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector1397312864.h"

// System.ServiceModel.Security.Tokens.IssuedSecurityTokenHandler
struct IssuedSecurityTokenHandler_t3476183706;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.Tokens.CommunicationSecurityTokenAuthenticator
struct  CommunicationSecurityTokenAuthenticator_t589097440  : public SecurityTokenAuthenticator_t1397312864
{
public:
	// System.ServiceModel.Security.Tokens.IssuedSecurityTokenHandler System.ServiceModel.Security.Tokens.CommunicationSecurityTokenAuthenticator::issuance_handler
	IssuedSecurityTokenHandler_t3476183706 * ___issuance_handler_0;

public:
	inline static int32_t get_offset_of_issuance_handler_0() { return static_cast<int32_t>(offsetof(CommunicationSecurityTokenAuthenticator_t589097440, ___issuance_handler_0)); }
	inline IssuedSecurityTokenHandler_t3476183706 * get_issuance_handler_0() const { return ___issuance_handler_0; }
	inline IssuedSecurityTokenHandler_t3476183706 ** get_address_of_issuance_handler_0() { return &___issuance_handler_0; }
	inline void set_issuance_handler_0(IssuedSecurityTokenHandler_t3476183706 * value)
	{
		___issuance_handler_0 = value;
		Il2CppCodeGenWriteBarrier(&___issuance_handler_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
