﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_Mono_Xml_Xsl_Operations_XslCompiledElemen50593777.h"

// Mono.Xml.Xsl.Operations.XslAvt
struct XslAvt_t1645109359;
// Mono.Xml.Xsl.Operations.XslOperation
struct XslOperation_t2153241355;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Xsl.Operations.XslProcessingInstruction
struct  XslProcessingInstruction_t3271313310  : public XslCompiledElement_t50593777
{
public:
	// Mono.Xml.Xsl.Operations.XslAvt Mono.Xml.Xsl.Operations.XslProcessingInstruction::name
	XslAvt_t1645109359 * ___name_3;
	// Mono.Xml.Xsl.Operations.XslOperation Mono.Xml.Xsl.Operations.XslProcessingInstruction::value
	XslOperation_t2153241355 * ___value_4;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XslProcessingInstruction_t3271313310, ___name_3)); }
	inline XslAvt_t1645109359 * get_name_3() const { return ___name_3; }
	inline XslAvt_t1645109359 ** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(XslAvt_t1645109359 * value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(XslProcessingInstruction_t3271313310, ___value_4)); }
	inline XslOperation_t2153241355 * get_value_4() const { return ___value_4; }
	inline XslOperation_t2153241355 ** get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(XslOperation_t2153241355 * value)
	{
		___value_4 = value;
		Il2CppCodeGenWriteBarrier(&___value_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
