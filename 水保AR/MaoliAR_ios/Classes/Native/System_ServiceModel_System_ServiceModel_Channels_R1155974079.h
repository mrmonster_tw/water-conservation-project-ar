﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M1904510157.h"

// System.ServiceModel.Channels.ChannelListenerBase
struct ChannelListenerBase_t990592377;
// System.IdentityModel.Tokens.SecurityToken
struct SecurityToken_t1271873540;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport
struct  RecipientMessageSecurityBindingSupport_t1155974079  : public MessageSecurityBindingSupport_t1904510157
{
public:
	// System.ServiceModel.Channels.ChannelListenerBase System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport::listener
	ChannelListenerBase_t990592377 * ___listener_6;
	// System.IdentityModel.Tokens.SecurityToken System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport::encryption_token
	SecurityToken_t1271873540 * ___encryption_token_7;
	// System.IdentityModel.Tokens.SecurityToken System.ServiceModel.Channels.RecipientMessageSecurityBindingSupport::signing_token
	SecurityToken_t1271873540 * ___signing_token_8;

public:
	inline static int32_t get_offset_of_listener_6() { return static_cast<int32_t>(offsetof(RecipientMessageSecurityBindingSupport_t1155974079, ___listener_6)); }
	inline ChannelListenerBase_t990592377 * get_listener_6() const { return ___listener_6; }
	inline ChannelListenerBase_t990592377 ** get_address_of_listener_6() { return &___listener_6; }
	inline void set_listener_6(ChannelListenerBase_t990592377 * value)
	{
		___listener_6 = value;
		Il2CppCodeGenWriteBarrier(&___listener_6, value);
	}

	inline static int32_t get_offset_of_encryption_token_7() { return static_cast<int32_t>(offsetof(RecipientMessageSecurityBindingSupport_t1155974079, ___encryption_token_7)); }
	inline SecurityToken_t1271873540 * get_encryption_token_7() const { return ___encryption_token_7; }
	inline SecurityToken_t1271873540 ** get_address_of_encryption_token_7() { return &___encryption_token_7; }
	inline void set_encryption_token_7(SecurityToken_t1271873540 * value)
	{
		___encryption_token_7 = value;
		Il2CppCodeGenWriteBarrier(&___encryption_token_7, value);
	}

	inline static int32_t get_offset_of_signing_token_8() { return static_cast<int32_t>(offsetof(RecipientMessageSecurityBindingSupport_t1155974079, ___signing_token_8)); }
	inline SecurityToken_t1271873540 * get_signing_token_8() const { return ___signing_token_8; }
	inline SecurityToken_t1271873540 ** get_address_of_signing_token_8() { return &___signing_token_8; }
	inline void set_signing_token_8(SecurityToken_t1271873540 * value)
	{
		___signing_token_8 = value;
		Il2CppCodeGenWriteBarrier(&___signing_token_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
