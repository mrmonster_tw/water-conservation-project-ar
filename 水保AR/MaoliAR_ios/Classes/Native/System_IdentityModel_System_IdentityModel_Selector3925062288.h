﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Selector2987988020.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IdentityModel.Selectors.CustomUserNameSecurityTokenAuthenticator/AuthorizedCustomUserPolicy
struct  AuthorizedCustomUserPolicy_t3925062288  : public SystemIdentityAuthorizationPolicy_t2987988020
{
public:
	// System.String System.IdentityModel.Selectors.CustomUserNameSecurityTokenAuthenticator/AuthorizedCustomUserPolicy::user
	String_t* ___user_1;

public:
	inline static int32_t get_offset_of_user_1() { return static_cast<int32_t>(offsetof(AuthorizedCustomUserPolicy_t3925062288, ___user_1)); }
	inline String_t* get_user_1() const { return ___user_1; }
	inline String_t** get_address_of_user_1() { return &___user_1; }
	inline void set_user_1(String_t* value)
	{
		___user_1 = value;
		Il2CppCodeGenWriteBarrier(&___user_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
