﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Configuration_System_Configuration_Configur3156163955.h"

// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Configuration.PagesSection
struct  PagesSection_t1064701840  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct PagesSection_t1064701840_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Web.Configuration.PagesSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::asyncTimeoutProp
	ConfigurationProperty_t3590861854 * ___asyncTimeoutProp_18;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::autoEventWireupProp
	ConfigurationProperty_t3590861854 * ___autoEventWireupProp_19;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::bufferProp
	ConfigurationProperty_t3590861854 * ___bufferProp_20;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::controlsProp
	ConfigurationProperty_t3590861854 * ___controlsProp_21;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::enableEventValidationProp
	ConfigurationProperty_t3590861854 * ___enableEventValidationProp_22;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::enableSessionStateProp
	ConfigurationProperty_t3590861854 * ___enableSessionStateProp_23;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::enableViewStateProp
	ConfigurationProperty_t3590861854 * ___enableViewStateProp_24;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::enableViewStateMacProp
	ConfigurationProperty_t3590861854 * ___enableViewStateMacProp_25;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::maintainScrollPositionOnPostBackProp
	ConfigurationProperty_t3590861854 * ___maintainScrollPositionOnPostBackProp_26;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::masterPageFileProp
	ConfigurationProperty_t3590861854 * ___masterPageFileProp_27;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::maxPageStateFieldLengthProp
	ConfigurationProperty_t3590861854 * ___maxPageStateFieldLengthProp_28;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::modeProp
	ConfigurationProperty_t3590861854 * ___modeProp_29;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::namespacesProp
	ConfigurationProperty_t3590861854 * ___namespacesProp_30;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::pageBaseTypeProp
	ConfigurationProperty_t3590861854 * ___pageBaseTypeProp_31;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::pageParserFilterTypeProp
	ConfigurationProperty_t3590861854 * ___pageParserFilterTypeProp_32;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::smartNavigationProp
	ConfigurationProperty_t3590861854 * ___smartNavigationProp_33;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::styleSheetThemeProp
	ConfigurationProperty_t3590861854 * ___styleSheetThemeProp_34;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::tagMappingProp
	ConfigurationProperty_t3590861854 * ___tagMappingProp_35;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::themeProp
	ConfigurationProperty_t3590861854 * ___themeProp_36;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::userControlBaseTypeProp
	ConfigurationProperty_t3590861854 * ___userControlBaseTypeProp_37;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::validateRequestProp
	ConfigurationProperty_t3590861854 * ___validateRequestProp_38;
	// System.Configuration.ConfigurationProperty System.Web.Configuration.PagesSection::viewStateEncryptionModeProp
	ConfigurationProperty_t3590861854 * ___viewStateEncryptionModeProp_39;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.Configuration.PagesSection::<>f__switch$mapB
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapB_40;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier(&___properties_17, value);
	}

	inline static int32_t get_offset_of_asyncTimeoutProp_18() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___asyncTimeoutProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_asyncTimeoutProp_18() const { return ___asyncTimeoutProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_asyncTimeoutProp_18() { return &___asyncTimeoutProp_18; }
	inline void set_asyncTimeoutProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___asyncTimeoutProp_18 = value;
		Il2CppCodeGenWriteBarrier(&___asyncTimeoutProp_18, value);
	}

	inline static int32_t get_offset_of_autoEventWireupProp_19() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___autoEventWireupProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_autoEventWireupProp_19() const { return ___autoEventWireupProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_autoEventWireupProp_19() { return &___autoEventWireupProp_19; }
	inline void set_autoEventWireupProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___autoEventWireupProp_19 = value;
		Il2CppCodeGenWriteBarrier(&___autoEventWireupProp_19, value);
	}

	inline static int32_t get_offset_of_bufferProp_20() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___bufferProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_bufferProp_20() const { return ___bufferProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_bufferProp_20() { return &___bufferProp_20; }
	inline void set_bufferProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___bufferProp_20 = value;
		Il2CppCodeGenWriteBarrier(&___bufferProp_20, value);
	}

	inline static int32_t get_offset_of_controlsProp_21() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___controlsProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_controlsProp_21() const { return ___controlsProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_controlsProp_21() { return &___controlsProp_21; }
	inline void set_controlsProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___controlsProp_21 = value;
		Il2CppCodeGenWriteBarrier(&___controlsProp_21, value);
	}

	inline static int32_t get_offset_of_enableEventValidationProp_22() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___enableEventValidationProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_enableEventValidationProp_22() const { return ___enableEventValidationProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableEventValidationProp_22() { return &___enableEventValidationProp_22; }
	inline void set_enableEventValidationProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___enableEventValidationProp_22 = value;
		Il2CppCodeGenWriteBarrier(&___enableEventValidationProp_22, value);
	}

	inline static int32_t get_offset_of_enableSessionStateProp_23() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___enableSessionStateProp_23)); }
	inline ConfigurationProperty_t3590861854 * get_enableSessionStateProp_23() const { return ___enableSessionStateProp_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableSessionStateProp_23() { return &___enableSessionStateProp_23; }
	inline void set_enableSessionStateProp_23(ConfigurationProperty_t3590861854 * value)
	{
		___enableSessionStateProp_23 = value;
		Il2CppCodeGenWriteBarrier(&___enableSessionStateProp_23, value);
	}

	inline static int32_t get_offset_of_enableViewStateProp_24() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___enableViewStateProp_24)); }
	inline ConfigurationProperty_t3590861854 * get_enableViewStateProp_24() const { return ___enableViewStateProp_24; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableViewStateProp_24() { return &___enableViewStateProp_24; }
	inline void set_enableViewStateProp_24(ConfigurationProperty_t3590861854 * value)
	{
		___enableViewStateProp_24 = value;
		Il2CppCodeGenWriteBarrier(&___enableViewStateProp_24, value);
	}

	inline static int32_t get_offset_of_enableViewStateMacProp_25() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___enableViewStateMacProp_25)); }
	inline ConfigurationProperty_t3590861854 * get_enableViewStateMacProp_25() const { return ___enableViewStateMacProp_25; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableViewStateMacProp_25() { return &___enableViewStateMacProp_25; }
	inline void set_enableViewStateMacProp_25(ConfigurationProperty_t3590861854 * value)
	{
		___enableViewStateMacProp_25 = value;
		Il2CppCodeGenWriteBarrier(&___enableViewStateMacProp_25, value);
	}

	inline static int32_t get_offset_of_maintainScrollPositionOnPostBackProp_26() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___maintainScrollPositionOnPostBackProp_26)); }
	inline ConfigurationProperty_t3590861854 * get_maintainScrollPositionOnPostBackProp_26() const { return ___maintainScrollPositionOnPostBackProp_26; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maintainScrollPositionOnPostBackProp_26() { return &___maintainScrollPositionOnPostBackProp_26; }
	inline void set_maintainScrollPositionOnPostBackProp_26(ConfigurationProperty_t3590861854 * value)
	{
		___maintainScrollPositionOnPostBackProp_26 = value;
		Il2CppCodeGenWriteBarrier(&___maintainScrollPositionOnPostBackProp_26, value);
	}

	inline static int32_t get_offset_of_masterPageFileProp_27() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___masterPageFileProp_27)); }
	inline ConfigurationProperty_t3590861854 * get_masterPageFileProp_27() const { return ___masterPageFileProp_27; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_masterPageFileProp_27() { return &___masterPageFileProp_27; }
	inline void set_masterPageFileProp_27(ConfigurationProperty_t3590861854 * value)
	{
		___masterPageFileProp_27 = value;
		Il2CppCodeGenWriteBarrier(&___masterPageFileProp_27, value);
	}

	inline static int32_t get_offset_of_maxPageStateFieldLengthProp_28() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___maxPageStateFieldLengthProp_28)); }
	inline ConfigurationProperty_t3590861854 * get_maxPageStateFieldLengthProp_28() const { return ___maxPageStateFieldLengthProp_28; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maxPageStateFieldLengthProp_28() { return &___maxPageStateFieldLengthProp_28; }
	inline void set_maxPageStateFieldLengthProp_28(ConfigurationProperty_t3590861854 * value)
	{
		___maxPageStateFieldLengthProp_28 = value;
		Il2CppCodeGenWriteBarrier(&___maxPageStateFieldLengthProp_28, value);
	}

	inline static int32_t get_offset_of_modeProp_29() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___modeProp_29)); }
	inline ConfigurationProperty_t3590861854 * get_modeProp_29() const { return ___modeProp_29; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_modeProp_29() { return &___modeProp_29; }
	inline void set_modeProp_29(ConfigurationProperty_t3590861854 * value)
	{
		___modeProp_29 = value;
		Il2CppCodeGenWriteBarrier(&___modeProp_29, value);
	}

	inline static int32_t get_offset_of_namespacesProp_30() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___namespacesProp_30)); }
	inline ConfigurationProperty_t3590861854 * get_namespacesProp_30() const { return ___namespacesProp_30; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_namespacesProp_30() { return &___namespacesProp_30; }
	inline void set_namespacesProp_30(ConfigurationProperty_t3590861854 * value)
	{
		___namespacesProp_30 = value;
		Il2CppCodeGenWriteBarrier(&___namespacesProp_30, value);
	}

	inline static int32_t get_offset_of_pageBaseTypeProp_31() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___pageBaseTypeProp_31)); }
	inline ConfigurationProperty_t3590861854 * get_pageBaseTypeProp_31() const { return ___pageBaseTypeProp_31; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_pageBaseTypeProp_31() { return &___pageBaseTypeProp_31; }
	inline void set_pageBaseTypeProp_31(ConfigurationProperty_t3590861854 * value)
	{
		___pageBaseTypeProp_31 = value;
		Il2CppCodeGenWriteBarrier(&___pageBaseTypeProp_31, value);
	}

	inline static int32_t get_offset_of_pageParserFilterTypeProp_32() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___pageParserFilterTypeProp_32)); }
	inline ConfigurationProperty_t3590861854 * get_pageParserFilterTypeProp_32() const { return ___pageParserFilterTypeProp_32; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_pageParserFilterTypeProp_32() { return &___pageParserFilterTypeProp_32; }
	inline void set_pageParserFilterTypeProp_32(ConfigurationProperty_t3590861854 * value)
	{
		___pageParserFilterTypeProp_32 = value;
		Il2CppCodeGenWriteBarrier(&___pageParserFilterTypeProp_32, value);
	}

	inline static int32_t get_offset_of_smartNavigationProp_33() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___smartNavigationProp_33)); }
	inline ConfigurationProperty_t3590861854 * get_smartNavigationProp_33() const { return ___smartNavigationProp_33; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_smartNavigationProp_33() { return &___smartNavigationProp_33; }
	inline void set_smartNavigationProp_33(ConfigurationProperty_t3590861854 * value)
	{
		___smartNavigationProp_33 = value;
		Il2CppCodeGenWriteBarrier(&___smartNavigationProp_33, value);
	}

	inline static int32_t get_offset_of_styleSheetThemeProp_34() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___styleSheetThemeProp_34)); }
	inline ConfigurationProperty_t3590861854 * get_styleSheetThemeProp_34() const { return ___styleSheetThemeProp_34; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_styleSheetThemeProp_34() { return &___styleSheetThemeProp_34; }
	inline void set_styleSheetThemeProp_34(ConfigurationProperty_t3590861854 * value)
	{
		___styleSheetThemeProp_34 = value;
		Il2CppCodeGenWriteBarrier(&___styleSheetThemeProp_34, value);
	}

	inline static int32_t get_offset_of_tagMappingProp_35() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___tagMappingProp_35)); }
	inline ConfigurationProperty_t3590861854 * get_tagMappingProp_35() const { return ___tagMappingProp_35; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_tagMappingProp_35() { return &___tagMappingProp_35; }
	inline void set_tagMappingProp_35(ConfigurationProperty_t3590861854 * value)
	{
		___tagMappingProp_35 = value;
		Il2CppCodeGenWriteBarrier(&___tagMappingProp_35, value);
	}

	inline static int32_t get_offset_of_themeProp_36() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___themeProp_36)); }
	inline ConfigurationProperty_t3590861854 * get_themeProp_36() const { return ___themeProp_36; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_themeProp_36() { return &___themeProp_36; }
	inline void set_themeProp_36(ConfigurationProperty_t3590861854 * value)
	{
		___themeProp_36 = value;
		Il2CppCodeGenWriteBarrier(&___themeProp_36, value);
	}

	inline static int32_t get_offset_of_userControlBaseTypeProp_37() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___userControlBaseTypeProp_37)); }
	inline ConfigurationProperty_t3590861854 * get_userControlBaseTypeProp_37() const { return ___userControlBaseTypeProp_37; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_userControlBaseTypeProp_37() { return &___userControlBaseTypeProp_37; }
	inline void set_userControlBaseTypeProp_37(ConfigurationProperty_t3590861854 * value)
	{
		___userControlBaseTypeProp_37 = value;
		Il2CppCodeGenWriteBarrier(&___userControlBaseTypeProp_37, value);
	}

	inline static int32_t get_offset_of_validateRequestProp_38() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___validateRequestProp_38)); }
	inline ConfigurationProperty_t3590861854 * get_validateRequestProp_38() const { return ___validateRequestProp_38; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_validateRequestProp_38() { return &___validateRequestProp_38; }
	inline void set_validateRequestProp_38(ConfigurationProperty_t3590861854 * value)
	{
		___validateRequestProp_38 = value;
		Il2CppCodeGenWriteBarrier(&___validateRequestProp_38, value);
	}

	inline static int32_t get_offset_of_viewStateEncryptionModeProp_39() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___viewStateEncryptionModeProp_39)); }
	inline ConfigurationProperty_t3590861854 * get_viewStateEncryptionModeProp_39() const { return ___viewStateEncryptionModeProp_39; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_viewStateEncryptionModeProp_39() { return &___viewStateEncryptionModeProp_39; }
	inline void set_viewStateEncryptionModeProp_39(ConfigurationProperty_t3590861854 * value)
	{
		___viewStateEncryptionModeProp_39 = value;
		Il2CppCodeGenWriteBarrier(&___viewStateEncryptionModeProp_39, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapB_40() { return static_cast<int32_t>(offsetof(PagesSection_t1064701840_StaticFields, ___U3CU3Ef__switchU24mapB_40)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapB_40() const { return ___U3CU3Ef__switchU24mapB_40; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapB_40() { return &___U3CU3Ef__switchU24mapB_40; }
	inline void set_U3CU3Ef__switchU24mapB_40(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapB_40 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapB_40, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
