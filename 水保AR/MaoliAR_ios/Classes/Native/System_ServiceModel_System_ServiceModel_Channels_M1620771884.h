﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_Cha19627360.h"

// System.ServiceModel.Channels.MsmqTransportBindingElement
struct MsmqTransportBindingElement_t1843421144;
// System.ServiceModel.Channels.MessageEncoder
struct MessageEncoder_t3063398011;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MsmqChannelFactory`1<System.Object>
struct  MsmqChannelFactory_1_t1620771884  : public ChannelFactoryBase_1_t19627360
{
public:
	// System.ServiceModel.Channels.MsmqTransportBindingElement System.ServiceModel.Channels.MsmqChannelFactory`1::source
	MsmqTransportBindingElement_t1843421144 * ___source_14;
	// System.ServiceModel.Channels.MessageEncoder System.ServiceModel.Channels.MsmqChannelFactory`1::encoder
	MessageEncoder_t3063398011 * ___encoder_15;

public:
	inline static int32_t get_offset_of_source_14() { return static_cast<int32_t>(offsetof(MsmqChannelFactory_1_t1620771884, ___source_14)); }
	inline MsmqTransportBindingElement_t1843421144 * get_source_14() const { return ___source_14; }
	inline MsmqTransportBindingElement_t1843421144 ** get_address_of_source_14() { return &___source_14; }
	inline void set_source_14(MsmqTransportBindingElement_t1843421144 * value)
	{
		___source_14 = value;
		Il2CppCodeGenWriteBarrier(&___source_14, value);
	}

	inline static int32_t get_offset_of_encoder_15() { return static_cast<int32_t>(offsetof(MsmqChannelFactory_1_t1620771884, ___encoder_15)); }
	inline MessageEncoder_t3063398011 * get_encoder_15() const { return ___encoder_15; }
	inline MessageEncoder_t3063398011 ** get_address_of_encoder_15() { return &___encoder_15; }
	inline void set_encoder_15(MessageEncoder_t3063398011 * value)
	{
		___encoder_15 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
