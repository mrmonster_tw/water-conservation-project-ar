﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Serialization_EnumMap448286691.h"
#include "System_Xml_System_Xml_Serialization_EnumMap_EnumMa3516323045.h"
#include "System_Xml_System_Xml_XQueryConvert2841811029.h"
#include "System_Xml_Mono_Xml_XPath_XmlWriterClosedEventHandl712903533.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler1533444722.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandler791314227.h"
#include "System_Xml_System_Xml_Schema_XmlValueGetter3904916812.h"
#include "System_Xml_System_Xml_Serialization_UnreferencedOb1397626473.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeEv3451815990.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati2053138828.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati1043552519.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3162758448.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializatio908927417.h"
#include "System_Xml_System_Xml_Serialization_XmlElementEven1089210577.h"
#include "System_Xml_System_Xml_Serialization_XmlNodeEventHan508855017.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3503460108.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1547144958.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1683523542.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2333946162.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar774576741.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1951609148.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2490092596.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar520724128.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3244137463.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1929481982.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A4290130235.h"
#include "System_U3CModuleU3E692745525.h"
#include "System_System_Runtime_InteropServices_DefaultParam3202874494.h"
#include "System_Locale4128636107.h"
#include "System_System_MonoTODOAttribute4131080581.h"
#include "System_System_MonoLimitationAttribute3672514598.h"
#include "System_Mono_CSharp_CSharpCodeCompiler3305382017.h"
#include "System_Mono_CSharp_CSharpCodeGenerator4201673143.h"
#include "System_Microsoft_CSharp_CSharpCodeProvider698494114.h"
#include "System_Microsoft_VisualBasic_VBCodeCompiler761065191.h"
#include "System_Microsoft_VisualBasic_VBCodeGenerator186779574.h"
#include "System_Microsoft_VisualBasic_VBCodeGenerator_LineH2528505353.h"
#include "System_Microsoft_VisualBasic_VBCodeProvider3387928232.h"
#include "System_System_CodeDom_CodeArgumentReferenceExpress2053988171.h"
#include "System_System_CodeDom_CodeArrayCreateExpression2421540765.h"
#include "System_System_CodeDom_CodeArrayIndexerExpression2029945469.h"
#include "System_System_CodeDom_CodeAssignStatement1561974704.h"
#include "System_System_CodeDom_CodeAttachEventStatement2851702928.h"
#include "System_System_CodeDom_CodeAttributeArgumentCollect2392619326.h"
#include "System_System_CodeDom_CodeAttributeArgument2420111610.h"
#include "System_System_CodeDom_CodeAttributeDeclarationColl3890917538.h"
#include "System_System_CodeDom_CodeAttributeDeclaration930161326.h"
#include "System_System_CodeDom_CodeBaseReferenceExpression1093908436.h"
#include "System_System_CodeDom_CodeBinaryOperatorExpression4171161493.h"
#include "System_System_CodeDom_CodeBinaryOperatorType1562930934.h"
#include "System_System_CodeDom_CodeCastExpression2486271560.h"
#include "System_System_CodeDom_CodeChecksumPragma2409807200.h"
#include "System_System_CodeDom_CodeComment1985552863.h"
#include "System_System_CodeDom_CodeCommentStatementCollection84489496.h"
#include "System_System_CodeDom_CodeCommentStatement2870438836.h"
#include "System_System_CodeDom_CodeCompileUnit2527618915.h"
#include "System_System_CodeDom_CodeConditionStatement4286328683.h"
#include "System_System_CodeDom_CodeConstructor1817072848.h"
#include "System_System_CodeDom_CodeDelegateCreateExpression2744709072.h"
#include "System_System_CodeDom_CodeDelegateInvokeExpression717025592.h"
#include "System_System_CodeDom_CodeDirectiveCollection1153190035.h"
#include "System_System_CodeDom_CodeDirective2939730587.h"
#include "System_System_CodeDom_CodeEntryPointMethod397085418.h"
#include "System_System_CodeDom_CodeEventReferenceExpression4048173689.h"
#include "System_System_CodeDom_CodeExpressionCollection2370433003.h"
#include "System_System_CodeDom_CodeExpression2166265795.h"
#include "System_System_CodeDom_CodeExpressionStatement3237869284.h"
#include "System_System_CodeDom_CodeFieldReferenceExpression589996812.h"
#include "System_System_CodeDom_CodeIndexerExpression356393666.h"
#include "System_System_CodeDom_CodeLinePragma3085002198.h"
#include "System_System_CodeDom_CodeMemberEvent1243791587.h"
#include "System_System_CodeDom_CodeMemberField2087256726.h"
#include "System_System_CodeDom_CodeMemberMethod3833357554.h"
#include "System_System_CodeDom_CodeMemberProperty2419270920.h"
#include "System_System_CodeDom_CodeMethodInvokeExpression967905108.h"
#include "System_System_CodeDom_CodeMethodReferenceExpressio2645505394.h"
#include "System_System_CodeDom_CodeMethodReturnStatement2523153104.h"
#include "System_System_CodeDom_CodeNamespaceCollection381303288.h"
#include "System_System_CodeDom_CodeNamespace2165007136.h"
#include "System_System_CodeDom_CodeNamespaceImportCollectio3818670277.h"
#include "System_System_CodeDom_CodeNamespaceImport2345755399.h"
#include "System_System_CodeDom_CodeObjectCreateExpression3700027432.h"
#include "System_System_CodeDom_CodeObject3927604602.h"
#include "System_System_CodeDom_CodeParameterDeclarationExpr3391789433.h"
#include "System_System_CodeDom_CodeParameterDeclarationExpre649585606.h"
#include "System_System_CodeDom_CodePrimitiveExpression3278681854.h"
#include "System_System_CodeDom_CodePropertyReferenceExpressi286208126.h"
#include "System_System_CodeDom_CodePropertySetValueReferenc3987330621.h"
#include "System_System_CodeDom_CodeRegionDirective2863244947.h"
#include "System_System_CodeDom_CodeRegionMode2445849902.h"
#include "System_System_CodeDom_CodeSnippetCompileUnit3711561202.h"
#include "System_System_CodeDom_CodeSnippetExpression3178014635.h"
#include "System_System_CodeDom_CodeSnippetStatement3439075847.h"
#include "System_System_CodeDom_CodeSnippetTypeMember4221488240.h"
#include "System_System_CodeDom_CodeStatementCollection1265263501.h"
#include "System_System_CodeDom_CodeStatement371410868.h"
#include "System_System_CodeDom_CodeThisReferenceExpression1372480056.h"
#include "System_System_CodeDom_CodeTypeConstructor2029904726.h"
#include "System_System_CodeDom_CodeTypeDeclarationCollectio2229959800.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (EnumMap_t448286691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[5] = 
{
	EnumMap_t448286691::get_offset_of__members_0(),
	EnumMap_t448286691::get_offset_of__isFlags_1(),
	EnumMap_t448286691::get_offset_of__enumNames_2(),
	EnumMap_t448286691::get_offset_of__xmlNames_3(),
	EnumMap_t448286691::get_offset_of__values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (EnumMapMember_t3516323045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[4] = 
{
	EnumMapMember_t3516323045::get_offset_of__xmlName_0(),
	EnumMapMember_t3516323045::get_offset_of__enumName_1(),
	EnumMapMember_t3516323045::get_offset_of__value_2(),
	EnumMapMember_t3516323045::get_offset_of__documentation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (XQueryConvert_t2841811029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (XmlWriterClosedEventHandler_t712903533), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (XmlNodeChangedEventHandler_t1533444722), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (ValidationEventHandler_t791314227), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (XmlValueGetter_t3904916812), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (UnreferencedObjectEventHandler_t1397626473), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (XmlAttributeEventHandler_t3451815990), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (XmlSerializationCollectionFixupCallback_t2053138828), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (XmlSerializationFixupCallback_t1043552519), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (XmlSerializationReadCallback_t3162758448), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (XmlSerializationWriteCallback_t908927417), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (XmlElementEventHandler_t1089210577), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (XmlNodeEventHandler_t508855017), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255364), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2115[32] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D12_12(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D13_13(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D14_14(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D15_15(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D16_16(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D17_17(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D18_18(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D19_19(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D20_20(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D21_21(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D22_22(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D36_23(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D37_24(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D38_25(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D39_26(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D40_27(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D41_28(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D42_29(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D43_30(),
	U3CPrivateImplementationDetailsU3E_t3057255364_StaticFields::get_offset_of_U24U24fieldU2D44_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (U24ArrayTypeU24208_t3503460108)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24208_t3503460108 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (U24ArrayTypeU24236_t1547144958)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24236_t1547144958 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (U24ArrayTypeU2472_t1683523543)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2472_t1683523543 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (U24ArrayTypeU241532_t2333946162)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241532_t2333946162 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (U24ArrayTypeU24304_t774576741)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24304_t774576741 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (U24ArrayTypeU241392_t1951609148)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241392_t1951609148 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (U24ArrayTypeU2412_t2490092598)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t2490092598 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (U24ArrayTypeU2452_t520724129)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2452_t520724129 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (U24ArrayTypeU248_t3244137465)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3244137465 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (U24ArrayTypeU24256_t1929481984)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1929481984 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (U24ArrayTypeU241280_t4290130235)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t4290130235 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (U3CModuleU3E_t692745530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (DefaultParameterValueAttribute_t3202874494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[1] = 
{
	DefaultParameterValueAttribute_t3202874494::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (Locale_t4128636111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (MonoTODOAttribute_t4131080585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[1] = 
{
	MonoTODOAttribute_t4131080585::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (MonoLimitationAttribute_t3672514599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (CSharpCodeCompiler_t3305382017), -1, sizeof(CSharpCodeCompiler_t3305382017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2132[5] = 
{
	CSharpCodeCompiler_t3305382017_StaticFields::get_offset_of_windowsMcsPath_11(),
	CSharpCodeCompiler_t3305382017_StaticFields::get_offset_of_windowsMonoPath_12(),
	CSharpCodeCompiler_t3305382017::get_offset_of_mcsOutMutex_13(),
	CSharpCodeCompiler_t3305382017::get_offset_of_mcsOutput_14(),
	CSharpCodeCompiler_t3305382017_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (CSharpCodeGenerator_t4201673143), -1, sizeof(CSharpCodeGenerator_t4201673143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2133[5] = 
{
	CSharpCodeGenerator_t4201673143::get_offset_of_providerOptions_6(),
	CSharpCodeGenerator_t4201673143::get_offset_of_dont_write_semicolon_7(),
	CSharpCodeGenerator_t4201673143_StaticFields::get_offset_of_keywordsTable_8(),
	CSharpCodeGenerator_t4201673143_StaticFields::get_offset_of_keywords_9(),
	CSharpCodeGenerator_t4201673143_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (CSharpCodeProvider_t698494114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[1] = 
{
	CSharpCodeProvider_t698494114::get_offset_of_providerOptions_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (VBCodeCompiler_t761065191), -1, sizeof(VBCodeCompiler_t761065191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2135[2] = 
{
	VBCodeCompiler_t761065191_StaticFields::get_offset_of_windowsMonoPath_8(),
	VBCodeCompiler_t761065191_StaticFields::get_offset_of_windowsvbncPath_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (VBCodeGenerator_t186779574), -1, sizeof(VBCodeGenerator_t186779574_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2136[2] = 
{
	VBCodeGenerator_t186779574::get_offset_of_Keywords_6(),
	VBCodeGenerator_t186779574_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (LineHandling_t2528505353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2137[4] = 
{
	LineHandling_t2528505353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (VBCodeProvider_t3387928232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (CodeArgumentReferenceExpression_t2053988171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[1] = 
{
	CodeArgumentReferenceExpression_t2053988171::get_offset_of_parameterName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (CodeArrayCreateExpression_t2421540765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[4] = 
{
	CodeArrayCreateExpression_t2421540765::get_offset_of_createType_1(),
	CodeArrayCreateExpression_t2421540765::get_offset_of_initializers_2(),
	CodeArrayCreateExpression_t2421540765::get_offset_of_sizeExpression_3(),
	CodeArrayCreateExpression_t2421540765::get_offset_of_size_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (CodeArrayIndexerExpression_t2029945469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[2] = 
{
	CodeArrayIndexerExpression_t2029945469::get_offset_of_indices_1(),
	CodeArrayIndexerExpression_t2029945469::get_offset_of_targetObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (CodeAssignStatement_t1561974704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[2] = 
{
	CodeAssignStatement_t1561974704::get_offset_of_left_4(),
	CodeAssignStatement_t1561974704::get_offset_of_right_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (CodeAttachEventStatement_t2851702928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[2] = 
{
	CodeAttachEventStatement_t2851702928::get_offset_of_eventRef_4(),
	CodeAttachEventStatement_t2851702928::get_offset_of_listener_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (CodeAttributeArgumentCollection_t2392619326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (CodeAttributeArgument_t2420111610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[2] = 
{
	CodeAttributeArgument_t2420111610::get_offset_of_name_0(),
	CodeAttributeArgument_t2420111610::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (CodeAttributeDeclarationCollection_t3890917538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (CodeAttributeDeclaration_t930161326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[3] = 
{
	CodeAttributeDeclaration_t930161326::get_offset_of_name_0(),
	CodeAttributeDeclaration_t930161326::get_offset_of_arguments_1(),
	CodeAttributeDeclaration_t930161326::get_offset_of_attribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (CodeBaseReferenceExpression_t1093908436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (CodeBinaryOperatorExpression_t4171161493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[3] = 
{
	CodeBinaryOperatorExpression_t4171161493::get_offset_of_left_1(),
	CodeBinaryOperatorExpression_t4171161493::get_offset_of_right_2(),
	CodeBinaryOperatorExpression_t4171161493::get_offset_of_op_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (CodeBinaryOperatorType_t1562930934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2150[18] = 
{
	CodeBinaryOperatorType_t1562930934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (CodeCastExpression_t2486271560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[2] = 
{
	CodeCastExpression_t2486271560::get_offset_of_targetType_1(),
	CodeCastExpression_t2486271560::get_offset_of_expression_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (CodeChecksumPragma_t2409807200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[3] = 
{
	CodeChecksumPragma_t2409807200::get_offset_of_fileName_1(),
	CodeChecksumPragma_t2409807200::get_offset_of_checksumAlgorithmId_2(),
	CodeChecksumPragma_t2409807200::get_offset_of_checksumData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (CodeComment_t1985552863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[2] = 
{
	CodeComment_t1985552863::get_offset_of_docComment_1(),
	CodeComment_t1985552863::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (CodeCommentStatementCollection_t84489496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (CodeCommentStatement_t2870438836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[1] = 
{
	CodeCommentStatement_t2870438836::get_offset_of_comment_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (CodeCompileUnit_t2527618915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[5] = 
{
	CodeCompileUnit_t2527618915::get_offset_of_attributes_1(),
	CodeCompileUnit_t2527618915::get_offset_of_namespaces_2(),
	CodeCompileUnit_t2527618915::get_offset_of_assemblies_3(),
	CodeCompileUnit_t2527618915::get_offset_of_startDirectives_4(),
	CodeCompileUnit_t2527618915::get_offset_of_endDirectives_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (CodeConditionStatement_t4286328683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[3] = 
{
	CodeConditionStatement_t4286328683::get_offset_of_condition_4(),
	CodeConditionStatement_t4286328683::get_offset_of_trueStatements_5(),
	CodeConditionStatement_t4286328683::get_offset_of_falseStatements_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (CodeConstructor_t1817072848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[2] = 
{
	CodeConstructor_t1817072848::get_offset_of_baseConstructorArgs_18(),
	CodeConstructor_t1817072848::get_offset_of_chainedConstructorArgs_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (CodeDelegateCreateExpression_t2744709072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[3] = 
{
	CodeDelegateCreateExpression_t2744709072::get_offset_of_delegateType_1(),
	CodeDelegateCreateExpression_t2744709072::get_offset_of_methodName_2(),
	CodeDelegateCreateExpression_t2744709072::get_offset_of_targetObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (CodeDelegateInvokeExpression_t717025592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[2] = 
{
	CodeDelegateInvokeExpression_t717025592::get_offset_of_parameters_1(),
	CodeDelegateInvokeExpression_t717025592::get_offset_of_targetObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (CodeDirectiveCollection_t1153190035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (CodeDirective_t2939730587), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (CodeEntryPointMethod_t397085418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (CodeEventReferenceExpression_t4048173689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[2] = 
{
	CodeEventReferenceExpression_t4048173689::get_offset_of_eventName_1(),
	CodeEventReferenceExpression_t4048173689::get_offset_of_targetObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (CodeExpressionCollection_t2370433003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (CodeExpression_t2166265795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (CodeExpressionStatement_t3237869284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[1] = 
{
	CodeExpressionStatement_t3237869284::get_offset_of_expression_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (CodeFieldReferenceExpression_t589996812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[2] = 
{
	CodeFieldReferenceExpression_t589996812::get_offset_of_targetObject_1(),
	CodeFieldReferenceExpression_t589996812::get_offset_of_fieldName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (CodeIndexerExpression_t356393666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[2] = 
{
	CodeIndexerExpression_t356393666::get_offset_of_targetObject_1(),
	CodeIndexerExpression_t356393666::get_offset_of_indices_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (CodeLinePragma_t3085002198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[2] = 
{
	CodeLinePragma_t3085002198::get_offset_of_fileName_0(),
	CodeLinePragma_t3085002198::get_offset_of_lineNumber_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (CodeMemberEvent_t1243791587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[3] = 
{
	CodeMemberEvent_t1243791587::get_offset_of_implementationTypes_8(),
	CodeMemberEvent_t1243791587::get_offset_of_privateImplementationType_9(),
	CodeMemberEvent_t1243791587::get_offset_of_type_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (CodeMemberField_t2087256726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[2] = 
{
	CodeMemberField_t2087256726::get_offset_of_initExpression_8(),
	CodeMemberField_t2087256726::get_offset_of_type_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (CodeMemberMethod_t3833357554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[10] = 
{
	CodeMemberMethod_t3833357554::get_offset_of_implementationTypes_8(),
	CodeMemberMethod_t3833357554::get_offset_of_parameters_9(),
	CodeMemberMethod_t3833357554::get_offset_of_privateImplements_10(),
	CodeMemberMethod_t3833357554::get_offset_of_returnType_11(),
	CodeMemberMethod_t3833357554::get_offset_of_statements_12(),
	CodeMemberMethod_t3833357554::get_offset_of_returnAttributes_13(),
	CodeMemberMethod_t3833357554::get_offset_of_typeParameters_14(),
	CodeMemberMethod_t3833357554::get_offset_of_PopulateImplementationTypes_15(),
	CodeMemberMethod_t3833357554::get_offset_of_PopulateParameters_16(),
	CodeMemberMethod_t3833357554::get_offset_of_PopulateStatements_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (CodeMemberProperty_t2419270920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[8] = 
{
	CodeMemberProperty_t2419270920::get_offset_of_getStatements_8(),
	CodeMemberProperty_t2419270920::get_offset_of_hasGet_9(),
	CodeMemberProperty_t2419270920::get_offset_of_hasSet_10(),
	CodeMemberProperty_t2419270920::get_offset_of_implementationTypes_11(),
	CodeMemberProperty_t2419270920::get_offset_of_parameters_12(),
	CodeMemberProperty_t2419270920::get_offset_of_privateImplementationType_13(),
	CodeMemberProperty_t2419270920::get_offset_of_setStatements_14(),
	CodeMemberProperty_t2419270920::get_offset_of_type_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (CodeMethodInvokeExpression_t967905108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[2] = 
{
	CodeMethodInvokeExpression_t967905108::get_offset_of_method_1(),
	CodeMethodInvokeExpression_t967905108::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (CodeMethodReferenceExpression_t2645505394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[3] = 
{
	CodeMethodReferenceExpression_t2645505394::get_offset_of_methodName_1(),
	CodeMethodReferenceExpression_t2645505394::get_offset_of_targetObject_2(),
	CodeMethodReferenceExpression_t2645505394::get_offset_of_typeArguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (CodeMethodReturnStatement_t2523153104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[1] = 
{
	CodeMethodReturnStatement_t2523153104::get_offset_of_expression_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (CodeNamespaceCollection_t381303288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (CodeNamespace_t2165007136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[7] = 
{
	CodeNamespace_t2165007136::get_offset_of_comments_1(),
	CodeNamespace_t2165007136::get_offset_of_imports_2(),
	CodeNamespace_t2165007136::get_offset_of_classes_3(),
	CodeNamespace_t2165007136::get_offset_of_name_4(),
	CodeNamespace_t2165007136::get_offset_of_PopulateComments_5(),
	CodeNamespace_t2165007136::get_offset_of_PopulateImports_6(),
	CodeNamespace_t2165007136::get_offset_of_PopulateTypes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (CodeNamespaceImportCollection_t3818670277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[2] = 
{
	CodeNamespaceImportCollection_t3818670277::get_offset_of_keys_0(),
	CodeNamespaceImportCollection_t3818670277::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (CodeNamespaceImport_t2345755399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[2] = 
{
	CodeNamespaceImport_t2345755399::get_offset_of_linePragma_1(),
	CodeNamespaceImport_t2345755399::get_offset_of_nameSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (CodeObjectCreateExpression_t3700027432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[2] = 
{
	CodeObjectCreateExpression_t3700027432::get_offset_of_createType_1(),
	CodeObjectCreateExpression_t3700027432::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (CodeObject_t3927604602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[1] = 
{
	CodeObject_t3927604602::get_offset_of_userData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (CodeParameterDeclarationExpressionCollection_t3391789433), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (CodeParameterDeclarationExpression_t649585606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[4] = 
{
	CodeParameterDeclarationExpression_t649585606::get_offset_of_customAttributes_1(),
	CodeParameterDeclarationExpression_t649585606::get_offset_of_direction_2(),
	CodeParameterDeclarationExpression_t649585606::get_offset_of_name_3(),
	CodeParameterDeclarationExpression_t649585606::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (CodePrimitiveExpression_t3278681854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[1] = 
{
	CodePrimitiveExpression_t3278681854::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (CodePropertyReferenceExpression_t286208126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[2] = 
{
	CodePropertyReferenceExpression_t286208126::get_offset_of_targetObject_1(),
	CodePropertyReferenceExpression_t286208126::get_offset_of_propertyName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (CodePropertySetValueReferenceExpression_t3987330621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (CodeRegionDirective_t2863244947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[2] = 
{
	CodeRegionDirective_t2863244947::get_offset_of_regionMode_1(),
	CodeRegionDirective_t2863244947::get_offset_of_regionText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (CodeRegionMode_t2445849902)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2190[4] = 
{
	CodeRegionMode_t2445849902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (CodeSnippetCompileUnit_t3711561202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[2] = 
{
	CodeSnippetCompileUnit_t3711561202::get_offset_of_linePragma_6(),
	CodeSnippetCompileUnit_t3711561202::get_offset_of_value_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (CodeSnippetExpression_t3178014635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[1] = 
{
	CodeSnippetExpression_t3178014635::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (CodeSnippetStatement_t3439075847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[1] = 
{
	CodeSnippetStatement_t3439075847::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (CodeSnippetTypeMember_t4221488240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[1] = 
{
	CodeSnippetTypeMember_t4221488240::get_offset_of_text_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (CodeStatementCollection_t1265263501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (CodeStatement_t371410868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[3] = 
{
	CodeStatement_t371410868::get_offset_of_linePragma_1(),
	CodeStatement_t371410868::get_offset_of_endDirectives_2(),
	CodeStatement_t371410868::get_offset_of_startDirectives_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (CodeThisReferenceExpression_t1372480056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (CodeTypeConstructor_t2029904726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (CodeTypeDeclarationCollection_t2229959800), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
