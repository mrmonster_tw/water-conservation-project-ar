﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.OperationContext
struct OperationContext_t2829644788;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.OperationContextScope
struct  OperationContextScope_t3090301225  : public Il2CppObject
{
public:
	// System.ServiceModel.OperationContext System.ServiceModel.OperationContextScope::previous
	OperationContext_t2829644788 * ___previous_0;

public:
	inline static int32_t get_offset_of_previous_0() { return static_cast<int32_t>(offsetof(OperationContextScope_t3090301225, ___previous_0)); }
	inline OperationContext_t2829644788 * get_previous_0() const { return ___previous_0; }
	inline OperationContext_t2829644788 ** get_address_of_previous_0() { return &___previous_0; }
	inline void set_previous_0(OperationContext_t2829644788 * value)
	{
		___previous_0 = value;
		Il2CppCodeGenWriteBarrier(&___previous_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
