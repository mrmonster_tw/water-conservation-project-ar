﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Dispatcher.BaseRequestProcessorHandler
struct BaseRequestProcessorHandler_t2537743531;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Dispatcher.BaseRequestProcessorHandler
struct  BaseRequestProcessorHandler_t2537743531  : public Il2CppObject
{
public:
	// System.ServiceModel.Dispatcher.BaseRequestProcessorHandler System.ServiceModel.Dispatcher.BaseRequestProcessorHandler::next
	BaseRequestProcessorHandler_t2537743531 * ___next_0;

public:
	inline static int32_t get_offset_of_next_0() { return static_cast<int32_t>(offsetof(BaseRequestProcessorHandler_t2537743531, ___next_0)); }
	inline BaseRequestProcessorHandler_t2537743531 * get_next_0() const { return ___next_0; }
	inline BaseRequestProcessorHandler_t2537743531 ** get_address_of_next_0() { return &___next_0; }
	inline void set_next_0(BaseRequestProcessorHandler_t2537743531 * value)
	{
		___next_0 = value;
		Il2CppCodeGenWriteBarrier(&___next_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
