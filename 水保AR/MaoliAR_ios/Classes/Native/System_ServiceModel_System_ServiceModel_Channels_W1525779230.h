﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Channels.IChannelListener
struct IChannelListener_t114797722;
// System.Collections.Generic.List`1<System.Threading.ManualResetEvent>
struct List_1_t1923316752;
// System.Collections.Generic.List`1<System.Web.HttpContext>
struct List_1_t3441333752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.WcfListenerInfo
struct  WcfListenerInfo_t1525779230  : public Il2CppObject
{
public:
	// System.ServiceModel.Channels.IChannelListener System.ServiceModel.Channels.WcfListenerInfo::<Listener>k__BackingField
	Il2CppObject * ___U3CListenerU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<System.Threading.ManualResetEvent> System.ServiceModel.Channels.WcfListenerInfo::<ProcessRequestHandles>k__BackingField
	List_1_t1923316752 * ___U3CProcessRequestHandlesU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<System.Web.HttpContext> System.ServiceModel.Channels.WcfListenerInfo::<Pending>k__BackingField
	List_1_t3441333752 * ___U3CPendingU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WcfListenerInfo_t1525779230, ___U3CListenerU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CListenerU3Ek__BackingField_0() const { return ___U3CListenerU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CListenerU3Ek__BackingField_0() { return &___U3CListenerU3Ek__BackingField_0; }
	inline void set_U3CListenerU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CListenerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CListenerU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CProcessRequestHandlesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WcfListenerInfo_t1525779230, ___U3CProcessRequestHandlesU3Ek__BackingField_1)); }
	inline List_1_t1923316752 * get_U3CProcessRequestHandlesU3Ek__BackingField_1() const { return ___U3CProcessRequestHandlesU3Ek__BackingField_1; }
	inline List_1_t1923316752 ** get_address_of_U3CProcessRequestHandlesU3Ek__BackingField_1() { return &___U3CProcessRequestHandlesU3Ek__BackingField_1; }
	inline void set_U3CProcessRequestHandlesU3Ek__BackingField_1(List_1_t1923316752 * value)
	{
		___U3CProcessRequestHandlesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProcessRequestHandlesU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CPendingU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WcfListenerInfo_t1525779230, ___U3CPendingU3Ek__BackingField_2)); }
	inline List_1_t3441333752 * get_U3CPendingU3Ek__BackingField_2() const { return ___U3CPendingU3Ek__BackingField_2; }
	inline List_1_t3441333752 ** get_address_of_U3CPendingU3Ek__BackingField_2() { return &___U3CPendingU3Ek__BackingField_2; }
	inline void set_U3CPendingU3Ek__BackingField_2(List_1_t3441333752 * value)
	{
		___U3CPendingU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPendingU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
