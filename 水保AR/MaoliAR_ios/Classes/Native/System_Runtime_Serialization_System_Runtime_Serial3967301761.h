﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.XmlObjectSerializer
struct  XmlObjectSerializer_t3967301761  : public Il2CppObject
{
public:
	// System.Int32 System.Runtime.Serialization.XmlObjectSerializer::max_items
	int32_t ___max_items_0;

public:
	inline static int32_t get_offset_of_max_items_0() { return static_cast<int32_t>(offsetof(XmlObjectSerializer_t3967301761, ___max_items_0)); }
	inline int32_t get_max_items_0() const { return ___max_items_0; }
	inline int32_t* get_address_of_max_items_0() { return &___max_items_0; }
	inline void set_max_items_0(int32_t value)
	{
		___max_items_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
