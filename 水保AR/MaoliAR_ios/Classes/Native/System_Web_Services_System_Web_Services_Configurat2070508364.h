﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute861562559.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Configuration.XmlFormatExtensionPrefixAttribute
struct  XmlFormatExtensionPrefixAttribute_t2070508364  : public Attribute_t861562559
{
public:
	// System.String System.Web.Services.Configuration.XmlFormatExtensionPrefixAttribute::prefix
	String_t* ___prefix_0;
	// System.String System.Web.Services.Configuration.XmlFormatExtensionPrefixAttribute::ns
	String_t* ___ns_1;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(XmlFormatExtensionPrefixAttribute_t2070508364, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefix_0, value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(XmlFormatExtensionPrefixAttribute_t2070508364, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier(&___ns_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
