﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_DateTime3738529785.h"

// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Web.Caching.CacheDependency
struct CacheDependency_t311635210;
// System.Web.Caching.Cache
struct Cache_t4020976765;
// System.IO.FileSystemWatcher[]
struct FileSystemWatcherU5BU5D_t963804286;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Caching.CacheDependency
struct  CacheDependency_t311635210  : public Il2CppObject
{
public:
	// System.String[] System.Web.Caching.CacheDependency::cachekeys
	StringU5BU5D_t1281789340* ___cachekeys_1;
	// System.Web.Caching.CacheDependency System.Web.Caching.CacheDependency::dependency
	CacheDependency_t311635210 * ___dependency_2;
	// System.DateTime System.Web.Caching.CacheDependency::start
	DateTime_t3738529785  ___start_3;
	// System.Web.Caching.Cache System.Web.Caching.CacheDependency::cache
	Cache_t4020976765 * ___cache_4;
	// System.IO.FileSystemWatcher[] System.Web.Caching.CacheDependency::watchers
	FileSystemWatcherU5BU5D_t963804286* ___watchers_5;
	// System.Boolean System.Web.Caching.CacheDependency::hasChanged
	bool ___hasChanged_6;
	// System.Boolean System.Web.Caching.CacheDependency::used
	bool ___used_7;
	// System.DateTime System.Web.Caching.CacheDependency::utcLastModified
	DateTime_t3738529785  ___utcLastModified_8;
	// System.Object System.Web.Caching.CacheDependency::locker
	Il2CppObject * ___locker_9;
	// System.ComponentModel.EventHandlerList System.Web.Caching.CacheDependency::events
	EventHandlerList_t1108123056 * ___events_10;

public:
	inline static int32_t get_offset_of_cachekeys_1() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___cachekeys_1)); }
	inline StringU5BU5D_t1281789340* get_cachekeys_1() const { return ___cachekeys_1; }
	inline StringU5BU5D_t1281789340** get_address_of_cachekeys_1() { return &___cachekeys_1; }
	inline void set_cachekeys_1(StringU5BU5D_t1281789340* value)
	{
		___cachekeys_1 = value;
		Il2CppCodeGenWriteBarrier(&___cachekeys_1, value);
	}

	inline static int32_t get_offset_of_dependency_2() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___dependency_2)); }
	inline CacheDependency_t311635210 * get_dependency_2() const { return ___dependency_2; }
	inline CacheDependency_t311635210 ** get_address_of_dependency_2() { return &___dependency_2; }
	inline void set_dependency_2(CacheDependency_t311635210 * value)
	{
		___dependency_2 = value;
		Il2CppCodeGenWriteBarrier(&___dependency_2, value);
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___start_3)); }
	inline DateTime_t3738529785  get_start_3() const { return ___start_3; }
	inline DateTime_t3738529785 * get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(DateTime_t3738529785  value)
	{
		___start_3 = value;
	}

	inline static int32_t get_offset_of_cache_4() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___cache_4)); }
	inline Cache_t4020976765 * get_cache_4() const { return ___cache_4; }
	inline Cache_t4020976765 ** get_address_of_cache_4() { return &___cache_4; }
	inline void set_cache_4(Cache_t4020976765 * value)
	{
		___cache_4 = value;
		Il2CppCodeGenWriteBarrier(&___cache_4, value);
	}

	inline static int32_t get_offset_of_watchers_5() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___watchers_5)); }
	inline FileSystemWatcherU5BU5D_t963804286* get_watchers_5() const { return ___watchers_5; }
	inline FileSystemWatcherU5BU5D_t963804286** get_address_of_watchers_5() { return &___watchers_5; }
	inline void set_watchers_5(FileSystemWatcherU5BU5D_t963804286* value)
	{
		___watchers_5 = value;
		Il2CppCodeGenWriteBarrier(&___watchers_5, value);
	}

	inline static int32_t get_offset_of_hasChanged_6() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___hasChanged_6)); }
	inline bool get_hasChanged_6() const { return ___hasChanged_6; }
	inline bool* get_address_of_hasChanged_6() { return &___hasChanged_6; }
	inline void set_hasChanged_6(bool value)
	{
		___hasChanged_6 = value;
	}

	inline static int32_t get_offset_of_used_7() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___used_7)); }
	inline bool get_used_7() const { return ___used_7; }
	inline bool* get_address_of_used_7() { return &___used_7; }
	inline void set_used_7(bool value)
	{
		___used_7 = value;
	}

	inline static int32_t get_offset_of_utcLastModified_8() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___utcLastModified_8)); }
	inline DateTime_t3738529785  get_utcLastModified_8() const { return ___utcLastModified_8; }
	inline DateTime_t3738529785 * get_address_of_utcLastModified_8() { return &___utcLastModified_8; }
	inline void set_utcLastModified_8(DateTime_t3738529785  value)
	{
		___utcLastModified_8 = value;
	}

	inline static int32_t get_offset_of_locker_9() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___locker_9)); }
	inline Il2CppObject * get_locker_9() const { return ___locker_9; }
	inline Il2CppObject ** get_address_of_locker_9() { return &___locker_9; }
	inline void set_locker_9(Il2CppObject * value)
	{
		___locker_9 = value;
		Il2CppCodeGenWriteBarrier(&___locker_9, value);
	}

	inline static int32_t get_offset_of_events_10() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210, ___events_10)); }
	inline EventHandlerList_t1108123056 * get_events_10() const { return ___events_10; }
	inline EventHandlerList_t1108123056 ** get_address_of_events_10() { return &___events_10; }
	inline void set_events_10(EventHandlerList_t1108123056 * value)
	{
		___events_10 = value;
		Il2CppCodeGenWriteBarrier(&___events_10, value);
	}
};

struct CacheDependency_t311635210_StaticFields
{
public:
	// System.Object System.Web.Caching.CacheDependency::dependencyChangedEvent
	Il2CppObject * ___dependencyChangedEvent_0;

public:
	inline static int32_t get_offset_of_dependencyChangedEvent_0() { return static_cast<int32_t>(offsetof(CacheDependency_t311635210_StaticFields, ___dependencyChangedEvent_0)); }
	inline Il2CppObject * get_dependencyChangedEvent_0() const { return ___dependencyChangedEvent_0; }
	inline Il2CppObject ** get_address_of_dependencyChangedEvent_0() { return &___dependencyChangedEvent_0; }
	inline void set_dependencyChangedEvent_0(Il2CppObject * value)
	{
		___dependencyChangedEvent_0 = value;
		Il2CppCodeGenWriteBarrier(&___dependencyChangedEvent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
