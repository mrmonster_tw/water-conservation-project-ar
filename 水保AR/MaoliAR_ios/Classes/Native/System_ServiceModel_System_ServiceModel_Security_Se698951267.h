﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_IdentityModel_System_IdentityModel_Tokens_S1943429813.h"

// System.Xml.UniqueId
struct UniqueId_t1383576913;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SecurityContextKeyIdentifierClause
struct  SecurityContextKeyIdentifierClause_t698951267  : public SecurityKeyIdentifierClause_t1943429813
{
public:
	// System.Xml.UniqueId System.ServiceModel.Security.SecurityContextKeyIdentifierClause::context
	UniqueId_t1383576913 * ___context_3;
	// System.Xml.UniqueId System.ServiceModel.Security.SecurityContextKeyIdentifierClause::generation
	UniqueId_t1383576913 * ___generation_4;

public:
	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(SecurityContextKeyIdentifierClause_t698951267, ___context_3)); }
	inline UniqueId_t1383576913 * get_context_3() const { return ___context_3; }
	inline UniqueId_t1383576913 ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(UniqueId_t1383576913 * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier(&___context_3, value);
	}

	inline static int32_t get_offset_of_generation_4() { return static_cast<int32_t>(offsetof(SecurityContextKeyIdentifierClause_t698951267, ___generation_4)); }
	inline UniqueId_t1383576913 * get_generation_4() const { return ___generation_4; }
	inline UniqueId_t1383576913 ** get_address_of_generation_4() { return &___generation_4; }
	inline void set_generation_4(UniqueId_t1383576913 * value)
	{
		___generation_4 = value;
		Il2CppCodeGenWriteBarrier(&___generation_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
