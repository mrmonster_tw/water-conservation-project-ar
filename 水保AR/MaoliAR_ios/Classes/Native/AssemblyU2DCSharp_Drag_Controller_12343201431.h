﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// HighlightingSystem.Highlighter
struct Highlighter_t672210414;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Texture[]
struct TextureU5BU5D_t908295702;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Drag_Controller_1
struct  Drag_Controller_1_t2343201431  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Drag_Controller_1::LOCKING
	bool ___LOCKING_2;
	// System.Boolean Drag_Controller_1::complete
	bool ___complete_3;
	// System.Boolean Drag_Controller_1::useClick
	bool ___useClick_4;
	// System.Boolean Drag_Controller_1::dontFixPosition
	bool ___dontFixPosition_5;
	// System.Boolean Drag_Controller_1::already_did
	bool ___already_did_6;
	// UnityEngine.Events.UnityEvent Drag_Controller_1::Events
	UnityEvent_t2581268647 * ___Events_7;
	// UnityEngine.Vector3 Drag_Controller_1::reset_pos
	Vector3_t3722313464  ___reset_pos_8;
	// UnityEngine.BoxCollider Drag_Controller_1::target
	BoxCollider_t1640800422 * ___target_9;
	// UnityEngine.Vector3 Drag_Controller_1::screenPoint
	Vector3_t3722313464  ___screenPoint_10;
	// UnityEngine.Vector3 Drag_Controller_1::offset
	Vector3_t3722313464  ___offset_11;
	// System.Boolean Drag_Controller_1::do_Once
	bool ___do_Once_12;
	// HighlightingSystem.Highlighter Drag_Controller_1::_highlighter
	Highlighter_t672210414 * ____highlighter_13;
	// UnityEngine.SpriteRenderer Drag_Controller_1::SR
	SpriteRenderer_t3235626157 * ___SR_14;
	// UnityEngine.Sprite[] Drag_Controller_1::MySprite
	SpriteU5BU5D_t2581906349* ___MySprite_15;
	// UnityEngine.MeshRenderer Drag_Controller_1::level4background
	MeshRenderer_t587009260 * ___level4background_16;
	// UnityEngine.Texture[] Drag_Controller_1::level4backgroundSpriteArray
	TextureU5BU5D_t908295702* ___level4backgroundSpriteArray_17;
	// UnityEngine.Sprite Drag_Controller_1::level5TrueImage
	Sprite_t280657092 * ___level5TrueImage_18;
	// UnityEngine.Sprite Drag_Controller_1::level5FalseImage
	Sprite_t280657092 * ___level5FalseImage_19;
	// System.Boolean Drag_Controller_1::fired
	bool ___fired_20;
	// System.Boolean Drag_Controller_1::sw
	bool ___sw_21;
	// UnityEngine.GameObject Drag_Controller_1::arrow
	GameObject_t1113636619 * ___arrow_22;
	// System.Boolean Drag_Controller_1::picking
	bool ___picking_23;
	// System.Single Drag_Controller_1::drag_dis
	float ___drag_dis_24;
	// UnityEngine.Vector3 Drag_Controller_1::cursorPosition
	Vector3_t3722313464  ___cursorPosition_25;
	// System.Single Drag_Controller_1::dis
	float ___dis_26;
	// UnityEngine.Rigidbody Drag_Controller_1::rigi
	Rigidbody_t3916780224 * ___rigi_27;
	// UnityEngine.GameObject Drag_Controller_1::finger
	GameObject_t1113636619 * ___finger_28;
	// UnityEngine.GameObject Drag_Controller_1::mark
	GameObject_t1113636619 * ___mark_29;
	// UnityEngine.GameObject Drag_Controller_1::s_1
	GameObject_t1113636619 * ___s_1_30;
	// UnityEngine.GameObject Drag_Controller_1::s_2
	GameObject_t1113636619 * ___s_2_31;
	// UnityEngine.GameObject Drag_Controller_1::s_3
	GameObject_t1113636619 * ___s_3_32;
	// UnityEngine.GameObject Drag_Controller_1::s_4
	GameObject_t1113636619 * ___s_4_33;
	// UnityEngine.GameObject Drag_Controller_1::s_5
	GameObject_t1113636619 * ___s_5_34;
	// UnityEngine.GameObject Drag_Controller_1::s_6
	GameObject_t1113636619 * ___s_6_35;
	// UnityEngine.GameObject Drag_Controller_1::s_7
	GameObject_t1113636619 * ___s_7_36;
	// UnityEngine.GameObject Drag_Controller_1::s_8
	GameObject_t1113636619 * ___s_8_37;
	// UnityEngine.GameObject Drag_Controller_1::hint
	GameObject_t1113636619 * ___hint_38;

public:
	inline static int32_t get_offset_of_LOCKING_2() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___LOCKING_2)); }
	inline bool get_LOCKING_2() const { return ___LOCKING_2; }
	inline bool* get_address_of_LOCKING_2() { return &___LOCKING_2; }
	inline void set_LOCKING_2(bool value)
	{
		___LOCKING_2 = value;
	}

	inline static int32_t get_offset_of_complete_3() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___complete_3)); }
	inline bool get_complete_3() const { return ___complete_3; }
	inline bool* get_address_of_complete_3() { return &___complete_3; }
	inline void set_complete_3(bool value)
	{
		___complete_3 = value;
	}

	inline static int32_t get_offset_of_useClick_4() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___useClick_4)); }
	inline bool get_useClick_4() const { return ___useClick_4; }
	inline bool* get_address_of_useClick_4() { return &___useClick_4; }
	inline void set_useClick_4(bool value)
	{
		___useClick_4 = value;
	}

	inline static int32_t get_offset_of_dontFixPosition_5() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___dontFixPosition_5)); }
	inline bool get_dontFixPosition_5() const { return ___dontFixPosition_5; }
	inline bool* get_address_of_dontFixPosition_5() { return &___dontFixPosition_5; }
	inline void set_dontFixPosition_5(bool value)
	{
		___dontFixPosition_5 = value;
	}

	inline static int32_t get_offset_of_already_did_6() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___already_did_6)); }
	inline bool get_already_did_6() const { return ___already_did_6; }
	inline bool* get_address_of_already_did_6() { return &___already_did_6; }
	inline void set_already_did_6(bool value)
	{
		___already_did_6 = value;
	}

	inline static int32_t get_offset_of_Events_7() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___Events_7)); }
	inline UnityEvent_t2581268647 * get_Events_7() const { return ___Events_7; }
	inline UnityEvent_t2581268647 ** get_address_of_Events_7() { return &___Events_7; }
	inline void set_Events_7(UnityEvent_t2581268647 * value)
	{
		___Events_7 = value;
		Il2CppCodeGenWriteBarrier(&___Events_7, value);
	}

	inline static int32_t get_offset_of_reset_pos_8() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___reset_pos_8)); }
	inline Vector3_t3722313464  get_reset_pos_8() const { return ___reset_pos_8; }
	inline Vector3_t3722313464 * get_address_of_reset_pos_8() { return &___reset_pos_8; }
	inline void set_reset_pos_8(Vector3_t3722313464  value)
	{
		___reset_pos_8 = value;
	}

	inline static int32_t get_offset_of_target_9() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___target_9)); }
	inline BoxCollider_t1640800422 * get_target_9() const { return ___target_9; }
	inline BoxCollider_t1640800422 ** get_address_of_target_9() { return &___target_9; }
	inline void set_target_9(BoxCollider_t1640800422 * value)
	{
		___target_9 = value;
		Il2CppCodeGenWriteBarrier(&___target_9, value);
	}

	inline static int32_t get_offset_of_screenPoint_10() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___screenPoint_10)); }
	inline Vector3_t3722313464  get_screenPoint_10() const { return ___screenPoint_10; }
	inline Vector3_t3722313464 * get_address_of_screenPoint_10() { return &___screenPoint_10; }
	inline void set_screenPoint_10(Vector3_t3722313464  value)
	{
		___screenPoint_10 = value;
	}

	inline static int32_t get_offset_of_offset_11() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___offset_11)); }
	inline Vector3_t3722313464  get_offset_11() const { return ___offset_11; }
	inline Vector3_t3722313464 * get_address_of_offset_11() { return &___offset_11; }
	inline void set_offset_11(Vector3_t3722313464  value)
	{
		___offset_11 = value;
	}

	inline static int32_t get_offset_of_do_Once_12() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___do_Once_12)); }
	inline bool get_do_Once_12() const { return ___do_Once_12; }
	inline bool* get_address_of_do_Once_12() { return &___do_Once_12; }
	inline void set_do_Once_12(bool value)
	{
		___do_Once_12 = value;
	}

	inline static int32_t get_offset_of__highlighter_13() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ____highlighter_13)); }
	inline Highlighter_t672210414 * get__highlighter_13() const { return ____highlighter_13; }
	inline Highlighter_t672210414 ** get_address_of__highlighter_13() { return &____highlighter_13; }
	inline void set__highlighter_13(Highlighter_t672210414 * value)
	{
		____highlighter_13 = value;
		Il2CppCodeGenWriteBarrier(&____highlighter_13, value);
	}

	inline static int32_t get_offset_of_SR_14() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___SR_14)); }
	inline SpriteRenderer_t3235626157 * get_SR_14() const { return ___SR_14; }
	inline SpriteRenderer_t3235626157 ** get_address_of_SR_14() { return &___SR_14; }
	inline void set_SR_14(SpriteRenderer_t3235626157 * value)
	{
		___SR_14 = value;
		Il2CppCodeGenWriteBarrier(&___SR_14, value);
	}

	inline static int32_t get_offset_of_MySprite_15() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___MySprite_15)); }
	inline SpriteU5BU5D_t2581906349* get_MySprite_15() const { return ___MySprite_15; }
	inline SpriteU5BU5D_t2581906349** get_address_of_MySprite_15() { return &___MySprite_15; }
	inline void set_MySprite_15(SpriteU5BU5D_t2581906349* value)
	{
		___MySprite_15 = value;
		Il2CppCodeGenWriteBarrier(&___MySprite_15, value);
	}

	inline static int32_t get_offset_of_level4background_16() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___level4background_16)); }
	inline MeshRenderer_t587009260 * get_level4background_16() const { return ___level4background_16; }
	inline MeshRenderer_t587009260 ** get_address_of_level4background_16() { return &___level4background_16; }
	inline void set_level4background_16(MeshRenderer_t587009260 * value)
	{
		___level4background_16 = value;
		Il2CppCodeGenWriteBarrier(&___level4background_16, value);
	}

	inline static int32_t get_offset_of_level4backgroundSpriteArray_17() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___level4backgroundSpriteArray_17)); }
	inline TextureU5BU5D_t908295702* get_level4backgroundSpriteArray_17() const { return ___level4backgroundSpriteArray_17; }
	inline TextureU5BU5D_t908295702** get_address_of_level4backgroundSpriteArray_17() { return &___level4backgroundSpriteArray_17; }
	inline void set_level4backgroundSpriteArray_17(TextureU5BU5D_t908295702* value)
	{
		___level4backgroundSpriteArray_17 = value;
		Il2CppCodeGenWriteBarrier(&___level4backgroundSpriteArray_17, value);
	}

	inline static int32_t get_offset_of_level5TrueImage_18() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___level5TrueImage_18)); }
	inline Sprite_t280657092 * get_level5TrueImage_18() const { return ___level5TrueImage_18; }
	inline Sprite_t280657092 ** get_address_of_level5TrueImage_18() { return &___level5TrueImage_18; }
	inline void set_level5TrueImage_18(Sprite_t280657092 * value)
	{
		___level5TrueImage_18 = value;
		Il2CppCodeGenWriteBarrier(&___level5TrueImage_18, value);
	}

	inline static int32_t get_offset_of_level5FalseImage_19() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___level5FalseImage_19)); }
	inline Sprite_t280657092 * get_level5FalseImage_19() const { return ___level5FalseImage_19; }
	inline Sprite_t280657092 ** get_address_of_level5FalseImage_19() { return &___level5FalseImage_19; }
	inline void set_level5FalseImage_19(Sprite_t280657092 * value)
	{
		___level5FalseImage_19 = value;
		Il2CppCodeGenWriteBarrier(&___level5FalseImage_19, value);
	}

	inline static int32_t get_offset_of_fired_20() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___fired_20)); }
	inline bool get_fired_20() const { return ___fired_20; }
	inline bool* get_address_of_fired_20() { return &___fired_20; }
	inline void set_fired_20(bool value)
	{
		___fired_20 = value;
	}

	inline static int32_t get_offset_of_sw_21() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___sw_21)); }
	inline bool get_sw_21() const { return ___sw_21; }
	inline bool* get_address_of_sw_21() { return &___sw_21; }
	inline void set_sw_21(bool value)
	{
		___sw_21 = value;
	}

	inline static int32_t get_offset_of_arrow_22() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___arrow_22)); }
	inline GameObject_t1113636619 * get_arrow_22() const { return ___arrow_22; }
	inline GameObject_t1113636619 ** get_address_of_arrow_22() { return &___arrow_22; }
	inline void set_arrow_22(GameObject_t1113636619 * value)
	{
		___arrow_22 = value;
		Il2CppCodeGenWriteBarrier(&___arrow_22, value);
	}

	inline static int32_t get_offset_of_picking_23() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___picking_23)); }
	inline bool get_picking_23() const { return ___picking_23; }
	inline bool* get_address_of_picking_23() { return &___picking_23; }
	inline void set_picking_23(bool value)
	{
		___picking_23 = value;
	}

	inline static int32_t get_offset_of_drag_dis_24() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___drag_dis_24)); }
	inline float get_drag_dis_24() const { return ___drag_dis_24; }
	inline float* get_address_of_drag_dis_24() { return &___drag_dis_24; }
	inline void set_drag_dis_24(float value)
	{
		___drag_dis_24 = value;
	}

	inline static int32_t get_offset_of_cursorPosition_25() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___cursorPosition_25)); }
	inline Vector3_t3722313464  get_cursorPosition_25() const { return ___cursorPosition_25; }
	inline Vector3_t3722313464 * get_address_of_cursorPosition_25() { return &___cursorPosition_25; }
	inline void set_cursorPosition_25(Vector3_t3722313464  value)
	{
		___cursorPosition_25 = value;
	}

	inline static int32_t get_offset_of_dis_26() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___dis_26)); }
	inline float get_dis_26() const { return ___dis_26; }
	inline float* get_address_of_dis_26() { return &___dis_26; }
	inline void set_dis_26(float value)
	{
		___dis_26 = value;
	}

	inline static int32_t get_offset_of_rigi_27() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___rigi_27)); }
	inline Rigidbody_t3916780224 * get_rigi_27() const { return ___rigi_27; }
	inline Rigidbody_t3916780224 ** get_address_of_rigi_27() { return &___rigi_27; }
	inline void set_rigi_27(Rigidbody_t3916780224 * value)
	{
		___rigi_27 = value;
		Il2CppCodeGenWriteBarrier(&___rigi_27, value);
	}

	inline static int32_t get_offset_of_finger_28() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___finger_28)); }
	inline GameObject_t1113636619 * get_finger_28() const { return ___finger_28; }
	inline GameObject_t1113636619 ** get_address_of_finger_28() { return &___finger_28; }
	inline void set_finger_28(GameObject_t1113636619 * value)
	{
		___finger_28 = value;
		Il2CppCodeGenWriteBarrier(&___finger_28, value);
	}

	inline static int32_t get_offset_of_mark_29() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___mark_29)); }
	inline GameObject_t1113636619 * get_mark_29() const { return ___mark_29; }
	inline GameObject_t1113636619 ** get_address_of_mark_29() { return &___mark_29; }
	inline void set_mark_29(GameObject_t1113636619 * value)
	{
		___mark_29 = value;
		Il2CppCodeGenWriteBarrier(&___mark_29, value);
	}

	inline static int32_t get_offset_of_s_1_30() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___s_1_30)); }
	inline GameObject_t1113636619 * get_s_1_30() const { return ___s_1_30; }
	inline GameObject_t1113636619 ** get_address_of_s_1_30() { return &___s_1_30; }
	inline void set_s_1_30(GameObject_t1113636619 * value)
	{
		___s_1_30 = value;
		Il2CppCodeGenWriteBarrier(&___s_1_30, value);
	}

	inline static int32_t get_offset_of_s_2_31() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___s_2_31)); }
	inline GameObject_t1113636619 * get_s_2_31() const { return ___s_2_31; }
	inline GameObject_t1113636619 ** get_address_of_s_2_31() { return &___s_2_31; }
	inline void set_s_2_31(GameObject_t1113636619 * value)
	{
		___s_2_31 = value;
		Il2CppCodeGenWriteBarrier(&___s_2_31, value);
	}

	inline static int32_t get_offset_of_s_3_32() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___s_3_32)); }
	inline GameObject_t1113636619 * get_s_3_32() const { return ___s_3_32; }
	inline GameObject_t1113636619 ** get_address_of_s_3_32() { return &___s_3_32; }
	inline void set_s_3_32(GameObject_t1113636619 * value)
	{
		___s_3_32 = value;
		Il2CppCodeGenWriteBarrier(&___s_3_32, value);
	}

	inline static int32_t get_offset_of_s_4_33() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___s_4_33)); }
	inline GameObject_t1113636619 * get_s_4_33() const { return ___s_4_33; }
	inline GameObject_t1113636619 ** get_address_of_s_4_33() { return &___s_4_33; }
	inline void set_s_4_33(GameObject_t1113636619 * value)
	{
		___s_4_33 = value;
		Il2CppCodeGenWriteBarrier(&___s_4_33, value);
	}

	inline static int32_t get_offset_of_s_5_34() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___s_5_34)); }
	inline GameObject_t1113636619 * get_s_5_34() const { return ___s_5_34; }
	inline GameObject_t1113636619 ** get_address_of_s_5_34() { return &___s_5_34; }
	inline void set_s_5_34(GameObject_t1113636619 * value)
	{
		___s_5_34 = value;
		Il2CppCodeGenWriteBarrier(&___s_5_34, value);
	}

	inline static int32_t get_offset_of_s_6_35() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___s_6_35)); }
	inline GameObject_t1113636619 * get_s_6_35() const { return ___s_6_35; }
	inline GameObject_t1113636619 ** get_address_of_s_6_35() { return &___s_6_35; }
	inline void set_s_6_35(GameObject_t1113636619 * value)
	{
		___s_6_35 = value;
		Il2CppCodeGenWriteBarrier(&___s_6_35, value);
	}

	inline static int32_t get_offset_of_s_7_36() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___s_7_36)); }
	inline GameObject_t1113636619 * get_s_7_36() const { return ___s_7_36; }
	inline GameObject_t1113636619 ** get_address_of_s_7_36() { return &___s_7_36; }
	inline void set_s_7_36(GameObject_t1113636619 * value)
	{
		___s_7_36 = value;
		Il2CppCodeGenWriteBarrier(&___s_7_36, value);
	}

	inline static int32_t get_offset_of_s_8_37() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___s_8_37)); }
	inline GameObject_t1113636619 * get_s_8_37() const { return ___s_8_37; }
	inline GameObject_t1113636619 ** get_address_of_s_8_37() { return &___s_8_37; }
	inline void set_s_8_37(GameObject_t1113636619 * value)
	{
		___s_8_37 = value;
		Il2CppCodeGenWriteBarrier(&___s_8_37, value);
	}

	inline static int32_t get_offset_of_hint_38() { return static_cast<int32_t>(offsetof(Drag_Controller_1_t2343201431, ___hint_38)); }
	inline GameObject_t1113636619 * get_hint_38() const { return ___hint_38; }
	inline GameObject_t1113636619 ** get_address_of_hint_38() { return &___hint_38; }
	inline void set_hint_38(GameObject_t1113636619 * value)
	{
		___hint_38 = value;
		Il2CppCodeGenWriteBarrier(&___hint_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
