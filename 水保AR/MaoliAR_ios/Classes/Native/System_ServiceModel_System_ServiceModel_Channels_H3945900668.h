﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_H2430035905.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.HttpSimpleChannelListener`1<System.ServiceModel.Channels.IReplyChannel>
struct  HttpSimpleChannelListener_1_t3945900668  : public HttpChannelListenerBase_1_t2430035905
{
public:
	// System.Object System.ServiceModel.Channels.HttpSimpleChannelListener`1::creator_lock
	Il2CppObject * ___creator_lock_20;

public:
	inline static int32_t get_offset_of_creator_lock_20() { return static_cast<int32_t>(offsetof(HttpSimpleChannelListener_1_t3945900668, ___creator_lock_20)); }
	inline Il2CppObject * get_creator_lock_20() const { return ___creator_lock_20; }
	inline Il2CppObject ** get_address_of_creator_lock_20() { return &___creator_lock_20; }
	inline void set_creator_lock_20(Il2CppObject * value)
	{
		___creator_lock_20 = value;
		Il2CppCodeGenWriteBarrier(&___creator_lock_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
