﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpriteOutline
struct  SpriteOutline_t3043537775  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color SpriteOutline::color
	Color_t2555686324  ___color_2;
	// UnityEngine.SpriteRenderer SpriteOutline::spriteRenderer
	SpriteRenderer_t3235626157 * ___spriteRenderer_3;

public:
	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(SpriteOutline_t3043537775, ___color_2)); }
	inline Color_t2555686324  get_color_2() const { return ___color_2; }
	inline Color_t2555686324 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color_t2555686324  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_spriteRenderer_3() { return static_cast<int32_t>(offsetof(SpriteOutline_t3043537775, ___spriteRenderer_3)); }
	inline SpriteRenderer_t3235626157 * get_spriteRenderer_3() const { return ___spriteRenderer_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of_spriteRenderer_3() { return &___spriteRenderer_3; }
	inline void set_spriteRenderer_3(SpriteRenderer_t3235626157 * value)
	{
		___spriteRenderer_3 = value;
		Il2CppCodeGenWriteBarrier(&___spriteRenderer_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
