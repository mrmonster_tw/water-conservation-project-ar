﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// System.Xml.XmlDictionary
struct XmlDictionary_t238028028;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Constants
struct  Constants_t3917698844  : public Il2CppObject
{
public:

public:
};

struct Constants_t3917698844_StaticFields
{
public:
	// System.String[] System.ServiceModel.Constants::dict_strings
	StringU5BU5D_t1281789340* ___dict_strings_0;
	// System.Xml.XmlDictionary System.ServiceModel.Constants::<SoapDictionary>k__BackingField
	XmlDictionary_t238028028 * ___U3CSoapDictionaryU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_dict_strings_0() { return static_cast<int32_t>(offsetof(Constants_t3917698844_StaticFields, ___dict_strings_0)); }
	inline StringU5BU5D_t1281789340* get_dict_strings_0() const { return ___dict_strings_0; }
	inline StringU5BU5D_t1281789340** get_address_of_dict_strings_0() { return &___dict_strings_0; }
	inline void set_dict_strings_0(StringU5BU5D_t1281789340* value)
	{
		___dict_strings_0 = value;
		Il2CppCodeGenWriteBarrier(&___dict_strings_0, value);
	}

	inline static int32_t get_offset_of_U3CSoapDictionaryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Constants_t3917698844_StaticFields, ___U3CSoapDictionaryU3Ek__BackingField_1)); }
	inline XmlDictionary_t238028028 * get_U3CSoapDictionaryU3Ek__BackingField_1() const { return ___U3CSoapDictionaryU3Ek__BackingField_1; }
	inline XmlDictionary_t238028028 ** get_address_of_U3CSoapDictionaryU3Ek__BackingField_1() { return &___U3CSoapDictionaryU3Ek__BackingField_1; }
	inline void set_U3CSoapDictionaryU3Ek__BackingField_1(XmlDictionary_t238028028 * value)
	{
		___U3CSoapDictionaryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSoapDictionaryU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
