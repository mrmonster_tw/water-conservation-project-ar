﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.IdentityModel.Tokens.SecurityToken
struct SecurityToken_t1271873540;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy>
struct ReadOnlyCollection_1_t778487864;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SecurityTokenSpecification
struct  SecurityTokenSpecification_t3911394864  : public Il2CppObject
{
public:
	// System.IdentityModel.Tokens.SecurityToken System.ServiceModel.Security.SecurityTokenSpecification::token
	SecurityToken_t1271873540 * ___token_0;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy> System.ServiceModel.Security.SecurityTokenSpecification::policies
	ReadOnlyCollection_1_t778487864 * ___policies_1;

public:
	inline static int32_t get_offset_of_token_0() { return static_cast<int32_t>(offsetof(SecurityTokenSpecification_t3911394864, ___token_0)); }
	inline SecurityToken_t1271873540 * get_token_0() const { return ___token_0; }
	inline SecurityToken_t1271873540 ** get_address_of_token_0() { return &___token_0; }
	inline void set_token_0(SecurityToken_t1271873540 * value)
	{
		___token_0 = value;
		Il2CppCodeGenWriteBarrier(&___token_0, value);
	}

	inline static int32_t get_offset_of_policies_1() { return static_cast<int32_t>(offsetof(SecurityTokenSpecification_t3911394864, ___policies_1)); }
	inline ReadOnlyCollection_1_t778487864 * get_policies_1() const { return ___policies_1; }
	inline ReadOnlyCollection_1_t778487864 ** get_address_of_policies_1() { return &___policies_1; }
	inline void set_policies_1(ReadOnlyCollection_1_t778487864 * value)
	{
		___policies_1 = value;
		Il2CppCodeGenWriteBarrier(&___policies_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
