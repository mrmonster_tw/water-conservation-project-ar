﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_DictionaryBase3062914256.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.Services.Discovery.DiscoveryClientDocumentCollection
struct  DiscoveryClientDocumentCollection_t1899304036  : public DictionaryBase_t3062914256
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
