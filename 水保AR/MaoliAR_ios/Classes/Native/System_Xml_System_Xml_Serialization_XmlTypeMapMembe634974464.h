﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Xml_System_Xml_Serialization_XmlTypeMapMemb1199680526.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTypeMapMemberExpandable
struct  XmlTypeMapMemberExpandable_t634974464  : public XmlTypeMapMemberElement_t1199680526
{
public:
	// System.Int32 System.Xml.Serialization.XmlTypeMapMemberExpandable::_flatArrayIndex
	int32_t ____flatArrayIndex_13;

public:
	inline static int32_t get_offset_of__flatArrayIndex_13() { return static_cast<int32_t>(offsetof(XmlTypeMapMemberExpandable_t634974464, ____flatArrayIndex_13)); }
	inline int32_t get__flatArrayIndex_13() const { return ____flatArrayIndex_13; }
	inline int32_t* get_address_of__flatArrayIndex_13() { return &____flatArrayIndex_13; }
	inline void set__flatArrayIndex_13(int32_t value)
	{
		____flatArrayIndex_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
