﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_System_CodeDom_CodeTypeMember1555525554.h"

// System.CodeDom.CodeTypeReferenceCollection
struct CodeTypeReferenceCollection_t3857551471;
// System.CodeDom.CodeParameterDeclarationExpressionCollection
struct CodeParameterDeclarationExpressionCollection_t3391789433;
// System.CodeDom.CodeTypeReference
struct CodeTypeReference_t3809997434;
// System.CodeDom.CodeStatementCollection
struct CodeStatementCollection_t1265263501;
// System.CodeDom.CodeAttributeDeclarationCollection
struct CodeAttributeDeclarationCollection_t3890917538;
// System.CodeDom.CodeTypeParameterCollection
struct CodeTypeParameterCollection_t1882362546;
// System.EventHandler
struct EventHandler_t1348719766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.CodeDom.CodeMemberMethod
struct  CodeMemberMethod_t3833357554  : public CodeTypeMember_t1555525554
{
public:
	// System.CodeDom.CodeTypeReferenceCollection System.CodeDom.CodeMemberMethod::implementationTypes
	CodeTypeReferenceCollection_t3857551471 * ___implementationTypes_8;
	// System.CodeDom.CodeParameterDeclarationExpressionCollection System.CodeDom.CodeMemberMethod::parameters
	CodeParameterDeclarationExpressionCollection_t3391789433 * ___parameters_9;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeMemberMethod::privateImplements
	CodeTypeReference_t3809997434 * ___privateImplements_10;
	// System.CodeDom.CodeTypeReference System.CodeDom.CodeMemberMethod::returnType
	CodeTypeReference_t3809997434 * ___returnType_11;
	// System.CodeDom.CodeStatementCollection System.CodeDom.CodeMemberMethod::statements
	CodeStatementCollection_t1265263501 * ___statements_12;
	// System.CodeDom.CodeAttributeDeclarationCollection System.CodeDom.CodeMemberMethod::returnAttributes
	CodeAttributeDeclarationCollection_t3890917538 * ___returnAttributes_13;
	// System.CodeDom.CodeTypeParameterCollection System.CodeDom.CodeMemberMethod::typeParameters
	CodeTypeParameterCollection_t1882362546 * ___typeParameters_14;
	// System.EventHandler System.CodeDom.CodeMemberMethod::PopulateImplementationTypes
	EventHandler_t1348719766 * ___PopulateImplementationTypes_15;
	// System.EventHandler System.CodeDom.CodeMemberMethod::PopulateParameters
	EventHandler_t1348719766 * ___PopulateParameters_16;
	// System.EventHandler System.CodeDom.CodeMemberMethod::PopulateStatements
	EventHandler_t1348719766 * ___PopulateStatements_17;

public:
	inline static int32_t get_offset_of_implementationTypes_8() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___implementationTypes_8)); }
	inline CodeTypeReferenceCollection_t3857551471 * get_implementationTypes_8() const { return ___implementationTypes_8; }
	inline CodeTypeReferenceCollection_t3857551471 ** get_address_of_implementationTypes_8() { return &___implementationTypes_8; }
	inline void set_implementationTypes_8(CodeTypeReferenceCollection_t3857551471 * value)
	{
		___implementationTypes_8 = value;
		Il2CppCodeGenWriteBarrier(&___implementationTypes_8, value);
	}

	inline static int32_t get_offset_of_parameters_9() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___parameters_9)); }
	inline CodeParameterDeclarationExpressionCollection_t3391789433 * get_parameters_9() const { return ___parameters_9; }
	inline CodeParameterDeclarationExpressionCollection_t3391789433 ** get_address_of_parameters_9() { return &___parameters_9; }
	inline void set_parameters_9(CodeParameterDeclarationExpressionCollection_t3391789433 * value)
	{
		___parameters_9 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_9, value);
	}

	inline static int32_t get_offset_of_privateImplements_10() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___privateImplements_10)); }
	inline CodeTypeReference_t3809997434 * get_privateImplements_10() const { return ___privateImplements_10; }
	inline CodeTypeReference_t3809997434 ** get_address_of_privateImplements_10() { return &___privateImplements_10; }
	inline void set_privateImplements_10(CodeTypeReference_t3809997434 * value)
	{
		___privateImplements_10 = value;
		Il2CppCodeGenWriteBarrier(&___privateImplements_10, value);
	}

	inline static int32_t get_offset_of_returnType_11() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___returnType_11)); }
	inline CodeTypeReference_t3809997434 * get_returnType_11() const { return ___returnType_11; }
	inline CodeTypeReference_t3809997434 ** get_address_of_returnType_11() { return &___returnType_11; }
	inline void set_returnType_11(CodeTypeReference_t3809997434 * value)
	{
		___returnType_11 = value;
		Il2CppCodeGenWriteBarrier(&___returnType_11, value);
	}

	inline static int32_t get_offset_of_statements_12() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___statements_12)); }
	inline CodeStatementCollection_t1265263501 * get_statements_12() const { return ___statements_12; }
	inline CodeStatementCollection_t1265263501 ** get_address_of_statements_12() { return &___statements_12; }
	inline void set_statements_12(CodeStatementCollection_t1265263501 * value)
	{
		___statements_12 = value;
		Il2CppCodeGenWriteBarrier(&___statements_12, value);
	}

	inline static int32_t get_offset_of_returnAttributes_13() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___returnAttributes_13)); }
	inline CodeAttributeDeclarationCollection_t3890917538 * get_returnAttributes_13() const { return ___returnAttributes_13; }
	inline CodeAttributeDeclarationCollection_t3890917538 ** get_address_of_returnAttributes_13() { return &___returnAttributes_13; }
	inline void set_returnAttributes_13(CodeAttributeDeclarationCollection_t3890917538 * value)
	{
		___returnAttributes_13 = value;
		Il2CppCodeGenWriteBarrier(&___returnAttributes_13, value);
	}

	inline static int32_t get_offset_of_typeParameters_14() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___typeParameters_14)); }
	inline CodeTypeParameterCollection_t1882362546 * get_typeParameters_14() const { return ___typeParameters_14; }
	inline CodeTypeParameterCollection_t1882362546 ** get_address_of_typeParameters_14() { return &___typeParameters_14; }
	inline void set_typeParameters_14(CodeTypeParameterCollection_t1882362546 * value)
	{
		___typeParameters_14 = value;
		Il2CppCodeGenWriteBarrier(&___typeParameters_14, value);
	}

	inline static int32_t get_offset_of_PopulateImplementationTypes_15() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___PopulateImplementationTypes_15)); }
	inline EventHandler_t1348719766 * get_PopulateImplementationTypes_15() const { return ___PopulateImplementationTypes_15; }
	inline EventHandler_t1348719766 ** get_address_of_PopulateImplementationTypes_15() { return &___PopulateImplementationTypes_15; }
	inline void set_PopulateImplementationTypes_15(EventHandler_t1348719766 * value)
	{
		___PopulateImplementationTypes_15 = value;
		Il2CppCodeGenWriteBarrier(&___PopulateImplementationTypes_15, value);
	}

	inline static int32_t get_offset_of_PopulateParameters_16() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___PopulateParameters_16)); }
	inline EventHandler_t1348719766 * get_PopulateParameters_16() const { return ___PopulateParameters_16; }
	inline EventHandler_t1348719766 ** get_address_of_PopulateParameters_16() { return &___PopulateParameters_16; }
	inline void set_PopulateParameters_16(EventHandler_t1348719766 * value)
	{
		___PopulateParameters_16 = value;
		Il2CppCodeGenWriteBarrier(&___PopulateParameters_16, value);
	}

	inline static int32_t get_offset_of_PopulateStatements_17() { return static_cast<int32_t>(offsetof(CodeMemberMethod_t3833357554, ___PopulateStatements_17)); }
	inline EventHandler_t1348719766 * get_PopulateStatements_17() const { return ___PopulateStatements_17; }
	inline EventHandler_t1348719766 ** get_address_of_PopulateStatements_17() { return &___PopulateStatements_17; }
	inline void set_PopulateStatements_17(EventHandler_t1348719766 * value)
	{
		___PopulateStatements_17 = value;
		Il2CppCodeGenWriteBarrier(&___PopulateStatements_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
