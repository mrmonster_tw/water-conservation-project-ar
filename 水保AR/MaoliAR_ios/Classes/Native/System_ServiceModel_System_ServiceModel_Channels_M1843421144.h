﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_M3589678829.h"
#include "System_ServiceModel_System_ServiceModel_QueueTrans3996023584.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.MsmqTransportBindingElement
struct  MsmqTransportBindingElement_t1843421144  : public MsmqBindingElementBase_t3589678829
{
public:
	// System.Int32 System.ServiceModel.Channels.MsmqTransportBindingElement::max_pool_size
	int32_t ___max_pool_size_15;
	// System.ServiceModel.QueueTransferProtocol System.ServiceModel.Channels.MsmqTransportBindingElement::queue_tr_protocol
	int32_t ___queue_tr_protocol_16;
	// System.Boolean System.ServiceModel.Channels.MsmqTransportBindingElement::use_ad
	bool ___use_ad_17;

public:
	inline static int32_t get_offset_of_max_pool_size_15() { return static_cast<int32_t>(offsetof(MsmqTransportBindingElement_t1843421144, ___max_pool_size_15)); }
	inline int32_t get_max_pool_size_15() const { return ___max_pool_size_15; }
	inline int32_t* get_address_of_max_pool_size_15() { return &___max_pool_size_15; }
	inline void set_max_pool_size_15(int32_t value)
	{
		___max_pool_size_15 = value;
	}

	inline static int32_t get_offset_of_queue_tr_protocol_16() { return static_cast<int32_t>(offsetof(MsmqTransportBindingElement_t1843421144, ___queue_tr_protocol_16)); }
	inline int32_t get_queue_tr_protocol_16() const { return ___queue_tr_protocol_16; }
	inline int32_t* get_address_of_queue_tr_protocol_16() { return &___queue_tr_protocol_16; }
	inline void set_queue_tr_protocol_16(int32_t value)
	{
		___queue_tr_protocol_16 = value;
	}

	inline static int32_t get_offset_of_use_ad_17() { return static_cast<int32_t>(offsetof(MsmqTransportBindingElement_t1843421144, ___use_ad_17)); }
	inline bool get_use_ad_17() const { return ___use_ad_17; }
	inline bool* get_address_of_use_ad_17() { return &___use_ad_17; }
	inline void set_use_ad_17(bool value)
	{
		___use_ad_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
