﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_UI_TemplateParser24149626.h"

// System.IO.TextReader
struct TextReader_t283511965;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.UI.ApplicationFileParser
struct  ApplicationFileParser_t2181270929  : public TemplateParser_t24149626
{
public:
	// System.IO.TextReader System.Web.UI.ApplicationFileParser::reader
	TextReader_t283511965 * ___reader_57;

public:
	inline static int32_t get_offset_of_reader_57() { return static_cast<int32_t>(offsetof(ApplicationFileParser_t2181270929, ___reader_57)); }
	inline TextReader_t283511965 * get_reader_57() const { return ___reader_57; }
	inline TextReader_t283511965 ** get_address_of_reader_57() { return &___reader_57; }
	inline void set_reader_57(TextReader_t283511965 * value)
	{
		___reader_57 = value;
		Il2CppCodeGenWriteBarrier(&___reader_57, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
