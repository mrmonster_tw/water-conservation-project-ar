﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sprite_Changer
struct  Sprite_Changer_t2983274214  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Sprite_Changer::nextImage
	GameObject_t1113636619 * ___nextImage_2;
	// System.Int32 Sprite_Changer::Ask_Numbers
	int32_t ___Ask_Numbers_3;

public:
	inline static int32_t get_offset_of_nextImage_2() { return static_cast<int32_t>(offsetof(Sprite_Changer_t2983274214, ___nextImage_2)); }
	inline GameObject_t1113636619 * get_nextImage_2() const { return ___nextImage_2; }
	inline GameObject_t1113636619 ** get_address_of_nextImage_2() { return &___nextImage_2; }
	inline void set_nextImage_2(GameObject_t1113636619 * value)
	{
		___nextImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___nextImage_2, value);
	}

	inline static int32_t get_offset_of_Ask_Numbers_3() { return static_cast<int32_t>(offsetof(Sprite_Changer_t2983274214, ___Ask_Numbers_3)); }
	inline int32_t get_Ask_Numbers_3() const { return ___Ask_Numbers_3; }
	inline int32_t* get_address_of_Ask_Numbers_3() { return &___Ask_Numbers_3; }
	inline void set_Ask_Numbers_3(int32_t value)
	{
		___Ask_Numbers_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
