﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Image
struct Image_t2670269651;
// Level_Controller
struct Level_Controller_t1433348427;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SendCompleteInfo
struct  SendCompleteInfo_t2296039845  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image SendCompleteInfo::Info
	Image_t2670269651 * ___Info_2;
	// System.Boolean SendCompleteInfo::complete
	bool ___complete_3;
	// System.Boolean SendCompleteInfo::checkimg
	bool ___checkimg_4;
	// Level_Controller SendCompleteInfo::MyCheckController
	Level_Controller_t1433348427 * ___MyCheckController_5;

public:
	inline static int32_t get_offset_of_Info_2() { return static_cast<int32_t>(offsetof(SendCompleteInfo_t2296039845, ___Info_2)); }
	inline Image_t2670269651 * get_Info_2() const { return ___Info_2; }
	inline Image_t2670269651 ** get_address_of_Info_2() { return &___Info_2; }
	inline void set_Info_2(Image_t2670269651 * value)
	{
		___Info_2 = value;
		Il2CppCodeGenWriteBarrier(&___Info_2, value);
	}

	inline static int32_t get_offset_of_complete_3() { return static_cast<int32_t>(offsetof(SendCompleteInfo_t2296039845, ___complete_3)); }
	inline bool get_complete_3() const { return ___complete_3; }
	inline bool* get_address_of_complete_3() { return &___complete_3; }
	inline void set_complete_3(bool value)
	{
		___complete_3 = value;
	}

	inline static int32_t get_offset_of_checkimg_4() { return static_cast<int32_t>(offsetof(SendCompleteInfo_t2296039845, ___checkimg_4)); }
	inline bool get_checkimg_4() const { return ___checkimg_4; }
	inline bool* get_address_of_checkimg_4() { return &___checkimg_4; }
	inline void set_checkimg_4(bool value)
	{
		___checkimg_4 = value;
	}

	inline static int32_t get_offset_of_MyCheckController_5() { return static_cast<int32_t>(offsetof(SendCompleteInfo_t2296039845, ___MyCheckController_5)); }
	inline Level_Controller_t1433348427 * get_MyCheckController_5() const { return ___MyCheckController_5; }
	inline Level_Controller_t1433348427 ** get_address_of_MyCheckController_5() { return &___MyCheckController_5; }
	inline void set_MyCheckController_5(Level_Controller_t1433348427 * value)
	{
		___MyCheckController_5 = value;
		Il2CppCodeGenWriteBarrier(&___MyCheckController_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
