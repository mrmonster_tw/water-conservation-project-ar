﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_System_ServiceModel_Channels_T4020609293.h"

// System.ServiceModel.Channels.TcpChannelInfo
struct TcpChannelInfo_t709592533;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Channels.TcpChannelFactory`1<System.Object>
struct  TcpChannelFactory_1_t850047787  : public TransportChannelFactoryBase_1_t4020609293
{
public:
	// System.ServiceModel.Channels.TcpChannelInfo System.ServiceModel.Channels.TcpChannelFactory`1::info
	TcpChannelInfo_t709592533 * ___info_15;

public:
	inline static int32_t get_offset_of_info_15() { return static_cast<int32_t>(offsetof(TcpChannelFactory_1_t850047787, ___info_15)); }
	inline TcpChannelInfo_t709592533 * get_info_15() const { return ___info_15; }
	inline TcpChannelInfo_t709592533 ** get_address_of_info_15() { return &___info_15; }
	inline void set_info_15(TcpChannelInfo_t709592533 * value)
	{
		___info_15 = value;
		Il2CppCodeGenWriteBarrier(&___info_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
