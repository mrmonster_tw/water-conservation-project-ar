﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Collections_ObjectModel_Collection1428300678.h"

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t2760654312;
// System.Collections.Generic.List`1<System.Runtime.Serialization.SerializationMap>
struct List_1_t1265181259;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.KnownTypeCollection
struct  KnownTypeCollection_t3509697551  : public Collection_1_t1428300678
{
public:
	// System.Collections.Generic.List`1<System.Runtime.Serialization.SerializationMap> System.Runtime.Serialization.KnownTypeCollection::contracts
	List_1_t1265181259 * ___contracts_24;

public:
	inline static int32_t get_offset_of_contracts_24() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551, ___contracts_24)); }
	inline List_1_t1265181259 * get_contracts_24() const { return ___contracts_24; }
	inline List_1_t1265181259 ** get_address_of_contracts_24() { return &___contracts_24; }
	inline void set_contracts_24(List_1_t1265181259 * value)
	{
		___contracts_24 = value;
		Il2CppCodeGenWriteBarrier(&___contracts_24, value);
	}
};

struct KnownTypeCollection_t3509697551_StaticFields
{
public:
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::any_type
	XmlQualifiedName_t2760654312 * ___any_type_2;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::bool_type
	XmlQualifiedName_t2760654312 * ___bool_type_3;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::byte_type
	XmlQualifiedName_t2760654312 * ___byte_type_4;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::date_type
	XmlQualifiedName_t2760654312 * ___date_type_5;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::decimal_type
	XmlQualifiedName_t2760654312 * ___decimal_type_6;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::double_type
	XmlQualifiedName_t2760654312 * ___double_type_7;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::float_type
	XmlQualifiedName_t2760654312 * ___float_type_8;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::string_type
	XmlQualifiedName_t2760654312 * ___string_type_9;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::short_type
	XmlQualifiedName_t2760654312 * ___short_type_10;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::int_type
	XmlQualifiedName_t2760654312 * ___int_type_11;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::long_type
	XmlQualifiedName_t2760654312 * ___long_type_12;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::ubyte_type
	XmlQualifiedName_t2760654312 * ___ubyte_type_13;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::ushort_type
	XmlQualifiedName_t2760654312 * ___ushort_type_14;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::uint_type
	XmlQualifiedName_t2760654312 * ___uint_type_15;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::ulong_type
	XmlQualifiedName_t2760654312 * ___ulong_type_16;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::any_uri_type
	XmlQualifiedName_t2760654312 * ___any_uri_type_17;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::base64_type
	XmlQualifiedName_t2760654312 * ___base64_type_18;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::duration_type
	XmlQualifiedName_t2760654312 * ___duration_type_19;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::qname_type
	XmlQualifiedName_t2760654312 * ___qname_type_20;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::char_type
	XmlQualifiedName_t2760654312 * ___char_type_21;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::guid_type
	XmlQualifiedName_t2760654312 * ___guid_type_22;
	// System.Xml.XmlQualifiedName System.Runtime.Serialization.KnownTypeCollection::dbnull_type
	XmlQualifiedName_t2760654312 * ___dbnull_type_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Serialization.KnownTypeCollection::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Serialization.KnownTypeCollection::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_26;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Serialization.KnownTypeCollection::<>f__switch$map2
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map2_27;

public:
	inline static int32_t get_offset_of_any_type_2() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___any_type_2)); }
	inline XmlQualifiedName_t2760654312 * get_any_type_2() const { return ___any_type_2; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_any_type_2() { return &___any_type_2; }
	inline void set_any_type_2(XmlQualifiedName_t2760654312 * value)
	{
		___any_type_2 = value;
		Il2CppCodeGenWriteBarrier(&___any_type_2, value);
	}

	inline static int32_t get_offset_of_bool_type_3() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___bool_type_3)); }
	inline XmlQualifiedName_t2760654312 * get_bool_type_3() const { return ___bool_type_3; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_bool_type_3() { return &___bool_type_3; }
	inline void set_bool_type_3(XmlQualifiedName_t2760654312 * value)
	{
		___bool_type_3 = value;
		Il2CppCodeGenWriteBarrier(&___bool_type_3, value);
	}

	inline static int32_t get_offset_of_byte_type_4() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___byte_type_4)); }
	inline XmlQualifiedName_t2760654312 * get_byte_type_4() const { return ___byte_type_4; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_byte_type_4() { return &___byte_type_4; }
	inline void set_byte_type_4(XmlQualifiedName_t2760654312 * value)
	{
		___byte_type_4 = value;
		Il2CppCodeGenWriteBarrier(&___byte_type_4, value);
	}

	inline static int32_t get_offset_of_date_type_5() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___date_type_5)); }
	inline XmlQualifiedName_t2760654312 * get_date_type_5() const { return ___date_type_5; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_date_type_5() { return &___date_type_5; }
	inline void set_date_type_5(XmlQualifiedName_t2760654312 * value)
	{
		___date_type_5 = value;
		Il2CppCodeGenWriteBarrier(&___date_type_5, value);
	}

	inline static int32_t get_offset_of_decimal_type_6() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___decimal_type_6)); }
	inline XmlQualifiedName_t2760654312 * get_decimal_type_6() const { return ___decimal_type_6; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_decimal_type_6() { return &___decimal_type_6; }
	inline void set_decimal_type_6(XmlQualifiedName_t2760654312 * value)
	{
		___decimal_type_6 = value;
		Il2CppCodeGenWriteBarrier(&___decimal_type_6, value);
	}

	inline static int32_t get_offset_of_double_type_7() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___double_type_7)); }
	inline XmlQualifiedName_t2760654312 * get_double_type_7() const { return ___double_type_7; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_double_type_7() { return &___double_type_7; }
	inline void set_double_type_7(XmlQualifiedName_t2760654312 * value)
	{
		___double_type_7 = value;
		Il2CppCodeGenWriteBarrier(&___double_type_7, value);
	}

	inline static int32_t get_offset_of_float_type_8() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___float_type_8)); }
	inline XmlQualifiedName_t2760654312 * get_float_type_8() const { return ___float_type_8; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_float_type_8() { return &___float_type_8; }
	inline void set_float_type_8(XmlQualifiedName_t2760654312 * value)
	{
		___float_type_8 = value;
		Il2CppCodeGenWriteBarrier(&___float_type_8, value);
	}

	inline static int32_t get_offset_of_string_type_9() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___string_type_9)); }
	inline XmlQualifiedName_t2760654312 * get_string_type_9() const { return ___string_type_9; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_string_type_9() { return &___string_type_9; }
	inline void set_string_type_9(XmlQualifiedName_t2760654312 * value)
	{
		___string_type_9 = value;
		Il2CppCodeGenWriteBarrier(&___string_type_9, value);
	}

	inline static int32_t get_offset_of_short_type_10() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___short_type_10)); }
	inline XmlQualifiedName_t2760654312 * get_short_type_10() const { return ___short_type_10; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_short_type_10() { return &___short_type_10; }
	inline void set_short_type_10(XmlQualifiedName_t2760654312 * value)
	{
		___short_type_10 = value;
		Il2CppCodeGenWriteBarrier(&___short_type_10, value);
	}

	inline static int32_t get_offset_of_int_type_11() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___int_type_11)); }
	inline XmlQualifiedName_t2760654312 * get_int_type_11() const { return ___int_type_11; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_int_type_11() { return &___int_type_11; }
	inline void set_int_type_11(XmlQualifiedName_t2760654312 * value)
	{
		___int_type_11 = value;
		Il2CppCodeGenWriteBarrier(&___int_type_11, value);
	}

	inline static int32_t get_offset_of_long_type_12() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___long_type_12)); }
	inline XmlQualifiedName_t2760654312 * get_long_type_12() const { return ___long_type_12; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_long_type_12() { return &___long_type_12; }
	inline void set_long_type_12(XmlQualifiedName_t2760654312 * value)
	{
		___long_type_12 = value;
		Il2CppCodeGenWriteBarrier(&___long_type_12, value);
	}

	inline static int32_t get_offset_of_ubyte_type_13() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___ubyte_type_13)); }
	inline XmlQualifiedName_t2760654312 * get_ubyte_type_13() const { return ___ubyte_type_13; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_ubyte_type_13() { return &___ubyte_type_13; }
	inline void set_ubyte_type_13(XmlQualifiedName_t2760654312 * value)
	{
		___ubyte_type_13 = value;
		Il2CppCodeGenWriteBarrier(&___ubyte_type_13, value);
	}

	inline static int32_t get_offset_of_ushort_type_14() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___ushort_type_14)); }
	inline XmlQualifiedName_t2760654312 * get_ushort_type_14() const { return ___ushort_type_14; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_ushort_type_14() { return &___ushort_type_14; }
	inline void set_ushort_type_14(XmlQualifiedName_t2760654312 * value)
	{
		___ushort_type_14 = value;
		Il2CppCodeGenWriteBarrier(&___ushort_type_14, value);
	}

	inline static int32_t get_offset_of_uint_type_15() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___uint_type_15)); }
	inline XmlQualifiedName_t2760654312 * get_uint_type_15() const { return ___uint_type_15; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_uint_type_15() { return &___uint_type_15; }
	inline void set_uint_type_15(XmlQualifiedName_t2760654312 * value)
	{
		___uint_type_15 = value;
		Il2CppCodeGenWriteBarrier(&___uint_type_15, value);
	}

	inline static int32_t get_offset_of_ulong_type_16() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___ulong_type_16)); }
	inline XmlQualifiedName_t2760654312 * get_ulong_type_16() const { return ___ulong_type_16; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_ulong_type_16() { return &___ulong_type_16; }
	inline void set_ulong_type_16(XmlQualifiedName_t2760654312 * value)
	{
		___ulong_type_16 = value;
		Il2CppCodeGenWriteBarrier(&___ulong_type_16, value);
	}

	inline static int32_t get_offset_of_any_uri_type_17() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___any_uri_type_17)); }
	inline XmlQualifiedName_t2760654312 * get_any_uri_type_17() const { return ___any_uri_type_17; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_any_uri_type_17() { return &___any_uri_type_17; }
	inline void set_any_uri_type_17(XmlQualifiedName_t2760654312 * value)
	{
		___any_uri_type_17 = value;
		Il2CppCodeGenWriteBarrier(&___any_uri_type_17, value);
	}

	inline static int32_t get_offset_of_base64_type_18() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___base64_type_18)); }
	inline XmlQualifiedName_t2760654312 * get_base64_type_18() const { return ___base64_type_18; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_base64_type_18() { return &___base64_type_18; }
	inline void set_base64_type_18(XmlQualifiedName_t2760654312 * value)
	{
		___base64_type_18 = value;
		Il2CppCodeGenWriteBarrier(&___base64_type_18, value);
	}

	inline static int32_t get_offset_of_duration_type_19() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___duration_type_19)); }
	inline XmlQualifiedName_t2760654312 * get_duration_type_19() const { return ___duration_type_19; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_duration_type_19() { return &___duration_type_19; }
	inline void set_duration_type_19(XmlQualifiedName_t2760654312 * value)
	{
		___duration_type_19 = value;
		Il2CppCodeGenWriteBarrier(&___duration_type_19, value);
	}

	inline static int32_t get_offset_of_qname_type_20() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___qname_type_20)); }
	inline XmlQualifiedName_t2760654312 * get_qname_type_20() const { return ___qname_type_20; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_qname_type_20() { return &___qname_type_20; }
	inline void set_qname_type_20(XmlQualifiedName_t2760654312 * value)
	{
		___qname_type_20 = value;
		Il2CppCodeGenWriteBarrier(&___qname_type_20, value);
	}

	inline static int32_t get_offset_of_char_type_21() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___char_type_21)); }
	inline XmlQualifiedName_t2760654312 * get_char_type_21() const { return ___char_type_21; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_char_type_21() { return &___char_type_21; }
	inline void set_char_type_21(XmlQualifiedName_t2760654312 * value)
	{
		___char_type_21 = value;
		Il2CppCodeGenWriteBarrier(&___char_type_21, value);
	}

	inline static int32_t get_offset_of_guid_type_22() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___guid_type_22)); }
	inline XmlQualifiedName_t2760654312 * get_guid_type_22() const { return ___guid_type_22; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_guid_type_22() { return &___guid_type_22; }
	inline void set_guid_type_22(XmlQualifiedName_t2760654312 * value)
	{
		___guid_type_22 = value;
		Il2CppCodeGenWriteBarrier(&___guid_type_22, value);
	}

	inline static int32_t get_offset_of_dbnull_type_23() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___dbnull_type_23)); }
	inline XmlQualifiedName_t2760654312 * get_dbnull_type_23() const { return ___dbnull_type_23; }
	inline XmlQualifiedName_t2760654312 ** get_address_of_dbnull_type_23() { return &___dbnull_type_23; }
	inline void set_dbnull_type_23(XmlQualifiedName_t2760654312 * value)
	{
		___dbnull_type_23 = value;
		Il2CppCodeGenWriteBarrier(&___dbnull_type_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_25() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___U3CU3Ef__switchU24map0_25)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_25() const { return ___U3CU3Ef__switchU24map0_25; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_25() { return &___U3CU3Ef__switchU24map0_25; }
	inline void set_U3CU3Ef__switchU24map0_25(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_26() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___U3CU3Ef__switchU24map1_26)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_26() const { return ___U3CU3Ef__switchU24map1_26; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_26() { return &___U3CU3Ef__switchU24map1_26; }
	inline void set_U3CU3Ef__switchU24map1_26(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_27() { return static_cast<int32_t>(offsetof(KnownTypeCollection_t3509697551_StaticFields, ___U3CU3Ef__switchU24map2_27)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map2_27() const { return ___U3CU3Ef__switchU24map2_27; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map2_27() { return &___U3CU3Ef__switchU24map2_27; }
	inline void set_U3CU3Ef__switchU24map2_27(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map2_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
