﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.List`1<System.ServiceModel.FaultReasonText>
struct List_1_t724458495;
// System.Collections.Generic.SynchronizedReadOnlyCollection`1<System.ServiceModel.FaultReasonText>
struct SynchronizedReadOnlyCollection_1_t1625918761;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.FaultReason
struct  FaultReason_t2245999938  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.ServiceModel.FaultReasonText> System.ServiceModel.FaultReason::trans
	List_1_t724458495 * ___trans_0;
	// System.Collections.Generic.SynchronizedReadOnlyCollection`1<System.ServiceModel.FaultReasonText> System.ServiceModel.FaultReason::public_trans
	SynchronizedReadOnlyCollection_1_t1625918761 * ___public_trans_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(FaultReason_t2245999938, ___trans_0)); }
	inline List_1_t724458495 * get_trans_0() const { return ___trans_0; }
	inline List_1_t724458495 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(List_1_t724458495 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier(&___trans_0, value);
	}

	inline static int32_t get_offset_of_public_trans_1() { return static_cast<int32_t>(offsetof(FaultReason_t2245999938, ___public_trans_1)); }
	inline SynchronizedReadOnlyCollection_1_t1625918761 * get_public_trans_1() const { return ___public_trans_1; }
	inline SynchronizedReadOnlyCollection_1_t1625918761 ** get_address_of_public_trans_1() { return &___public_trans_1; }
	inline void set_public_trans_1(SynchronizedReadOnlyCollection_1_t1625918761 * value)
	{
		___public_trans_1 = value;
		Il2CppCodeGenWriteBarrier(&___public_trans_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
