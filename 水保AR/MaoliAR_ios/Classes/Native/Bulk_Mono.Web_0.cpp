﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "Mono_Web_U3CModuleU3E692745525.h"
#include "Mono_Web_Mono_Web_Util_SettingsMapping162400643.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator787956054.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "mscorlib_System_Type2483944760.h"
#include "mscorlib_System_String1847450689.h"
#include "System_System_ComponentModel_EnumConverter1688858217.h"
#include "mscorlib_System_Object3080106164.h"
#include "System_System_ComponentModel_TypeConverter2249118273.h"
#include "mscorlib_System_Int322950945753.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingPlatform242231579.h"
#include "mscorlib_System_Boolean97287965.h"
#include "mscorlib_System_InvalidOperationException56020091.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2935062715.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingWhat1462987973.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator3667290188.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingManager1609949884.h"
#include "mscorlib_System_PlatformID897822290.h"
#include "mscorlib_System_OperatingSystem3730783609.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606747707.h"
#include "mscorlib_System_StringComparison3657712135.h"
#include "System_System_Collections_Specialized_NameValueColl407452768.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge132545152.h"
#include "mscorlib_System_AppDomainSetup123196401.h"
#include "mscorlib_System_AppDomain1571427825.h"
#include "mscorlib_System_Exception1436737249.h"
#include "System_Xml_System_Xml_XPath_XPathDocument1673143697.h"
#include "mscorlib_System_ApplicationException2339761290.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingWhatOperatio2264203516.h"
#include "Mono_Web_Mono_Web_Util_SettingsMappingWhatContents3035976441.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2736202052.h"
#include "mscorlib_System_Collections_Generic_List_1_gen213083887.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1632706988.h"
#include "System_Xml_System_Xml_XPath_XPathItem4250588140.h"

// Mono.Web.Util.SettingsMapping
struct SettingsMapping_t162400643;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t787956054;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.ComponentModel.EnumConverter
struct EnumConverter_t1688858217;
// System.ComponentModel.TypeConverter
struct TypeConverter_t2249118273;
// System.String
struct String_t;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhat>
struct List_1_t2935062715;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// Mono.Web.Util.SettingsMappingWhat
struct SettingsMappingWhat_t1462987973;
// Mono.Web.Util.SettingsMappingManager
struct SettingsMappingManager_t1609949884;
// System.OperatingSystem
struct OperatingSystem_t3730783609;
// System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping>
struct Dictionary_2_t2606747707;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// System.AppDomain
struct AppDomain_t1571427825;
// System.AppDomainSetup
struct AppDomainSetup_t123196401;
// System.Xml.XPath.XPathDocument
struct XPathDocument_t1673143697;
// System.ApplicationException
struct ApplicationException_t2339761290;
// System.Exception
struct Exception_t1436737249;
// System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhatContents>
struct List_1_t213083887;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3384741;
// Mono.Web.Util.SettingsMappingWhatContents
struct SettingsMappingWhatContents_t3035976441;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
extern const Il2CppType* SettingsMappingPlatform_t242231579_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* EnumConverter_t1688858217_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3045068588;
extern Il2CppCodeGenString* _stringLiteral2852594079;
extern Il2CppCodeGenString* _stringLiteral38630993;
extern const uint32_t SettingsMapping__ctor_m1031401458_MetadataUsageId;
extern const uint32_t SettingsMapping_get_SectionType_m1339900675_MetadataUsageId;
extern const Il2CppType* ISectionSettingsMapper_t700051055_0_0_0_var;
extern Il2CppClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2446077969;
extern const uint32_t SettingsMapping_get_MapperType_m4292266596_MetadataUsageId;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern Il2CppClass* ISectionSettingsMapper_t700051055_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3243520166;
extern Il2CppCodeGenString* _stringLiteral2021392300;
extern const uint32_t SettingsMapping_MapSection_m3400563575_MetadataUsageId;
extern Il2CppClass* List_1_t2935062715_il2cpp_TypeInfo_var;
extern Il2CppClass* SettingsMappingWhat_t1462987973_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m498646632_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2645597586_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2587866895;
extern const uint32_t SettingsMapping_LoadContents_m1428328068_MetadataUsageId;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t1605229823_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2093328143;
extern const uint32_t SettingsMappingManager__cctor_m2057122072_MetadataUsageId;
extern const uint32_t SettingsMappingManager_get_IsRunningOnWindows_m2279500381_MetadataUsageId;
extern const MethodInfo* Dictionary_2_get_Count_m496412577_MethodInfo_var;
extern const uint32_t SettingsMappingManager_get_HasMappings_m3927615084_MetadataUsageId;
extern Il2CppClass* WebConfigurationManager_t2451561378_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t132545152_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m518943619_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1394531641;
extern Il2CppCodeGenString* _stringLiteral1747235521;
extern Il2CppCodeGenString* _stringLiteral4002445229;
extern const uint32_t SettingsMappingManager_Init_m2535688069_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3570349736;
extern const uint32_t SettingsMappingManager_LoadMappings_m2438702594_MetadataUsageId;
extern Il2CppClass* XPathDocument_t1673143697_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1436737249_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t2339761290_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2606747707_il2cpp_TypeInfo_var;
extern Il2CppClass* SettingsMapping_t162400643_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2784141040_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m494501331_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2602161479_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2178838266_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m2249713395_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4045477590;
extern Il2CppCodeGenString* _stringLiteral870795415;
extern Il2CppCodeGenString* _stringLiteral151233591;
extern const uint32_t SettingsMappingManager_LoadMappings_m3484196195_MetadataUsageId;
extern const MethodInfo* Dictionary_2_TryGetValue_m3280774074_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m3615294064_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2387223709_MethodInfo_var;
extern const uint32_t SettingsMappingManager_MapSection_m2542008484_MetadataUsageId;
extern const MethodInfo* Dictionary_2_TryGetValue_m811555400_MethodInfo_var;
extern const uint32_t SettingsMappingManager_MapSection_m155973185_MetadataUsageId;
extern Il2CppClass* List_1_t213083887_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2736202052_il2cpp_TypeInfo_var;
extern Il2CppClass* SettingsMappingWhatContents_t3035976441_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2715731410_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2392909825_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m282647386_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1013208020_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2004102038_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3493618073;
extern Il2CppCodeGenString* _stringLiteral297723178;
extern Il2CppCodeGenString* _stringLiteral3551364850;
extern Il2CppCodeGenString* _stringLiteral3265744053;
extern Il2CppCodeGenString* _stringLiteral555474;
extern Il2CppCodeGenString* _stringLiteral13686479;
extern const uint32_t SettingsMappingWhat__ctor_m1787333801_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t1632706988_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3167535709_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3143216913_MethodInfo_var;
extern const uint32_t SettingsMappingWhatContents__ctor_m2589085261_MetadataUsageId;



// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3919933788_gshared (Dictionary_2_t132545152 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m1938428402_gshared (Dictionary_2_t132545152 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m3615294064_gshared (Dictionary_2_t132545152 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m2387223709_gshared (Dictionary_2_t132545152 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m3474379962_gshared (Dictionary_2_t132545152 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3280774074_gshared (Dictionary_2_t132545152 * __this, Il2CppObject * p0, Il2CppObject ** p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m182537451_gshared (Dictionary_2_t3384741 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1279427033_gshared (Dictionary_2_t3384741 * __this, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3959998165_gshared (Dictionary_2_t3384741 * __this, Il2CppObject * p0, int32_t* p1, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.EnumConverter::.ctor(System.Type)
extern "C"  void EnumConverter__ctor_m4001517758 (EnumConverter_t1688858217 * __this, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.TypeConverter::ConvertFromInvariantString(System.String)
extern "C"  Il2CppObject * TypeConverter_ConvertFromInvariantString_m1039388749 (TypeConverter_t2249118273 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Web.Util.SettingsMapping::LoadContents(System.Xml.XPath.XPathNavigator)
extern "C"  void SettingsMapping_LoadContents_m1428328068 (SettingsMapping_t162400643 * __this, XPathNavigator_t787956054 * ___nav0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetType(System.String,System.Boolean)
extern "C"  Type_t * Type_GetType_m3605423543 (Il2CppObject * __this /* static, unused */, String_t* p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Mono.Web.Util.SettingsMapping::get_SectionType()
extern "C"  Type_t * SettingsMapping_get_SectionType_m1339900675 (SettingsMapping_t162400643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m1216717135 (ArgumentException_t132251570 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Mono.Web.Util.SettingsMapping::get_MapperType()
extern "C"  Type_t * SettingsMapping_get_MapperType_m4292266596 (SettingsMapping_t162400643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type)
extern "C"  Il2CppObject * Activator_CreateInstance_m3631483688 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhat>::.ctor()
#define List_1__ctor_m498646632(__this, method) ((  void (*) (List_1_t2935062715 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void Mono.Web.Util.SettingsMappingWhat::.ctor(System.Xml.XPath.XPathNavigator)
extern "C"  void SettingsMappingWhat__ctor_m1787333801 (SettingsMappingWhat_t1462987973 * __this, XPathNavigator_t787956054 * ___nav0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhat>::Add(!0)
#define List_1_Add_m2645597586(__this, p0, method) ((  void (*) (List_1_t2935062715 *, SettingsMappingWhat_t1462987973 *, const MethodInfo*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.String System.Runtime.InteropServices.RuntimeEnvironment::get_SystemConfigurationFile()
extern "C"  String_t* RuntimeEnvironment_get_SystemConfigurationFile_m4061971677 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::GetDirectoryName(System.String)
extern "C"  String_t* Path_GetDirectoryName_m3496866581 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.Path::Combine(System.String,System.String)
extern "C"  String_t* Path_Combine_m3389272516 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.OperatingSystem System.Environment::get_OSVersion()
extern "C"  OperatingSystem_t3730783609 * Environment_get_OSVersion_m961136977 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.PlatformID System.OperatingSystem::get_Platform()
extern "C"  int32_t OperatingSystem_get_Platform_m2793423729 (OperatingSystem_t3730783609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping>::get_Count()
#define Dictionary_2_get_Count_m496412577(__this, method) ((  int32_t (*) (Dictionary_2_t2606747707 *, const MethodInfo*))Dictionary_2_get_Count_m3919933788_gshared)(__this, method)
// System.String System.Environment::GetEnvironmentVariable(System.String)
extern "C"  String_t* Environment_GetEnvironmentVariable_m394552009 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.NameValueCollection System.Web.Configuration.WebConfigurationManager::get_AppSettings()
extern "C"  NameValueCollection_t407452768 * WebConfigurationManager_get_AppSettings_m298294418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Specialized.NameValueCollection::get_Item(System.String)
extern "C"  String_t* NameValueCollection_get_Item_m1249025201 (NameValueCollection_t407452768 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::Compare(System.String,System.String,System.StringComparison)
extern "C"  int32_t String_Compare_m3203413707 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Web.Util.SettingsMappingManager::get_IsRunningOnWindows()
extern "C"  bool SettingsMappingManager_get_IsRunningOnWindows_m2279500381 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Web.Util.SettingsMappingManager::.ctor()
extern "C"  void SettingsMappingManager__ctor_m2834454242 (SettingsMappingManager_t1609949884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Web.Util.SettingsMappingManager::LoadMappings()
extern "C"  void SettingsMappingManager_LoadMappings_m2438702594 (SettingsMappingManager_t1609949884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Web.Util.SettingsMappingManager::get_HasMappings()
extern "C"  bool SettingsMappingManager_get_HasMappings_m3927615084 (SettingsMappingManager_t1609949884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
#define Dictionary_2__ctor_m518943619(__this, method) ((  void (*) (Dictionary_2_t132545152 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Boolean System.IO.File::Exists(System.String)
extern "C"  bool File_Exists_m3943585060 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Web.Util.SettingsMappingManager::LoadMappings(System.String)
extern "C"  void SettingsMappingManager_LoadMappings_m3484196195 (SettingsMappingManager_t1609949884 * __this, String_t* ___mappingFilePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::get_CurrentDomain()
extern "C"  AppDomain_t1571427825 * AppDomain_get_CurrentDomain_m182766250 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomainSetup System.AppDomain::get_SetupInformation()
extern "C"  AppDomainSetup_t123196401 * AppDomain_get_SetupInformation_m3308136943 (AppDomain_t1571427825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomainSetup::get_ApplicationBase()
extern "C"  String_t* AppDomainSetup_get_ApplicationBase_m3595447654 (AppDomainSetup_t123196401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathDocument::.ctor(System.String)
extern "C"  void XPathDocument__ctor_m1637441265 (XPathDocument_t1673143697 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathDocument::CreateNavigator()
extern "C"  XPathNavigator_t787956054 * XPathDocument_CreateNavigator_m1454500353 (XPathDocument_t1673143697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ApplicationException::.ctor(System.String,System.Exception)
extern "C"  void ApplicationException__ctor_m692455299 (ApplicationException_t2339761290 * __this, String_t* p0, Exception_t1436737249 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping>::.ctor()
#define Dictionary_2__ctor_m2784141040(__this, method) ((  void (*) (Dictionary_2_t2606747707 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping>::Clear()
#define Dictionary_2_Clear_m494501331(__this, method) ((  void (*) (Dictionary_2_t2606747707 *, const MethodInfo*))Dictionary_2_Clear_m1938428402_gshared)(__this, method)
// System.Void Mono.Web.Util.SettingsMapping::.ctor(System.Xml.XPath.XPathNavigator)
extern "C"  void SettingsMapping__ctor_m1031401458 (SettingsMapping_t162400643 * __this, XPathNavigator_t787956054 * ___nav0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Web.Util.SettingsMappingPlatform Mono.Web.Util.SettingsMapping::get_Platform()
extern "C"  int32_t SettingsMapping_get_Platform_m2921929333 (SettingsMapping_t162400643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m2602161479(__this, p0, method) ((  bool (*) (Dictionary_2_t2606747707 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m3615294064_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping>::Add(!0,!1)
#define Dictionary_2_Add_m2178838266(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2606747707 *, Type_t *, SettingsMapping_t162400643 *, const MethodInfo*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m2249713395(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2606747707 *, Type_t *, SettingsMapping_t162400643 *, const MethodInfo*))Dictionary_2_set_Item_m3474379962_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3280774074(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t132545152 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3280774074_gshared)(__this, p0, p1, method)
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Web.Util.SettingsMappingManager::MapSection(System.Object,System.Type)
extern "C"  Il2CppObject * SettingsMappingManager_MapSection_m155973185 (SettingsMappingManager_t1609949884 * __this, Il2CppObject * ___input0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2249409497 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m3615294064(__this, p0, method) ((  bool (*) (Dictionary_2_t132545152 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m3615294064_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
#define Dictionary_2_Add_m2387223709(__this, p0, p1, method) ((  void (*) (Dictionary_2_t132545152 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m3585316909 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Mono.Web.Util.SettingsMapping>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m811555400(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t2606747707 *, Type_t *, SettingsMapping_t162400643 **, const MethodInfo*))Dictionary_2_TryGetValue_m3280774074_gshared)(__this, p0, p1, method)
// System.Object Mono.Web.Util.SettingsMapping::MapSection(System.Object,System.Type)
extern "C"  Il2CppObject * SettingsMapping_MapSection_m3400563575 (SettingsMapping_t162400643 * __this, Il2CppObject * ___input0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhatContents>::.ctor()
#define List_1__ctor_m2715731410(__this, method) ((  void (*) (List_1_t213083887 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2392909825(__this, p0, method) ((  void (*) (Dictionary_2_t2736202052 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m182537451_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1)
#define Dictionary_2_Add_m282647386(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2736202052 *, String_t*, int32_t, const MethodInfo*))Dictionary_2_Add_m1279427033_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1013208020(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t2736202052 *, String_t*, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m3959998165_gshared)(__this, p0, p1, method)
// System.Void Mono.Web.Util.SettingsMappingWhatContents::.ctor(System.Xml.XPath.XPathNavigator,Mono.Web.Util.SettingsMappingWhatOperation)
extern "C"  void SettingsMappingWhatContents__ctor_m2589085261 (SettingsMappingWhatContents_t3035976441 * __this, XPathNavigator_t787956054 * ___nav0, int32_t ___operation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhatContents>::Add(!0)
#define List_1_Add_m2004102038(__this, p0, method) ((  void (*) (List_1_t213083887 *, SettingsMappingWhatContents_t3035976441 *, const MethodInfo*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
#define Dictionary_2__ctor_m3167535709(__this, method) ((  void (*) (Dictionary_2_t1632706988 *, const MethodInfo*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1)
#define Dictionary_2_Add_m3143216913(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1632706988 *, String_t*, String_t*, const MethodInfo*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.Web.Util.SettingsMapping::.ctor(System.Xml.XPath.XPathNavigator)
extern "C"  void SettingsMapping__ctor_m1031401458 (SettingsMapping_t162400643 * __this, XPathNavigator_t787956054 * ___nav0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMapping__ctor_m1031401458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EnumConverter_t1688858217 * V_0 = NULL;
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		XPathNavigator_t787956054 * L_0 = ___nav0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(30 /* System.String System.Xml.XPath.XPathNavigator::GetAttribute(System.String,System.String) */, L_0, _stringLiteral3045068588, L_1);
		__this->set__sectionTypeName_0(L_2);
		XPathNavigator_t787956054 * L_3 = ___nav0;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(30 /* System.String System.Xml.XPath.XPathNavigator::GetAttribute(System.String,System.String) */, L_3, _stringLiteral2852594079, L_4);
		__this->set__mapperTypeName_2(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(SettingsMappingPlatform_t242231579_0_0_0_var), /*hidden argument*/NULL);
		EnumConverter_t1688858217 * L_7 = (EnumConverter_t1688858217 *)il2cpp_codegen_object_new(EnumConverter_t1688858217_il2cpp_TypeInfo_var);
		EnumConverter__ctor_m4001517758(L_7, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		EnumConverter_t1688858217 * L_8 = V_0;
		XPathNavigator_t787956054 * L_9 = ___nav0;
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_9);
		String_t* L_11 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(30 /* System.String System.Xml.XPath.XPathNavigator::GetAttribute(System.String,System.String) */, L_9, _stringLiteral38630993, L_10);
		NullCheck(L_8);
		Il2CppObject * L_12 = TypeConverter_ConvertFromInvariantString_m1039388749(L_8, L_11, /*hidden argument*/NULL);
		__this->set__platform_4(((*(int32_t*)((int32_t*)UnBox(L_12, Int32_t2950945753_il2cpp_TypeInfo_var)))));
		XPathNavigator_t787956054 * L_13 = ___nav0;
		SettingsMapping_LoadContents_m1428328068(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Mono.Web.Util.SettingsMapping::get_SectionType()
extern "C"  Type_t * SettingsMapping_get_SectionType_m1339900675 (SettingsMapping_t162400643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMapping_get_SectionType_m1339900675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = __this->get__sectionType_1();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_1 = __this->get__sectionTypeName_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m3605423543, L_1, (bool)0, "Mono.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756");
		__this->set__sectionType_1(L_2);
	}

IL_001d:
	{
		Type_t * L_3 = __this->get__sectionType_1();
		return L_3;
	}
}
// System.Type Mono.Web.Util.SettingsMapping::get_MapperType()
extern "C"  Type_t * SettingsMapping_get_MapperType_m4292266596 (SettingsMapping_t162400643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMapping_get_MapperType_m4292266596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = __this->get__mapperType_3();
		if (L_0)
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_1 = __this->get__mapperTypeName_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m3605423543, L_1, (bool)1, "Mono.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756");
		__this->set__mapperType_3(L_2);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(ISectionSettingsMapper_t700051055_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_4 = __this->get__mapperType_3();
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, Type_t * >::Invoke(47 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_3, L_4);
		if (L_5)
		{
			goto IL_0049;
		}
	}
	{
		__this->set__mapperType_3((Type_t *)NULL);
		InvalidOperationException_t56020091 * L_6 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_6, _stringLiteral2446077969, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0049:
	{
		Type_t * L_7 = __this->get__mapperType_3();
		return L_7;
	}
}
// Mono.Web.Util.SettingsMappingPlatform Mono.Web.Util.SettingsMapping::get_Platform()
extern "C"  int32_t SettingsMapping_get_Platform_m2921929333 (SettingsMapping_t162400643 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__platform_4();
		return L_0;
	}
}
// System.Object Mono.Web.Util.SettingsMapping::MapSection(System.Object,System.Type)
extern "C"  Il2CppObject * SettingsMapping_MapSection_m3400563575 (SettingsMapping_t162400643 * __this, Il2CppObject * ___input0, Type_t * ___type1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMapping_MapSection_m3400563575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Type_t * L_0 = ___type1;
		Type_t * L_1 = SettingsMapping_get_SectionType_m1339900675(__this, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		ArgumentException_t132251570 * L_2 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1216717135(L_2, _stringLiteral3243520166, _stringLiteral2021392300, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		Type_t * L_3 = SettingsMapping_get_MapperType_m4292266596(__this, /*hidden argument*/NULL);
		Il2CppObject * L_4 = Activator_CreateInstance_m3631483688(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = ((Il2CppObject *)IsInst(L_4, ISectionSettingsMapper_t700051055_il2cpp_TypeInfo_var));
		Il2CppObject * L_5 = V_0;
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		Il2CppObject * L_6 = ___input0;
		return L_6;
	}

IL_0035:
	{
		Il2CppObject * L_7 = V_0;
		Il2CppObject * L_8 = ___input0;
		List_1_t2935062715 * L_9 = __this->get__whats_5();
		NullCheck(L_7);
		Il2CppObject * L_10 = InterfaceFuncInvoker2< Il2CppObject *, Il2CppObject *, List_1_t2935062715 * >::Invoke(0 /* System.Object Mono.Web.Util.ISectionSettingsMapper::MapSection(System.Object,System.Collections.Generic.List`1<Mono.Web.Util.SettingsMappingWhat>) */, ISectionSettingsMapper_t700051055_il2cpp_TypeInfo_var, L_7, L_8, L_9);
		return L_10;
	}
}
// System.Void Mono.Web.Util.SettingsMapping::LoadContents(System.Xml.XPath.XPathNavigator)
extern "C"  void SettingsMapping_LoadContents_m1428328068 (SettingsMapping_t162400643 * __this, XPathNavigator_t787956054 * ___nav0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMapping_LoadContents_m1428328068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNodeIterator_t3667290188 * V_0 = NULL;
	{
		XPathNavigator_t787956054 * L_0 = ___nav0;
		NullCheck(L_0);
		XPathNodeIterator_t3667290188 * L_1 = VirtFuncInvoker1< XPathNodeIterator_t3667290188 *, String_t* >::Invoke(48 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.String) */, L_0, _stringLiteral2587866895);
		V_0 = L_1;
		List_1_t2935062715 * L_2 = (List_1_t2935062715 *)il2cpp_codegen_object_new(List_1_t2935062715_il2cpp_TypeInfo_var);
		List_1__ctor_m498646632(L_2, /*hidden argument*/List_1__ctor_m498646632_MethodInfo_var);
		__this->set__whats_5(L_2);
		goto IL_0032;
	}

IL_001c:
	{
		List_1_t2935062715 * L_3 = __this->get__whats_5();
		XPathNodeIterator_t3667290188 * L_4 = V_0;
		NullCheck(L_4);
		XPathNavigator_t787956054 * L_5 = VirtFuncInvoker0< XPathNavigator_t787956054 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		SettingsMappingWhat_t1462987973 * L_6 = (SettingsMappingWhat_t1462987973 *)il2cpp_codegen_object_new(SettingsMappingWhat_t1462987973_il2cpp_TypeInfo_var);
		SettingsMappingWhat__ctor_m1787333801(L_6, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Add_m2645597586(L_3, L_6, /*hidden argument*/List_1_Add_m2645597586_MethodInfo_var);
	}

IL_0032:
	{
		XPathNodeIterator_t3667290188 * L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_7);
		if (L_8)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}
}
// System.Void Mono.Web.Util.SettingsMappingManager::.ctor()
extern "C"  void SettingsMappingManager__ctor_m2834454242 (SettingsMappingManager_t1609949884 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Web.Util.SettingsMappingManager::.cctor()
extern "C"  void SettingsMappingManager__cctor_m2057122072 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingManager__cctor_m2057122072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_0, /*hidden argument*/NULL);
		((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->set_mapperLock_0(L_0);
		String_t* L_1 = RuntimeEnvironment_get_SystemConfigurationFile_m4061971677(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_2 = Path_GetDirectoryName_m3496866581(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_3 = Path_Combine_m3389272516(NULL /*static, unused*/, L_2, _stringLiteral2093328143, /*hidden argument*/NULL);
		((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->set__mappingFile_2(L_3);
		OperatingSystem_t3730783609 * L_4 = Environment_get_OSVersion_m961136977(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = OperatingSystem_get_Platform_m2793423729(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)128))))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)4)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_0;
		G_B4_0 = ((((int32_t)((((int32_t)L_8) == ((int32_t)6))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004a;
	}

IL_0049:
	{
		G_B4_0 = 0;
	}

IL_004a:
	{
		((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->set__runningOnWindows_6((bool)G_B4_0);
		return;
	}
}
// System.Boolean Mono.Web.Util.SettingsMappingManager::get_IsRunningOnWindows()
extern "C"  bool SettingsMappingManager_get_IsRunningOnWindows_m2279500381 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingManager_get_IsRunningOnWindows_m2279500381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		bool L_0 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__runningOnWindows_6();
		return L_0;
	}
}
// System.Boolean Mono.Web.Util.SettingsMappingManager::get_HasMappings()
extern "C"  bool SettingsMappingManager_get_HasMappings_m3927615084 (SettingsMappingManager_t1609949884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingManager_get_HasMappings_m3927615084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Dictionary_2_t2606747707 * L_0 = __this->get__mappers_3();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Dictionary_2_t2606747707 * L_1 = __this->get__mappers_3();
		NullCheck(L_1);
		int32_t L_2 = Dictionary_2_get_Count_m496412577(L_1, /*hidden argument*/Dictionary_2_get_Count_m496412577_MethodInfo_var);
		G_B3_0 = ((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Void Mono.Web.Util.SettingsMappingManager::Init()
extern "C"  void SettingsMappingManager_Init_m2535688069 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingManager_Init_m2535688069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NameValueCollection_t407452768 * V_0 = NULL;
	String_t* V_1 = NULL;
	SettingsMappingManager_t1609949884 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		SettingsMappingManager_t1609949884 * L_0 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__instance_1();
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_1 = Environment_GetEnvironmentVariable_m394552009(NULL /*static, unused*/, _stringLiteral1394531641, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		return;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebConfigurationManager_t2451561378_il2cpp_TypeInfo_var);
		NameValueCollection_t407452768 * L_2 = WebConfigurationManager_get_AppSettings_m298294418(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		NameValueCollection_t407452768 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		NameValueCollection_t407452768 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = NameValueCollection_get_Item_m1249025201(L_4, _stringLiteral1747235521, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_7 = String_Compare_m3203413707(NULL /*static, unused*/, L_6, _stringLiteral4002445229, 5, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0045;
		}
	}
	{
		return;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		bool L_8 = SettingsMappingManager_get_IsRunningOnWindows_m2279500381(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->set__myPlatform_5(0);
		goto IL_0060;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->set__myPlatform_5(1);
	}

IL_0060:
	{
		SettingsMappingManager_t1609949884 * L_9 = (SettingsMappingManager_t1609949884 *)il2cpp_codegen_object_new(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		SettingsMappingManager__ctor_m2834454242(L_9, /*hidden argument*/NULL);
		V_2 = L_9;
		SettingsMappingManager_t1609949884 * L_10 = V_2;
		NullCheck(L_10);
		SettingsMappingManager_LoadMappings_m2438702594(L_10, /*hidden argument*/NULL);
		SettingsMappingManager_t1609949884 * L_11 = V_2;
		NullCheck(L_11);
		bool L_12 = SettingsMappingManager_get_HasMappings_m3927615084(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0087;
		}
	}
	{
		SettingsMappingManager_t1609949884 * L_13 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->set__instance_1(L_13);
		Dictionary_2_t132545152 * L_14 = (Dictionary_2_t132545152 *)il2cpp_codegen_object_new(Dictionary_2_t132545152_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m518943619(L_14, /*hidden argument*/Dictionary_2__ctor_m518943619_MethodInfo_var);
		((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->set__mappedSections_4(L_14);
	}

IL_0087:
	{
		return;
	}
}
// System.Void Mono.Web.Util.SettingsMappingManager::LoadMappings()
extern "C"  void SettingsMappingManager_LoadMappings_m2438702594 (SettingsMappingManager_t1609949884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingManager_LoadMappings_m2438702594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AppDomainSetup_t123196401 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		String_t* L_0 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__mappingFile_2();
		bool L_1 = File_Exists_m3943585060(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		String_t* L_2 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__mappingFile_2();
		SettingsMappingManager_LoadMappings_m3484196195(__this, L_2, /*hidden argument*/NULL);
	}

IL_001a:
	{
		AppDomain_t1571427825 * L_3 = AppDomain_get_CurrentDomain_m182766250(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AppDomainSetup_t123196401 * L_4 = AppDomain_get_SetupInformation_m3308136943(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		AppDomainSetup_t123196401 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = AppDomainSetup_get_ApplicationBase_m3595447654(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1605229823_il2cpp_TypeInfo_var);
		String_t* L_7 = Path_Combine_m3389272516(NULL /*static, unused*/, L_6, _stringLiteral3570349736, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = V_1;
		bool L_9 = File_Exists_m3943585060(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_10 = V_1;
		SettingsMappingManager_LoadMappings_m3484196195(__this, L_10, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void Mono.Web.Util.SettingsMappingManager::LoadMappings(System.String)
extern "C"  void SettingsMappingManager_LoadMappings_m3484196195 (SettingsMappingManager_t1609949884 * __this, String_t* ___mappingFilePath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingManager_LoadMappings_m3484196195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t787956054 * V_0 = NULL;
	XPathDocument_t1673143697 * V_1 = NULL;
	Exception_t1436737249 * V_2 = NULL;
	XPathNodeIterator_t3667290188 * V_3 = NULL;
	SettingsMapping_t162400643 * V_4 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___mappingFilePath0;
		XPathDocument_t1673143697 * L_1 = (XPathDocument_t1673143697 *)il2cpp_codegen_object_new(XPathDocument_t1673143697_il2cpp_TypeInfo_var);
		XPathDocument__ctor_m1637441265(L_1, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		XPathDocument_t1673143697 * L_2 = V_1;
		NullCheck(L_2);
		XPathNavigator_t787956054 * L_3 = XPathDocument_CreateNavigator_m1454500353(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0025;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1436737249 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1436737249_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t1436737249 *)__exception_local);
			Exception_t1436737249 * L_4 = V_2;
			ApplicationException_t2339761290 * L_5 = (ApplicationException_t2339761290 *)il2cpp_codegen_object_new(ApplicationException_t2339761290_il2cpp_TypeInfo_var);
			ApplicationException__ctor_m692455299(L_5, _stringLiteral4045477590, L_4, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
		}

IL_0020:
		{
			goto IL_0025;
		}
	} // end catch (depth: 1)

IL_0025:
	{
		Dictionary_2_t2606747707 * L_6 = __this->get__mappers_3();
		if (L_6)
		{
			goto IL_0040;
		}
	}
	{
		Dictionary_2_t2606747707 * L_7 = (Dictionary_2_t2606747707 *)il2cpp_codegen_object_new(Dictionary_2_t2606747707_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2784141040(L_7, /*hidden argument*/Dictionary_2__ctor_m2784141040_MethodInfo_var);
		__this->set__mappers_3(L_7);
		goto IL_0062;
	}

IL_0040:
	{
		XPathNavigator_t787956054 * L_8 = V_0;
		NullCheck(L_8);
		XPathNodeIterator_t3667290188 * L_9 = VirtFuncInvoker1< XPathNodeIterator_t3667290188 *, String_t* >::Invoke(48 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.String) */, L_8, _stringLiteral870795415);
		V_3 = L_9;
		XPathNodeIterator_t3667290188 * L_10 = V_3;
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_10);
		if (!L_11)
		{
			goto IL_0062;
		}
	}
	{
		Dictionary_2_t2606747707 * L_12 = __this->get__mappers_3();
		NullCheck(L_12);
		Dictionary_2_Clear_m494501331(L_12, /*hidden argument*/Dictionary_2_Clear_m494501331_MethodInfo_var);
	}

IL_0062:
	{
		XPathNavigator_t787956054 * L_13 = V_0;
		NullCheck(L_13);
		XPathNodeIterator_t3667290188 * L_14 = VirtFuncInvoker1< XPathNodeIterator_t3667290188 *, String_t* >::Invoke(48 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.String) */, L_13, _stringLiteral151233591);
		V_3 = L_14;
		goto IL_00da;
	}

IL_0073:
	{
		XPathNodeIterator_t3667290188 * L_15 = V_3;
		NullCheck(L_15);
		XPathNavigator_t787956054 * L_16 = VirtFuncInvoker0< XPathNavigator_t787956054 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_15);
		SettingsMapping_t162400643 * L_17 = (SettingsMapping_t162400643 *)il2cpp_codegen_object_new(SettingsMapping_t162400643_il2cpp_TypeInfo_var);
		SettingsMapping__ctor_m1031401458(L_17, L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		int32_t L_18 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__myPlatform_5();
		SettingsMapping_t162400643 * L_19 = V_4;
		NullCheck(L_19);
		int32_t L_20 = SettingsMapping_get_Platform_m2921929333(L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_18) == ((int32_t)L_20)))
		{
			goto IL_0096;
		}
	}
	{
		goto IL_00da;
	}

IL_0096:
	{
		Dictionary_2_t2606747707 * L_21 = __this->get__mappers_3();
		SettingsMapping_t162400643 * L_22 = V_4;
		NullCheck(L_22);
		Type_t * L_23 = SettingsMapping_get_SectionType_m1339900675(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		bool L_24 = Dictionary_2_ContainsKey_m2602161479(L_21, L_23, /*hidden argument*/Dictionary_2_ContainsKey_m2602161479_MethodInfo_var);
		if (L_24)
		{
			goto IL_00c6;
		}
	}
	{
		Dictionary_2_t2606747707 * L_25 = __this->get__mappers_3();
		SettingsMapping_t162400643 * L_26 = V_4;
		NullCheck(L_26);
		Type_t * L_27 = SettingsMapping_get_SectionType_m1339900675(L_26, /*hidden argument*/NULL);
		SettingsMapping_t162400643 * L_28 = V_4;
		NullCheck(L_25);
		Dictionary_2_Add_m2178838266(L_25, L_27, L_28, /*hidden argument*/Dictionary_2_Add_m2178838266_MethodInfo_var);
		goto IL_00da;
	}

IL_00c6:
	{
		Dictionary_2_t2606747707 * L_29 = __this->get__mappers_3();
		SettingsMapping_t162400643 * L_30 = V_4;
		NullCheck(L_30);
		Type_t * L_31 = SettingsMapping_get_SectionType_m1339900675(L_30, /*hidden argument*/NULL);
		SettingsMapping_t162400643 * L_32 = V_4;
		NullCheck(L_29);
		Dictionary_2_set_Item_m2249713395(L_29, L_31, L_32, /*hidden argument*/Dictionary_2_set_Item_m2249713395_MethodInfo_var);
	}

IL_00da:
	{
		XPathNodeIterator_t3667290188 * L_33 = V_3;
		NullCheck(L_33);
		bool L_34 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_33);
		if (L_34)
		{
			goto IL_0073;
		}
	}
	{
		return;
	}
}
// System.Object Mono.Web.Util.SettingsMappingManager::MapSection(System.Object)
extern "C"  Il2CppObject * SettingsMappingManager_MapSection_m2542008484 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingManager_MapSection_m2542008484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		SettingsMappingManager_t1609949884 * L_0 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__instance_1();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Il2CppObject * L_1 = ___input0;
		if (L_1)
		{
			goto IL_0012;
		}
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___input0;
		return L_2;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		Dictionary_2_t132545152 * L_3 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__mappedSections_4();
		Il2CppObject * L_4 = ___input0;
		NullCheck(L_3);
		bool L_5 = Dictionary_2_TryGetValue_m3280774074(L_3, L_4, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3280774074_MethodInfo_var);
		if (!L_5)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_6 = V_0;
		return L_6;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
		SettingsMappingManager_t1609949884 * L_7 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__instance_1();
		Il2CppObject * L_8 = ___input0;
		Il2CppObject * L_9 = ___input0;
		NullCheck(L_9);
		Type_t * L_10 = Object_GetType_m88164663(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		Il2CppObject * L_11 = SettingsMappingManager_MapSection_m155973185(L_7, L_8, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		Il2CppObject * L_12 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get_mapperLock_0();
		V_2 = L_12;
		Il2CppObject * L_13 = V_2;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_14 = V_1;
			if (!L_14)
			{
				goto IL_0066;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
			Dictionary_2_t132545152 * L_15 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__mappedSections_4();
			Il2CppObject * L_16 = V_1;
			NullCheck(L_15);
			bool L_17 = Dictionary_2_ContainsKey_m3615294064(L_15, L_16, /*hidden argument*/Dictionary_2_ContainsKey_m3615294064_MethodInfo_var);
			if (L_17)
			{
				goto IL_0066;
			}
		}

IL_005a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var);
			Dictionary_2_t132545152 * L_18 = ((SettingsMappingManager_t1609949884_StaticFields*)SettingsMappingManager_t1609949884_il2cpp_TypeInfo_var->static_fields)->get__mappedSections_4();
			Il2CppObject * L_19 = V_1;
			Il2CppObject * L_20 = V_1;
			NullCheck(L_18);
			Dictionary_2_Add_m2387223709(L_18, L_19, L_20, /*hidden argument*/Dictionary_2_Add_m2387223709_MethodInfo_var);
		}

IL_0066:
		{
			IL2CPP_LEAVE(0x72, FINALLY_006b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_006b;
	}

FINALLY_006b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_21 = V_2;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(107)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(107)
	{
		IL2CPP_JUMP_TBL(0x72, IL_0072)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0072:
	{
		Il2CppObject * L_22 = V_1;
		return L_22;
	}
}
// System.Object Mono.Web.Util.SettingsMappingManager::MapSection(System.Object,System.Type)
extern "C"  Il2CppObject * SettingsMappingManager_MapSection_m155973185 (SettingsMappingManager_t1609949884 * __this, Il2CppObject * ___input0, Type_t * ___type1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingManager_MapSection_m155973185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SettingsMapping_t162400643 * V_0 = NULL;
	{
		Dictionary_2_t2606747707 * L_0 = __this->get__mappers_3();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		Dictionary_2_t2606747707 * L_1 = __this->get__mappers_3();
		NullCheck(L_1);
		int32_t L_2 = Dictionary_2_get_Count_m496412577(L_1, /*hidden argument*/Dictionary_2_get_Count_m496412577_MethodInfo_var);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Dictionary_2_t2606747707 * L_3 = __this->get__mappers_3();
		Type_t * L_4 = ___type1;
		NullCheck(L_3);
		bool L_5 = Dictionary_2_ContainsKey_m2602161479(L_3, L_4, /*hidden argument*/Dictionary_2_ContainsKey_m2602161479_MethodInfo_var);
		if (L_5)
		{
			goto IL_002e;
		}
	}

IL_002c:
	{
		Il2CppObject * L_6 = ___input0;
		return L_6;
	}

IL_002e:
	{
		Dictionary_2_t2606747707 * L_7 = __this->get__mappers_3();
		Type_t * L_8 = ___type1;
		NullCheck(L_7);
		bool L_9 = Dictionary_2_TryGetValue_m811555400(L_7, L_8, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m811555400_MethodInfo_var);
		if (L_9)
		{
			goto IL_0043;
		}
	}
	{
		Il2CppObject * L_10 = ___input0;
		return L_10;
	}

IL_0043:
	{
		SettingsMapping_t162400643 * L_11 = V_0;
		if (L_11)
		{
			goto IL_004b;
		}
	}
	{
		Il2CppObject * L_12 = ___input0;
		return L_12;
	}

IL_004b:
	{
		SettingsMapping_t162400643 * L_13 = V_0;
		Il2CppObject * L_14 = ___input0;
		Type_t * L_15 = ___type1;
		NullCheck(L_13);
		Il2CppObject * L_16 = SettingsMapping_MapSection_m3400563575(L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void Mono.Web.Util.SettingsMappingWhat::.ctor(System.Xml.XPath.XPathNavigator)
extern "C"  void SettingsMappingWhat__ctor_m1787333801 (SettingsMappingWhat_t1462987973 * __this, XPathNavigator_t787956054 * ___nav0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingWhat__ctor_m1787333801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNodeIterator_t3667290188 * V_0 = NULL;
	XPathNavigator_t787956054 * V_1 = NULL;
	String_t* V_2 = NULL;
	Dictionary_2_t2736202052 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		XPathNavigator_t787956054 * L_0 = ___nav0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(30 /* System.String System.Xml.XPath.XPathNavigator::GetAttribute(System.String,System.String) */, L_0, _stringLiteral3493618073, L_1);
		__this->set__value_0(L_2);
		XPathNavigator_t787956054 * L_3 = ___nav0;
		NullCheck(L_3);
		XPathNodeIterator_t3667290188 * L_4 = VirtFuncInvoker1< XPathNodeIterator_t3667290188 *, String_t* >::Invoke(48 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.String) */, L_3, _stringLiteral297723178);
		V_0 = L_4;
		List_1_t213083887 * L_5 = (List_1_t213083887 *)il2cpp_codegen_object_new(List_1_t213083887_il2cpp_TypeInfo_var);
		List_1__ctor_m2715731410(L_5, /*hidden argument*/List_1__ctor_m2715731410_MethodInfo_var);
		__this->set__contents_1(L_5);
		goto IL_011d;
	}

IL_0038:
	{
		XPathNodeIterator_t3667290188 * L_6 = V_0;
		NullCheck(L_6);
		XPathNavigator_t787956054 * L_7 = VirtFuncInvoker0< XPathNavigator_t787956054 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_6);
		V_1 = L_7;
		XPathNavigator_t787956054 * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_8);
		V_2 = L_9;
		String_t* L_10 = V_2;
		if (!L_10)
		{
			goto IL_011d;
		}
	}
	{
		Dictionary_2_t2736202052 * L_11 = ((SettingsMappingWhat_t1462987973_StaticFields*)SettingsMappingWhat_t1462987973_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_2();
		if (L_11)
		{
			goto IL_0093;
		}
	}
	{
		Dictionary_2_t2736202052 * L_12 = (Dictionary_2_t2736202052 *)il2cpp_codegen_object_new(Dictionary_2_t2736202052_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2392909825(L_12, 4, /*hidden argument*/Dictionary_2__ctor_m2392909825_MethodInfo_var);
		V_3 = L_12;
		Dictionary_2_t2736202052 * L_13 = V_3;
		NullCheck(L_13);
		Dictionary_2_Add_m282647386(L_13, _stringLiteral3551364850, 0, /*hidden argument*/Dictionary_2_Add_m282647386_MethodInfo_var);
		Dictionary_2_t2736202052 * L_14 = V_3;
		NullCheck(L_14);
		Dictionary_2_Add_m282647386(L_14, _stringLiteral3265744053, 1, /*hidden argument*/Dictionary_2_Add_m282647386_MethodInfo_var);
		Dictionary_2_t2736202052 * L_15 = V_3;
		NullCheck(L_15);
		Dictionary_2_Add_m282647386(L_15, _stringLiteral555474, 2, /*hidden argument*/Dictionary_2_Add_m282647386_MethodInfo_var);
		Dictionary_2_t2736202052 * L_16 = V_3;
		NullCheck(L_16);
		Dictionary_2_Add_m282647386(L_16, _stringLiteral13686479, 3, /*hidden argument*/Dictionary_2_Add_m282647386_MethodInfo_var);
		Dictionary_2_t2736202052 * L_17 = V_3;
		((SettingsMappingWhat_t1462987973_StaticFields*)SettingsMappingWhat_t1462987973_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_2(L_17);
	}

IL_0093:
	{
		Dictionary_2_t2736202052 * L_18 = ((SettingsMappingWhat_t1462987973_StaticFields*)SettingsMappingWhat_t1462987973_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_2();
		String_t* L_19 = V_2;
		NullCheck(L_18);
		bool L_20 = Dictionary_2_TryGetValue_m1013208020(L_18, L_19, (&V_4), /*hidden argument*/Dictionary_2_TryGetValue_m1013208020_MethodInfo_var);
		if (!L_20)
		{
			goto IL_011d;
		}
	}
	{
		int32_t L_21 = V_4;
		switch (L_21)
		{
			case 0:
			{
				goto IL_00c1;
			}
			case 1:
			{
				goto IL_00d8;
			}
			case 2:
			{
				goto IL_00ef;
			}
			case 3:
			{
				goto IL_0106;
			}
		}
	}
	{
		goto IL_011d;
	}

IL_00c1:
	{
		List_1_t213083887 * L_22 = __this->get__contents_1();
		XPathNavigator_t787956054 * L_23 = V_1;
		SettingsMappingWhatContents_t3035976441 * L_24 = (SettingsMappingWhatContents_t3035976441 *)il2cpp_codegen_object_new(SettingsMappingWhatContents_t3035976441_il2cpp_TypeInfo_var);
		SettingsMappingWhatContents__ctor_m2589085261(L_24, L_23, 2, /*hidden argument*/NULL);
		NullCheck(L_22);
		List_1_Add_m2004102038(L_22, L_24, /*hidden argument*/List_1_Add_m2004102038_MethodInfo_var);
		goto IL_011d;
	}

IL_00d8:
	{
		List_1_t213083887 * L_25 = __this->get__contents_1();
		XPathNavigator_t787956054 * L_26 = V_1;
		SettingsMappingWhatContents_t3035976441 * L_27 = (SettingsMappingWhatContents_t3035976441 *)il2cpp_codegen_object_new(SettingsMappingWhatContents_t3035976441_il2cpp_TypeInfo_var);
		SettingsMappingWhatContents__ctor_m2589085261(L_27, L_26, 0, /*hidden argument*/NULL);
		NullCheck(L_25);
		List_1_Add_m2004102038(L_25, L_27, /*hidden argument*/List_1_Add_m2004102038_MethodInfo_var);
		goto IL_011d;
	}

IL_00ef:
	{
		List_1_t213083887 * L_28 = __this->get__contents_1();
		XPathNavigator_t787956054 * L_29 = V_1;
		SettingsMappingWhatContents_t3035976441 * L_30 = (SettingsMappingWhatContents_t3035976441 *)il2cpp_codegen_object_new(SettingsMappingWhatContents_t3035976441_il2cpp_TypeInfo_var);
		SettingsMappingWhatContents__ctor_m2589085261(L_30, L_29, 1, /*hidden argument*/NULL);
		NullCheck(L_28);
		List_1_Add_m2004102038(L_28, L_30, /*hidden argument*/List_1_Add_m2004102038_MethodInfo_var);
		goto IL_011d;
	}

IL_0106:
	{
		List_1_t213083887 * L_31 = __this->get__contents_1();
		XPathNavigator_t787956054 * L_32 = V_1;
		SettingsMappingWhatContents_t3035976441 * L_33 = (SettingsMappingWhatContents_t3035976441 *)il2cpp_codegen_object_new(SettingsMappingWhatContents_t3035976441_il2cpp_TypeInfo_var);
		SettingsMappingWhatContents__ctor_m2589085261(L_33, L_32, 3, /*hidden argument*/NULL);
		NullCheck(L_31);
		List_1_Add_m2004102038(L_31, L_33, /*hidden argument*/List_1_Add_m2004102038_MethodInfo_var);
		goto IL_011d;
	}

IL_011d:
	{
		XPathNodeIterator_t3667290188 * L_34 = V_0;
		NullCheck(L_34);
		bool L_35 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_34);
		if (L_35)
		{
			goto IL_0038;
		}
	}
	{
		return;
	}
}
// System.Void Mono.Web.Util.SettingsMappingWhatContents::.ctor(System.Xml.XPath.XPathNavigator,Mono.Web.Util.SettingsMappingWhatOperation)
extern "C"  void SettingsMappingWhatContents__ctor_m2589085261 (SettingsMappingWhatContents_t3035976441 * __this, XPathNavigator_t787956054 * ___nav0, int32_t ___operation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingsMappingWhatContents__ctor_m2589085261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1632706988 * L_0 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3167535709(L_0, /*hidden argument*/Dictionary_2__ctor_m3167535709_MethodInfo_var);
		__this->set__attributes_1(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___operation1;
		__this->set__operation_0(L_1);
		XPathNavigator_t787956054 * L_2 = ___nav0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.XPathNavigator::get_HasAttributes() */, L_2);
		if (!L_3)
		{
			goto IL_0068;
		}
	}
	{
		XPathNavigator_t787956054 * L_4 = ___nav0;
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(39 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, L_4);
		Dictionary_2_t1632706988 * L_5 = __this->get__attributes_1();
		XPathNavigator_t787956054 * L_6 = ___nav0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_6);
		XPathNavigator_t787956054 * L_8 = ___nav0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_8);
		NullCheck(L_5);
		Dictionary_2_Add_m3143216913(L_5, L_7, L_9, /*hidden argument*/Dictionary_2_Add_m3143216913_MethodInfo_var);
		goto IL_005d;
	}

IL_0046:
	{
		Dictionary_2_t1632706988 * L_10 = __this->get__attributes_1();
		XPathNavigator_t787956054 * L_11 = ___nav0;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_11);
		XPathNavigator_t787956054 * L_13 = ___nav0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_13);
		NullCheck(L_10);
		Dictionary_2_Add_m3143216913(L_10, L_12, L_14, /*hidden argument*/Dictionary_2_Add_m3143216913_MethodInfo_var);
	}

IL_005d:
	{
		XPathNavigator_t787956054 * L_15 = ___nav0;
		NullCheck(L_15);
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(44 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, L_15);
		if (L_16)
		{
			goto IL_0046;
		}
	}

IL_0068:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
