﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_ServiceModel_Mono_Security_Protocol_Ntlm_Mes422981883.h"

// System.Byte[]
struct ByteU5BU5D_t4116647657;
// Mono.Security.Protocol.Ntlm.NtlmTargetInformation
struct NtlmTargetInformation_t4089317265;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Ntlm.Type2Message
struct  Type2Message_t2139513824  : public MessageBase_t422981883
{
public:
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::_nonce
	ByteU5BU5D_t4116647657* ____nonce_6;
	// System.Byte[] Mono.Security.Protocol.Ntlm.Type2Message::_context
	ByteU5BU5D_t4116647657* ____context_7;
	// Mono.Security.Protocol.Ntlm.NtlmTargetInformation Mono.Security.Protocol.Ntlm.Type2Message::_target
	NtlmTargetInformation_t4089317265 * ____target_8;
	// System.String Mono.Security.Protocol.Ntlm.Type2Message::_target_name
	String_t* ____target_name_9;

public:
	inline static int32_t get_offset_of__nonce_6() { return static_cast<int32_t>(offsetof(Type2Message_t2139513824, ____nonce_6)); }
	inline ByteU5BU5D_t4116647657* get__nonce_6() const { return ____nonce_6; }
	inline ByteU5BU5D_t4116647657** get_address_of__nonce_6() { return &____nonce_6; }
	inline void set__nonce_6(ByteU5BU5D_t4116647657* value)
	{
		____nonce_6 = value;
		Il2CppCodeGenWriteBarrier(&____nonce_6, value);
	}

	inline static int32_t get_offset_of__context_7() { return static_cast<int32_t>(offsetof(Type2Message_t2139513824, ____context_7)); }
	inline ByteU5BU5D_t4116647657* get__context_7() const { return ____context_7; }
	inline ByteU5BU5D_t4116647657** get_address_of__context_7() { return &____context_7; }
	inline void set__context_7(ByteU5BU5D_t4116647657* value)
	{
		____context_7 = value;
		Il2CppCodeGenWriteBarrier(&____context_7, value);
	}

	inline static int32_t get_offset_of__target_8() { return static_cast<int32_t>(offsetof(Type2Message_t2139513824, ____target_8)); }
	inline NtlmTargetInformation_t4089317265 * get__target_8() const { return ____target_8; }
	inline NtlmTargetInformation_t4089317265 ** get_address_of__target_8() { return &____target_8; }
	inline void set__target_8(NtlmTargetInformation_t4089317265 * value)
	{
		____target_8 = value;
		Il2CppCodeGenWriteBarrier(&____target_8, value);
	}

	inline static int32_t get_offset_of__target_name_9() { return static_cast<int32_t>(offsetof(Type2Message_t2139513824, ____target_name_9)); }
	inline String_t* get__target_name_9() const { return ____target_name_9; }
	inline String_t** get_address_of__target_name_9() { return &____target_name_9; }
	inline void set__target_name_9(String_t* value)
	{
		____target_name_9 = value;
		Il2CppCodeGenWriteBarrier(&____target_name_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
