﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_Configur3010527243.h"
#include "System_Configuration_System_Configuration_Configura372136312.h"
#include "System_Configuration_System_Configuration_Configur1538619295.h"
#include "System_Configuration_System_Configuration_Configura161400359.h"
#include "System_Configuration_System_Configuration_Configur4066281341.h"
#include "System_Configuration_System_Configuration_Configura386529156.h"
#include "System_Configuration_System_Configuration_Configur3590861854.h"
#include "System_Configuration_System_Configuration_Configur2659780707.h"
#include "System_Configuration_System_Configuration_Configur2852175726.h"
#include "System_Configuration_System_Configuration_Configur1059028815.h"
#include "System_Configuration_System_Configuration_Configur1605032206.h"
#include "System_Configuration_System_Configuration_Configur3286633228.h"
#include "System_Configuration_System_Configuration_Configur3156163955.h"
#include "System_Configuration_System_Configuration_Configur2786897858.h"
#include "System_Configuration_System_Configuration_Configur2103655423.h"
#include "System_Configuration_System_Configuration_Configur4179402520.h"
#include "System_Configuration_System_Configuration_Configur1151641153.h"
#include "System_Configuration_System_Configuration_Configur2700976468.h"
#include "System_Configuration_System_Configuration_Configura448955463.h"
#include "System_Configuration_System_Configuration_Configura888490966.h"
#include "System_Configuration_ConfigXmlTextReader3683698114.h"
#include "System_Configuration_System_Configuration_ContextI3569091057.h"
#include "System_Configuration_System_Configuration_DefaultSe752822653.h"
#include "System_Configuration_System_Configuration_DefaultV1354184753.h"
#include "System_Configuration_System_Configuration_ElementI2651568025.h"
#include "System_Configuration_System_Configuration_ExeConfi3751566101.h"
#include "System_Configuration_System_Configuration_GenericEn266649130.h"
#include "System_Configuration_System_Configuration_IgnoreSe2596619786.h"
#include "System_Configuration_System_Configuration_Infinite2860508531.h"
#include "System_Configuration_System_Configuration_Infinite2957091789.h"
#include "System_Configuration_System_Configuration_IntegerVa780581956.h"
#include "System_Configuration_System_Configuration_IntegerV3655374351.h"
#include "System_Configuration_System_Configuration_Internal2066639612.h"
#include "System_Configuration_System_Configuration_InternalC844406905.h"
#include "System_Configuration_System_Configuration_InternalC969478808.h"
#include "System_Configuration_System_Configuration_ExeConfi3079009552.h"
#include "System_Configuration_System_Configuration_Internal3745416310.h"
#include "System_Configuration_System_Configuration_LongVali4166902823.h"
#include "System_Configuration_System_Configuration_LongVali2953091832.h"
#include "System_Configuration_System_Configuration_PositiveT714847674.h"
#include "System_Configuration_System_Configuration_Property4136807793.h"
#include "System_Configuration_System_Configuration_Property3866802480.h"
#include "System_Configuration_System_Configuration_Property3780095919.h"
#include "System_Configuration_System_Configuration_PropertyV225613163.h"
#include "System_Configuration_System_Configuration_ProtectedC25042150.h"
#include "System_Configuration_System_Configuration_Protected413290519.h"
#include "System_Configuration_System_Configuration_Protected729952796.h"
#include "System_Configuration_System_Configuration_Protecte1525901209.h"
#include "System_Configuration_System_Configuration_Provider4011220768.h"
#include "System_Configuration_System_Configuration_Provider3942935190.h"
#include "System_Configuration_System_Configuration_SectionIn640588401.h"
#include "System_Configuration_System_Configuration_SectionGr687896682.h"
#include "System_Configuration_System_Configuration_ConfigInf215413917.h"
#include "System_Configuration_System_Configuration_SectionI2821611020.h"
#include "System_Configuration_System_MonoTODOAttribute4131080581.h"
#include "System_Configuration_System_MonoInternalNoteAttrib2284022217.h"
#include "System_Configuration_System_Configuration_StringVa4226114403.h"
#include "System_Configuration_System_Configuration_StringVal905173687.h"
#include "System_Configuration_System_Configuration_TimeSpanM730211177.h"
#include "System_Configuration_System_Configuration_TimeSpan2683336721.h"
#include "System_Configuration_System_Configuration_TimeSpan1574829041.h"
#include "System_Configuration_System_Configuration_TimeSpan2792648515.h"
#include "System_Configuration_System_Configuration_TimeSpanV679610852.h"
#include "System_Configuration_System_Configuration_TimeSpan3672454306.h"
#include "System_Configuration_System_Configuration_WhiteSpa4027530507.h"
#include "System_Configuration_System_Configuration_Configur4105454811.h"
#include "System_Configuration_System_Configuration_Validato1396763873.h"
#include "System_Security_U3CModuleU3E692745525.h"
#include "System_Security_Locale4128636107.h"
#include "System_Security_Mono_Security_Cryptography_Managed1203008551.h"
#include "System_Security_Mono_Security_Cryptography_NativeD3020714393.h"
#include "System_Security_Mono_Security_Cryptography_NativeDa454421128.h"
#include "System_Security_Mono_Security_Cryptography_NativeD1592014442.h"
#include "System_Security_Mono_Xml_XmlCanonicalizer3076776375.h"
#include "System_Security_Mono_Xml_XmlCanonicalizer_XmlCanoni745999092.h"
#include "System_Security_Mono_Xml_XmlDsigC14NTransformAttri2276634801.h"
#include "System_Security_Mono_Xml_XmlDsigC14NTransformNames3034574274.h"
#include "System_Security_System_Security_Cryptography_DataPr711050909.h"
#include "System_Security_System_Security_Cryptography_Prote1645840789.h"
#include "System_Security_System_Security_Cryptography_Prote2680138268.h"
#include "System_Security_System_Security_Cryptography_Xml_C4021546579.h"
#include "System_Security_System_Security_Cryptography_Xml_Ci859746092.h"
#include "System_Security_System_Security_Cryptography_Xml_D2873365799.h"
#include "System_Security_System_Security_Cryptography_Xml_D3754427214.h"
#include "System_Security_System_Security_Cryptography_Xml_DS682905322.h"
#include "System_Security_System_Security_Cryptography_Xml_E3129875747.h"
#include "System_Security_System_Security_Cryptography_Xml_En805343673.h"
#include "System_Security_System_Security_Cryptography_Xml_E3282913964.h"
#include "System_Security_System_Security_Cryptography_Xml_E2124600183.h"
#include "System_Security_System_Security_Cryptography_Xml_E2455217639.h"
#include "System_Security_System_Security_Cryptography_Xml_E2271876579.h"
#include "System_Security_System_Security_Cryptography_Xml_E3294881567.h"
#include "System_Security_System_Security_Cryptography_Xml_E3099724478.h"
#include "System_Security_System_Security_Cryptography_Xml_K4210275625.h"
#include "System_Security_System_Security_Cryptography_Xml_K3757684699.h"
#include "System_Security_System_Security_Cryptography_Xml_Ke109830476.h"
#include "System_Security_System_Security_Cryptography_Xml_K2325312217.h"
#include "System_Security_System_Security_Cryptography_Xml_K3535559014.h"
#include "System_Security_System_Security_Cryptography_Xml_K2493599240.h"
#include "System_Security_System_Security_Cryptography_Xml_K3389067689.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (ConfigurationFileMap_t3010527243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1300[1] = 
{
	ConfigurationFileMap_t3010527243::get_offset_of_machineConfigFilename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (ConfigurationLocation_t372136312), -1, sizeof(ConfigurationLocation_t372136312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1301[7] = 
{
	ConfigurationLocation_t372136312_StaticFields::get_offset_of_pathTrimChars_0(),
	ConfigurationLocation_t372136312::get_offset_of_path_1(),
	ConfigurationLocation_t372136312::get_offset_of_configuration_2(),
	ConfigurationLocation_t372136312::get_offset_of_parent_3(),
	ConfigurationLocation_t372136312::get_offset_of_xmlContent_4(),
	ConfigurationLocation_t372136312::get_offset_of_parentResolved_5(),
	ConfigurationLocation_t372136312::get_offset_of_allowOverride_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (ConfigurationLocationCollection_t1538619295), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (ConfigurationLockType_t161400359)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1303[4] = 
{
	ConfigurationLockType_t161400359::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (ConfigurationLockCollection_t4066281341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1304[6] = 
{
	ConfigurationLockCollection_t4066281341::get_offset_of_names_0(),
	ConfigurationLockCollection_t4066281341::get_offset_of_element_1(),
	ConfigurationLockCollection_t4066281341::get_offset_of_lockType_2(),
	ConfigurationLockCollection_t4066281341::get_offset_of_is_modified_3(),
	ConfigurationLockCollection_t4066281341::get_offset_of_valid_name_hash_4(),
	ConfigurationLockCollection_t4066281341::get_offset_of_valid_names_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (ConfigurationManager_t386529156), -1, sizeof(ConfigurationManager_t386529156_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1305[3] = 
{
	ConfigurationManager_t386529156_StaticFields::get_offset_of_configFactory_0(),
	ConfigurationManager_t386529156_StaticFields::get_offset_of_configSystem_1(),
	ConfigurationManager_t386529156_StaticFields::get_offset_of_lockobj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (ConfigurationProperty_t3590861854), -1, sizeof(ConfigurationProperty_t3590861854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1306[9] = 
{
	ConfigurationProperty_t3590861854_StaticFields::get_offset_of_NoDefaultValue_0(),
	ConfigurationProperty_t3590861854::get_offset_of_name_1(),
	ConfigurationProperty_t3590861854::get_offset_of_type_2(),
	ConfigurationProperty_t3590861854::get_offset_of_default_value_3(),
	ConfigurationProperty_t3590861854::get_offset_of_converter_4(),
	ConfigurationProperty_t3590861854::get_offset_of_validation_5(),
	ConfigurationProperty_t3590861854::get_offset_of_flags_6(),
	ConfigurationProperty_t3590861854::get_offset_of_description_7(),
	ConfigurationProperty_t3590861854::get_offset_of_collectionAttribute_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (ConfigurationPropertyAttribute_t2659780707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1307[3] = 
{
	ConfigurationPropertyAttribute_t2659780707::get_offset_of_name_0(),
	ConfigurationPropertyAttribute_t2659780707::get_offset_of_default_value_1(),
	ConfigurationPropertyAttribute_t2659780707::get_offset_of_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (ConfigurationPropertyCollection_t2852175726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1308[1] = 
{
	ConfigurationPropertyCollection_t2852175726::get_offset_of_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (ConfigurationPropertyOptions_t1059028815)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1309[5] = 
{
	ConfigurationPropertyOptions_t1059028815::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (ConfigurationSaveMode_t1605032206)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1310[4] = 
{
	ConfigurationSaveMode_t1605032206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (ConfigurationSaveEventArgs_t3286633228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1311[2] = 
{
	ConfigurationSaveEventArgs_t3286633228::get_offset_of_U3CStreamPathU3Ek__BackingField_1(),
	ConfigurationSaveEventArgs_t3286633228::get_offset_of_U3CStartU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (ConfigurationSection_t3156163955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1312[4] = 
{
	ConfigurationSection_t3156163955::get_offset_of_sectionInformation_13(),
	ConfigurationSection_t3156163955::get_offset_of_section_handler_14(),
	ConfigurationSection_t3156163955::get_offset_of_externalDataXml_15(),
	ConfigurationSection_t3156163955::get_offset_of__configContext_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (ConfigurationSectionCollection_t2786897858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1313[2] = 
{
	ConfigurationSectionCollection_t2786897858::get_offset_of_group_10(),
	ConfigurationSectionCollection_t2786897858::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t2103655423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1314[5] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U3CU24s_32U3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U3CkeyU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U24PC_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (ConfigurationSectionGroup_t4179402520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1315[5] = 
{
	ConfigurationSectionGroup_t4179402520::get_offset_of_sections_0(),
	ConfigurationSectionGroup_t4179402520::get_offset_of_groups_1(),
	ConfigurationSectionGroup_t4179402520::get_offset_of_config_2(),
	ConfigurationSectionGroup_t4179402520::get_offset_of_group_3(),
	ConfigurationSectionGroup_t4179402520::get_offset_of_initialized_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (ConfigurationSectionGroupCollection_t1151641153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1316[2] = 
{
	ConfigurationSectionGroupCollection_t1151641153::get_offset_of_group_10(),
	ConfigurationSectionGroupCollection_t1151641153::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (ConfigurationUserLevel_t2700976468)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1317[4] = 
{
	ConfigurationUserLevel_t2700976468::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (ConfigurationValidatorAttribute_t448955463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1318[2] = 
{
	ConfigurationValidatorAttribute_t448955463::get_offset_of_validatorType_0(),
	ConfigurationValidatorAttribute_t448955463::get_offset_of_instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (ConfigurationValidatorBase_t888490966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (ConfigXmlTextReader_t3683698114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1320[1] = 
{
	ConfigXmlTextReader_t3683698114::get_offset_of_fileName_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (ContextInformation_t3569091057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1321[2] = 
{
	ContextInformation_t3569091057::get_offset_of_ctx_0(),
	ContextInformation_t3569091057::get_offset_of_config_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (DefaultSection_t752822653), -1, sizeof(DefaultSection_t752822653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1322[1] = 
{
	DefaultSection_t752822653_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (DefaultValidator_t1354184753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (ElementInformation_t2651568025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1324[3] = 
{
	ElementInformation_t2651568025::get_offset_of_propertyInfo_0(),
	ElementInformation_t2651568025::get_offset_of_owner_1(),
	ElementInformation_t2651568025::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (ExeConfigurationFileMap_t3751566101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1325[3] = 
{
	ExeConfigurationFileMap_t3751566101::get_offset_of_exeConfigFilename_1(),
	ExeConfigurationFileMap_t3751566101::get_offset_of_localUserConfigFilename_2(),
	ExeConfigurationFileMap_t3751566101::get_offset_of_roamingUserConfigFilename_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (GenericEnumConverter_t266649130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1326[1] = 
{
	GenericEnumConverter_t266649130::get_offset_of_typeEnum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (IgnoreSection_t2596619786), -1, sizeof(IgnoreSection_t2596619786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1327[2] = 
{
	IgnoreSection_t2596619786::get_offset_of_xml_17(),
	IgnoreSection_t2596619786_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (InfiniteIntConverter_t2860508531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (InfiniteTimeSpanConverter_t2957091789), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (IntegerValidator_t780581956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1330[4] = 
{
	IntegerValidator_t780581956::get_offset_of_rangeIsExclusive_0(),
	IntegerValidator_t780581956::get_offset_of_minValue_1(),
	IntegerValidator_t780581956::get_offset_of_maxValue_2(),
	IntegerValidator_t780581956::get_offset_of_resolution_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (IntegerValidatorAttribute_t3655374351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1331[4] = 
{
	IntegerValidatorAttribute_t3655374351::get_offset_of_excludeRange_2(),
	IntegerValidatorAttribute_t3655374351::get_offset_of_maxValue_3(),
	IntegerValidatorAttribute_t3655374351::get_offset_of_minValue_4(),
	IntegerValidatorAttribute_t3655374351::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (InternalConfigurationFactory_t2066639612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (InternalConfigurationSystem_t844406905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1333[3] = 
{
	InternalConfigurationSystem_t844406905::get_offset_of_host_0(),
	InternalConfigurationSystem_t844406905::get_offset_of_root_1(),
	InternalConfigurationSystem_t844406905::get_offset_of_hostInitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (InternalConfigurationHost_t969478808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (ExeConfigurationHost_t3079009552), -1, sizeof(ExeConfigurationHost_t3079009552_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1335[3] = 
{
	ExeConfigurationHost_t3079009552::get_offset_of_map_0(),
	ExeConfigurationHost_t3079009552::get_offset_of_level_1(),
	ExeConfigurationHost_t3079009552_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (InternalConfigurationRoot_t3745416310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1336[2] = 
{
	InternalConfigurationRoot_t3745416310::get_offset_of_host_0(),
	InternalConfigurationRoot_t3745416310::get_offset_of_isDesignTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (LongValidator_t4166902823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1337[4] = 
{
	LongValidator_t4166902823::get_offset_of_rangeIsExclusive_0(),
	LongValidator_t4166902823::get_offset_of_minValue_1(),
	LongValidator_t4166902823::get_offset_of_maxValue_2(),
	LongValidator_t4166902823::get_offset_of_resolution_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (LongValidatorAttribute_t2953091832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1338[4] = 
{
	LongValidatorAttribute_t2953091832::get_offset_of_excludeRange_2(),
	LongValidatorAttribute_t2953091832::get_offset_of_maxValue_3(),
	LongValidatorAttribute_t2953091832::get_offset_of_minValue_4(),
	LongValidatorAttribute_t2953091832::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (PositiveTimeSpanValidator_t714847674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (PropertyInformation_t4136807793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1340[5] = 
{
	PropertyInformation_t4136807793::get_offset_of_isModified_0(),
	PropertyInformation_t4136807793::get_offset_of_val_1(),
	PropertyInformation_t4136807793::get_offset_of_origin_2(),
	PropertyInformation_t4136807793::get_offset_of_owner_3(),
	PropertyInformation_t4136807793::get_offset_of_property_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (PropertyInformationCollection_t3866802480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (PropertyInformationEnumerator_t3780095919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1342[2] = 
{
	PropertyInformationEnumerator_t3780095919::get_offset_of_collection_0(),
	PropertyInformationEnumerator_t3780095919::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (PropertyValueOrigin_t225613163)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1343[4] = 
{
	PropertyValueOrigin_t225613163::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (ProtectedConfiguration_t25042150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (ProtectedConfigurationProvider_t413290519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (ProtectedConfigurationProviderCollection_t729952796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (ProtectedConfigurationSection_t1525901209), -1, sizeof(ProtectedConfigurationSection_t1525901209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1347[4] = 
{
	ProtectedConfigurationSection_t1525901209_StaticFields::get_offset_of_defaultProviderProp_17(),
	ProtectedConfigurationSection_t1525901209_StaticFields::get_offset_of_providersProp_18(),
	ProtectedConfigurationSection_t1525901209_StaticFields::get_offset_of_properties_19(),
	ProtectedConfigurationSection_t1525901209::get_offset_of_providers_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (ProviderSettings_t4011220768), -1, sizeof(ProviderSettings_t4011220768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1348[4] = 
{
	ProviderSettings_t4011220768::get_offset_of_parameters_13(),
	ProviderSettings_t4011220768_StaticFields::get_offset_of_nameProp_14(),
	ProviderSettings_t4011220768_StaticFields::get_offset_of_typeProp_15(),
	ProviderSettings_t4011220768_StaticFields::get_offset_of_properties_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (ProviderSettingsCollection_t3942935190), -1, sizeof(ProviderSettingsCollection_t3942935190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1349[1] = 
{
	ProviderSettingsCollection_t3942935190_StaticFields::get_offset_of_props_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (SectionInfo_t640588401), -1, sizeof(SectionInfo_t640588401_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1350[6] = 
{
	SectionInfo_t640588401::get_offset_of_allowLocation_6(),
	SectionInfo_t640588401::get_offset_of_requirePermission_7(),
	SectionInfo_t640588401::get_offset_of_restartOnExternalChanges_8(),
	SectionInfo_t640588401::get_offset_of_allowDefinition_9(),
	SectionInfo_t640588401::get_offset_of_allowExeDefinition_10(),
	SectionInfo_t640588401_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (SectionGroupInfo_t687896682), -1, sizeof(SectionGroupInfo_t687896682_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1351[3] = 
{
	SectionGroupInfo_t687896682::get_offset_of_sections_6(),
	SectionGroupInfo_t687896682::get_offset_of_groups_7(),
	SectionGroupInfo_t687896682_StaticFields::get_offset_of_emptyList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (ConfigInfoCollection_t215413917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (SectionInformation_t2821611020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1353[13] = 
{
	SectionInformation_t2821611020::get_offset_of_parent_0(),
	SectionInformation_t2821611020::get_offset_of_allow_definition_1(),
	SectionInformation_t2821611020::get_offset_of_allow_exe_definition_2(),
	SectionInformation_t2821611020::get_offset_of_allow_location_3(),
	SectionInformation_t2821611020::get_offset_of_allow_override_4(),
	SectionInformation_t2821611020::get_offset_of_inherit_on_child_apps_5(),
	SectionInformation_t2821611020::get_offset_of_restart_on_external_changes_6(),
	SectionInformation_t2821611020::get_offset_of_require_permission_7(),
	SectionInformation_t2821611020::get_offset_of_config_source_8(),
	SectionInformation_t2821611020::get_offset_of_name_9(),
	SectionInformation_t2821611020::get_offset_of_raw_xml_10(),
	SectionInformation_t2821611020::get_offset_of_protection_provider_11(),
	SectionInformation_t2821611020::get_offset_of_U3CConfigFilePathU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (MonoTODOAttribute_t4131080582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1354[1] = 
{
	MonoTODOAttribute_t4131080582::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (MonoInternalNoteAttribute_t2284022217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (StringValidator_t4226114403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1356[3] = 
{
	StringValidator_t4226114403::get_offset_of_invalidCharacters_0(),
	StringValidator_t4226114403::get_offset_of_maxLength_1(),
	StringValidator_t4226114403::get_offset_of_minLength_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (StringValidatorAttribute_t905173687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1357[4] = 
{
	StringValidatorAttribute_t905173687::get_offset_of_invalidCharacters_2(),
	StringValidatorAttribute_t905173687::get_offset_of_maxLength_3(),
	StringValidatorAttribute_t905173687::get_offset_of_minLength_4(),
	StringValidatorAttribute_t905173687::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (TimeSpanMinutesConverter_t730211177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (TimeSpanMinutesOrInfiniteConverter_t2683336721), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (TimeSpanSecondsConverter_t1574829041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (TimeSpanSecondsOrInfiniteConverter_t2792648515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (TimeSpanValidator_t679610852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1362[4] = 
{
	TimeSpanValidator_t679610852::get_offset_of_rangeIsExclusive_0(),
	TimeSpanValidator_t679610852::get_offset_of_minValue_1(),
	TimeSpanValidator_t679610852::get_offset_of_maxValue_2(),
	TimeSpanValidator_t679610852::get_offset_of_resolutionInSeconds_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (TimeSpanValidatorAttribute_t3672454306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1363[4] = 
{
	TimeSpanValidatorAttribute_t3672454306::get_offset_of_excludeRange_2(),
	TimeSpanValidatorAttribute_t3672454306::get_offset_of_maxValueString_3(),
	TimeSpanValidatorAttribute_t3672454306::get_offset_of_minValueString_4(),
	TimeSpanValidatorAttribute_t3672454306::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (WhiteSpaceTrimStringConverter_t4027530507), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (ConfigurationSaveEventHandler_t4105454811), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (ValidatorCallback_t1396763873), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (U3CModuleU3E_t692745528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (Locale_t4128636109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (ManagedProtection_t1203008551), -1, sizeof(ManagedProtection_t1203008551_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1369[2] = 
{
	ManagedProtection_t1203008551_StaticFields::get_offset_of_user_0(),
	ManagedProtection_t1203008551_StaticFields::get_offset_of_machine_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (NativeDapiProtection_t3020714393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (DATA_BLOB_t454421128)+ sizeof (Il2CppObject), sizeof(DATA_BLOB_t454421128 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1371[2] = 
{
	DATA_BLOB_t454421128::get_offset_of_cbData_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DATA_BLOB_t454421128::get_offset_of_pbData_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (CRYPTPROTECT_PROMPTSTRUCT_t1592014442)+ sizeof (Il2CppObject), sizeof(CRYPTPROTECT_PROMPTSTRUCT_t1592014442_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1372[4] = 
{
	CRYPTPROTECT_PROMPTSTRUCT_t1592014442::get_offset_of_cbSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CRYPTPROTECT_PROMPTSTRUCT_t1592014442::get_offset_of_dwPromptFlags_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CRYPTPROTECT_PROMPTSTRUCT_t1592014442::get_offset_of_hwndApp_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CRYPTPROTECT_PROMPTSTRUCT_t1592014442::get_offset_of_szPrompt_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (XmlCanonicalizer_t3076776375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1373[10] = 
{
	XmlCanonicalizer_t3076776375::get_offset_of_comments_0(),
	XmlCanonicalizer_t3076776375::get_offset_of_exclusive_1(),
	XmlCanonicalizer_t3076776375::get_offset_of_inclusiveNamespacesPrefixList_2(),
	XmlCanonicalizer_t3076776375::get_offset_of_xnl_3(),
	XmlCanonicalizer_t3076776375::get_offset_of_res_4(),
	XmlCanonicalizer_t3076776375::get_offset_of_state_5(),
	XmlCanonicalizer_t3076776375::get_offset_of_visibleNamespaces_6(),
	XmlCanonicalizer_t3076776375::get_offset_of_prevVisibleNamespacesStart_7(),
	XmlCanonicalizer_t3076776375::get_offset_of_prevVisibleNamespacesEnd_8(),
	XmlCanonicalizer_t3076776375::get_offset_of_propagatedNss_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (XmlCanonicalizerState_t745999092)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1374[4] = 
{
	XmlCanonicalizerState_t745999092::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (XmlDsigC14NTransformAttributesComparer_t2276634801), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (XmlDsigC14NTransformNamespacesComparer_t3034574274), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (DataProtectionScope_t711050909)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1377[3] = 
{
	DataProtectionScope_t711050909::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (ProtectedData_t1645840789), -1, sizeof(ProtectedData_t1645840789_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1378[1] = 
{
	ProtectedData_t1645840789_StaticFields::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (DataProtectionImplementation_t2680138268)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1379[5] = 
{
	DataProtectionImplementation_t2680138268::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (CipherData_t4021546579), -1, sizeof(CipherData_t4021546579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1380[3] = 
{
	CipherData_t4021546579::get_offset_of_cipherValue_0(),
	CipherData_t4021546579::get_offset_of_cipherReference_1(),
	CipherData_t4021546579_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (CipherReference_t859746092), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (DataObject_t2873365799), -1, sizeof(DataObject_t2873365799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1382[3] = 
{
	DataObject_t2873365799::get_offset_of_element_0(),
	DataObject_t2873365799::get_offset_of_propertyModified_1(),
	DataObject_t2873365799_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (DataReference_t3754427214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (DSAKeyValue_t682905322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1384[1] = 
{
	DSAKeyValue_t682905322::get_offset_of_dsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (EncryptedData_t3129875747), -1, sizeof(EncryptedData_t3129875747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1385[1] = 
{
	EncryptedData_t3129875747_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (EncryptedKey_t805343673), -1, sizeof(EncryptedKey_t805343673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1386[5] = 
{
	EncryptedKey_t805343673::get_offset_of_carriedKeyName_8(),
	EncryptedKey_t805343673::get_offset_of_recipient_9(),
	EncryptedKey_t805343673::get_offset_of_referenceList_10(),
	EncryptedKey_t805343673_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_11(),
	EncryptedKey_t805343673_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (EncryptedReference_t3282913964), -1, sizeof(EncryptedReference_t3282913964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1387[5] = 
{
	EncryptedReference_t3282913964::get_offset_of_referenceType_0(),
	EncryptedReference_t3282913964::get_offset_of_uri_1(),
	EncryptedReference_t3282913964::get_offset_of_tc_2(),
	EncryptedReference_t3282913964_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_3(),
	EncryptedReference_t3282913964_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (EncryptedType_t2124600183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1388[8] = 
{
	EncryptedType_t2124600183::get_offset_of_cipherData_0(),
	EncryptedType_t2124600183::get_offset_of_encoding_1(),
	EncryptedType_t2124600183::get_offset_of_encryptionMethod_2(),
	EncryptedType_t2124600183::get_offset_of_encryptionProperties_3(),
	EncryptedType_t2124600183::get_offset_of_id_4(),
	EncryptedType_t2124600183::get_offset_of_keyInfo_5(),
	EncryptedType_t2124600183::get_offset_of_mimeType_6(),
	EncryptedType_t2124600183::get_offset_of_type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (EncryptedXml_t2455217639), -1, sizeof(EncryptedXml_t2455217639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1389[7] = 
{
	EncryptedXml_t2455217639::get_offset_of_encoding_0(),
	EncryptedXml_t2455217639::get_offset_of_keyNameMapping_1(),
	EncryptedXml_t2455217639::get_offset_of_mode_2(),
	EncryptedXml_t2455217639::get_offset_of_padding_3(),
	EncryptedXml_t2455217639::get_offset_of_document_4(),
	EncryptedXml_t2455217639_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_5(),
	EncryptedXml_t2455217639_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (EncryptionMethod_t2271876579), -1, sizeof(EncryptionMethod_t2271876579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1390[3] = 
{
	EncryptionMethod_t2271876579::get_offset_of_algorithm_0(),
	EncryptionMethod_t2271876579::get_offset_of_keySize_1(),
	EncryptionMethod_t2271876579_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (EncryptionPropertyCollection_t3294881567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1391[1] = 
{
	EncryptionPropertyCollection_t3294881567::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (EncryptionProperty_t3099724478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1392[2] = 
{
	EncryptionProperty_t3099724478::get_offset_of_id_0(),
	EncryptionProperty_t3099724478::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (KeyInfoClause_t4210275625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (KeyInfo_t3757684699), -1, sizeof(KeyInfo_t3757684699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1394[4] = 
{
	KeyInfo_t3757684699::get_offset_of_Info_0(),
	KeyInfo_t3757684699::get_offset_of_id_1(),
	KeyInfo_t3757684699_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_2(),
	KeyInfo_t3757684699_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (KeyInfoEncryptedKey_t109830476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1395[1] = 
{
	KeyInfoEncryptedKey_t109830476::get_offset_of_encryptedKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (KeyInfoName_t2325312217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1396[1] = 
{
	KeyInfoName_t2325312217::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (KeyInfoNode_t3535559014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1397[1] = 
{
	KeyInfoNode_t3535559014::get_offset_of_Node_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (KeyInfoRetrievalMethod_t2493599240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1398[3] = 
{
	KeyInfoRetrievalMethod_t2493599240::get_offset_of_URI_0(),
	KeyInfoRetrievalMethod_t2493599240::get_offset_of_element_1(),
	KeyInfoRetrievalMethod_t2493599240::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (KeyInfoX509Data_t3389067689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1399[5] = 
{
	KeyInfoX509Data_t3389067689::get_offset_of_x509crl_0(),
	KeyInfoX509Data_t3389067689::get_offset_of_IssuerSerialList_1(),
	KeyInfoX509Data_t3389067689::get_offset_of_SubjectKeyIdList_2(),
	KeyInfoX509Data_t3389067689::get_offset_of_SubjectNameList_3(),
	KeyInfoX509Data_t3389067689::get_offset_of_X509CertificateList_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
