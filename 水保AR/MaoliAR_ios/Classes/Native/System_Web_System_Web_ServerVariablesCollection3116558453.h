﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "System_Web_System_Web_BaseParamsCollection196591189.h"

// System.Web.HttpRequest
struct HttpRequest_t809700260;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Web.ServerVariablesCollection
struct  ServerVariablesCollection_t3116558453  : public BaseParamsCollection_t196591189
{
public:
	// System.Web.HttpRequest System.Web.ServerVariablesCollection::request
	HttpRequest_t809700260 * ___request_16;
	// System.Boolean System.Web.ServerVariablesCollection::loaded
	bool ___loaded_17;

public:
	inline static int32_t get_offset_of_request_16() { return static_cast<int32_t>(offsetof(ServerVariablesCollection_t3116558453, ___request_16)); }
	inline HttpRequest_t809700260 * get_request_16() const { return ___request_16; }
	inline HttpRequest_t809700260 ** get_address_of_request_16() { return &___request_16; }
	inline void set_request_16(HttpRequest_t809700260 * value)
	{
		___request_16 = value;
		Il2CppCodeGenWriteBarrier(&___request_16, value);
	}

	inline static int32_t get_offset_of_loaded_17() { return static_cast<int32_t>(offsetof(ServerVariablesCollection_t3116558453, ___loaded_17)); }
	inline bool get_loaded_17() const { return ___loaded_17; }
	inline bool* get_address_of_loaded_17() { return &___loaded_17; }
	inline void set_loaded_17(bool value)
	{
		___loaded_17 = value;
	}
};

struct ServerVariablesCollection_t3116558453_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Web.ServerVariablesCollection::<>f__switch$mapE
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24mapE_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapE_18() { return static_cast<int32_t>(offsetof(ServerVariablesCollection_t3116558453_StaticFields, ___U3CU3Ef__switchU24mapE_18)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24mapE_18() const { return ___U3CU3Ef__switchU24mapE_18; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24mapE_18() { return &___U3CU3Ef__switchU24mapE_18; }
	inline void set_U3CU3Ef__switchU24mapE_18(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24mapE_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapE_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
