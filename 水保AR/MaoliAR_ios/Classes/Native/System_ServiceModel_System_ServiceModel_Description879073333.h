﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Uri
struct Uri_t100236324;
// System.ServiceModel.Channels.Binding
struct Binding_t859993683;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Description.ServiceDebugBehavior
struct  ServiceDebugBehavior_t879073333  : public Il2CppObject
{
public:
	// System.Boolean System.ServiceModel.Description.ServiceDebugBehavior::<IncludeExceptionDetailInFaults>k__BackingField
	bool ___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_0;
	// System.Boolean System.ServiceModel.Description.ServiceDebugBehavior::<HttpHelpPageEnabled>k__BackingField
	bool ___U3CHttpHelpPageEnabledU3Ek__BackingField_1;
	// System.Uri System.ServiceModel.Description.ServiceDebugBehavior::<HttpHelpPageUrl>k__BackingField
	Uri_t100236324 * ___U3CHttpHelpPageUrlU3Ek__BackingField_2;
	// System.Boolean System.ServiceModel.Description.ServiceDebugBehavior::<HttpsHelpPageEnabled>k__BackingField
	bool ___U3CHttpsHelpPageEnabledU3Ek__BackingField_3;
	// System.Uri System.ServiceModel.Description.ServiceDebugBehavior::<HttpsHelpPageUrl>k__BackingField
	Uri_t100236324 * ___U3CHttpsHelpPageUrlU3Ek__BackingField_4;
	// System.ServiceModel.Channels.Binding System.ServiceModel.Description.ServiceDebugBehavior::<HttpHelpPageBinding>k__BackingField
	Binding_t859993683 * ___U3CHttpHelpPageBindingU3Ek__BackingField_5;
	// System.ServiceModel.Channels.Binding System.ServiceModel.Description.ServiceDebugBehavior::<HttpsHelpPageBinding>k__BackingField
	Binding_t859993683 * ___U3CHttpsHelpPageBindingU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ServiceDebugBehavior_t879073333, ___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_0)); }
	inline bool get_U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_0() const { return ___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_0() { return &___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_0; }
	inline void set_U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_0(bool value)
	{
		___U3CIncludeExceptionDetailInFaultsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHttpHelpPageEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ServiceDebugBehavior_t879073333, ___U3CHttpHelpPageEnabledU3Ek__BackingField_1)); }
	inline bool get_U3CHttpHelpPageEnabledU3Ek__BackingField_1() const { return ___U3CHttpHelpPageEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CHttpHelpPageEnabledU3Ek__BackingField_1() { return &___U3CHttpHelpPageEnabledU3Ek__BackingField_1; }
	inline void set_U3CHttpHelpPageEnabledU3Ek__BackingField_1(bool value)
	{
		___U3CHttpHelpPageEnabledU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CHttpHelpPageUrlU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ServiceDebugBehavior_t879073333, ___U3CHttpHelpPageUrlU3Ek__BackingField_2)); }
	inline Uri_t100236324 * get_U3CHttpHelpPageUrlU3Ek__BackingField_2() const { return ___U3CHttpHelpPageUrlU3Ek__BackingField_2; }
	inline Uri_t100236324 ** get_address_of_U3CHttpHelpPageUrlU3Ek__BackingField_2() { return &___U3CHttpHelpPageUrlU3Ek__BackingField_2; }
	inline void set_U3CHttpHelpPageUrlU3Ek__BackingField_2(Uri_t100236324 * value)
	{
		___U3CHttpHelpPageUrlU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpHelpPageUrlU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CHttpsHelpPageEnabledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ServiceDebugBehavior_t879073333, ___U3CHttpsHelpPageEnabledU3Ek__BackingField_3)); }
	inline bool get_U3CHttpsHelpPageEnabledU3Ek__BackingField_3() const { return ___U3CHttpsHelpPageEnabledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CHttpsHelpPageEnabledU3Ek__BackingField_3() { return &___U3CHttpsHelpPageEnabledU3Ek__BackingField_3; }
	inline void set_U3CHttpsHelpPageEnabledU3Ek__BackingField_3(bool value)
	{
		___U3CHttpsHelpPageEnabledU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CHttpsHelpPageUrlU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ServiceDebugBehavior_t879073333, ___U3CHttpsHelpPageUrlU3Ek__BackingField_4)); }
	inline Uri_t100236324 * get_U3CHttpsHelpPageUrlU3Ek__BackingField_4() const { return ___U3CHttpsHelpPageUrlU3Ek__BackingField_4; }
	inline Uri_t100236324 ** get_address_of_U3CHttpsHelpPageUrlU3Ek__BackingField_4() { return &___U3CHttpsHelpPageUrlU3Ek__BackingField_4; }
	inline void set_U3CHttpsHelpPageUrlU3Ek__BackingField_4(Uri_t100236324 * value)
	{
		___U3CHttpsHelpPageUrlU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpsHelpPageUrlU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CHttpHelpPageBindingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ServiceDebugBehavior_t879073333, ___U3CHttpHelpPageBindingU3Ek__BackingField_5)); }
	inline Binding_t859993683 * get_U3CHttpHelpPageBindingU3Ek__BackingField_5() const { return ___U3CHttpHelpPageBindingU3Ek__BackingField_5; }
	inline Binding_t859993683 ** get_address_of_U3CHttpHelpPageBindingU3Ek__BackingField_5() { return &___U3CHttpHelpPageBindingU3Ek__BackingField_5; }
	inline void set_U3CHttpHelpPageBindingU3Ek__BackingField_5(Binding_t859993683 * value)
	{
		___U3CHttpHelpPageBindingU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpHelpPageBindingU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CHttpsHelpPageBindingU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ServiceDebugBehavior_t879073333, ___U3CHttpsHelpPageBindingU3Ek__BackingField_6)); }
	inline Binding_t859993683 * get_U3CHttpsHelpPageBindingU3Ek__BackingField_6() const { return ___U3CHttpsHelpPageBindingU3Ek__BackingField_6; }
	inline Binding_t859993683 ** get_address_of_U3CHttpsHelpPageBindingU3Ek__BackingField_6() { return &___U3CHttpsHelpPageBindingU3Ek__BackingField_6; }
	inline void set_U3CHttpsHelpPageBindingU3Ek__BackingField_6(Binding_t859993683 * value)
	{
		___U3CHttpsHelpPageBindingU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHttpsHelpPageBindingU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
