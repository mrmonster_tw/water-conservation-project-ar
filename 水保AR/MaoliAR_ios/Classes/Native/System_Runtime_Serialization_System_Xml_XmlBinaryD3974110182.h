﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlBinaryDictionaryWriter/<CreateNewPrefix>c__AnonStorey2
struct  U3CCreateNewPrefixU3Ec__AnonStorey2_t3974110182  : public Il2CppObject
{
public:
	// System.String System.Xml.XmlBinaryDictionaryWriter/<CreateNewPrefix>c__AnonStorey2::p
	String_t* ___p_0;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(U3CCreateNewPrefixU3Ec__AnonStorey2_t3974110182, ___p_0)); }
	inline String_t* get_p_0() const { return ___p_0; }
	inline String_t** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(String_t* value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier(&___p_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
