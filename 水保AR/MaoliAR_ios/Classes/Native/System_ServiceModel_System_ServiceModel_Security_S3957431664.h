﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.ServiceModel.Security.SecurityTokenSpecification
struct SecurityTokenSpecification_t3911394864;
// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Security.SupportingTokenSpecification>
struct Collection_1_t2906149379;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy>
struct ReadOnlyCollection_1_t778487864;
// System.String
struct String_t;
// System.ServiceModel.ServiceSecurityContext
struct ServiceSecurityContext_t3919255606;
// System.Collections.ObjectModel.Collection`1<System.String>
struct Collection_1_t791806607;
// System.Byte[]
struct ByteU5BU5D_t4116647657;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ServiceModel.Security.SecurityMessageProperty
struct  SecurityMessageProperty_t3957431664  : public Il2CppObject
{
public:
	// System.ServiceModel.Security.SecurityTokenSpecification System.ServiceModel.Security.SecurityMessageProperty::initiator_token
	SecurityTokenSpecification_t3911394864 * ___initiator_token_0;
	// System.ServiceModel.Security.SecurityTokenSpecification System.ServiceModel.Security.SecurityMessageProperty::protection_token
	SecurityTokenSpecification_t3911394864 * ___protection_token_1;
	// System.Collections.ObjectModel.Collection`1<System.ServiceModel.Security.SupportingTokenSpecification> System.ServiceModel.Security.SecurityMessageProperty::incoming_supp_tokens
	Collection_1_t2906149379 * ___incoming_supp_tokens_2;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IdentityModel.Policy.IAuthorizationPolicy> System.ServiceModel.Security.SecurityMessageProperty::policies
	ReadOnlyCollection_1_t778487864 * ___policies_3;
	// System.String System.ServiceModel.Security.SecurityMessageProperty::sender_id_prefix
	String_t* ___sender_id_prefix_4;
	// System.ServiceModel.ServiceSecurityContext System.ServiceModel.Security.SecurityMessageProperty::context
	ServiceSecurityContext_t3919255606 * ___context_5;
	// System.Collections.ObjectModel.Collection`1<System.String> System.ServiceModel.Security.SecurityMessageProperty::ConfirmedSignatures
	Collection_1_t791806607 * ___ConfirmedSignatures_6;
	// System.Byte[] System.ServiceModel.Security.SecurityMessageProperty::EncryptionKey
	ByteU5BU5D_t4116647657* ___EncryptionKey_7;

public:
	inline static int32_t get_offset_of_initiator_token_0() { return static_cast<int32_t>(offsetof(SecurityMessageProperty_t3957431664, ___initiator_token_0)); }
	inline SecurityTokenSpecification_t3911394864 * get_initiator_token_0() const { return ___initiator_token_0; }
	inline SecurityTokenSpecification_t3911394864 ** get_address_of_initiator_token_0() { return &___initiator_token_0; }
	inline void set_initiator_token_0(SecurityTokenSpecification_t3911394864 * value)
	{
		___initiator_token_0 = value;
		Il2CppCodeGenWriteBarrier(&___initiator_token_0, value);
	}

	inline static int32_t get_offset_of_protection_token_1() { return static_cast<int32_t>(offsetof(SecurityMessageProperty_t3957431664, ___protection_token_1)); }
	inline SecurityTokenSpecification_t3911394864 * get_protection_token_1() const { return ___protection_token_1; }
	inline SecurityTokenSpecification_t3911394864 ** get_address_of_protection_token_1() { return &___protection_token_1; }
	inline void set_protection_token_1(SecurityTokenSpecification_t3911394864 * value)
	{
		___protection_token_1 = value;
		Il2CppCodeGenWriteBarrier(&___protection_token_1, value);
	}

	inline static int32_t get_offset_of_incoming_supp_tokens_2() { return static_cast<int32_t>(offsetof(SecurityMessageProperty_t3957431664, ___incoming_supp_tokens_2)); }
	inline Collection_1_t2906149379 * get_incoming_supp_tokens_2() const { return ___incoming_supp_tokens_2; }
	inline Collection_1_t2906149379 ** get_address_of_incoming_supp_tokens_2() { return &___incoming_supp_tokens_2; }
	inline void set_incoming_supp_tokens_2(Collection_1_t2906149379 * value)
	{
		___incoming_supp_tokens_2 = value;
		Il2CppCodeGenWriteBarrier(&___incoming_supp_tokens_2, value);
	}

	inline static int32_t get_offset_of_policies_3() { return static_cast<int32_t>(offsetof(SecurityMessageProperty_t3957431664, ___policies_3)); }
	inline ReadOnlyCollection_1_t778487864 * get_policies_3() const { return ___policies_3; }
	inline ReadOnlyCollection_1_t778487864 ** get_address_of_policies_3() { return &___policies_3; }
	inline void set_policies_3(ReadOnlyCollection_1_t778487864 * value)
	{
		___policies_3 = value;
		Il2CppCodeGenWriteBarrier(&___policies_3, value);
	}

	inline static int32_t get_offset_of_sender_id_prefix_4() { return static_cast<int32_t>(offsetof(SecurityMessageProperty_t3957431664, ___sender_id_prefix_4)); }
	inline String_t* get_sender_id_prefix_4() const { return ___sender_id_prefix_4; }
	inline String_t** get_address_of_sender_id_prefix_4() { return &___sender_id_prefix_4; }
	inline void set_sender_id_prefix_4(String_t* value)
	{
		___sender_id_prefix_4 = value;
		Il2CppCodeGenWriteBarrier(&___sender_id_prefix_4, value);
	}

	inline static int32_t get_offset_of_context_5() { return static_cast<int32_t>(offsetof(SecurityMessageProperty_t3957431664, ___context_5)); }
	inline ServiceSecurityContext_t3919255606 * get_context_5() const { return ___context_5; }
	inline ServiceSecurityContext_t3919255606 ** get_address_of_context_5() { return &___context_5; }
	inline void set_context_5(ServiceSecurityContext_t3919255606 * value)
	{
		___context_5 = value;
		Il2CppCodeGenWriteBarrier(&___context_5, value);
	}

	inline static int32_t get_offset_of_ConfirmedSignatures_6() { return static_cast<int32_t>(offsetof(SecurityMessageProperty_t3957431664, ___ConfirmedSignatures_6)); }
	inline Collection_1_t791806607 * get_ConfirmedSignatures_6() const { return ___ConfirmedSignatures_6; }
	inline Collection_1_t791806607 ** get_address_of_ConfirmedSignatures_6() { return &___ConfirmedSignatures_6; }
	inline void set_ConfirmedSignatures_6(Collection_1_t791806607 * value)
	{
		___ConfirmedSignatures_6 = value;
		Il2CppCodeGenWriteBarrier(&___ConfirmedSignatures_6, value);
	}

	inline static int32_t get_offset_of_EncryptionKey_7() { return static_cast<int32_t>(offsetof(SecurityMessageProperty_t3957431664, ___EncryptionKey_7)); }
	inline ByteU5BU5D_t4116647657* get_EncryptionKey_7() const { return ___EncryptionKey_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_EncryptionKey_7() { return &___EncryptionKey_7; }
	inline void set_EncryptionKey_7(ByteU5BU5D_t4116647657* value)
	{
		___EncryptionKey_7 = value;
		Il2CppCodeGenWriteBarrier(&___EncryptionKey_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
